// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_BASE_CODE_MODEL_H
#define SPEC_BASE_CODE_MODEL_H

#include <QDebug>
#include <QList>
#include <QObject>
#include <QString>

class spec_base_code_model  : public QObject {
    Q_OBJECT
        
    public:
        spec_base_code_model();
        
        QString             get_directory();
        
        QString             get_group_name();
        QString             get_project_name();
        QString             get_prefix();
        
        void                set_directory(
            QString             dir_path
        );
        void                set_group_name(
            QString             name
        );
        void                set_project_name(
            QString             name
        );
        
    private:
        QString             uvc_directory;
        
        // Information form the Group and Interface text-boxes
        QString             prefix_group_name;
        QString             prefix_project_name;
};

#endif // SPEC_BASE_CODE_MODEL_H
