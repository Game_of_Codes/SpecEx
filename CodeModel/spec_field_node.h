// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_FIELDNODE_H
#define SPEC_FIELDNODE_H

#include <QDebug>
#include <QList>
#include <QObject>
#include <QString>

class spec_field_node : public QObject {
    Q_OBJECT
        
    public:
        spec_field_node();
        spec_field_node(
            QString             name,
            QList <QString*>    props
        );
        spec_field_node(
            spec_field_node *node
        );
        
        void                set_field_name(
            QString         name
        );
        
        void                set_field_value(
            QString         value
        );
        
        void                set_field_properties(
            QList<QString*> props
        );
        
        void                set_enclosing_struct(
                QString     struct_name
                );
        QString             get_enclosing_struct();
        
        void                set_constraint_connected_struct(
                QString     struct_name
                );
        QString             get_constraint_connected_struct();
        
        void                set_constraint_connected_field(
                QString     field_name
                );
        QString             get_constraint_connected_field();
        
        
        QString             get_field_name();
        QString             get_field_type();
        QString             get_field_value();
        QList <QString*>    get_field_properties();
        
        bool                get_is_no_gen();
        bool                get_is_physical();
        int                 get_list_depth();
        QString             get_data_type();
        int                 get_data_width();
        bool                get_is_instance();
        QString             get_port_type();
        QString             get_port_kind();
        QString             get_port_data_type();
        QString             get_port_direction();
        
        // Convenience method to determine which information of the field object is relevant
        bool                is_port();
        
    private:
        bool                is_no_gen;
        bool                is_physical;
        int                 list_depth;
        QString             data_type;
        int                 data_width;
        bool                is_instance;
        QString             port_type;
        QString             port_kind;
        QString             port_data_type;
        QString             port_direction;
        
        bool                is_port_field;
        
        QString             field_name;
        QString             field_value;
        QString             field_enclosing_struct;
        QString             field_constraint_connected_struct;
        QString             field_constraint_connected_field;
        QList <QString*>    properties;
        
};

#endif // SPEC_FIELDNDE_H
