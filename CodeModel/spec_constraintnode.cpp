// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_constraintnode.h"

spec_constraint_node::spec_constraint_node(QObject *parent) : QObject(parent) {
    
}

spec_constraint_node::spec_constraint_node( spec_constraint_node * node ) {
    // Simply copy/assign the values to the new object
    constraint_name         = node->constraint_name;
    enclosing_struct_name   = node->enclosing_struct_name;
    is_soft                 = node->is_soft;
    op                      = node->op;
    
    exprs                   = node -> exprs;
    is_expr_soft            = node -> is_expr_soft;
    exprs_type              = node -> exprs_type;
    exprs_type_path         = node -> exprs_type_path;
    exprs_value             = node -> exprs_value;
    exprs_attrs             = node -> exprs_attrs;
}   // end of constructor with spec_constraint_node

void spec_constraint_node::add_expression(
    QString             name,
    QList <QString*>    properties
) {
    
    // Initialize the constraint semantic analyzer
    int     prop_index      = 0;
    int     prop_count      = properties.count();
    
    // Since there are only two possible expressions, the expression index tracks which expression is currently evaluated
    int     expr_index      = exprs.count();
    QString prop            = "";
    
    QList <QString>         expr_path;
    QList <QString>         tokenized_expression;
    
    bool    has_soft        = false;
    
    // Also set the global soft attribute
    is_soft = has_soft;
    
    int             is_removed      = false;
    QList<QString>  prop_l          ;
    QString         expr_type       = "";
    QString         expr_value      = "";
    QString         expr_attribute  = "";
    
    ///////////////////////////////////////////////////////////////////////////////////////////////
    // Analyzing each property and store the information in lists.
    // The strategy is to directly do the assignments and avoid backtracking and/or tracing work from a higher abstraction
    // layer.
    for( QString *str : properties ) {
        // By default, nothing is removed from the constraint string. However, we want to remove the keywords that
        // describe the constraint properties and only track fields.
        is_removed = false;
//        qDebug() << "[" << prop_index << "/" <<prop_count-1 << "]: " << *str;
        
        // Detecting a single escaped quotation string like "..."
        if( str -> contains( QRegularExpression("^\".*\"$") ) ) {
            expr_type       ="string";
            expr_value      = *str;
        };
        
        if( *str =="append" ) {
            // The result of this is always going to be a string.
            is_removed      = true;
            expr_attribute  = *str;
            expr_type       = "string";
        };
        
        // Detecting a single integer number in decimal base
        if( str -> contains( QRegularExpression("^(-)?\\d+$")) ) {
            expr_type   = "int";
            expr_value  = *str;
        };
        
        if( *str == "TRUE" or *str == "FALSE" ) {
            expr_type   = "bool";
            expr_value  = *str;
        };
        
        if( *str == "rise" or *str == "fall" or *str == "change" ) {
            expr_type   = "temporal";
            expr_value  = *str;
        };
        
        if( *str == "(" or *str == ")" ) {
            is_removed = true;
        };
        
        if( *str == "." ) {
            is_removed = true;
        };
        
        if( *str == "," ) {
            is_removed = true;
        };
        
        if( *str == "size" ) {
            is_removed      = true;
            expr_attribute  = *str;
            expr_type       = "int";
        };
        
        if( *str == "hdl_path" ) {
            is_removed      = true;
            expr_attribute  = *str;
            expr_type       = "string";
        };
        
        if( *str == "verilog_wire" ) {
            is_removed      = true;
            expr_attribute  = *str;
            expr_type       = "bool";
        };
        
        if( *str == "declared_range" ) {
            is_removed      = true;
            expr_attribute  = *str;
            expr_type       = "string";
        };
        
        if( *str == "edge" ) {
            is_removed      = true;
            expr_attribute  = *str;
            expr_type       = "temporal";
        };
        
        if( *str == "bind" ) {
            // Any bind() constraint is ignored, because it does not provide any additional value
            // for the purpose of tracking the e objects.
            is_removed      = true;
            expr_attribute  = *str;
            expr_type       = "bool";
        };
        
        if( *str == "read_only" or *str == "value" ) {
            is_removed      = true;
            expr_attribute  = *str;
        };
        
        if( *str == "soft" ) {
            is_removed  = true;
            is_soft     = true;
        };
        
        // Only add the fields as a list for easier processing
        if( is_removed == false ) {
            prop_l.append( *str );
        };
        ++prop_index;
    };  // end of iterating over each property string token
    
    exprs           . append( prop_l            );
    is_expr_soft    . append( is_soft           );
    exprs_attrs     . append( expr_attribute    );
    exprs_type      . append( expr_type         );
    
    // Only perform an assignment, if the type was detectable
    if( expr_type != "" ) {
        append_expr_type_path( expr_index, expr_type );
    };
    exprs_value     . append( expr_value        );
    
    if( expr_attribute == "append" ) {
        for( QString str : prop_l ) {
            expr_value += str;
        };
    };
    
    if( expr_attribute == "bind" ) {
        // This is a single constraint and usually does not get accompanied by any other value
        exprs           . append( {"TRUE"}          );
        is_expr_soft    . append( false             );
        exprs_type      . append( expr_type         );
        exprs_attrs     . append( expr_attribute    );
        exprs_value     . append( expr_value        );
    };
    
//    qDebug() << "CONCATENATED PROP_L: " << prop_l;
    
    // If a constraint can be resolved after both expressions are parsed, then we will assign the value and type info
    // on both sides of the expression. This allows for directly accessing the constraint from the spec_struct_data_node
    if( exprs.count() == 2 ) {
        for( int i = 0; i < 2; ++i ) {
            // Now check if values can be assigned directly
            if( exprs_value.at( (i+1)%2 ) != "" ) {
//                qDebug() << "Copying the value from index " << (i+1)%2 << " to index " << i;
                exprs_value.replace( i, exprs_value.at( (i+1)%2 )  );
                exprs_type.replace( i, exprs_type.at( (i+1)%2 )  );
                break;
            };
        };
//        qDebug() << "============================================================================================";
//        qDebug() << "        ==> " << exprs.at(0) << " " << op << " " << exprs.at(1);
//        qDebug() << "Attribute : " << exprs_attrs.at(0) << "     " << exprs_attrs.at(1);
//        qDebug() << "  is_soft : " << is_expr_soft.at(0) << "       " << is_expr_soft.at(1);
//        qDebug() << "     Type : " << exprs_type.at(0) << "         " << exprs_type.at(1);
//        qDebug() << "expr_value: " << exprs_value.at(0) << "       " << exprs_value.at(1);
//        qDebug() << "============================================================================================";
    };
}   // end of add_expression

void spec_constraint_node::add_operator(
    QString             name,
    QList <QString*>    properties
) {
    op = *(properties.at(0));
}   // end of add_operator


QString spec_constraint_node::get_constraint_name() {
    return constraint_name;
}

QString spec_constraint_node::get_operation() {
    return op;
}

QList<QString> spec_constraint_node::get_expr( int index ) {
    return exprs.at( index );
}
QList<QString> spec_constraint_node::get_expr_type_path( int index ) {
    return exprs_type_path.at( index );
}
bool spec_constraint_node::get_is_expr_soft( int index ) {
    return is_expr_soft.at( index );
}
QString spec_constraint_node::get_expr_value( int index ) {
    return exprs_value.at( index );
}
QString spec_constraint_node::get_expr_type( int index ) {
    return exprs_type.at( index );
}
QString spec_constraint_node::get_expr_attr( int index ) {
    return exprs_attrs.at( index );
}

void spec_constraint_node::append_expr_type_path(
    int     exp_index,
    QString type
) {
    // We can't manipulate the const list that is returned from the at(index) method, so we need to use the
    // replace(index) method instead after making changes to the copy of the list.
    spec_msg( "exp_index " + QString::number(exp_index) + "  type " + type << " exprs_type_path " << exprs_type_path );
    
    if( exp_index > 1 ) {
        // TODO: The crash happens at this location in the code because there are two elements in the exprs_type_path, but
        // we are trying to access the index 2, which is not present... why does this happen?????
        spec_msg( "(ERROR)  ==> ABORTING CONSTRAINT PARSING DUE TO exp_index " + QString::number(exp_index));
        return;
    };
    
    QList<QString>  new_list = exprs_type_path.at( exp_index );
    if( new_list.size() == 1 and new_list.at(0) == "" ) {
        new_list.replace(0, type);
    } else {
        // Only add new types, if the limit of the expression has not been reached yet.
        
        if( new_list.count() < exprs.at(exp_index).count() ) {
            new_list.append( type );
        };
    };
    exprs_type_path.replace( exp_index, new_list );
}   // end of set_expr_type_path

QString spec_constraint_node::show_constraint_info() {
    QString result;
    QString ex0 = "", ex1 = "";
    
    int exp_index = 0, exp_count = exprs.at(0).count()-1;
    
    for( QString str : exprs.at(0) ) {
        ex0 += (exp_index == exp_count)? str : (str + " ");
        ++exp_index;
    };
    exp_index = 0;
    if( exprs.count() > 1 ) {
        exp_count = exprs.at(1).count()-1;
        for( QString str : exprs.at(1) ) {
            ex1 += (exp_index == exp_count)? str : (str + " ");
            ++exp_index;
        };
    } else {
        qDebug() << "spec_constraint_node::show_constraint_info(): " << "Constraint has only one expression";
    }
    
    result = "(INFO) Constraint " + constraint_name + " is  " + ex0 + " " + op + " " + ex1;
    
    return result;
}


//QList<QString> spec_constraint_node::get_expression_list_0() {
//    return expr_path_0;
//}

//QList<QString> spec_constraint_node::get_expression_list_1() {
//    return expr_path_1;
//}


//QString spec_constraint_node::get_expression_0() {
//    return expression_0;
//}

//QString spec_constraint_node::get_expression_1() {
//    return expression_1;
//}

