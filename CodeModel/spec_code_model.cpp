// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_code_model.h"

spec_code_model::spec_code_model(QObject *parent) : QObject(parent) {
    // Generate pre-defined data types
    
    // any_struct
    
    // any_sequence
    
    // any_sequence_driver
    
    // any_sequence_item
    
    // simple_port
    spec_struct_data_node   *simple_port_struct = new spec_struct_data_node();
    simple_port_struct -> set_struct_properties(
        new QList<QString*> {
            new QString( "simple_port"      ),
            new QString( "main"             ),
            new QString( "any_simple_port"  ),
            new QString( "true"             ),
            new QString( "BUILTIN"          )
        }
    );
    spec_field_node     *type_attribute = new spec_field_node();
    type_attribute -> set_field_name( "port_data_type" );
    type_attribute -> set_field_value( "" );
    
    spec_field_node     *hdl_path_field = new spec_field_node();
    hdl_path_field -> set_field_name("hdl_path");
    hdl_path_field -> set_field_value( "" );
    
    simple_port_struct -> set_field( type_attribute );
    simple_port_struct -> set_field( hdl_path_field );
    struct_list->append( simple_port_struct );
    
    // event_port
    spec_struct_data_node   *event_port_struct = new spec_struct_data_node();
    event_port_struct -> set_struct_properties(
        new QList<QString*> {
            new QString( "event_port"       ),
            new QString( "main"             ),
            new QString( "any_event_port"   ),
            new QString( "true"             ),
            new QString( "BUILTIN"          )
        }
    );
    struct_list->append( event_port_struct );
    
    // interface_port
    spec_struct_data_node   *interface_port_struct = new spec_struct_data_node();
    interface_port_struct -> set_struct_properties(
        new QList<QString*> {
            new QString( "interface_port"       ),
            new QString( "main"                 ),
            new QString( "any_interface_port"   ),
            new QString( "true"                 ),
            new QString( "BUILTIN"              )
        }
    );
    struct_list->append( interface_port_struct );
    
}   // end of spec_code_model constructor

bool spec_code_model::has_struct_declaration(
    QString         name
) {
//    qDebug() << "has_struct_declaration(): " << " called with name " << name;
    for( spec_struct_data_node* node : *struct_list) {
        if( node->get_struct_name() == name ) {
            // No need to further search the list
            return true;
        };
    };   // end of iterating over each element
    
    // Ensure that we return false, since there was no entry found in the list
    return false;
}   // end of has_struct_declaration

bool spec_code_model::has_like_struct_declaration(
    QString         name
) {
//    qDebug() << "has_struct_declaration(): " << " called with name " << name;
    for( spec_struct_data_node* node : *struct_list) {
        if( node->get_like_name() == name ) {
            // No need to further search the list
            return true;
        };
    };   // end of iterating over each element
    
    // Ensure that we return false, since there was no entry found in the list
    return false;
}   // end of has_struct_declaration

bool spec_code_model::is_enum_type( QString name ) {
    bool result = false;
    for( spec_type_node *node : *type_list ) {
        if( node->get_name() == name ) {
            return true;
        };
    };  // end of iterating over each type node
    
    // If we get to this point, then the provided name is not an enumerated type
    return result;
}

QList<QString> spec_code_model::get_enum_type_field_values( QString name, QString fld_name ) {
    QList<QString> result;
    for( spec_type_node *node : *type_list ) {
        if( node->get_name() == name ) {
            for( QString str : node->get_values() ) {
                result.append( str + "'" + fld_name );
            };
            return result;
        };
    };  // end of iterating over each type node
    
    // In case nothing was found, then simply return an empty list
    return QList<QString> {};
}

void spec_code_model::set_top_file( QString file_name ) {
    top_file_string = file_name;
}   // end of set_top_file

QString spec_code_model::get_top_file() {
    return top_file_string;
}   // end of get_top_file

void spec_code_model::set_prefix(
    QString prefix
) {
    code_prefix = prefix;
}

QString spec_code_model::get_prefix() {
    return code_prefix;
}

void spec_code_model::set_active_agent_count( int count ) {
    active_agent_count = count;
}
void spec_code_model::set_passive_agent_count( int count ) {
    passive_agent_count = count;
}

spec_struct_data_node *spec_code_model::get_env_node       () {
    return static_cast<spec_struct_data_node*>(hierarchy_tree);
}

spec_struct_data_node *spec_code_model::get_env_configuration_node() {
    return static_cast<spec_struct_data_node*>(hierarchy_tree) -> get_env_configuration_node();
}

spec_struct_data_node *spec_code_model::get_agent_configuration_node( QString exclude_name ) {
    return static_cast<spec_struct_data_node*>(hierarchy_tree) -> get_agent_configuration_node(exclude_name);
}

spec_struct_data_node *spec_code_model::get_monitor_node() {
    return static_cast<spec_struct_data_node*>(hierarchy_tree) -> get_monitor_node();
}

spec_struct_data_node *spec_code_model::get_signal_map_node() {
    return static_cast<spec_struct_data_node*>(hierarchy_tree) -> get_signal_map_node();
}

spec_struct_data_node *spec_code_model::get_bfm_node() {
    return static_cast<spec_struct_data_node*>(hierarchy_tree) -> get_bfm_node();
}

spec_struct_data_node *spec_code_model::get_driver_node() {
    return static_cast<spec_struct_data_node*>(hierarchy_tree) -> get_driver_node();
}

QList<spec_struct_data_node*> spec_code_model::get_passive_agent_nodes() {
    return static_cast<spec_struct_data_node*>(hierarchy_tree)->get_passive_agent_nodes();
}
QList<spec_struct_data_node*> spec_code_model::get_active_agent_nodes() {
    return static_cast<spec_struct_data_node*>(hierarchy_tree)->get_active_agent_nodes();
}

int spec_code_model::get_active_agent_count() {
    return active_agent_count;
}
int spec_code_model::get_passive_agent_count() {
    return passive_agent_count;
}

void spec_code_model::print_struct_list() {
    QList <QString> output;
    for( spec_struct_data_node *s_node : *struct_list ) {
        output.append("==========================================================================================================");
        output.append("|| Node Struct Name:  " + s_node -> get_struct_name());
        output.append("||--------------------------------------------------------------------------------------------------------");
        output.append("|| Fields");
        if( not s_node->get_fields().isEmpty() ) {
            for( spec_field_node *f_node : s_node -> get_fields() ) {
                output.append("||   " + f_node -> get_field_name() + " : " + (f_node ->is_port()? f_node -> get_port_data_type() : f_node -> get_data_type()) + " = " + f_node ->get_field_value() );
            };
        } else {
            output.append("|| No fields defined");
        };
        output.append("||--------------------------------------------------------------------------------------------------------");
        output.append("|| Constraints");
        if( not s_node->get_constraints_list().isEmpty() ) {
            for( spec_constraint_node *c_node : s_node->get_constraints_list() ) {
//                output.append("||  " + c_node -> get_expression_0() + "  " + c_node ->get_operation() + "  " + c_node -> get_expression_1() );
                c_node->show_constraint_info();
            };
        } else {
            output.append("|| No constraints defined");
        };
        output.append("==========================================================================================================");
    };  // end of iterating over each declared node
    
    for( QString out : output ) {
        qDebug() << out;
    };
}

void spec_code_model::print_full_hierarchy() {
    hierarchy_tree->print_tree_recursive(0);
}

QList<QString> spec_code_model::get_incomplete_config_elements() {
    QList<QString>  result;
    if( active_agent_count <= 0 and passive_agent_count <= 0 ) {
        result.append( QString( get_prefix() + ": Must configure at least one agent.") );
    } else {
        if( active_agent_count > 0 and passive_agent_count > 0 ) {
            result = hierarchy_tree->get_incomplete_config_elements();
            for( int index = 0; index < result.count();++index ) {
                result.replace( index, QString( get_prefix() + ": " + result.at(index) ) );
            };
        } else if ( active_agent_count <= 0 ) { // and passive_agent_count != 0
            for( spec_struct_data_node *passive_agent :  hierarchy_tree->get_env_node()->get_passive_agent_nodes() ) {
                result.append( passive_agent->get_incomplete_config_elements() );
            };
        } else {    // passive_agent_count == 0 and active_agent_count != 0
            for( spec_struct_data_node *active_agent :  hierarchy_tree->get_env_node()->get_active_agent_nodes() ) {
                result.append( active_agent->get_incomplete_config_elements() );
            };
        };
        
        for( int index = 0; index < result.count(); ++index ) {
            result.replace( index, QString( " env." + result.at(index) ) );
        };
    };
    
    return result;
}

QList <spec_struct_data_node*>  spec_code_model::get_all_tree_nodes() {
    qDebug() << "spec_code_model::get_all_tree_nodes(): " << " called";
    
    return hierarchy_tree->get_all_tree_nodes();
}   // end of get_all_tree_nodes

spec_struct_data_node* spec_code_model::get_hierarchy_tree(
    QString top_unit
) {
    qDebug() << "spec_code_model::get_hierarchy_tree(): " << "called";
//    if( hierarchy_tree->get_like_name() != top_unit ) {
//        build_new_hierarchy(top_unit);
//    };
    return hierarchy_tree;
}   // end of get_hierarchy_tree


void spec_code_model::set_hierarchy(
    spec_struct_data_node *node
) {
    spec_msg( "Setting " << node << " in object " << this );
    hierarchy_tree = node;
    spec_msg( "Setting done...");
}

void spec_code_model::set_struct_name(
    QString             name
) {
    struct_name = &name;
}   // end of set_struct_name

void spec_code_model::add_enum_type(
    QString         name,
    QList<QString*> values
) {
    // This only yields a single element in the list...
    QList<QString> value_list = values.at(0)->split(QRegularExpression(","));
    
    spec_type_node *new_node = new spec_type_node();
    new_node -> set_name    ( name );
    new_node -> set_values  ( value_list );
    
    type_list->append( new_node );
};

void spec_code_model::add_struct(
    QString             parent,
    QList<QString *>    struct_properties
) {
    // ===> Debug Code
//    QString temp;
//    for(QString *property: struct_properties) { temp.append( *property + ", "); };
//    qDebug() << "spec_code_model::add_struct(): " << "Received add_struct --> PARENT : " << parent << " with properties " << temp;
    // <====
    
    spec_struct_data_node   *new_struct = new spec_struct_data_node();
    QList<spec_field_node*> fields;
    
    if( *(struct_properties.at(1)) == "SUBTYPE" ) {
        QStringList             tokenized_subtype   = parent.split(QRegularExpression("__"));
        QString                 base_struct         = tokenized_subtype.last();
        
        for( spec_struct_data_node* node : *struct_list ) {
            // TODO: This code is adding only the first level of the subtyping process
            //       for completeness sake, this should be done, drilling down into the subtype inheritance tree
            if( node->get_struct_name() == base_struct ) {
                qDebug() << "spec_code_model::add_struct(): " << "Found base struct " << base_struct;
                // Change the properties to correctly reflect the package, inheritance and struct/unit declaration
                *(struct_properties.at(1)) = node->get_package_name();
                *(struct_properties.at(2)) = node->get_like_name();
                *(struct_properties.at(3)) = (node->is_unit()? "true" : "false");
                fields.append( node->get_fields() );
            };
        };   // end of iterating over each defined base struct class
        
        // First create the new struct's properties
        new_struct  -> set_struct_properties ( &struct_properties   );
        // and copy over all the previously base and struct's fields
        new_struct  -> set_fields            ( fields               );
        
        
        // If the like construct exists, get its fields and assign those to the current struct
        if( has_struct_declaration( *( struct_properties.at(2) ) ) ) {
            spec_struct_data_node node = get_struct( *( struct_properties.at(2) ) );
            fields.append( node.get_fields() );
            new_struct->set_fields( fields );
        };
        struct_list -> append( new_struct );
    } else {   // Handle adding non-subtyped struct
        // Set the properties of the new struct and add it to the list of already declared structs
        new_struct  -> set_struct_properties(&struct_properties);
        QString     like_name = new_struct->get_like_name();
        
        if( has_struct_declaration( like_name ) ) {
            new_struct->set_fields( get_struct( like_name ).get_fields() );
        };
        struct_list -> append( new_struct );
        
        qDebug()    << "spec_code_model::add_struct(): " << "Added new struct " << (struct_list->last()->get_struct_name())
                    << " counting " << struct_list->count() << " elements.";
        qDebug() << "======================================================================================";
        qDebug() << " ";
    };
}   // end of add_struct


void spec_code_model::add_struct_method(
    QString             parent,
    QString             method_name,
    QList<QString *>    method_properties
) {
    qDebug() << "Received " << "PARENT: " << parent << "  METHOD: " << method_name;

    QString temp;
    for(QString *property: method_properties) {
        temp.append( *property + ", ");
    };
    
    qDebug() << "Properties: " << temp;
}   // end of add_struct_method

void spec_code_model::add_struct_field(
    QString             parent,
    QString             field_name,
    QList <QString*>    field_properties
) {
    // ===> START INITIAL DEBUG
    QString temp;
    for(QString *property: field_properties) { temp.append( *property + ", "); };
    qDebug()    << "add_struct_field(): " << "Received PARENT: " << parent << "  FIELD: " << field_name
                << "  with properties " << temp;
    // <=== END INITIAL DEBUG
    
//    int struct_index = -1;
    
    for(spec_struct_data_node *node: *struct_list) {
        // Find the struct that matches the parent name
        if( QString::compare( parent, node->get_struct_name()) == 0 ) {
            node->add_struct_field(
                field_name,
                field_properties
            );
        };   // end of finding the target struct
    };   // end of finding the appropriate node
//    qDebug() << "add_struct_field(): " << " counting structs " << struct_list->count() << " elements.";
}   // end of add_struct_field

spec_struct_data_node spec_code_model::get_struct(QString struct_name) {
    spec_struct_data_node result;
    for(spec_struct_data_node *node : *struct_list) {
        if( QString::compare( struct_name, node->get_struct_name() ) == 0 ) {
            result = *node;
        };
    };
    return result;
}   // end of get_struct 

spec_struct_data_node *spec_code_model::get_struct_pointer( QString struct_name ) {
    for( spec_struct_data_node *node : *struct_list ) {
        if( QString::compare( struct_name, node->get_struct_name() ) == 0 ) {
            return node;
        };
    };
}

spec_struct_data_node spec_code_model::get_struct_like(QString struct_name) {
    spec_struct_data_node result;
    for(spec_struct_data_node *node : *struct_list) {
        if( QString::compare( struct_name, node->get_like_name() ) == 0 ) {
            result = *node;
            return result;
        };
    };
    return result;
}   // end of get_struct_like 


QList<spec_field_node*> spec_code_model::get_struct_fields( QString struct_name ) {
    QList<spec_field_node*> result;
    
    for( spec_struct_data_node *node : *struct_list ) {
        if( QString::compare( struct_name, node->get_struct_name() ) == 0 ) {
//            qDebug() << "FOUND " << struct_name;
            result = node->get_fields();
        } else {
//            qDebug() << node->get_struct_name() << "  didn't match " << struct_name;
        };
    };   // end of iterating over each field node
    return result;
}   // end of get_struct_fields

// To build the path from the given root node to the target data type object, it is needed to descent into the 
// hierarchy tree.
// The break conditions are:
//  -> The given node matches the query type
//  -> There is no more node node to process
// The return value is the string that contains the hierarchical path
// This algorithm only supports a single query return match
QString spec_code_model::get_single_hierarchy_path(
    spec_struct_data_node   *node,
    QString                 node_query_operation,
    QString                 node_query_search
) {
    QString result;
    bool    is_query_match = false;
    
    if( node_query_operation == "get_like_name" ) {
        is_query_match = ( node->get_like_name() == node_query_search );
    } else if( node_query_operation == "get_struct_name" ) {
        is_query_match = ( node->get_struct_name() == node_query_search );
    };
    
    // The node is a driver, if the any_sequence_driver inheritance type is detected
    if( is_query_match ) {
//        qDebug() << "spec_code_model::assemble_protocol_driver_path(): " << "FOUND node " << node->get_struct_name();
        result = node->get_object_name();
        return result;
    } else if( node->childCount() == 0 ) {
        // Return an empty string if there are no children to process and the current node is not the
        // requested any_sequence_driver
//         qDebug() << "spec_code_model::assemble_protocol_driver_path(): " << "NO kids " << node->get_struct_name();
        return "";
    } else {
        // In case the current node is not what we are looking for and has more children, then descend into
        // each additional child node
//        qDebug() << "spec_code_model::assemble_protocol_driver_path(): " << "Loop " << node->get_struct_name();
        for( int i = 0; i < node->childCount();++i ) {
//            qDebug() << "spec_code_model::assemble_protocol_driver_path(): " << "LOOPED node " << static_cast<spec_struct_data_node*>(node->child(i))->get_object_name();
            result = get_single_hierarchy_path(
                        static_cast<spec_struct_data_node*>(node->child(i)),
                        node_query_operation,
                        node_query_search
            );
//            qDebug() << "spec_code_model::assemble_protocol_driver_path(): " << " ==> " << result;
            if( not result.isEmpty() ) {
//                qDebug() << "spec_code_model::assemble_protocol_driver_path(): " << "result is not empty";
                if( node->get_object_name() == "" ) {
                    result = node->get_struct_name() + "." + result;
                } else {
                    // Split the struct name to see if this is a subtype
                    QList<QString>  struct_name = node->get_struct_name().split(QRegularExpression("__"));
                    QString         casting_string = "";
                    // In case it is a subtype, then we'll throw in the casting operator for the construction
                    if( struct_name.count() > 1 ) {
                        casting_string = ".as_a(";
                        for( QString cast : struct_name ) {
                            casting_string = casting_string + " " + cast;
                        };
                        casting_string = casting_string + " )";
                    };
                    // We don't need to add an index placeholder, since the actual agent list will be determined by the
                    // configured agent index name
                    result = node->get_object_name() + casting_string + "." + result;
                };
                
                return result;
            };   // end of passing non-empty result
        };
//        qDebug() << "spec_code_model::assemble_protocol_driver_path(): " << "Loop processed " << node->get_struct_name();
        if( result != "" ) {
            return result;
        } else {
            return "";
        };
    };   // end of processing additional tree children
}   // end of get_single_hierarchy_path

// Method returns strings for the struct_names that match the input parameter string
QList<QString*> spec_code_model::get_all_like_structs_names(
    QString     like_type
) {
    QList<QString*> result;
    for( spec_struct_data_node* node : *struct_list ) {
        if( node->get_like_name() == like_type ) {
            result.append( new QString(node->get_struct_name()) );
        };
    };
    return result;
}   // end of get_all_like_structs


void spec_code_model::visit_all_tree_nodes( ) {
    qDebug() << "HIERARCHY TREE ROOT NODE like name: " << hierarchy_tree->get_like_name();
    
    for( int i = 0; i < hierarchy_tree->childCount();++i ) {
        qDebug() << "Child like name " << static_cast<spec_struct_data_node*> (hierarchy_tree->child( i ))->get_like_name();
        visit_node( static_cast<spec_struct_data_node*> (hierarchy_tree->child( i )) );
    };
}

void spec_code_model::visit_node( spec_struct_data_node *node ) {
    
    qDebug() << "Visiting Node: " << node->get_object_name() << " : " << node->get_like_name();// << "  parent -> " << static_cast<spec_struct_data_node*> (node->parent())->get_object_name();
    
    for( int i = 0; i < node->childCount();++i ) {
        visit_node( static_cast<spec_struct_data_node*> (node->child( i )) );
    };
}

void spec_code_model::build_traverse_hierarchy(
    QString                 top_unit,
    spec_struct_data_node   *node
) {
    QString spaces;
    cur_build_stack++;
    for( int i = 0; i < cur_build_stack; ++i) {
        spaces += "    ";
    };
    spec_msg( spaces << " ==>  ENTER  " << node->get_struct_name() );
    
    // Store the current node's struct name
    QString cur_node_struct_name            = node->get_struct_name();
    // Store the current node's like inheritance name
    QString cur_node_like_name              = node->get_like_name();
    // Store the current node's when inheritance name
    QString cur_node_when_name              = node->get_when_name();
    
    QList<spec_field_node*> node_fields     = node->get_fields();
    QString                 field_name      ;
    QString                 decl_like_name  ;
    QString                 field_data_type ;
    bool                    is_in_hierarchy = false;
    
    // Iterate over the current node only, if there are any fields
    if( node_fields.count() > 0 ) {
        for( spec_field_node *field : node_fields ) {
            // Getting convenience field name
            field_name                      = field->get_field_name();
            
//            spec_msg( spaces << " FIELD " << field_name );
            // Each field is of a specific type, so we are checking each element in the declared struct list
            for( spec_struct_data_node *declared_struct : *struct_list ) {
                // Check if the field is a port, and if that is the case, assign the port kind
                if( field->is_port() ) {
                    field_data_type = field->get_port_type();
                } else {
                    field_data_type = field->get_data_type();
                };
//                spec_msg( << spaces << "Comparing FIELD " << field_name << "  :  " << field_data_type << "  ==  " << declared_struct->get_struct_name() );
                if( field_data_type == declared_struct->get_struct_name() ) {
                    // Now we know which struct declaration is used for this field
                    
                    // First condition that needs to be true is that the field is not marked as "do not generate"
                    is_in_hierarchy = not field->get_is_no_gen();
                    decl_like_name  = declared_struct->get_like_name();
                    
                    // Next conditions only allow the indicated components
                    is_in_hierarchy = is_in_hierarchy and (
                                decl_like_name == "any_simple_port"      or
                                decl_like_name == "any_event_port"       or
                                decl_like_name == "any_sequence_driver"  or
                                decl_like_name == "any_interface_port"   or
                                decl_like_name == "uvm_config_params_s"  or
                                decl_like_name == "uvm_config_params"    or
                                decl_like_name == "uvm_env_config"       or
                                decl_like_name == "uvm_env"              or
                                decl_like_name == "uvm_agent"            or
                                decl_like_name == "uvm_signal_map"       or
                                decl_like_name == "uvm_monitor"          or
                                decl_like_name == "uvm_bfm"
                    );
                    
                    // Check if the field has to be instantiated
                    if( is_in_hierarchy == true ) {
//                        spec_msg( spaces << "Instantiating field " << field->get_field_name() << "  :  " << field_data_type );
                        field_data_type = (
                            field->is_port()?
                                        field->get_port_data_type() : 
                                        field->get_data_type()
                        );
                        
                        // Create a new instance of a node
                        spec_struct_data_node   *new_node = new spec_struct_data_node();
                        
                        // Copy the declared unit's properties
                        new_node->set_struct_properties(
                            new QList<QString*> {
                                new QString( declared_struct->get_struct_name()     ),
                                new QString( declared_struct->get_package_name()    ),
                                new QString( declared_struct->get_like_name()       ),
                                new QString( declared_struct->is_unit()             ),
                                new QString( declared_struct->get_when_name()       )
                            }
                        );
                        // Copy the declared unit's fields
                        new_node -> set_fields( declared_struct->get_fields() );
                        
                        // Set the constraints in here
                        new_node -> set_constraints( declared_struct -> get_constraints_list() );
                        
                        // Set the object name
                        new_node->set_object_name       ( field_name                    );
                        // Set a flag to indicate if object is a list
                        new_node->set_is_list_object    ( field->get_list_depth() > 0   );
                        
                        // To track concrete instantiation values, we have to track the current field's associated
                        // information, such as constraints, values and data type.
                        // To do so, we simply assign the field that is currently part of the hierarchy and add it to the
                        // actual new data node
                        new_node -> set_instance_field( field );                        
                        
                        // Now set the GUI appearance
                        new_node -> setText( 0, field_name                );   // object name
                        new_node -> setText( 1, field_data_type           );   // object type
//                        new_node->setIcon( 2, QIcon( QPixmap(":images/1024px-Windows_Settings_app_icon.png") ) );  // show a little cog wheel :-)
                        
//                        spec_msg( " ");
//                        spec_msg( "================================================================================");
//                        spec_msg( "spec_code_model::build_traverse_hierarchy(): " << "NEW_NODE Information");
//                        spec_msg( "================================================================================");
//                        spec_msg( "  field_name  :  " << new_node->get_object_name() );
//                        spec_msg( "  struct_name :  " << new_node->get_struct_name() );
//                        spec_msg( "  package_name:  " << new_node->get_package_name() );
//                        spec_msg( "  like_name   :  " << new_node->get_like_name() );
//                        spec_msg( "  is_unit     :  " << new_node->is_unit() );
//                        spec_msg( "  when_name   :  " << new_node->get_when_name() );
//                        spec_msg( "  fields      :  " << new_node->get_fields().contains( field ) );
//                        spec_msg( " " );
                        
                        // To avoid cluttering the tree with a bunch of configuration objects, we're simply trimming down
                        // on the agent and env configurations.
                        // If the verification environment is now using configurations anywhere else, then these may not 
                        // be shown appropriately
                        bool is_new_config_node = (
                            new_node->get_like_name() == "uvm_config_params_s"
                                or
                            new_node->get_like_name() == "uvm_config_params"
                        );
                        bool is_current_env_node    = ( node->get_like_name() == "uvm_env" );
                        bool is_current_agent_node  = ( node->get_like_name() == "uvm_agent" );
                        
                        if( is_new_config_node ) {
                            if( is_current_env_node or is_current_agent_node ) {
                                // Only add agents to either environment or agent instances
                                node -> addChild( new_node );
                            };
                        } else {
                            // Adding the newly created node to the current node's tree anywhere, since it is not an
                            // configuration object
                            node -> addChild( new_node );
                        };
                        
                        // Only proceed further down the tree, if the new node and the current node are not of the same type
                        if( cur_node_struct_name != field_name ) {
                            // Descend into the new node
                            build_traverse_hierarchy( top_unit, new_node );
                            
                            // Now ensure that the constraints are being applied after the sub-tree was built
                            new_node -> apply_constraints();
                        };   // end of recursively traversing through the nodes
                        
                        // Search for any possible subtypes of the current struct.
                        if( new_node->get_when_name() == "STATEMENT" ) {
                            for( spec_struct_data_node *subtype_node : *struct_list ) {
                                if( subtype_node->get_when_name().split("__").last() == new_node->get_struct_name() ) {
                                    // Also instatiate this subtype!
//                                    qDebug() << "Subtype found " << subtype_node->get_when_name();
                                    
                                    // Create a new copy of a node
                                    spec_struct_data_node   *new_subtype_node = new spec_struct_data_node();
                                    
                                    // Copy the declared unit's properties
                                    new_subtype_node->set_struct_properties(
                                        new QList<QString*> {
                                            new QString( subtype_node->get_when_name()      ),
                                            new QString( subtype_node->get_package_name()   ),
                                            new QString( subtype_node->get_like_name()      ),
                                            new QString( subtype_node->is_unit()            ),
                                            new QString( subtype_node->get_when_name()      )
                                        }
                                    );
                                    // Copy the declared unit's fields
                                    new_subtype_node -> set_fields( subtype_node->get_fields() );
                                    new_subtype_node -> set_constraints( subtype_node -> get_constraints_list() );
                                    
                                    // Set the object name
                                    new_subtype_node->set_object_name       ( field_name                    );
                                    // Set a flag to indicate if object is a list
                                    new_subtype_node->set_is_list_object    ( field->get_list_depth() > 0   );
                                    
                                    // Now set the GUI appearance
                                    new_subtype_node->setText( 0, field_name                    );   // object name
                                    new_subtype_node->setText( 1, subtype_node->get_when_name() );   // object type
//                                    new_subtype_node->setIcon( 2, QIcon( QPixmap(":images/1024px-Windows_Settings_app_icon.png") ) );  // show a little cog wheel :-)
                                    
//                                    qDebug() << " ";
//                                    qDebug() << "================================================================================";
//                                    qDebug() << "spec_code_model::build_traverse_hierarchy(): " << "SUBTYPE_NODE Information";
//                                    qDebug() << "================================================================================";
//                                    qDebug() << "  field_name  :  " << new_subtype_node->get_object_name();
//                                    qDebug() << "  struct_name :  " << new_subtype_node->get_struct_name();
//                                    qDebug() << "  package_name:  " << new_subtype_node->get_package_name();
//                                    qDebug() << "  like_name   :  " << new_subtype_node->get_like_name();
//                                    qDebug() << "  is_unit     :  " << new_subtype_node->is_unit();
//                                    qDebug() << "  when_name   :  " << new_subtype_node->get_when_name();
//                                    qDebug() << "  fields      :  " << new_subtype_node->get_fields().contains( field );
//                                    qDebug() << " ";
                                    
                                    // Adding the newly created node to the current node's tree
                                    node -> addChild( new_subtype_node );
                                    
                                    // Only proceed further down the tree, if the new node and the current node are not of the same type
                                    if( cur_node_struct_name != field_name ) {
                                        // Descend into the new node
                                        build_traverse_hierarchy( top_unit, new_subtype_node );
                                        
                                        // Now ensure that the constraints are being applied after the sub-tree was built
                                        new_subtype_node    -> apply_constraints();
                                    };   // end of recursively traversing through the nodes
                                    
                                    break;
                                };
                            };   // end of iterating over each declared struct
                        };   // end of subtype handling branch
                    };   // end of instantiating field handling
                    break;
                };   // end of detecting to be instantiated node
            };   // end of iterating over the declared structs
            
            
            
        };   // end of field iteration loop
    };   // end of processing a node with fields
    
    
    // After recursively traversing the tree, the declared top_unit struct name unit will be the root of the
    // hierarchy tree
    if( cur_node_struct_name == top_unit ) {
        qDebug() << spaces << "spec_code_model::build_traverse_hierarchy(): " << "top_unit " << top_unit << " found";
        
        // Only set the struct_name of the node...
        node->setText( 1, top_unit );
        // ... and make it the root node of the hierarchy tree
        hierarchy_tree = node;
    };
    
    cur_build_stack--;
    qDebug() << spaces << " <==  RETURN " << node->get_struct_name();
}   // end of new_build_traverse_hierarchy


void spec_code_model::build_new_hierarchy(
    QString     top_unit
) {
//    qDebug() << "spec_code_model::build_hierarchy() - Searching for " << top_unit << " to build hirarchy...";
    
    // Instantiate a new hierarchy tree
    hierarchy_tree                          = new spec_struct_data_node();
    bool    has_uvm_env = has_like_struct_declaration("uvm_env");
    
    // Find the entry point sys in the declared struct_list and only start build if an object instance underneath the 
    // sys object was found
    for( spec_struct_data_node *node : *struct_list) {
        if( node->get_struct_name() == top_unit ) {
            // Start building the hierarchical tree, starting with the current 'node' object
            build_traverse_hierarchy( top_unit, node );
            
            // Also extract the prefix for easier code generation
            if( has_uvm_env ) {
                QList<QString> env_split_str = get_struct_like("uvm_env").get_struct_name().split("_");
                set_prefix( env_split_str.at(0) + "_" + env_split_str.at(1) );
            };
            
            qDebug() << "Driver Path " << get_single_hierarchy_path(
                            node,
                            "get_like_name",
                            "any_sequence_driver"
            );
            // This is debug and shows that building the tree doesn't take care of the constraints attachment
            // only the uvm_env constraints are applied and those are applied to all items.
            
            // By issuing a return, we'll simply stop cycling through the list.
            return;
        };   // end of handling sys struct_node
    };   // end of looking for sys struct_node
}   // end of build_new_hierarchy
