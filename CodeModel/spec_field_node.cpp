// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_field_node.h"

spec_field_node::spec_field_node() {
    
};   // end of spec_FieldNode

//&field_no_gen,        0
//&field_phyiscal,      1
//&field_list_depth,    2
//&field_type,          3
//&field_width,         4
//&field_is_instance,   5
//&field_port,          6
//&field_port_kind,     7
//&field_port_data_type,8
//&field_port_direction 9

spec_field_node::spec_field_node(QString name, QList<QString *> props) {
    field_name  = name;
    properties  = QList<QString*>(props);  // TODO: Analyze why accessing properties later on only 
    
    // Analyzing and storing the information of the detected field
    is_no_gen   = (*props.at(0) == "true");
    is_physical = (*props.at(1) == "true");
    list_depth  = props.at(2)->toInt();
    is_instance = (*props.at(5) == "true");
    
    is_port_field = (not props.at(6)->isEmpty());
    
    // Pre-initialize to -1 to indicate invalid value if not set
    data_width      = -1;
    
    if( is_port_field ) {
        port_type       = (*props.at(6));
        port_kind       = (*props.at(7));
        port_data_type  = (*props.at(8));
        port_direction  = (*props.at(9));
    } else {        // Populate non-port attributes
        data_type       = (*props.at(3));
        data_width      = (*props.at(4)).toInt();
    };   // end of non-port attributes
//    for( QString *elem : properties ) {
//        qDebug() << "prop_value: " << *elem;
//    };
//    qDebug() << "Field Properties " << *(get_field_properties().at(0));
};

spec_field_node::spec_field_node(
    spec_field_node *node
) {
    // Copy the provided spec_field_node
    field_name      = node -> get_field_name();
    // TODO: The field properties are causing a segmentation fault.
    //       It seems the properties are not copied properly
    properties      = node -> get_field_properties();
    is_no_gen       = node -> get_is_no_gen();//(*properties.at(0) == "true");
    list_depth      = node -> get_list_depth();//properties.at(2)->toInt();
    is_instance     = node -> get_is_instance();//*properties.at(5) == "true");
    is_port_field   = node -> is_port_field;//(not properties.at(6)->isEmpty());
    data_width      = -1;
    if( is_port_field ) {
        port_type       = node -> get_port_type();//*properties.at(6));
        port_kind       = node -> get_port_kind();//*properties.at(7));
        port_data_type  = node -> get_port_data_type();//*properties.at(8));
        port_direction  = node -> get_port_direction();//*properties.at(9));
    } else {
        data_type       = node -> get_data_type();//*properties.at(3));
        data_width      = node -> get_data_width();//*properties.at(4)).toInt();
    };
}

void spec_field_node::set_field_name(
    QString         name
) {
    field_name = name;
}

QString spec_field_node::get_field_type() {
    QString result;
    
    if( is_port_field ) {
        result = port_type;
    } else {
        result = data_type;
    };
    
    return result;
}   // end of get_field_type

void spec_field_node::set_field_value(
    QString value
) {
    field_value = value;
}

void spec_field_node::set_field_properties(
    QList<QString*> props
) {
    properties = props;
}

QString spec_field_node::get_field_name() {
    return field_name;
}

QString spec_field_node::get_field_value() {
    return field_value;
}

QList <QString*> spec_field_node::get_field_properties() {
    return properties;
}

void spec_field_node::set_enclosing_struct(
        QString     struct_name
        ) {
    field_enclosing_struct = struct_name;
}
QString spec_field_node::get_enclosing_struct() {
    return field_enclosing_struct;
}

void spec_field_node::set_constraint_connected_struct(
        QString     struct_name
        ) {
    field_constraint_connected_struct = struct_name;
}
QString spec_field_node::get_constraint_connected_struct() {
    return field_constraint_connected_struct;
}

void spec_field_node::set_constraint_connected_field(
        QString     field_name
        ) {
    field_constraint_connected_field = field_name;
}
QString spec_field_node::get_constraint_connected_field() {
    return field_constraint_connected_field;
}

//======================================================================================================================
// Field Property Direct access methods
bool spec_field_node::get_is_no_gen() {
    return is_no_gen;
}
bool spec_field_node::get_is_physical() {
    return is_physical;
}
int spec_field_node::get_list_depth() {
    return list_depth;
}
QString spec_field_node::get_data_type() {
    return data_type;
}
int spec_field_node::get_data_width() {
    return data_width;
}
bool spec_field_node::get_is_instance() {
    return is_instance;
}
QString spec_field_node::get_port_type() {
    return (port_type.isEmpty()? (tr("")) : (port_type));
}
QString spec_field_node::get_port_kind() {
    return (port_kind.isEmpty()? (tr("")) : (port_kind));
}
QString spec_field_node::get_port_data_type() {
    return (port_data_type.isEmpty()? (tr("")) : (port_data_type));
}
QString spec_field_node::get_port_direction() {
    return (port_direction.isEmpty()? tr("inout") : (port_direction));
}
//======================================================================================================================

// Convenience method to determine which information of the field object is relevant
bool spec_field_node::is_port() {
    return is_port_field;
}
