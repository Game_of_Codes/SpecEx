// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_CLOCK_MODEL_H
#define SPEC_CLOCK_MODEL_H

#include <CodeModel/spec_base_code_model.h>
#include <QObject>

class spec_clock_model : public spec_base_code_model {
    Q_OBJECT
        
    public:
        explicit spec_clock_model(QObject *parent = nullptr);
        
        void    add_clock_domain(
            QList<QString*>     prop
        );
        
        int     get_clock_domain_count      ();
        QString get_clock_domain_name       ( int index );
        QString get_clock_domain_hdl_name   ( int index );
        QString get_clock_domain_jitter     ( int index );
        QString get_clock_domain_period     ( int index );
        QString get_clock_domain_shift      ( int index );
        QString get_clock_domain_init       ( int index );
        void    set_time_resolution         ( QString resolution );
        QString get_time_resolution         ();
    
    signals:
        
    public slots:
        
    private:
        QList<QList<QString*>>  properties;
        QString                 time_resolution;
};

#endif // SPEC_CLOCK_MODEL_H
