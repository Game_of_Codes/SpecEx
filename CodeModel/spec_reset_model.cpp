// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_reset_model.h"

spec_reset_model::spec_reset_model(QObject *parent) : spec_base_code_model() {
    
};

void spec_reset_model::add_reset_domain(
    QList<QString*>     prop
) {
    properties.append( prop );
}   // end of add_reset_domain

int spec_reset_model::get_reset_domain_count          () {
    return properties.count();
}

QString spec_reset_model::get_reset_domain_name           ( int index ) {
    return *properties.at( index ).at( 0 );
}

QString spec_reset_model::get_reset_domain_hdl_name       ( int index ) {
    return *properties.at( index ).at( 1 );
}

QString spec_reset_model::get_reset_domain_active_level   ( int index ) {
    return *properties.at( index ).at( 2 );
}

QString spec_reset_model::get_reset_domain_sync_clock     ( int index ) {
    return *properties.at( index ).at( 3 );
}

QString spec_reset_model::get_reset_domain_init_value     ( int index ) {
    return *properties.at( index ).at( 4 );
}

