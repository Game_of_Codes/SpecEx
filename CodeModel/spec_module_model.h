// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_MODULE_MODEL_H
#define SPEC_MODULE_MODEL_H

//#include <QDebug>
//#include <QList>
//#include <QObject>
//#include <QString>

#include <CodeModel/spec_base_code_model.h>
#include <CodeModel/spec_clock_model.h>
#include <CodeModel/spec_reset_model.h>
#include <CodeModel/spec_code_model.h>

class spec_module_model : public spec_base_code_model {
    Q_OBJECT
    
    public:
        explicit spec_module_model(QObject *parent = nullptr);
        
        // This method allows for setting a pre-configured SPECMAN_PATH variable with all
        // folders at once
        void                set_all_specman_paths(
            QList<QString*> path
        );
        
        // This method returns the list of all SPECMAN_PATH sections
        QList<QString*>     get_specman_paths();
        
        // THis method returns a single SPECMAN_PATH at the given index
        QString*            get_specman_path_at(
            int             index
        );
        
        // Add a single SPECMAN_PATH folder
        void                add_specman_path(
            QString         *path
        );
        
        // Remove a single SPECMAN_PATH folder from the list
        void                remove_specman_path(
            int    index
        );
        
        void                set_clock_model(
            spec_clock_model    *model
        );
        void                set_reset_model(
            spec_reset_model    *model
        );
        
        spec_clock_model    *get_clock_model            ();
        spec_reset_model    *get_reset_model            ();
        
        bool                get_enable_register_model   () const;
        void                set_enable_register_model   ( bool value );
        
        bool                get_enable_clock_uvc        () const;
        void                set_enable_clock_uvc        ( bool value );
        
        bool                get_enable_reset_uvc        () const;
        void                set_enable_reset_uvc        ( bool value );
        
        bool                get_enable_reference_model  () const;
        void                set_enable_reference_model  ( bool value );
        
        bool                get_enable_scoreboard       () const;
        void                set_enable_scoreboard       ( bool value );
        
        bool                get_enable_uvm_scoreboard   () const;
        void                set_enable_uvm_scoreboard   ( bool value );
        
        unsigned int        get_register_max_addr_width () const;
        void                set_register_max_addr_width ( unsigned int value );
        
        unsigned int        get_register_max_data_width () const;
        void                set_register_max_data_width ( unsigned int value );
        
        void                add_code_model              (
            spec_code_model *model
        );
        
        QString                     get_hdl_tb_top      ();
        void                        set_hdl_tb_top( QString new_tb_top );
        
        QList<spec_code_model*>     get_code_model_list ();
        
        void                        update_all_uvc_code_models  (
            QList<spec_code_model*> code_models
        );
        
        void                        update_code_model(
            int             index,
            spec_code_model *model
        );
        
    signals:
        
    public slots:
        
    private:
        
        QList <QString*>            specman_path_names;
        
        bool                        enable_register_model;
        bool                        enable_clock_uvc;
        bool                        enable_reset_uvc;
        bool                        enable_reference_model;
        bool                        enable_scoreboard;
        bool                        enable_uvm_scoreboard;
        unsigned int                register_max_addr_width;
        unsigned int                register_max_data_width;
        
        QString                     hdl_tb_top;
        
        // The clock model contains a HDL and e model to create a configurable clock UVC.
        // The clock generation is actually done in HDL due to better performance of HDL clock generation
        spec_clock_model            *clk_model              = nullptr;
        
        // The reset model contains an e model to create a reset UVC
        spec_reset_model            *res_model              = nullptr;
        
        // The code model list contains a list of UVCs that have been loaded by the integration engineer
        QList<spec_code_model*>     code_model              ;
};   // end of spec_module_model

#endif // SPEC_MODULE_MODEL_H
