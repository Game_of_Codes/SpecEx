// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_base_code_model.h"

spec_base_code_model::spec_base_code_model() {
    
};

QString spec_base_code_model::get_directory() {
    return uvc_directory;
};   // end of get_directory

QString spec_base_code_model::get_group_name() {
    return prefix_group_name;
};   // end of get_group_name

QString spec_base_code_model::get_project_name() {
    return prefix_project_name;
};   // end of get_interface_name

// Returns the UVC prefix in this format:
//   "<group_name>_<interface_name>"
QString spec_base_code_model::get_prefix() {
    return QString( prefix_group_name + "_" + prefix_project_name );
};   // end of get_uvc_prefix

void spec_base_code_model::set_directory(
        QString dir_path
) {
    uvc_directory = dir_path;
};   // end of set_directory

void spec_base_code_model::set_group_name(
        QString             name
) {
    prefix_group_name = name;
};   // end of set_group_name

void spec_base_code_model::set_project_name(
        QString             name
) {
    prefix_project_name = name;
};   // end of set_interface_name
