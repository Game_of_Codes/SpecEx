// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_TYPENODE_H
#define SPEC_TYPENODE_H

#include <QObject>

class spec_type_node : public QObject {
    Q_OBJECT
    
    public:
        explicit spec_type_node(QObject *parent = nullptr);
        
    signals:
        
    public slots:
        void                set_name(
            QString             name
        );
        void                add_value(
            QString             value
        );
        void                set_values(
            QList <QString>    values
        );
        
        QString             get_name    (  );
        QList<QString>      get_values  (  );
        QString             get_value_at( int index );
        bool                has_value   ( QString value );
        
    private:
        QString             enum_type_name;
        QList <QString>     type_value_list;
};

#endif // SPEC_TYPENODE_H
