// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_struct_data_node.h"

spec_struct_data_node::spec_struct_data_node(QTreeWidgetItem *parent, int type) : QTreeWidgetItem(parent, type) {
//    spec_msg( "Constructor (create) " << this )
}

spec_struct_data_node::spec_struct_data_node( spec_struct_data_node *current_pointer ) {
//    spec_msg( "Constructor (copy) from " << current_pointer << " to " << this << current_pointer->get_struct_name() );
//    spec_msg( "Parent node " << static_cast<spec_struct_data_node*>( parent() ) );
    // Create a cloned version of the provided node...
    // This is intended to create on-the-fly copies without resorting back to QTreeWidgetItem base classes
    
    // Since this data structure is quite a custom data structure, we need to manually clone all
    // properties in the constructor!
    set_struct_properties   ( new QList<QString*> (current_pointer->get_struct_properties()) );
    set_fields              ( current_pointer -> get_fields() );
    set_object_name         ( current_pointer -> get_object_name() );
    set_clock_domain_list   ( current_pointer -> get_clock_domain_list() );
    set_reset_domain_list   ( current_pointer -> get_reset_domain_list() );
    set_hdl_path            ( current_pointer -> get_hdl_path() );
    
    // Only copy the field, if there was a field to be copied
    if( current_pointer -> get_instance_field() != nullptr ) {
        set_instance_field  ( new spec_field_node(current_pointer -> get_instance_field()) );
    };
    // Also copy each of the constraint nodes
    if( not current_pointer -> get_constraints_list().isEmpty() ) {
        for( spec_constraint_node *c_node: current_pointer -> get_constraints_list() ) {
//            spec_msg( c_node->show_constraint_info() );
//            qDebug() << "spec_struct_data_node " << get_struct_name() << " -> c_node: " << c_node -> get_expression_0() <<
//                        "  " << c_node -> get_operation() << "  " << c_node -> get_expression_1();
            constraint_nodes.append( new spec_constraint_node( c_node ) );
        };
    };
    
    // Set the current tree item's object name
    setText( 0, object_name );
    
    // Set the current tree item's struct type
    setText( 1, current_pointer->get_struct_name() );
    
    // Also copy the widget
    if( current_pointer->node_widget != nullptr ) {
        node_widget = get_node_widget_copy( current_pointer );
    };
    
    // Cool thing about trees is that by simply descending through their children and calling the
    // constructor for each new kid, everything gets cloned as exepcted :-)
    for(int i = 0; i < current_pointer->childCount(); ++i ) {
//        addChild( new spec_struct_data_node( static_cast<spec_struct_data_node*>( current_pointer->child(i) ) ) );
        addChild( new spec_struct_data_node( current_pointer->get_child(i) ) );
        
    };
}

// This returns the specific spec_struct_data_node
spec_struct_data_node* spec_struct_data_node::get_child( int i ) {
    return static_cast<spec_struct_data_node*>(child(i));
}

void spec_struct_data_node::set_instance_field(
    spec_field_node         *node
) {
//    qDebug() << "spec_struct_data_node::set_instance_field: " << "Copying FIELD " << node->get_field_name();
    me_node = node;
}

spec_field_node* spec_struct_data_node::get_instance_field() {
//    qDebug() << "spec_struct_data_node::get_instance_field: " << "Accessing FIELD "<< me_node->get_field_name();
    return me_node;
}

QWidget     *spec_struct_data_node::get_node_widget_copy( spec_struct_data_node *current_pointer ) {
    QWidget *result = new QWidget();
    
    QString like_name = current_pointer->get_like_name();
    if( like_name == "uvm_env" ) {
        result = new spec_env_widget( static_cast<spec_env_widget*>(current_pointer->node_widget) );
    };
    
    if( like_name == "uvm_agent" ) {
        result = new spec_agent_widget( static_cast<spec_agent_widget*>(current_pointer->node_widget) );
    };
    
    if( like_name == "uvm_config_params_s" or like_name == "uvm_config_params" ) {
        result = new spec_config_params_widget( static_cast<spec_config_params_widget*>(current_pointer->node_widget) );
    };
    
    if( like_name == "any_event_port" ) {
        result = new spec_event_port_widget( static_cast<spec_event_port_widget*>(current_pointer->node_widget) );
    };
    
    if( like_name == "any_simple_port" ) {
        result = new spec_simple_port_widget( static_cast<spec_simple_port_widget*>(current_pointer->node_widget) );
    };
    
    if( like_name == "any_interface_port" ) {
        result = new spec_tlm_port_widget( static_cast<spec_tlm_port_widget*>(current_pointer->node_widget) );
    };
    
    return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
// New data structure methods

QList <spec_struct_data_node*>  spec_struct_data_node::get_all_tree_nodes() {
    qDebug() << "spec_struct_data_node::get_all_tree_nodes(): " << " called";
    // TODO Iterate over the complete tree and return a serialized list of items
    
    
}   // end of get_all_tree_nodes


// Method accepts list of strings with the following arguments
//  0: struct name
//  1: package name
//  2: like name
//  3: is_a_declared_unit
//  4: when name
void spec_struct_data_node::set_struct_properties(
    QList <QString*>    *properties
) {
    // These are the base properties
    struct_name     = *(properties->at(0));
    if( !properties->at(1)->isEmpty() ) {
        // Only overwrite if a package name was given, otherwise remain at main
        package_name    = *(properties->at(1));
    } else {
        package_name    = ("main");
    };
    like_name       = *(properties->at(2));
    
    is_declared_unit= *(properties->at(3)) == "true";
    
    when_name       = *(properties->at(4));
    
//    qDebug() << "spec_struct_data_node::set_struct_properties(): " << "Struct Name : " << struct_name << "  Package Name" << package_name << "  Like Name " << like_name << "  When Name " << when_name << "  Is Unit? " << is_declared_unit;
}   // end of set_struct_properties

QList<QString*> spec_struct_data_node::get_struct_properties() {
    return QList<QString*> {
        new QString( struct_name        ),
        new QString( package_name       ),
        new QString( like_name          ),
        new QString( is_declared_unit   ),
        new QString( when_name          )
    };
}

void spec_struct_data_node::add_struct_field(
    QString             field_name,
    QList <QString*>    properties
) {
    spec_field_node *new_node = new spec_field_node(field_name, properties);
    field_nodes.append( new_node );
}   // end of add_struct_field

void spec_struct_data_node::add_constraint_node(
    QString                 constraint_name,
    QList <QString*>        properties
) {
    // Let's understand what kind of constraint expression/operation we have in front of us.
    bool    is_complete     = *(properties.at(0)) == "END_EX";
    bool    is_expression   = *(properties.at(1)) == "EX";
    
    // Remove the first two elements of the list, since these have now been evaluated.
    properties.takeFirst();
    properties.takeFirst();
    
    // Check if we haven't been assembling a constraint (no object assigned)
    if( cur_constraint_node == nullptr ) {
//        qDebug() << "Creating new constraint node";
        // Create a new intermediate constraint node that is required to be assembled
        cur_constraint_node = new spec_constraint_node();
        
        // Then add either a new expression or operator
        if( is_expression ) {
            cur_constraint_node->add_expression( constraint_name, properties );
        } else {
            cur_constraint_node->add_operator( constraint_name, properties );
        };
    } else {
        // If a constraint is currently being constructed, add either a new expression or operator
        if( is_expression ) {
            QString debug_str;
            for( QString *str : properties ) {
                debug_str += *str + " ";
            };
            spec_msg( "Adding CONSTRAINT_NAME: " + constraint_name + " with " << debug_str );
            cur_constraint_node->add_expression( constraint_name, properties );
        } else {
            cur_constraint_node->add_operator( constraint_name, properties );
        };
    };  // end of handling constraint construction in progress
    
    // And check for the current constraint to be readily assembled.
    if( is_complete ) {
//        qDebug() << "Adding full constraint node to list of constraint nodes";
        // Add a copy of the intermediate constraint node
        constraint_nodes.append( new spec_constraint_node( cur_constraint_node ) );
        // and NULL the current constraint node
        delete cur_constraint_node;
        cur_constraint_node = nullptr;
        // and stop executing the current method... nothing else to be done
        return;
    };  // end of handling a completed constraint
}   // end of add_constraint_node method

void spec_struct_data_node::add_struct_node( spec_struct_data_node *node ) {
    this->addChild( node );
}

QString spec_struct_data_node::get_struct_name() {
    return struct_name;
}

QString spec_struct_data_node::get_package_name() {
    return package_name;
}

QString spec_struct_data_node::get_like_name() {
    return like_name;
}
QString spec_struct_data_node::get_when_name() {
    return when_name;
}

void spec_struct_data_node::set_field(spec_field_node *node) {
    qDebug() << "spec_struct_data_node::set_field() called";
}

QList<spec_field_node*> spec_struct_data_node::get_fields() {
    return field_nodes;
}

void spec_struct_data_node::set_fields          (
    QList<spec_field_node*> fields
) {
    // Filter any field names that appear twice
    // This ensures avoiding duplicates in the database
    for( spec_field_node *field : fields ) {
        if( not field_nodes.contains( field ) ) {
            field_nodes.append( field );
        };
    };
}   // end of set_fields


void spec_struct_data_node::set_field_constraint(
    QString field_name,
    QString field_enclosing_struct_type,
    QString field_instance_name
) {
    
}




QString spec_struct_data_node::get_object_name() {
    return object_name;
}
void spec_struct_data_node::set_object_name(
    QString name
) {
    object_name = name;
}

bool spec_struct_data_node::is_unit() {
    return is_declared_unit;
}

void spec_struct_data_node::set_is_list_object(
    bool is
) {
    is_list = is;
}

bool spec_struct_data_node::get_is_list_object() {
    return is_list;
}

void spec_struct_data_node::set_hdl_path( QString attibute ) {
    hdl_path_attribute = attibute;
}

QString spec_struct_data_node::get_hdl_path() {
    return hdl_path_attribute;
}

void spec_struct_data_node::set_clock_domain_list( QList<QString> domains ) {
    clock_domains = domains;
    if( like_name == "uvm_agent" ) {
        if( node_widget == nullptr ) {
            node_widget = new spec_agent_widget();
            static_cast<spec_agent_widget*>(node_widget) -> initialize();
        };
        static_cast<spec_agent_widget*>(node_widget) -> set_clock_domain_list( domains );
    };
}

void spec_struct_data_node::set_reset_domain_list( QList<QString> domains ) {
    reset_domains = domains;
    if( like_name == "uvm_agent" ) {
        static_cast<spec_agent_widget*>(node_widget) -> set_reset_domain_list( domains );
    };
}

QList<QString> spec_struct_data_node::get_clock_domain_list() {
    return clock_domains;
}

QList<QString> spec_struct_data_node::get_reset_domain_list() {
    return reset_domains;
}

QString spec_struct_data_node::get_clock_domain_conf() {
    if( like_name == "uvm_agent" ) {
        return static_cast<spec_agent_widget*>(node_widget)->get_selected_clock_domain();
    } else {
        return QString("");
    };
}

QString spec_struct_data_node::get_reset_domain_conf() {
    if( like_name == "uvm_agent" ) {
        return static_cast<spec_agent_widget*>(node_widget)->get_selected_reset_domain();
    } else {
        return QString("");
    };
}

void spec_struct_data_node::set_enum_type_selector(
    QString         field_name,
    QList<QString>  enum_type_values
) {
    
    if( like_name == "uvm_agent" ) {
        if( static_cast<spec_agent_widget*>(node_widget) -> get_selected_interface_domain() == "" ) {
            static_cast<spec_agent_widget*>(node_widget) -> set_interface_domain_list( enum_type_values );
        };
    };
    if( like_name == "uvm_env" ) {
        if( static_cast<spec_env_widget*>(node_widget) -> get_selected_env_name() == "" ) {
            static_cast<spec_env_widget*>(node_widget) -> set_env_name_list( enum_type_values );
        };
    };
    
    if( like_name == "uvm_config_params_s" ) {
        if( not static_cast<spec_config_params_widget*>(node_widget) -> has_field_name( field_name ) ) {
            if( enum_type_values.count() > 0 ) {
                static_cast<spec_config_params_widget*>(node_widget) -> add_field_enum_type(
                    field_name,
                    enum_type_values
                );
            };
        };
    };
}

bool spec_struct_data_node::has_object_inst_of_driver() {
    
}

QString spec_struct_data_node::get_inst_path_to_driver() {
    qDebug() << "spec_struct_data_node::get_inst_path_to_driver(): " << " This will FAIL because there is no STRING RETURN value";
    
}

// This method is intended to traverse the full hierarchy tree of the given code base.
// Great for debugging data structures in the tree.
void spec_struct_data_node::node_visitor(
    spec_struct_data_node   *node,
    int                     stack_cnt
) {
    QString str;
    int     child_count = node->childCount();
    spec_struct_data_node *traversal_node;
    
    for( int i = 0; i < stack_cnt; ++i ) { str += "  "; };
    stack_cnt++;
    
    qDebug() << str + "==> Entering node  -->  " + node->get_struct_name() + "  object  " + node->get_object_name();
    
//    for( spec_constraint_node* c_node : get_constraints_list() ) {
//        qDebug() << "node_visitor NODE: " + node->get_struct_name() + " " + c_node->show_constraint_info();
//    };
    
    if( child_count > 0 ) {
        for( int i = 0; i < child_count; ++i ) {
            traversal_node = static_cast<spec_struct_data_node*>( node->child(i) );
            node_visitor(
                traversal_node,
                stack_cnt
            );
        };
    } else {
        qDebug() << str + "No more children to process";
    };
    
    stack_cnt--;
    qDebug() << str + "<== Exiting node  -->  " + node->get_struct_name() + "  object  " + node->get_object_name();
}   // end of node_visitor

void spec_struct_data_node::set_widget_nullptr() {
    node_widget = nullptr;
}

void spec_struct_data_node::init_node_widget() {
    QString property_string;
    
    if( node_widget == nullptr ) {
        // Create a default widget first to avoid any crashes when setting the actual widget parameters later
        node_widget     = new QWidget();
        
        // Initialize a new widget, based on the node type
        if( like_name == "uvm_env" ) {
            node_widget = new spec_env_widget();
            static_cast<spec_env_widget*>(node_widget) -> initialize();
        };  // end of initializing uvm_env
        
        if( like_name == "uvm_agent" ) {
            node_widget = new spec_agent_widget();
            static_cast<spec_agent_widget*>(node_widget) -> initialize();
        };  // end of initializing uvm_agent
        
        if( like_name == "uvm_config_params_s" or like_name == "uvm_config_params" ) {
            node_widget = new spec_config_params_widget();
            static_cast<spec_config_params_widget*>(node_widget) -> initialize();
        };  // end of initializing uvm_config_params_s
        
        if( like_name == "any_event_port" ) {
            node_widget = new spec_event_port_widget();
            static_cast<spec_event_port_widget*>(node_widget) -> initialize();
        };  // end of initializing any_event_port
       
        if( like_name == "any_simple_port" ) {
            node_widget = new spec_simple_port_widget();
            static_cast<spec_simple_port_widget*>(node_widget) -> initialize();
        };  // end of initializing simple_port widget
        
        if( like_name == "any_interface_port" ) {
            node_widget = new spec_tlm_port_widget();
            static_cast<spec_tlm_port_widget*>(node_widget) -> initialize();
        };  // end of initializing any_interface_port
    };  // end of initializing all widget types
    
    // Move all the configuration settings outside the nullptr checks.
    if( like_name == "uvm_agent" ) {
        // Set ACTIVE and PASSIVE selector
        static_cast<spec_agent_widget*>(node_widget) -> set_active_passive(
            when_name.contains( QRegularExpression("ACTIVE") ),
            false
        );
        // Setting the CLOCK and RESET domain is done from the code model, which encapsulates this object.
        // The same applies for setting the selection of possible interface names
    };  // end of handling uvm_agent
    
    if( like_name == "any_event_port" ) {
        static_cast<spec_event_port_widget*>(node_widget) -> set_port_object_name   ( get_object_name() , false );
        property_string = me_node->get_port_direction();
        if( property_string == "in" ) {
            property_string = "Event Receiver";
        } else if( property_string == "out" ) {
            property_string = "Event Emitter";
        } else {
            property_string = "Event Receiver and Emitter";
        };
        static_cast<spec_event_port_widget*>(node_widget) -> set_direction          ( property_string, false );
        static_cast<spec_event_port_widget*>(node_widget) -> set_hdl_name           ( get_hdl_path()    );
        
        property_string = "";
//            if(  ) {
            
//            } else if (  ) {
            
//            } else {
            
//            };
        static_cast<spec_event_port_widget*>(node_widget) -> set_event_trigger      ( "", false );
    };
    
    if( like_name == "any_simple_port" ) {
        static_cast<spec_simple_port_widget*>(node_widget) -> set_port_name         ( get_instance_field()->get_field_name()    , false );
        static_cast<spec_simple_port_widget*>(node_widget) -> set_access_direction  ( get_instance_field()->get_port_direction(), true  );
        static_cast<spec_simple_port_widget*>(node_widget) -> set_signal_name       ( ""                                        , false );
        static_cast<spec_simple_port_widget*>(node_widget) -> set_type_value        ( get_instance_field()->get_port_data_type(), false );
        static_cast<spec_simple_port_widget*>(node_widget) -> set_hdl_hierarchy     ( get_hdl_path()                                    );
    };
    
    if( like_name == "any_interface_port" ) {
        property_string = me_node -> get_field_name();      // name of object
        static_cast<spec_tlm_port_widget*>(node_widget) -> set_port_object_name( property_string, false );
        property_string = me_node -> get_port_data_type();  // data type signature of port
        static_cast<spec_tlm_port_widget*>(node_widget) -> set_port_data_type( property_string, false );
        property_string = me_node -> get_port_kind();       // tlm_analysis...
        static_cast<spec_tlm_port_widget*>(node_widget) -> set_port_kind( property_string, false );
        property_string = me_node ->get_port_direction();   // in/out/inout
        static_cast<spec_tlm_port_widget*>(node_widget) -> set_port_direction( property_string, false );
    };
    
    // Descent into each of the current node's children and also initialize those widgets recursively
    for( int index = 0; index < childCount();++index ) {
        get_child( index )->init_node_widget();
    };
}

// This method is used to visualize the appropriate widget to each given node
void spec_struct_data_node::show_node_widget(QWidget *parent) {
//    spec_msg( "Showing the node_widget " << this << struct_name );
    QString property_string;
    
    // Only generate a new widget, if the current node does not contain a widget
    if( node_widget == nullptr ) {
        // Create a default widget first to avoid any crashes when setting the actual widget parameters later
        node_widget     = new QWidget();
        
        // Initialize a new widget, based on the node type
        if( like_name == "uvm_env" ) {
            node_widget = new spec_env_widget();
            static_cast<spec_env_widget*>(node_widget) -> initialize();
        };
        if( like_name == "uvm_agent" ) {
            node_widget = new spec_agent_widget();
            static_cast<spec_agent_widget*>(node_widget) -> initialize();
            
            // Set ACTIVE and PASSIVE selector
            static_cast<spec_agent_widget*>(node_widget) -> set_active_passive(
                when_name.contains( QRegularExpression("ACTIVE") ),
                false
            );
            
            // Setting the CLOCK and RESET domain is done from the code model, which encapsulates this object.
            // The same applies for setting the selection of possible interface names
        };
        if( like_name == "uvm_config_params_s" or like_name == "uvm_config_params" ) {
            node_widget = new spec_config_params_widget();
            static_cast<spec_config_params_widget*>(node_widget) -> initialize();
            
        };
        if( like_name == "any_event_port" ) {
            node_widget = new spec_event_port_widget();
            static_cast<spec_event_port_widget*>(node_widget) -> initialize();
            static_cast<spec_event_port_widget*>(node_widget) -> set_port_object_name   ( get_object_name() , false );
            property_string = me_node->get_port_direction();
            if( property_string == "in" ) {
                property_string = "Event Receiver";
            } else if( property_string == "out" ) {
                property_string = "Event Emitter";
            } else {
                property_string = "Event Receiver and Emitter";
            };
            static_cast<spec_event_port_widget*>(node_widget) -> set_direction          ( property_string, false );
            static_cast<spec_event_port_widget*>(node_widget) -> set_hdl_name           ( get_hdl_path()    );
            
            property_string = "";
//            if(  ) {
                
//            } else if (  ) {
                
//            } else {
                
//            };
            
            static_cast<spec_event_port_widget*>(node_widget) -> set_event_trigger      ( "", false );
        };
        if( like_name == "any_simple_port" ) {
            node_widget = new spec_simple_port_widget();
            static_cast<spec_simple_port_widget*>(node_widget) -> initialize();
            static_cast<spec_simple_port_widget*>(node_widget) -> set_port_name         ( get_instance_field()->get_field_name()    , false );
            static_cast<spec_simple_port_widget*>(node_widget) -> set_access_direction  ( get_instance_field()->get_port_direction(), true  );
            static_cast<spec_simple_port_widget*>(node_widget) -> set_signal_name       ( ""                                        , false );
            static_cast<spec_simple_port_widget*>(node_widget) -> set_type_value        ( get_instance_field()->get_port_data_type(), false );
            static_cast<spec_simple_port_widget*>(node_widget) -> set_hdl_hierarchy     ( get_hdl_path()                                    );
        };
        if( like_name == "any_interface_port" ) {
            node_widget = new spec_tlm_port_widget();
            static_cast<spec_tlm_port_widget*>(node_widget) -> initialize();
            property_string = me_node -> get_field_name();      // name of object
            static_cast<spec_tlm_port_widget*>(node_widget) -> set_port_object_name( property_string, false );
            property_string = me_node -> get_port_data_type();  // data type signature of port
            static_cast<spec_tlm_port_widget*>(node_widget) -> set_port_data_type( property_string, false );
            property_string = me_node -> get_port_kind();       // tlm_analysis...
            static_cast<spec_tlm_port_widget*>(node_widget) -> set_port_kind( property_string, false );
            property_string = me_node ->get_port_direction();   // in/out/inout
            static_cast<spec_tlm_port_widget*>(node_widget) -> set_port_direction( property_string, false );
        };
        if( like_name == "" ) {
            
        };
        
        // Show widget of current data node
        node_widget -> setGeometry( 10, 25, 850, 850 );
        node_widget -> setParent( parent );
        node_widget -> show();
    } else {
        // Simply show the widget on the parent
        node_widget -> setGeometry( 10, 25, 850, 850 );
        node_widget -> setParent( parent );
        node_widget -> show();
    };
    
    // Constraint node debugging
//    for( spec_constraint_node *c_node : constraint_nodes ) {
//        qDebug() << c_node->show_constraint_info();
//    };
    
}   // end of show_node_widget

void spec_struct_data_node::update_widget() {
    // Updating only the table view of the widget, if it has a table view.
    // This ensures that the enumerated types have been previously set.
    if( like_name == "uvm_config_params_s" or like_name == "uvm_config_params" ) {
        
        // Add configuration step in here
        for( int field_index = 0; field_index < field_nodes.count(); ++field_index ) {
            
            // Add each field to the configuration table
            static_cast<spec_config_params_widget*>(node_widget) -> add_field_info(
                field_index,
                field_nodes.at( field_index )->get_field_name(),
                field_nodes.at( field_index )->get_field_type(),
                field_nodes.at( field_index )->get_field_value()
            );
        };
        
    };
}

spec_env_widget                 *spec_struct_data_node::get_env_widget                 () {
    return static_cast<spec_env_widget*>(node_widget);
}
spec_agent_widget               *spec_struct_data_node::get_agent_widget               () {
    return static_cast<spec_agent_widget*>(node_widget);
}
spec_config_params_widget       *spec_struct_data_node::get_config_params_widget       () {
    return static_cast<spec_config_params_widget*>(node_widget);
}
spec_event_port_widget          *spec_struct_data_node::get_event_port_widget          () {
    return static_cast<spec_event_port_widget*>(node_widget);
}
spec_simple_port_widget         *spec_struct_data_node::get_simple_port_widget         () {
    return static_cast<spec_simple_port_widget*>(node_widget);
}
spec_tlm_port_widget            *spec_struct_data_node::get_tlm_port_widget            () {
    return static_cast<spec_tlm_port_widget*>(node_widget);
}


void spec_struct_data_node::set_constraints(
    QList <spec_constraint_node*>   constraints
) {
    constraint_nodes = constraints;
}
QList <spec_constraint_node*> spec_struct_data_node::get_constraints_list() {
    return constraint_nodes;
}

void spec_struct_data_node::apply_constraints() {
    QString         op;
    
    QList<QString>  exp_list;
    QString         exp;
    
    QString         target_type;
    QString         enclosing_struct = struct_name;
    QString         cur_field_name;
    QString         cur_field_type;
    QString         prev_field_name;
    QString         prev_field_type;
    QString         hdl_path_value  ="";
    QString         literal_value   = "";
    bool            is_soft         = false;
    bool            is_hdl_path     = false;
    bool            is_struct_found = false;
    int             path_count      = 0;
    
    int             field_index         = -1;
    int             node_child_index    = -1;
    
    spec_struct_data_node *cur_node = this;
    
    int c_node_index= 0;
    int field_count = 0;
    int cur_child_count = 0;
    int path_index  = 0;
    
    for( spec_constraint_node * c_node : constraint_nodes ) {
        // Get the operation first, and only handle any == operation assignments
        op = c_node -> get_operation();
        
        // Only support the == assignment operation
        if( op == "==" ) {
//            qDebug() << "=======================================================================================================";
//            qDebug() << "==                  APPLYING CONSTRAINTS                                                               ";
//            qDebug() << "-------------------------------------------------------------------------------------------------------";
//            qDebug() << "== Constraint Node Index " << c_node_index;
            
            // Reset constraint detection
            
            // Since it is not allowed to assign a literal to a literal, we will reset the literal value per constraint and not
            // per expression
            literal_value = "";
            
            // TODO: Why is the actual type detection not working?
            //       Currently the algorithm stumbles and assigns any single port as an any_event_port, which is not
            //       correct for the simple_ports.
            
//            qDebug() << "EXPRESSION 0 " << e0 << "    EXPRESSION 1" << e1;
            // We need to cycle over expression 0 and expression 1 using the same algorithm
            for( int ex_index = 0; ex_index < 2; ++ex_index ) {
//                qDebug() << "== Expression Index      " << ex_index;
                
                // Each new constraint expression evaluation starts with this current struct as the new starting constraint
                cur_node = this;
                
                exp_list = c_node->get_expr( ex_index );
                
                // Resetting the path iteration index
                path_index      = 0;
                
                // How many tokens are in the expression list?
                path_count      = exp_list.count();
                
                cur_field_name  = "";
                cur_field_type  = "";
                prev_field_name = "";
                prev_field_type = "";
                is_soft         = false;
                is_struct_found = false;
                is_hdl_path     = false;
                
                // TODO: Check for the subtyped nodes!!!!
                
                // Constraint hierarchical path traversal
                for( QString path_elem : exp_list ) {
                    // Always get the number of children in the current struct node.
                    // Please note this only works if all the sub-trees had already been parsed.
                    field_count     = cur_node -> childCount();//cur_node -> field_nodes.count();//field_nodes.count();//cur_node -> childCount();
                    cur_child_count = cur_node -> childCount();
                    
                    int prev_field_index        = field_index;
                    int prev_node_child_index   = node_child_index;
                    
                    if( (cur_node -> childCount()) == (cur_node -> field_nodes.count()) ) {
//                        qDebug() << "Number of Field Nodes and Child Nodes are the same " << cur_node -> childCount();
//                        field_index         = -1;
//                        node_child_index    = -1;
                        
                        if( cur_node ->childCount() == 0 ) {
                            qDebug() << " ==> PATH ELEMENT is a pre-defined literal " << path_elem;
                        } else {
                            field_index         = -1;
                            node_child_index    = -1;
                        };
                        
                        for( int it_index = 0; it_index < cur_node -> field_nodes.count(); ++it_index ) {
//                            qDebug() << "Field(" << it_index << ") == " << cur_node->field_nodes.at(it_index)->get_data_type() << "  NAME " << cur_node->field_nodes.at(it_index)->get_field_name();
                            if( path_elem == cur_node->field_nodes.at(it_index)->get_field_name() ) {
//                                qDebug() << "path_elem found at field[" << it_index << "]";
                                field_index = it_index;
                                break;
                            };
                        };
                        
                        for( int it_index = 0; it_index < cur_child_count; ++it_index ) {
//                            qDebug() << "Child(" << it_index << ") == " << cur_node->get_child( it_index ) -> get_struct_name();
                            if(path_elem == cur_node->get_child( it_index ) -> get_object_name()  ) {
//                                qDebug() << "path_elem found at child[" << it_index << "]";
                                node_child_index = it_index;
                                break;
                            };
                        };
                        
                        // Only if the current constraint element was found in 
                        if( node_child_index != -1 ) {
                            // Now we will be able to assign a proper next current node
                            cur_node        = cur_node->get_child(node_child_index);
                            c_node          -> append_expr_type_path( ex_index, cur_node -> get_struct_name() );
                            
//                            qDebug() << "  ==> Found NODE at(" << node_child_index << ") == " << 
//                                        cur_node->get_object_name() << " : " << cur_node->get_struct_name();
                        } else {
                            if( field_index != -1 ) {
                                c_node -> append_expr_type_path( ex_index, cur_node -> field_nodes.at(field_index)->get_data_type() );
                                // This is a scalar field ans has no further object hierarchy to analyze
//                                qDebug() << "  ==> Found FIELD at (" << field_index << ") == " << 
//                                            cur_node -> field_nodes.at(field_index)->get_field_name() << " : " << 
//                                            cur_node -> field_nodes.at(field_index)->get_data_type();
                            } else {
                                qDebug() << "(0)  ==> PATH ELEMENT is a pre-defined literal " << path_elem;
                                field_index         = prev_field_index;
                                node_child_index    = prev_node_child_index;
                                literal_value       = c_node ->get_expr( ex_index ).last();
                            };
                        };
                        
//                        for( int it_index = 0; it_index < cur_child_count; ++it_index ) {
//                            qDebug() << "Child(" << it_index << ") == " << cur_node->get_child( it_index ) -> get_struct_name();
//                            qDebug() << "Field(" << it_index << ") == " << cur_node->field_nodes.at(it_index)->get_data_type() << "  NAME " << cur_node->field_nodes.at(it_index)->get_field_name();
                            
//                            if( path_elem == cur_node->field_nodes.at(it_index)->get_field_name() ) {
//                                qDebug() << "path_elem found at field[" << it_index << "]";
//                                field_index         = it_index;
//                                node_child_index    = it_index;
//                            };
//                        };
                        
                    } else {
//                        qDebug() << "Number of Field Nodes and Child Nodes differ: ";
//                        qDebug() << "  ==> field_count " << field_nodes.count() << "    cur_child_count " << cur_child_count << "  NAME: " << cur_field_name;
                        int prev_field_index        = field_index;
                        int prev_node_child_index   = node_child_index;
                        field_index         = -1;
                        node_child_index    = -1;
                        
                        for( int it_index = 0; it_index < cur_node -> field_nodes.count(); ++it_index ) {
//                            qDebug() << "Field(" << it_index << ") == " << cur_node->field_nodes.at(it_index)->get_data_type() << "  NAME " << cur_node->field_nodes.at(it_index)->get_field_name();
                            if( path_elem == cur_node->field_nodes.at(it_index)->get_field_name() ) {
//                                qDebug() << "path_elem found at field[" << it_index << "]";
                                field_index = it_index;
                                break;
                            };
                        };
                        
                        for( int it_index = 0; it_index < cur_child_count; ++it_index ) {
//                            qDebug() << "Child(" << it_index << ") == " << cur_node->get_child( it_index ) -> get_struct_name();
                            if(path_elem == cur_node->get_child( it_index ) -> get_object_name()  ) {
//                                qDebug() << "path_elem found at child[" << it_index << "]";
                                node_child_index = it_index;
                                break;
                            };
                        };
                        
                        // Only if the current constraint element was found in 
                        if( node_child_index != -1 ) {
                            // Now we will be able to assign a proper next current node
                            cur_node        = cur_node->get_child(node_child_index);
                            c_node          -> append_expr_type_path( ex_index, cur_node -> get_struct_name() );
                            
//                            qDebug() << "  ==> Found NODE at(" << node_child_index << ") == " << 
//                                        cur_node->get_object_name() << " : " << cur_node->get_struct_name();
                        } else {
                            if( field_index != -1 ) {
                                c_node -> append_expr_type_path( ex_index, cur_node -> field_nodes.at(field_index)->get_data_type() );
                                // This is a scalar field ans has no further object hierarchy to analyze
//                                qDebug() << "  ==> Found FIELD at (" << field_index << ") == " << 
//                                            cur_node -> field_nodes.at(field_index)->get_field_name() << " : " << 
//                                            cur_node -> field_nodes.at(field_index)->get_data_type();
                            } else {
                                qDebug() << "(1)  ==> PATH ELEMENT is a pre-defined literal " << path_elem;// << "  value : " << c_node ->get_expr( ex_index );
                                field_index         = prev_field_index;
                                node_child_index    = prev_node_child_index;
                                literal_value       = c_node ->get_expr( ex_index ).last();
                            };
                        };
                    };  // end of handling differently sized field and child lists
//                    qDebug() << "== Path Index            " << path_index << " with path_elem   " << path_elem;
                    
//                    qDebug() << "== [" << path_index << "/" << (path_count-1) << "] : " << path_elem;
                    
                    // First Expression after full expression path analysis
                    // Please note there is no constraint detection/handling on non UVM derived classes.
                    // In case you are missing a field value, then you may have to derive from uvm_config_params_s or add
                    // any other uvm_* struct/unit class inheritance.
                    if( ex_index == 1 and path_index == path_count-1 ) {
//                        qDebug() << "spec_struct_data_node::apply_constraints(): " << "field_index : " << field_index << ", node_child_index : " << node_child_index;
                        
                        // The recursive constraint application will handle each node that is a data structure, but any
                        // literal fields will not be updated
                        set_constraint_recursive( c_node );
                        
                        // To assign values from constraint that were assigned on literal fields, we need to first get
                        // the field name and then we'll be able to assign the value.
                        if( not literal_value.isEmpty() ) {
                            QString literal_field_name = c_node->get_expr((ex_index+1)%2).last();
                            // Assigning literal value to field
                            
//                            qDebug() << "Field Index " << field_index << "  Node Child Index " << node_child_index;
                            if( field_count >= 0 ) {
//                                qDebug() << "(INFO): Assign literal value (" << literal_value << ") to field " << field_nodes.at(field_index)->get_field_name();
                                field_nodes.at(field_index)->set_field_value( literal_value );
                            };
                        };  // end of setting literal field values
                        
//                        qDebug() << "=======================================================================================";
//                        qDebug() << "== Struct Name     : " + struct_name;
//                        qDebug() << "== Struct WHEN Name: " + when_name;
//                        qDebug() << "== Like Name       : " + like_name;
//                        qDebug() << "---------------------------------------------------------------------------------------";
//                        qDebug() << "==  -> Field Name   : " + cur_field_name;
//                        qDebug() << "==  -> Object Struct: " + cur_node -> get_struct_name();
//                        qDebug() << "==  -> Object Like  : " + cur_node -> get_like_name();
//                        qDebug() << "==  -> Object Value : " + cur_node -> get_hdl_path();
                        
//                        if( c_node -> get_expr_attr(0)  == "hdl_path" ) {
//                            qDebug() << "==  -> HDL Path     : " + c_node -> get_expr_value(0);
//                        };
//                        if( c_node -> get_expr_attr(0) == "edge" ) {
//                            qDebug() << "==  -> Clock Edge is: " + c_node -> get_expr_value(0);
//                        };
//                        if( c_node -> get_expr_attr(0) == "size" ) {
//                            qDebug() << "==  -> List Size    : " + c_node -> get_expr_value(0);
//                        };
//                        if( c_node -> get_expr_value(0) != "" ) {
//                            // These infos are needed to set pre-fill the values in the GUI
//                            qDebug() << "==  -> Field        : " << c_node->get_expr(0) << " : " << c_node->get_expr_type(0) << " == " << c_node->get_expr_value(0);
//                            qDebug() << "==  -> Configurable : " << (c_node->get_is_expr_soft(0)? "YES" : "NO");
//                        };
//                        qDebug() << "==  -> Constraint Path       : " << c_node->get_expr( 0 ) << "   ||   " << c_node->get_expr( 1 );
//                        qDebug() << "==  -> Constraint Type Path  : " << c_node->get_expr_type_path( 0 ) << "   ||   " << c_node->get_expr_type_path( 1 );
//                        qDebug() << "=======================================================================================";
                    };
                    
                    ++path_index;
                };  // end of cycling over each expression list element
            };  // end of iterating over each expression
        };  // end of processing only assignment constraints
        ++c_node_index;
    };  // end of iterating over each constraint
}

void spec_struct_data_node::set_constraint_recursive(
    spec_constraint_node    *c_node
) {
    bool is_print_debug = true;
    
    if( is_print_debug ) {
        qDebug() << "=======================================================================================";
        qDebug() << "== Struct Name     : " + struct_name;
        qDebug() << "== Struct WHEN Name: " + when_name;
        qDebug() << "== Like Name       : " + like_name;
        qDebug() << "---------------------------------------------------------------------------------------";
        
        if( c_node -> get_expr_attr(0)  == "hdl_path" ) {
            qDebug() << "==  -> HDL Path     : " + c_node -> get_expr_value(0);
        };
        if( c_node -> get_expr_attr(0) == "edge" ) {
            qDebug() << "==  -> Clock Edge is: " + c_node -> get_expr_value(0);
        };
        if( c_node -> get_expr_attr(0) == "size" ) {
            qDebug() << "==  -> List Size    : " + c_node -> get_expr_value(0);
        };
        if( c_node->get_expr_attr(0) != "" ) {
            qDebug() << "==  -> Attribute Config: " << c_node->get_expr_attr(0) << " == " << c_node->get_expr_value(0);
        };
        
        if( c_node -> get_expr_value(0) != "" ) {
            // These infos are needed to set pre-fill the values in the GUI
            qDebug() << "==  -> Field        : " << c_node->get_expr(0) << " : " << c_node->get_expr_type(0) << " == " << c_node->get_expr_value(0);
            qDebug() << "==  -> Configurable : " << (c_node->get_is_expr_soft(0)? "YES" : "NO");
        };
        qDebug() << "==  -> Constraint Path       : " << c_node->get_expr( 0 ) << "   ||   " << c_node->get_expr( 1 );
        qDebug() << "==  -> Constraint Type Path  : " << c_node->get_expr_type_path( 0 ) << "   ||   " << c_node->get_expr_type_path( 1 );
        qDebug() << "=======================================================================================";
    };
    
    spec_struct_data_node   *target_node;
    QString                 cons_obj_name   = c_node->get_expr(0).at(0);
    QString                 cons_value      = c_node->get_expr(1).at(0);
    
    // Setting the hdl_path attribute is currently only working for a one-level deep traversal
    // TODO: Get multi-level traversal built-in to the handling for more general constraint support.
    bool is_attribute_value = (
        c_node->get_expr_attr(0) == "hdl_path"
                or
        c_node->get_expr_attr(0) == "verilog_wire"
                or
        c_node->get_expr_attr(0) == "declared_range"
    );
    
    if( is_attribute_value ) {
        for( int i = 0; i < childCount(); ++i ) {
            target_node = get_child( i );
            if( target_node->get_object_name() == cons_obj_name ) {
                // Due to get_child() returning a non-modifiable object we need to remove the child
                target_node = static_cast<spec_struct_data_node*>( takeChild( i ) );
                // Get rid of the quotation marks
                cons_value = cons_value.replace( "\"", "" );
                
                if( c_node->get_expr_attr(0) == "hdl_path" ) {
                    target_node->set_hdl_path( cons_value );
                };
                
                if( c_node->get_expr_attr(0) == "verilog_wire" ) {
                    
                };
                
                if( c_node->get_expr_attr(0) == "declared_range" ) {
                    
                };
                
                // ... and insert the child back.
                insertChild( i , target_node );
                break;
            };  // end of handling matching object names
        };  // end of iterating over each of the node children
    };  // end of processing the hdl_path attribute setting
}

spec_struct_data_node *spec_struct_data_node::get_env_node() {
    if( like_name == "uvm_env" ) {
        return this;
    } else {
        return nullptr;
    };
}

QList<spec_struct_data_node*> spec_struct_data_node::get_active_agent_nodes() {
    QList<spec_struct_data_node*>   result;
    spec_struct_data_node           *node;
    
    if( like_name == "uvm_env" ) {
        for( int index = 0; index < childCount(); ++index ) {
            node = static_cast<spec_struct_data_node*>(child( index ));
            if( node->get_like_name() == "uvm_agent"  and node->get_object_name().contains( "[") ) {
                if( node->get_when_name().contains( QRegularExpression( "ACTIVE" ) ) ) {
                    result.append( node );
                };
            };
        };
    };
    
    if( result.count() > 0 ) {
        return result;
    };
    qDebug() << "spec_struct_data_node::get_active_agent_node() called on a struct that has not been derived by uvm_env";
    return result;
}

QList<spec_struct_data_node*> spec_struct_data_node::get_passive_agent_nodes() {
    QList<spec_struct_data_node*>   result;
    spec_struct_data_node           *node;
    
    if( like_name == "uvm_env" ) {
        for( int index = 0; index < childCount(); ++index ) {
            node = static_cast<spec_struct_data_node*>(child( index ));
            if( node->get_like_name() == "uvm_agent" and node->get_object_name().contains( "[") ) {
                if( not node->get_when_name().contains( QRegularExpression( "ACTIVE" ) ) ) {
                    result.append( node );
                };
            };
        };
    };
    
    if( result.count() > 0 ) {
        return result;
    };
    spec_msg( "Called on a struct that has not been derived by uvm_env" );
    return result;
}   // end of get_passive_agent_nodes

spec_struct_data_node *spec_struct_data_node::get_env_configuration_node() {
    QString                 child_like_name;
    if( like_name == "uvm_env" ) {
        for( int child_index = 0; child_index < childCount();++child_index ) {
            child_like_name = get_child( child_index )->get_like_name();
            if( child_like_name == "uvm_config_params_s" or child_like_name == "uvm_config_params" ) {
                return get_child( child_index );
            };
        };
    } else {
        return nullptr;
    }
    // We should not reach this instruction, but in case we do, we will send a nullpointer back.
    return nullptr;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TODO IMPLEMENT THESE METHODS
//
// 
spec_struct_data_node *spec_struct_data_node::get_agent_configuration_node( QString exclude_name ) {
    QString child_struct_name, child_like_name;
    
    if( like_name == "uvm_agent" ) {
        for( int child_index = 0; child_index < childCount(); ++child_index ) {
            child_struct_name   = get_child( child_index )->get_struct_name();
            child_like_name     = get_child( child_index )->get_like_name();
            
            if(
               child_struct_name != exclude_name and
               (child_like_name == "uvm_config_params_s" or child_like_name == "uvm_config_params")
               ) {
                return get_child( child_index );
            };
        };
    } else {
        spec_msg( "Method call performed on a non-agent entity: " << like_name  << " -- Returning nullptr.");
        return nullptr;
    };
    return nullptr;
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

spec_struct_data_node *spec_struct_data_node::get_monitor_node() {
    spec_struct_data_node   *result;
    
    if( like_name == "uvm_agent" ) {
        for( int index = 0; index < childCount(); ++index ) {
            result = get_child( index );
            if( result->get_like_name() == "uvm_monitor" ) {
                return result;
            };
        };
    };  // end of handling uvm_agent case
    spec_msg( "Called on struct " << struct_name << "like" << like_name <<"that can't determine its uvm_monitor instance" );
    return nullptr;
}

spec_struct_data_node *spec_struct_data_node::get_signal_map_node() {
    spec_struct_data_node   *result;
    if( like_name == "uvm_agent" ) {
        for( int index = 0; index < childCount(); ++ index ) {
            result = get_child( index );
            if( result->get_like_name() == "uvm_signal_map" ) {
                return result;
            };
        };
    };  // end of handling uvm_signal_map case
    spec_msg( "Called on struct " << struct_name << "like" << like_name << "that can't dertermine its uvm_signal_map instance." );
    return nullptr;
}   // end of get_signal_map_node

spec_struct_data_node *spec_struct_data_node::get_bfm_node() {
    spec_struct_data_node   *result;
    // Only search in ACTIVE uvm_agents for a BFM
    if( like_name == "uvm_agent" and when_name.contains( QRegularExpression( "ACTIVE" ) ) ) {
        for( int index = 0; index < childCount(); ++index ) {
            result = get_child( index );
            if( result->get_like_name() == "uvm_bfm" ) {
                return result;
            };
        };
    };  // end of handling uvm_agent case
    spec_msg( "Called on struct " << struct_name << "like" << like_name << "that can't dertermine its uvm_bfm instance." );
    return nullptr;
}   // end of get_bfm_node

spec_struct_data_node *spec_struct_data_node::get_driver_node() {
    spec_struct_data_node   *result;
    // Only search in ACTIVE uvm_agents for a driver
    if( like_name == "uvm_agent" and when_name.contains( QRegularExpression( "ACTIVE" ) ) ) {
        for( int index = 0; index < childCount();++index ) {
            result = get_child( index );
            if( result->get_like_name() == "any_sequence_driver" ) {
                return result;
            };
        };
    };
    spec_msg( "Called on struct " << struct_name << "like" << like_name << "that can't dertermine its uvm_driver instance." );
    return nullptr;
}   // end of get_driver_node

int spec_struct_data_node::get_active_agents_count() {
    if( like_name == "uvm_env" ) {
        // TODO  The node-widget check is not the best check and should be changed
        // --> the widget should be there... why is it not?????
        if( node_widget == nullptr ) {
            return 0;
        } else {
            return static_cast<spec_env_widget*>(node_widget) ->get_active_agents_count();
        }
    } else {
        return 0;
    };
}

int spec_struct_data_node::get_passive_agents_count() {
    if( like_name == "uvm_env" ) {
        if( node_widget == nullptr ) {
            return 0;
        } else {
            return static_cast<spec_env_widget*>(node_widget) -> get_passive_agents_count();
        }
    } else {
        return 0;
    };
}

void spec_struct_data_node::print_tree_recursive (
    int level_index
) {
    int             children_count      = childCount();
    QString         indentation_space   ;
    QList<QString>  message_output      ;
    
    for( int index = 0; index < level_index; ++index ) {
        indentation_space += "    ";
    };
    if( level_index == 0 ) {
        message_output.append("========================================================================================");
        message_output.append("     Object Name            :        Type Name");
        message_output.append("----------------------------------------------------------------------------------------");
    };
    message_output.append(indentation_space + get_object_name() + "        :         " + get_struct_name() + "\n" );
    if( like_name == "uvm_env" ) {
        get_env_widget()->print_widget_infos();
    };
    if( like_name == "uvm_agent" ) {
        get_agent_widget()->print_widget_info();
    };
    if( like_name == "uvm_config_params_s" ) {
//        get_config_params_widget()->print_widget_info();
    };
    if( like_name == "any_event_port" ) {
        get_event_port_widget()->print_widget_info();
    };
    if( like_name == "any_simple_port" ) {
        get_simple_port_widget()->print_widget_info();
    };
    if( like_name == "any_interface_port" ) {
//        get_tlm_port_widget()->print_widget_info();
    };
    
    spec_msg( message_output );
    for( int index = 0; index < children_count; ++index ) {
        get_child(index)->print_tree_recursive(level_index+1);
    };
}

QList<QString> spec_struct_data_node::get_incomplete_config_elements() {
    QList<QString>  result;
    QString         cur_object_name = object_name;
    
    if( like_name == "uvm_env" ) {
        if( node_widget == nullptr ) {
            cur_object_name = "env";
            result.append( struct_name + "  ->  Not configured.");
        } else {
            result.append(static_cast<spec_env_widget*>(node_widget)->get_incomplete_config_elements() );
        };
    };
    if( like_name == "uvm_agent" ) {
        if( node_widget == nullptr ) {
            result.append( cur_object_name + " : " + struct_name + "  ->  Not configured." );
        } else {
            result.append(static_cast<spec_agent_widget*>(node_widget)->get_incomplete_config_elements() );
        };
    };
    if( like_name == "uvm_config_params_s" ) {
        if( node_widget == nullptr ) {
            result.append( cur_object_name + " : " + struct_name + "  ->  Not configured." );
        };
    };
    if( like_name == "any_event_port" ) {
        if( node_widget == nullptr ) {
            result.append( cur_object_name + " : " + struct_name + "  ->  Not configured." );
        } else {
            result.append(static_cast<spec_event_port_widget*>(node_widget)->get_incomplete_config_elements() );
        };
    };
    if( like_name == "any_simple_port" ) {
        if( node_widget == nullptr ) {
            result.append( cur_object_name + " : " + struct_name + "  ->  Not configured.");
        } else {
            result.append(static_cast<spec_simple_port_widget*>(node_widget)->get_incomplete_config_elements() );
        };
    };
    if( like_name == "any_interface_port" ) {
        
    };
    for( int index = 0; index < childCount(); ++index ) {
        result.append( get_child(index)->get_incomplete_config_elements() );
    };
    
    if( not result.isEmpty() ) {
        for( int index = 0; index < result.count(); ++index ) {
            result.replace( index, QString( cur_object_name + "." + result.at(index) ) );
        };
    };
    
    return result;
}
