// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_INTERFACE_MODEL_H
#define SPEC_INTERFACE_MODEL_H

#include <CodeModel/spec_base_code_model.h>
#include <Widgets/spec_interface_wave_table.h>

class spec_interface_model : public spec_base_code_model {
        Q_OBJECT
        
    public:
        explicit spec_interface_model(QObject *parent = nullptr);
        
//        QString             get_directory();
        
//        QString             get_group_name();
//        QString             get_interface_name();
//        QString             get_uvc_prefix();
        
        QList <QString*>    get_clock_properties();
        QList <QString*>    get_reset_properties();
        
        int                 type_count();
        int                 signal_count();
        int                 item_count();
        int                 global_count();
        int                 agent_count();
        
        QList <QString*>    *get_type_line(
                int        index
                );
        QList <QString*>    *get_signal_line(
                int        index
                );
        QList <QString*>    *get_item_line(
                int        index
                );
        QList <QString*>    *get_global_line(
                int        index
                );
        QList <QString*>    *get_agent_line(
                int        index
                );
        
        
//        void                set_directory(
//                QString             dir_path
//                );
//        void                set_group_name(
//                QString             name
//                );
//        void                set_interface_name(
//                QString             name
//                );
        void                set_clock_properties        (
                QList <QString*>    property_list
                );
        void                set_reset_properties        (
                QList <QString*>    property_list
                );
        
        void                add_type_properties         (
                QList <QString*>    *property_list
                );
        void                add_signal_properties       (
                QList <QString*>    *property_list
                );
        void                add_item_properties         (
                QList <QString*>    *property_list
                );
        void                add_global_properties       (
                QList <QString*>    *property_list
                );
        void                add_agent_properties        (
                QList <QString*>    *property_list
                );
        
        void                add_scenario_table          (
                spec_interface_wave_table   *scenario_table
                );
        
        int                         get_scenario_count  ();
        spec_interface_wave_table   *get_scenario_table ( int index );
        
    signals:
        
    public slots:
        
    private:
        QString             uvc_directory;
        
        // Information form the Group and Interface text-boxes
        QString             prefix_group_name;
        QString             prefix_project_name;
        
        // Clock and reset mapping informtion
        QString             *clock_tb_name;
        QString             *clock_hdl_name;
        QString             *clock_trigger_edge;
        // type             clock_edge_sensitivity_t : [RISE, FALL, CHANGE, ASYNC];
        
        QString             *reset_tb_name;
        QString             *reset_hdl_name;
        QString             *reset_active_level;
        // type             reset_edge_active : [ACTIVE_LOW, ACTIVE_HIGH];
        
        // These fields resemble the 'Interface Signal' table information
        // Each table line is stored at the same index of all the other elements
        QList <QString*>    *type_names       = new QList <QString*>;
        QList <QString*>    *type_data_values = new QList <QString*>;
        
        // These fields resemble the 'Interface Signal' table information
        // Each table line is stored at the same index of all the other elements
        QList <QString*>    *signal_names       = new QList <QString*>;
        QList <QString*>    *signal_data_types  = new QList <QString*>;
        QList <QString*>    *signal_type_widths = new QList <QString*>;
        QList <QString*>    *signal_accesses    = new QList <QString*>;
        QList <QString*>    *signal_value       = new QList <QString*>;
        
        // These fields resemble the 'Data Item' transaction fields
        // Each table line is stored at the same index of all the other elements
        QList <bool>        *is_item_genable    = new QList <bool>;
        QList <bool>        *is_item_phyiscal   = new QList <bool>;
        QList <QString*>    *item_names         = new QList <QString*>;
        QList <QString*>    *item_data_types    = new QList <QString*>;
        QList <QString*>    *item_type_widths   = new QList <QString*>;
        QList <QString*>    *item_list_depth    = new QList <QString*>;
        QList <QString*>    *item_values        = new QList <QString*>;
        
        // These fields resemble the 'Global Configuration' fields
        // Each table line is stored at the same index of all the other elements
        QList <bool>        *is_global_genable  = new QList <bool>;
        QList <bool>        *is_global_phyiscal = new QList <bool>;
        QList <QString*>    *global_names       = new QList <QString*>;
        QList <QString*>    *global_data_types  = new QList <QString*>;
        QList <QString*>    *global_type_widths = new QList <QString*>;
        QList <QString*>    *global_list_depth  = new QList <QString*>;
        QList <QString*>    *global_values      = new QList <QString*>;
        
        // These fields resemble the 'Global Configuration' fields
        // Each table line is stored at the same index of all the other elements
        QList <bool>        *is_agent_genable   = new QList <bool>;
        QList <bool>        *is_agent_phyiscal  = new QList <bool>;
        QList <QString*>    *agent_names        = new QList <QString*>;
        QList <QString*>    *agent_data_types   = new QList <QString*>;
        QList <QString*>    *agent_type_widths  = new QList <QString*>;
        QList <QString*>    *agent_list_depth   = new QList <QString*>;
        QList <QString*>    *agent_values       = new QList <QString*>;
        
        QList <QString>     *scenario_names     = new QList<QString>();
        QList <spec_interface_wave_table*>  scenario_tables;
};

#endif // SPEC_INTERFACE_MODEL_H
