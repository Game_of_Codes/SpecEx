// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_CODE_MODEL_H
#define SPEC_CODE_MODEL_H

#include <QObject>
#include <QPushButton>

#include <BaseClasses/spec_logger.h>
#include <CodeModel/spec_type_node.h>
#include <CodeModel/spec_struct_data_node.h>

class spec_code_model : public QObject {
    Q_OBJECT
    
    public:
        explicit spec_code_model(QObject *parent = nullptr);
        
        bool                            has_struct_declaration(
            QString         name
        );
        
        bool                            has_like_struct_declaration(
            QString         name
        );
        
        QList <spec_struct_data_node*>  get_all_tree_nodes();
        
        spec_struct_data_node*          get_hierarchy_tree(
            QString top_unit = "sys"
        );
        
        void                            visit_all_tree_nodes();
        void                            visit_node( spec_struct_data_node* node );
        
        void                            set_top_file(
            QString file_name
        );
        
        QString                         get_top_file();
        
        void                            set_active_agent_count( int count );
        void                            set_passive_agent_count( int count );
        
        
        int                             get_active_agent_count          ();
        int                             get_passive_agent_count         ();
        
        spec_struct_data_node           *get_env_node                   ();
        QList<spec_struct_data_node*>   get_passive_agent_nodes         ();
        spec_struct_data_node           *get_env_configuration_node     ();
        spec_struct_data_node           *get_agent_configuration_node   (QString exclude_name);
        spec_struct_data_node           *get_monitor_node               ();
        spec_struct_data_node           *get_signal_map_node            ();
        
        QList<spec_struct_data_node*>   get_active_agent_nodes          ();
        spec_struct_data_node           *get_bfm_node                   ();
        spec_struct_data_node           *get_driver_node                ();
        
        void                            print_struct_list               ();
        void                            print_full_hierarchy            ();
        QList<QString>                  get_incomplete_config_elements  ();
        
    signals:
        
    public slots:
        void    set_prefix(
            QString prefix
        );
        
        QString get_prefix();
        
        void    add_enum_type(
            QString         name,
            QList<QString*> values
        );
        
        void    add_struct(
            QString         parent,
            QList<QString*> struct_properties
        );
        
        void add_struct_method(
            QString             parent,
            QString             method_name,
            QList<QString *>    method_properties
        );
        
        void add_struct_field(
            QString             parent,
            QString             field_name,
            QList <QString*>    field_properties
        );
        
        void set_struct_name(
            QString             name
        );
        
        void set_hierarchy(
            spec_struct_data_node *node
        );
        
        spec_struct_data_node get_struct(
            QString struct_name
        );
        
        spec_struct_data_node *get_struct_pointer(
            QString struct_name
        );
        
        spec_struct_data_node get_struct_like(
            QString struct_name
        );
        
        QList<spec_field_node*> get_struct_fields(
            QString struct_name
        );
        
        void                    build_new_hierarchy(
            QString             top_unit
        );
        
        void                    build_traverse_hierarchy(
            QString                 top_unit,
            spec_struct_data_node   *node
        );
        
        QString                 get_single_hierarchy_path(
            spec_struct_data_node   *node,
            QString                 node_query_operation,
            QString                 node_query_search
        );
        
        QList<QString*>         get_all_like_structs_names(
            QString             like_type
        );
        
        bool                    is_enum_type( QString name );
        QList<QString>          get_enum_type_field_values( QString name, QString fld_name );
        
    private:
        QString                         code_prefix;
        QString                         *struct_name    = new QString();
        QList <spec_type_node*>         *type_list      = new QList<spec_type_node*>;
        QList <spec_struct_data_node*>  *struct_list    = new QList <spec_struct_data_node*>;
        
        spec_struct_data_node           *hierarchy_tree;
        
        QString                         top_file_string;
        QString                         driver_path_string;
        
        int                             cur_build_stack = 0;
        
        // This tracks how many ACTIVE and PASSIVE agents have been configured
        int                             active_agent_count  = -1;
        int                             passive_agent_count = -1;
};

#endif // SPEC_CODE_MODEL_H
