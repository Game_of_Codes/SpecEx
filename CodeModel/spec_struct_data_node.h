// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_STRUCTDATANODE_H
#define SPEC_STRUCTDATANODE_H

#include <QDebug>
#include <QObject>
#include <QString>
#include <QStringList>
#include <QTreeWidgetItem>

#include <BaseClasses/spec_logger.h>

// Data structures
#include <CodeModel/spec_field_node.h>
#include <CodeModel/spec_methodnode.h>
#include <CodeModel/spec_constraintnode.h>
#include <CodeModel/spec_temporalnode.h>
#include <CodeModel/spec_coveragegroupnode.h>

// GUI widgets to hold/show the configuration values
#include <Widgets/spec_env_widget.h>
#include <Widgets/spec_agent_widget.h>
#include <Widgets/spec_config_params_widget.h>
#include <Widgets/spec_event_port_widget.h>
#include <Widgets/spec_simple_port_widget.h>
#include <Widgets/spec_tlm_port_widget.h>


class spec_struct_data_node : public QTreeWidgetItem {
    
    public:
        explicit spec_struct_data_node(QTreeWidgetItem *parent = nullptr, int type = Type);
        spec_struct_data_node( spec_struct_data_node *current_pointer );
        
//        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        // Methods for handling new data structure
        QList <spec_struct_data_node*>  get_all_tree_nodes();
        
//        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        spec_struct_data_node           *get_child( int i );
        
        void                            node_visitor(
            spec_struct_data_node   *node,
            int                     stack_cnt = 0
        );
        
        void                            set_widget_nullptr();
        
        void                            set_instance_field(
            spec_field_node         *node
        );
        
        spec_field_node*                 get_instance_field();
        QWidget                         *get_node_widget_copy( spec_struct_data_node *current_pointer );
        
    public slots:
        void                            set_struct_properties(
            QList <QString*>        *properties
        );
        
        QList<QString*>                 get_struct_properties();
        
        void                            add_struct_field(
            QString                 field_name,
            QList <QString*>        properties
        );
        
        void                            add_struct_node(
            spec_struct_data_node   *node
        );
        
        void                            add_constraint_node(
            QString                 constraint_name,
            QList <QString*>        properties
        );
        
        QString                         get_struct_name     ();
        QString                         get_package_name    ();
        QString                         get_like_name       ();
        QString                         get_when_name       ();
        
        QList<spec_field_node*>         get_fields          ();
        void                            set_fields          (
            QList<spec_field_node*> fields
        );
        
        QString                         get_object_name();
        void                            set_object_name(
            QString name
        );
        
        void                            set_field_constraint(
            QString field_name,
            QString field_enclosing_struct_type,
            QString field_instance_name
        );
        
        QString                         get_field_constraint_enclosing_struct(
            QString field_name
        );
        QString                         get_field_constraint_field_instance_name(
            QString field_name
        );
        
        void                            set_constraints(
            QList <spec_constraint_node*>   constraints
        );
        QList <spec_constraint_node*>   get_constraints_list();
        
        void                            apply_constraints();
        
        void                            set_constraint_recursive(
            spec_constraint_node    *c_node
        );
        
        void                            set_field(
            spec_field_node *node
        );
        
        spec_field_node                 *get_field(
            QString field_name
        );
        
        bool                            is_unit();
        void                            set_is_list_object(
            bool    is
        );
        bool                            get_is_list_object  ();
        
        void                            set_hdl_path        ( QString attribute );
        QString                         get_hdl_path        ();
        
        void                            set_clock_domain_list( QList<QString> );
        QList<QString>                  get_clock_domain_list();
        void                            set_reset_domain_list( QList<QString> );
        QList<QString>                  get_reset_domain_list();
        
        QString                         get_clock_domain_conf();
        QString                         get_reset_domain_conf();
        
        void                            set_enum_type_selector(
            QString         field_name,
            QList<QString>  enum_type_values
        );
        
        spec_struct_data_node           *get_env_node                   ();
        QList<spec_struct_data_node*>   get_active_agent_nodes          ();
        QList<spec_struct_data_node*>   get_passive_agent_nodes         ();
        spec_struct_data_node           *get_env_configuration_node     ();
        spec_struct_data_node           *get_agent_configuration_node   (QString exclude_name);
        spec_struct_data_node           *get_monitor_node               ();
        spec_struct_data_node           *get_signal_map_node            ();
        
        spec_struct_data_node           *get_bfm_node                   ();
        spec_struct_data_node           *get_driver_node                ();
        
        
        int                             get_active_agents_count ();
        int                             get_passive_agents_count();
        
        // Returns true, if a driver instance was found at the leaves
        bool                            has_object_inst_of_driver();
        // Returns the string in the form "env.agent[].drv" if driver exists, "" (empty string) if not
        QString                         get_inst_path_to_driver();
        
        // GUI Widget Accessibility Methods
        void                            init_node_widget                ();
        void                            show_node_widget                (QWidget *parent);
        void                            update_widget                   ();
        spec_env_widget                 *get_env_widget                 ();
        spec_agent_widget               *get_agent_widget               ();
        spec_config_params_widget       *get_config_params_widget       ();
        spec_event_port_widget          *get_event_port_widget          ();
        spec_simple_port_widget         *get_simple_port_widget         ();
        spec_tlm_port_widget            *get_tlm_port_widget            ();
        
        void                            print_tree_recursive            (
            int level_index
        );
        
        QList<QString>                  get_incomplete_config_elements();
        
    signals:
        
    private slots:
        
        
    private:
        
        // Contains the name of the struct-type
        QString                         struct_name            ;
        // Contains the associated package name. By default this value is "main", unless specified
        QString                         package_name;
        // Contains the name of the like-base class.
        QString                         like_name              ;
        // Contains the names of all nested sub-type extensions
        QString                         when_name              ;
        // Reflects all the sub-typed layers that are also described as struct data nodes
        QList <spec_struct_data_node*>  sub_type_struct_list   ;
        // Indicates if struct is a unit, when set to true
        bool                            is_declared_unit       ;
        
        spec_field_node                 *me_node                = nullptr;
        
        // The QList objects need to be hard copies, otherwise "append()" won't work
        QList <spec_field_node*>        field_nodes;
        
        QString                         object_name         = "";
        bool                            is_list;
        QString                         hdl_path_attribute  = "";
        
        QList <QString>                 clock_domains;
        QString                         selected_clock_domain;
        QList <QString>                 reset_domains;
        QString                         selected_reset_domain;
        
        
        // Contains all detected constraints of a given node
        QList <spec_constraint_node*>   constraint_nodes;
        spec_constraint_node            *cur_constraint_node    = nullptr;
        
//        QList <spec_MethodNode>         method_nodes;
//        QList <spec_TemporalNode>       temporal_nodes;
//        QList <spec_CoverageGroupNode>  coverage_nodes;
        
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // GUI Elements
        //==============
        // These GUI elements are built-in to the tree node and are used to simply visualize the data structure without
        // having to create any kind of overhead
        QWidget                         *node_widget            = nullptr;
};

#endif // SPEC_HIERARCHYDATANODE_H
