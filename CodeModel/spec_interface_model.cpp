// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_interface_model.h"

spec_interface_model::spec_interface_model(QObject *parent) : spec_base_code_model() {
    
};   // end of spec_interface_model constructor

//QString spec_interface_model::get_directory() {
//    return uvc_directory;
//};   // end of get_directory

//QString spec_interface_model::get_group_name() {
//    return uvc_group_name;
//};   // end of get_group_name

//QString spec_interface_model::get_interface_name() {
//    return uvc_interface_name;
//};   // end of get_interface_name

//// Returns the UVC prefix in this format:
////   "<group_name>_<interface_name>"
//QString spec_interface_model::get_uvc_prefix() {
//    return QString( uvc_group_name + "_" + uvc_interface_name );
//};   // end of get_uvc_prefix

int spec_interface_model::type_count()     { return type_names     -> count(); };
int spec_interface_model::signal_count()   { return signal_names   -> count(); };
int spec_interface_model::item_count()     { return item_names     -> count(); };
int spec_interface_model::global_count()   { return global_names   -> count(); };
int spec_interface_model::agent_count()    { return agent_names    -> count(); };

// Returns the CLOCK properties in this order
//  clock_tb_name, clock_hdl_name, clock_trigger_edge
QList <QString*> spec_interface_model::get_clock_properties() {
    return QList <QString*> ( { clock_tb_name, clock_hdl_name, clock_trigger_edge } );
};   // end of get_clock_properties

// Returns the RESET properties in this order
//  reset_tb_name, reset_hdl_name, reset_trigger_edge
QList <QString*> spec_interface_model::get_reset_properties() {
    return QList <QString*> ( { reset_tb_name, reset_hdl_name, reset_active_level } );
};   // end of get_reset_properties

// Returns the enumerated type values in this order
//   0: name
//   1: values
QList <QString*> *spec_interface_model::get_type_line(
        int        index
) {
    QList <QString*> *result = new QList <QString*>();
    result -> append( type_names            -> at( index ) );
    result -> append( type_data_values      -> at( index ) );
    
    return result;
};   // end of get_signal_line


// Returns the Signal properties in this order
//   0: name
//   1: data type
//   2: type width
//   3: access
//   4: default value
QList <QString*> *spec_interface_model::get_signal_line(
        int        index
) {
    QList <QString*> *result = new QList <QString*>();
    result -> append( signal_names          -> at( index ) );
    result -> append( signal_data_types     -> at( index ) );
    result -> append( signal_type_widths    -> at( index ) );
    result -> append( signal_accesses       -> at( index ) );
    result -> append( signal_value          -> at( index ) );
    
    return result;
};   // end of get_signal_line

QList <QString*> *spec_interface_model::get_item_line(
        int        index
) {
    QList <QString*> *result = new QList <QString*>();
    result -> append( new QString( (is_item_genable    -> at( index ) == true)? "true" : "false") );
    result -> append( new QString( (is_item_phyiscal  -> at( index ) == true)? "true" : "false") );
    result -> append( item_names        -> at( index ) );
    result -> append( item_data_types   -> at( index ) );
    result -> append( item_type_widths  -> at( index ) );
    result -> append( item_list_depth   -> at( index ) );
    result -> append( item_values       -> at( index ) );
    
//    qDebug() << "Show Item line " << *(result->at(0));
    return result;
};   // end of get_item_line

QList <QString*> *spec_interface_model::get_global_line(
        int        index
) {
    QList <QString*> *result = new QList <QString*>();
    result -> append( new QString( (is_global_genable    -> at( index ) == true)? "true" : "false") );
    result -> append( new QString( (is_global_phyiscal  -> at( index ) == true)? "true" : "false") );
    result -> append( global_names          -> at( index ) );
    result -> append( global_data_types     -> at( index ) );
    result -> append( global_type_widths    -> at( index ) );
    result -> append( global_list_depth     -> at( index ) );
    result -> append( global_values         -> at( index ) );
    
    return result;
};   // end of get_global_line

QList <QString*> *spec_interface_model::get_agent_line(
        int        index
) {
    QList <QString*> *result = new QList <QString*>();
    result -> append( new QString( (is_agent_genable    -> at( index ) == true)? "true" : "false") );
    result -> append( new QString( (is_agent_phyiscal  -> at( index ) == true)? "true" : "false") );
    result -> append( agent_names          -> at( index ) );
    result -> append( agent_data_types     -> at( index ) );
    result -> append( agent_type_widths    -> at( index ) );
    result -> append( agent_list_depth     -> at( index ) );
    result -> append( agent_values         -> at( index ) );
    
    return result;
};   // end of get_agent_line

// The QList <QString*> expects the order of CLOCK property items to be defined:
//   clock_tb_name, clock_hdl_name, clock_trigger_edge
void spec_interface_model::set_clock_properties(
        QList <QString*>    property_list
) {
    // Each element is allowed to appear in a dedicated location only
    clock_tb_name       = property_list.at(0);
    clock_hdl_name      = property_list.at(1);
    clock_trigger_edge  = property_list.at(2);
};   // end of set_clock_properties

// The QList <QString*> expects the order of RESET property items to be defined:
//   clock_tb_name, clock_hdl_name, clock_trigger_edge
void spec_interface_model::set_reset_properties(
        QList <QString*>    property_list
) {
    reset_tb_name       = property_list.at(0);
    reset_hdl_name      = property_list.at(1);
    reset_active_level  = property_list.at(2);    
};   // end of set_reset_properties

void spec_interface_model::add_type_properties(
        QList <QString*>    *property_list
) {
    type_names          -> append( property_list->at(0) );
    type_data_values    -> append( property_list->at(1) );
};   // end of add_type_properties

void spec_interface_model::add_signal_properties(
        QList <QString*>    *property_list
) {
    signal_names        -> append( property_list->at(0) );
    signal_data_types   -> append( property_list->at(1) );
    signal_type_widths  -> append( property_list->at(2) );
    signal_accesses     -> append( property_list->at(3) );
    signal_value        -> append( property_list->at(4) );
};   // end of add_signal_properties

void spec_interface_model::add_item_properties(
        QList <QString*>    *property_list
) {
    is_item_genable     -> append( (QString::compare( *property_list->at(0), "true") == 0) );
    is_item_phyiscal    -> append( (QString::compare( *property_list->at(1), "true") == 0) );
    item_names          -> append( property_list->at(2) );
    item_data_types     -> append( property_list->at(3) );
    item_type_widths    -> append( property_list->at(4) );
    item_list_depth     -> append( property_list->at(5) );
    item_values         -> append( property_list->at(6) );
};   // end of add_item_properties

void spec_interface_model::add_global_properties(
        QList <QString*>    *property_list
) {
    is_global_genable   -> append( (QString::compare( *property_list->at(0), "true") == 0) );
    is_global_phyiscal  -> append( (QString::compare( *property_list->at(1), "true") == 0) );
    global_names        -> append( property_list->at(2) );
    global_data_types   -> append( property_list->at(3) );
    global_type_widths  -> append( property_list->at(4) );
    global_list_depth   -> append( property_list->at(5) );
    global_values       -> append( property_list->at(6) );
};   // end of add_global_properties

void spec_interface_model::add_agent_properties(
        QList <QString*>    *property_list
) {
    is_agent_genable   -> append( (QString::compare( *property_list->at(0), "true") == 0) );
    is_agent_phyiscal  -> append( (QString::compare( *property_list->at(1), "true") == 0) );
    agent_names        -> append( property_list->at(2) );
    agent_data_types   -> append( property_list->at(3) );
    agent_type_widths  -> append( property_list->at(4) );
    agent_list_depth   -> append( property_list->at(5) );
    agent_values       -> append( property_list->at(6) );
};   // end of add_agent_properties

void spec_interface_model::add_scenario_table(spec_interface_wave_table *scenario_table) {
    scenario_names->append( scenario_table->get_scenario_name() );
    scenario_tables.append( scenario_table );
}

int spec_interface_model::get_scenario_count() {
    return scenario_tables.count();
}

spec_interface_wave_table *spec_interface_model::get_scenario_table(int index) {
    return scenario_tables.at( index );
}
