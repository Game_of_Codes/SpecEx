// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_clock_model.h"

spec_clock_model::spec_clock_model(QObject *parent) : spec_base_code_model() {
    
}   // end of spec_clock_model

void spec_clock_model::add_clock_domain(
    QList<QString*>     prop
) {
    properties.append( prop );
}   // end of add_clock_domain

int spec_clock_model::get_clock_domain_count() {
    return properties.count();
}   // end of get_clock_domain_count

QString spec_clock_model::get_clock_domain_name     ( int index ) {
    return *properties.at( index ).at( 0 );
}   // end of get_clock_domain_name

QString spec_clock_model::get_clock_domain_hdl_name ( int index ) {
    return *properties.at( index ).at( 1 );
}   // end of get_clock_domain_edge

QString spec_clock_model::get_clock_domain_jitter   ( int index ) {
    return *properties.at( index ).at( 2 );
}   // end of get_clock_domain_jitter

QString spec_clock_model::get_clock_domain_period   ( int index ) {
    return *properties.at( index ).at( 3 );
}   // end of get_clock_domain_period

QString spec_clock_model::get_clock_domain_shift    ( int index ) {
    return *properties.at( index ).at( 4 );
}   // end of get_clock_domain_shift

QString spec_clock_model::get_clock_domain_init     ( int index ) {
    return *properties.at( index ).at( 5 );
}   // end of get_clock_domain_init

void spec_clock_model::set_time_resolution          ( QString resolution ) {
    time_resolution = resolution;
}
QString spec_clock_model::get_time_resolution         () {
    return time_resolution;
}
