// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_COVERAGEITEM_H
#define SPEC_COVERAGEITEM_H

#include <QObject>

#include <CodeModel/spec_coverageitemoptionnode.h>

class spec_CoverageItemNode : public QObject {
    Q_OBJECT
    
    public:
        explicit spec_CoverageItemNode(QObject *parent = nullptr);
        
    signals:
        
    public slots:
        
    private:
        QString                                 coverageItemType;   // this may assume "item", "cross" and "transition"
        QString                                 coverageItemName;
        QString                                 coverageItemPointer;
        QList <spec_CoverageItemOptionNode>   *coverageItemOptionNode   = new QList<spec_CoverageItemOptionNode>();
};

#endif // SPEC_COVERAGEITEM_H
