// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_COVERAGEGROUP_H
#define SPEC_COVERAGEGROUP_H

#include <QObject>

#include <CodeModel/spec_coverageitemnode.h>

class spec_CoverageGroupNode : public QObject {
    Q_OBJECT
    
    public:
        explicit spec_CoverageGroupNode(QObject *parent = nullptr);
        
    signals:
        
    public slots:
        
    private:
        QString                         coverGroupName;
        QList <spec_CoverageItemNode> *coverItemNodes = new QList <spec_CoverageItemNode>();
        
};

#endif // SPEC_COVERAGEGROUP_H
