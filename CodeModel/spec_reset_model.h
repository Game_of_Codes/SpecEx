// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_RESET_MODEL_H
#define SPEC_RESET_MODEL_H

#include <CodeModel/spec_base_code_model.h>
#include <QObject>

class spec_reset_model : public spec_base_code_model {
    Q_OBJECT
    
    public:
        explicit spec_reset_model(QObject *parent = nullptr);
        
    void    add_reset_domain(
        QList<QString*>     prop
    );
    
    int     get_reset_domain_count          ();
    QString get_reset_domain_name           ( int index );
    QString get_reset_domain_hdl_name       ( int index );
    QString get_reset_domain_active_level   ( int index );
    QString get_reset_domain_sync_clock     ( int index );
    QString get_reset_domain_init_value     ( int index );
    
    signals:
        
    public slots:
    
    private:
        QList<QList<QString*>>  properties;
};

#endif // SPEC_RESET_MODEL_H
