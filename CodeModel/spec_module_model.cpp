// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_module_model.h"

spec_module_model::spec_module_model(QObject *parent) : spec_base_code_model() {
    // Dereference the clock and reset model at each generation
    clk_model = nullptr;
    res_model = nullptr;
};   // end of spec_module_model constructor

void spec_module_model::set_all_specman_paths(
    QList<QString*> path
) {
    specman_path_names = path;
}

QList<QString*> spec_module_model::get_specman_paths() {
    return specman_path_names;
}

QString*    spec_module_model::get_specman_path_at(
    int index
) {
    return specman_path_names.at( index );
}

void spec_module_model::add_specman_path(
    QString         *path
) {
    specman_path_names.append( path );
}

void spec_module_model::remove_specman_path(
    int    index
) {
    specman_path_names.removeAt( index );
}

void spec_module_model::set_clock_model(
    spec_clock_model    *model
) {
    clk_model = model;
}   // end of set_clock_model

void spec_module_model::set_reset_model(
    spec_reset_model    *model
) {
    res_model = model;
}   // end of set_reset_model

spec_clock_model *spec_module_model::get_clock_model() {
    return clk_model;
}
spec_reset_model *spec_module_model::get_reset_model() {
    return res_model;
}

bool spec_module_model::get_enable_reset_uvc() const {
    return enable_reset_uvc;
}

void spec_module_model::set_enable_reset_uvc(bool value) {
    enable_reset_uvc = value;
}

bool spec_module_model::get_enable_reference_model() const {
    return enable_reference_model;
}

void spec_module_model::set_enable_reference_model(bool value) {
    enable_reference_model = value;
}

bool spec_module_model::get_enable_scoreboard() const {
    return enable_scoreboard;
}

void spec_module_model::set_enable_scoreboard(bool value) {
    enable_scoreboard = value;
}

bool spec_module_model::get_enable_uvm_scoreboard() const {
    return enable_uvm_scoreboard;
}

void spec_module_model::set_enable_uvm_scoreboard(bool value) {
    enable_uvm_scoreboard = value;
}

bool spec_module_model::get_enable_register_model() const {
    return enable_register_model;
}

void spec_module_model::set_enable_register_model(bool value) {
    enable_register_model = value;
}

unsigned int spec_module_model::get_register_max_addr_width() const {
    return register_max_addr_width;
}

void spec_module_model::set_register_max_addr_width( unsigned int value ) {
    register_max_addr_width = value;
}

unsigned int spec_module_model::get_register_max_data_width() const {
    return register_max_data_width;
}

void spec_module_model::set_register_max_data_width( unsigned int value ) {
    register_max_data_width = value;
}

bool spec_module_model::get_enable_clock_uvc() const {
    return enable_clock_uvc;
}

void spec_module_model::set_enable_clock_uvc(bool value) {
    enable_clock_uvc = value;
}

QString spec_module_model::get_hdl_tb_top() {
    return hdl_tb_top;
}

void spec_module_model::set_hdl_tb_top( QString new_tb_top ) {
    hdl_tb_top = new_tb_top;
}

void spec_module_model::add_code_model(
    spec_code_model *model
) {
    code_model.append( model );
}   // end of add_code_model

QList<spec_code_model*> spec_module_model::get_code_model_list() {
    return code_model;
}   // end of get_code_model_list

void spec_module_model:: update_all_uvc_code_models  (
    QList<spec_code_model*> code_models
) {
    spec_msg("");
    for( int index = code_model.count()-1 ; index > 0; --index ) {
        code_model.at(index)->print_full_hierarchy();
    };
    for( spec_code_model *model : code_models ) {
        code_model.append( model );
    };
}

void spec_module_model::update_code_model(
    int             index,
    spec_code_model *model
) {
    spec_msg( "Replacing " << code_model.at(index) << " with " << model );
    code_model.replace( index, model );
}
