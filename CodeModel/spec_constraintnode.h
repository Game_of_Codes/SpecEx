// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_CONSTRAINT_NODE_H
#define SPEC_CONSTRAINT_NODE_H

#include <QDebug>
#include <QList>
#include <QObject>
#include <QRegularExpression>
#include <QString>

#include <BaseClasses/spec_logger.h>

class spec_constraint_node : public QObject {
    Q_OBJECT
    
    public:
        explicit spec_constraint_node(QObject *parent = nullptr);
        spec_constraint_node( spec_constraint_node *node );
        
        QList<QString>          get_expression_list_0();
        QList<QString>          get_expression_list_1();
        
    signals:
        
    public slots:
        void add_expression(
            QString             name,
            QList<QString*>     properties
        );
        
        void add_operator(
            QString             name,
            QList <QString*>    properties
        );
        
        QString                 get_constraint_name     ();
        QList<QString>          get_expr                 ( int index );
        QList<QString>          get_expr_type_path      ( int index );
        bool                    get_is_expr_soft         ( int index );
        QString                 get_expr_value           ( int index );
        QString                 get_expr_type            ( int index );
        QString                 get_expr_attr            ( int index );
        QString                 get_operation           ();
        
        void                    append_expr_type_path   (
                int     exp_index,
                QString type
        );
        
        
        QString                 show_constraint_info();
        
    private:
        QString                 constraint_name         = "";
        
        // Contains the name of the spec_struct_data_node in which the constraint is implemented
        QString                 enclosing_struct_name   = "";
        // Indicates if a constraint is hard or soft
        bool                    is_soft                 = false;
        
        // Containts the constraint operation. We only support handling the assignment '==' operator
        QString                 op                      = "";
        
        QList<QList<QString>>   exprs;
        QList<QList<QString>>   exprs_type_path = QList<QList<QString>>{QList<QString>({""}),QList<QString>({""})};
        QList<bool>             is_expr_soft;
        QList<QString>          exprs_type;
        QList<QString>          exprs_value;
        QList<QString>          exprs_attrs;
        
};

#endif // SPEC_CONSTRAINT_NODE_H
