// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_type_node.h"

spec_type_node::spec_type_node(QObject *parent) : QObject(parent){
    
};

void spec_type_node::set_name(
    QString             name
) {
    enum_type_name = name;
};

void spec_type_node::add_value(
    QString             value
) {
    type_value_list.append( value );
};
void spec_type_node::set_values(
    QList <QString>    values
) {
    type_value_list = values;
};

QString spec_type_node::get_name() {
    return enum_type_name;
}

QList<QString> spec_type_node::get_values() {
    return type_value_list;
}

QString spec_type_node::get_value_at( int index ) {
    
}

bool spec_type_node::has_value( QString value ) {
    for(QString v : type_value_list) {
        if( v == value ) {
            return true;
        };
    };  // end of iterating over each of the values
    
    // If we reached this line of code, then there was no matching value found
    return false;
}
