#include "spec_process_viewer.h"

spec_process_viewer::spec_process_viewer() {
    // Instantiate all the GUI elements
//    process_text_browser        = new QTextBrowser();
//    process_text_browser->setGeometry( 10, 10, 800, 650 );
//    process_text_browser->setParent( this );
    process_text_edit           = new QTextEdit();
    process_text_edit->setGeometry( 10, 10, 800, 650 );
    process_text_edit->setFont( QFont("Courier New") );
    process_text_edit->setParent( this );
    
    abort_push_button           = new QPushButton( "Stop" );
    abort_push_button->setGeometry( 10, 700, 100, 50 );
    abort_push_button->setParent( this );
    
    rerun_process_push_button   = new QPushButton( "Rerun" );
    rerun_process_push_button->setGeometry( 150, 700, 100, 50 );
    rerun_process_push_button->setParent( this );
    
    close_push_button           = new QPushButton( "Close" );
    close_push_button->setGeometry( 710, 700, 100, 50 );
    close_push_button->setParent( this );
    
    connect(
        close_push_button   , &QPushButton::pressed,
        this                , &spec_process_viewer::hide
    );
}

void spec_process_viewer::set_process( QProcess *process ) {
    monitored_process = process;
    monitored_process->setProcessChannelMode(QProcess::MergedChannels);
    
    connect(
        monitored_process           , &QProcess::readyReadStandardOutput,
        this                        , &spec_process_viewer::append_standard_output
    );
    connect(
        monitored_process           , &QProcess::readyReadStandardError,
        this                        , &spec_process_viewer::append_error_output
    );
    
    connect(
        abort_push_button           , &QPushButton::pressed,
        monitored_process           , &QProcess::kill
    );
    
    connect(
        rerun_process_push_button   , &QPushButton::pressed,
        this                        , &spec_process_viewer::start_command_queue
    );
    
    connect(
        monitored_process           , &QProcess::started,
        this                        , &spec_process_viewer::process_started
    );
    
    connect(
        monitored_process           , SIGNAL( finished(int, QProcess::ExitStatus) ),
        this                        , SLOT( process_finished(int, QProcess::ExitStatus) )
    );
    
}

void spec_process_viewer::initialize() {
    qDebug() << "initialize";
//    process_text_browser->append(  );    
    
}

void spec_process_viewer::reset_command_queue() {
    command_queue.clear();
    argument_queue.clear();
    work_dir_queue.clear();
    
    queue_counter           = 0;
    queued_command_index    = 0;
    
    process_text_edit->clear();
}

void spec_process_viewer::queue_command( QString command, QString work_dir ) {
    command_queue   . append( command );
    argument_queue  . append( "" );
    work_dir_queue  . append( work_dir );
    
    queue_counter++;
}

void spec_process_viewer::queue_shell_command( QString command, QString work_dir ) {
    command_queue   . append( "" );
    argument_queue  . append( command );
    work_dir_queue  . append( work_dir );
    
    queue_counter++;
}

void spec_process_viewer::execute_process( QString command, QString work_dir ) {
//    append_output( "<b>" + work_dir + ">   " + command + "</b>\n" );
    
    monitored_process->setWorkingDirectory( work_dir );
    
    monitored_process->start( command, QIODevice::ReadWrite );
//    append_output( "...\n" );
}

void spec_process_viewer::execute_shell_process( QString command, QString work_dir ) {
    execute_process( "sh -c \"" + command + "\"", work_dir );
}

void spec_process_viewer::append_output( QString msg ) {
//    process_text_browser->append( msg );
    buffer_string += msg;
    if( msg.contains("\n") ) {
        process_text_edit->append( buffer_string );
        buffer_string = "";
    };
}

void spec_process_viewer::append_standard_output() {
    append_output( monitored_process->readAllStandardOutput() );
}

void spec_process_viewer::append_error_output() {
    append_output( monitored_process->readAllStandardError() );
}

void spec_process_viewer::start_command_queue() {
    int q_index = 0;
    // Provide info on which processes are being run
    append_output( "Running list of commands:\n[#]:\tFOLDER   ->   CMD\n" );
    for(QString cmd_str : command_queue) {
        if( cmd_str == "" ) {
            append_output( "[" + QString::number(q_index) + "]: \t" + work_dir_queue.at(q_index) + "   ->   " + argument_queue.at(q_index) + "\n" );
        } else {
            append_output(  "[" + QString::number(q_index) + "]: \t" + work_dir_queue.at(q_index) + "   ->   " + cmd_str + "\n" );
        };
        q_index++;
    }
    
    // We can set the working directory, since this one doesn't need to be differentiated
    monitored_process->setWorkingDirectory( work_dir_queue.at(0) );
    
    // Start a shell command, if the current command is empty
    if( command_queue.at(queued_command_index) == "" ) {
        execute_shell_process( argument_queue.at(0), work_dir_queue.at(0) );
    } else {
        execute_process( command_queue.at(0), work_dir_queue.at(0) );
    };
}

void spec_process_viewer::process_started() {
    QString cmd = command_queue.at( queued_command_index );
    if( cmd == "" ) {
        cmd = argument_queue.at( queued_command_index );
    };
    append_output( "Started <br><b>" + cmd + "</b>\n" );
}

void spec_process_viewer::process_finished( int exitCode, QProcess::ExitStatus exitStatus ) {
//    qDebug() << "process finished: " + QString::number( queued_command_index );
    
    QString cmd = command_queue.at( queued_command_index );
    if( cmd == "" ) {
        cmd = argument_queue.at( queued_command_index );
    };
    
    append_output("Finished ");
    append_output("[" + QString::number(queued_command_index+1) + "/" + QString::number(queue_counter) + "]:  ");
    append_output("<b>" + cmd + "</b>\n\n");
    
    if( queued_command_index+1 == queue_counter ) {
        queued_command_index = 0;
        append_output("Done...\n");
        return;
    } else {
        queued_command_index++;
    };
    
    QString command     = command_queue.at(queued_command_index);
    QString work_dir    = work_dir_queue.at(queued_command_index);
    
//    QString debug_str   = "[" + QString::number(queued_command_index) + "]: Starting  >> " +
//            command + " <<  --OR--  >> " + argument_queue.at(queued_command_index) + " <<";
    
    if( command == "" ) {
        command = argument_queue.at(queued_command_index);
        execute_shell_process( command, work_dir );
    } else {
        execute_process( command, work_dir );
    };
}

