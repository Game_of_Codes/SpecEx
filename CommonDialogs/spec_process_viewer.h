// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_PROCESS_VIEWER_H
#define SPEC_PROCESS_VIEWER_H

#include <QDialog>
#include <QTextEdit>
#include <QObject>
#include <QPushButton>
#include <QProcess>
#include <QWidget>

#include <BaseClasses/spec_logger.h>

class spec_process_viewer : public QDialog {
    Q_OBJECT
    
    public:
        spec_process_viewer();
        
        QTextEdit       *process_text_edit;
        
        QPushButton     *rerun_process_push_button;
        QPushButton     *abort_push_button;
        QPushButton     *close_push_button;
        
        void            set_process( QProcess *process );
        
        void            initialize();
        
        void            reset_command_queue         ();
        
        void            execute_process             ( QString command, QString work_dir );
        void            execute_shell_process       ( QString command, QString work_dir );
        void            queue_command               ( QString command, QString work_dir );
        void            queue_shell_command         ( QString command, QString work_dir );
        
    public slots:
        void            append_output               ( QString msg );
        void            append_standard_output      ();
        void            append_error_output         ();
        
        void            start_command_queue         ();
        
        void            process_started             ();
        void            process_finished            ( int exitCode, QProcess::ExitStatus exitStatus );
        
        
    private:
        int             queue_counter;
        int             queued_command_index;
        
        QProcess        *monitored_process;
        QString         buffer_string;
        
        QStringList     command_queue;
        QStringList     argument_queue;
        QStringList     work_dir_queue;
};

#endif // SPEC_PROCESS_VIEWER_H
