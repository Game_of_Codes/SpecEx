// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_e_code_generator.h"

spec_e_code_generator::spec_e_code_generator(QObject *parent) : QObject(parent){
    
};

// Write the given content of codeList into a file with the given file_name.
void spec_e_code_generator::write_file(QString file_name, QList <QString*> *codeList) {
    QFile top_file( file_name );
    if( !top_file.open(QIODevice::WriteOnly | QIODevice::Text) ) {
        spec_msg( "Couldn't open file for writing " << file_name );
    } else {
        QTextStream top_out(&top_file);
        foreach(QString* str, *codeList) {
            top_out << *str;
        };
    };
};   // end of write_file()

// This method expects a list of strings with exactly 6 elements:
//  0: Field is generatable? -> "true" otherwise "false"
//  1: Field is physical?    -> "true" otherwise "false"
//  2: Item Name
//  3: Item Data Type
//  4: Item Type Width
//  5: Item Default value    -> via soft constraint
QString spec_e_code_generator::get_interface_struct_member(
        QList <QString*> *properties
) {
    // First check if a name AND a data type are given and only then start assembling the result
//    for(QString *it : *properties) {
//        qDebug() << "Property " << *it;
//    };
    bool can_create_struct_member = (
        (QString::compare( *properties->at(2), "") != 0)   // <- item_name not empty
                                        and
        (QString::compare( *properties->at(3), "") != 0)   // <- item_type not empty
    );
    if( can_create_struct_member ) {
        QString result = "  ";
        
        // Check if Do-Not-Generate character must be generated, and create if necessary
        if( QString::compare( *properties->at(0), "false") == 0 ) {
            result += "!";
        };
        
        // Check if physical character must be generated, and create if necessary
        if( QString::compare( *properties->at(1), "false") == 0 ) {
            result += "%";
        };
        
        // Create field name
        result += *properties->at(2) + " : ";
        
        // Convert the list depth indicator to integer and ...
        int list_depth = (*properties->at(5)).toInt();
        if( list_depth > 0 ) {
            // ... add as many 'list of' depths as required to reflect the list dimensionality
            for( int list_index = 0; list_index < list_depth;++list_index ) {
                result += " list of ";
            };
        };
        
        // Create field type
        result += *properties->at(3);
        
        // Check if the type_widths field has no content. If left blank, then do not add bit-width indication
        if( QString::compare( *properties->at(4), "") != 0 ) {
            result += "(bits: " + *properties->at(4) + ")";
        };
        
        // Check if a default value (soft constraint) was given. In case there was one provided, then also
        // create the soft constraint
        if( QString::compare( *properties->at(6), "") != 0 ) {
            result += ";\n";
            result += "  keep soft " +*properties->at(2) + " == " + *properties->at(6);
        };
        // Terminate the line and add a line-break
        result += ";\n";
        
        return result;
    } else {
        // Simply return an empty QString to avoid NULL pointer and field generation
        return QString("");
    };
};   // end of get_interface_struct_member

// This method expects a list of strings with exactly 6 elements:
//  0: Signal name in testbench
//  1: Signal data type
//  2: Signal bit-width
//  3: Access direction for Signal with value:
//      "HDL Read/Write"
//      "HDL Read"
//      "HDL Write"
//  4: Signal HDL Path
QString spec_e_code_generator::get_interface_signal_port( QList <QString*> *properties ) {
    // Only create a signal port, if there was a given name
    bool can_create_signal_port = (QString::compare(*properties->at(0), "") != 0);
    
    if( can_create_signal_port ) {
        QString result = "  ";
        
        // Create the name string
        result += *properties->at(0) + " : ";
        
        // Then calculate the HDL direction access attribute
        if( QString::compare( *properties->at(3), "HDL Read") == 0 ) {
            result += "in ";
        } else if( QString::compare( *properties->at(3), "HDL Write") == 0 ) {
            result += "out ";
        } else {
            result += "inout ";
        };
        
        // Create type 
        result += "simple_port of " + *properties->at(1);
        
        // Check if a bit-width indication must be created
        if( QString::compare(*properties->at(2), "") != 0 ) {
            result += "(bits: " + *properties->at(2)  + ") ";
        };
        // Create instantiation call and end of line termination
        result += " is instance;\n";
        
        // Create the hdl_path and port binding configuration
        result += "  keep soft " + *properties->at(0) + ".hdl_path() == \"" + *properties->at(4) + "\";\n";
        result += "  keep bind(" + *properties->at(0) + ", external);\n\n";
        
        return result;
    } else {
        return QString("");
    };
};   // end of get_interface_signal_port

QList <QString*> spec_e_code_generator::get_file_header(spec_interface_model *if_model) {
    QList <QString*> *result = new QList <QString*>();
    // TODO: This is just a placeholder :-)
    if( false ) {
        result->append( new QString("Create the license info here\n") );
    } else {
        result->append( new QString("") );
    };
    
    return *result;
};
