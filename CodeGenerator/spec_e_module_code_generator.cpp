// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_e_module_code_generator.h"

spec_e_module_code_generator::spec_e_module_code_generator() {
    spec_msg( "Module Code Generator created " << this );
}   // end of spec_e_module_code_generator


void spec_e_module_code_generator::create_module_top_file( spec_module_model *mod_model ) {
    QString uvc_prefix = mod_model->get_prefix();
    QString uvc_group  = mod_model->get_group_name();
    
    spec_msg("Generating file " << (uvc_prefix + "_top.e"));
    
    QList <QString*>    *code = new QList <QString*>;
    
    bool                enable_register_memory_model    = mod_model -> get_enable_register_model();
    bool                enable_clock_uvc                = mod_model -> get_enable_clock_uvc();
    bool                enable_reset_uvc                = mod_model -> get_enable_reset_uvc();
    bool                enable_reference_model          = mod_model -> get_enable_reference_model();
    bool                enable_scoreboard               = mod_model -> get_enable_scoreboard();
    bool                enable_uvm_scoreboard           = mod_model -> get_enable_uvm_scoreboard();
    
    QString             relative_top_name;
    QString             assemble_top_name;
    QStringList         split_top_file;
    
//    code->append( get_file_header( mod_model ) );
    code->append( new QString( "File: " +uvc_prefix + "_top.e\n") );
    code->append( new QString( "<'\n" ) );
    code->append( new QString( "package " + uvc_prefix + ";\n") );
    code->append( new QString( "\n" ));
    code->append( new QString( "import " + uvc_prefix + "/e/" + uvc_prefix + "_defines;\n" ));
    code->append( new QString( "\n" ) );
    code->append( new QString( "import uvm_e/e/uvm_e_top;\n" ) );
    if( enable_scoreboard and enable_uvm_scoreboard ) {
        code->append( new QString( "import uvm_scbd/e/uvm_scbd_top;\n") );
    };
    code->append( new QString( "\n" ) );
    
    if( enable_register_memory_model ) {
        code->append( new QString( "import vr_ad/e/vr_ad_top.e;\n" ) );
    };
    
    if( enable_clock_uvc ) {
        code->append( new QString("import " + uvc_group + "_clock/e/" + uvc_group + "_clock_top;\n"));
    };
    
    if( enable_reset_uvc ) {
        code->append( new QString("import " + uvc_group + "_reset/e/" + uvc_group + "_reset_top;\n"));
    };
    
    code->append( new QString( "\n" ) );
    code->append( new QString( "// Adding each interface and sub-module UVC\n" ) );
    // Now cycle over each UVC that has been imported
    for( spec_code_model *model : mod_model -> get_code_model_list() ) {
        // Iterate over each module's top-level file and import it
        // First we'll get the full top file path, we'll split the path along the path delimiters
        split_top_file      = (model->get_top_file()).split( QRegularExpression("/") );
        // and assemble the relative top name, based on where the top-file location
        relative_top_name =
                split_top_file.at(split_top_file.count()-3) + "/" +
                split_top_file.at(split_top_file.count()-2) + "/" +
                split_top_file.at(split_top_file.count()-1);
        
        code->append( new QString( "import " + relative_top_name + ";\n") );
    };
    code->append( new QString( "\n" ) );
    code->append( new QString( "// Importing the reusable components of the module UVC\n"           ) );
    code->append( new QString( "import " + uvc_prefix + "/e/" + uvc_prefix + "_types;\n"            ) );
    code->append( new QString( "import " + uvc_prefix + "/e/" + uvc_prefix + "_config;\n"           ) );
    
    if( enable_reference_model ) {
        code->append( new QString( "import " + uvc_prefix + "/e/" + uvc_prefix + "_ref_model_top;\n" ) );
        
    };
    if( enable_scoreboard ) {
        code->append( new QString( "import " + uvc_prefix + "/e/" + uvc_prefix + "_scoreboard;\n"   ) );
    };
    
    code->append( new QString( "import " + uvc_prefix + "/e/" + uvc_prefix + "_monitor;\n"          ) );
    code->append( new QString( "import " + uvc_prefix + "/e/" + uvc_prefix + "_virtual_driver;\n"   ) );
    code->append( new QString( "import " + uvc_prefix + "/e/" + uvc_prefix + "_env;\n"              ) );
    code->append( new QString( "import " + uvc_prefix + "/e/" + uvc_prefix + "_objections;\n"       ) );
    code->append( new QString( "import " + uvc_prefix + "/e/" + uvc_prefix + "_sequence_library;\n" ) );
//    code->append( new QString( "import " + uvc_prefix + "/e/" + uvc_prefix + "_coverage;\n"     ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "'>\n" ) );
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        mod_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_top.e",
        code
    );
}   // end of create_module_top_file

void spec_e_module_code_generator::create_module_defines_file( spec_module_model *mod_model ) {
    QString uvc_prefix = mod_model->get_prefix();
    
    spec_msg("Generating file " << (uvc_prefix + "_defines.e"));
    
    QList <QString*>    *code = new QList <QString*>;
    
    bool                enable_register_memory_model    = mod_model -> get_enable_register_model();
    unsigned int        reg_max_address_width           = mod_model -> get_register_max_addr_width();
    unsigned int        reg_max_data_width              = mod_model -> get_register_max_data_width();
    
//    code->append( get_file_header( mod_model ) );
    code->append( new QString( "File: " + uvc_prefix + "_defines.e" + "\n" ) );
    code->append( new QString( "<'\n" ) );
    
    if( enable_register_memory_model ) {
        code->append( new QString( "#define VR_AD_ADDRESS_WIDTH " + QString::number(reg_max_address_width) + ";\n" ) );
        code->append( new QString( "#define VR_AD_DATA_WIDTH    " + QString::number(reg_max_data_width) + ";\n" ) );
    };
    // TODO: Extract all user-defined defines
//    for (int index = 0; index < mod_model->define_count(); ++index) {
//        
//    };
    code->append( new QString( "'>\n" ) );
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        mod_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_defines.e",
        code
    );
}   // end of create_module_defines_file

void spec_e_module_code_generator::create_module_types_file( spec_module_model *mod_model ) {
    QString uvc_prefix = mod_model->get_prefix();
    
    qDebug() << "Generating file " << (uvc_prefix + "_types.e");
    
    QList <QString*>    *code = new QList <QString*>;
    
    bool                enable_register_memory_model= mod_model->get_enable_register_model();
    
//    code->append( get_file_header( mod_model ) );
    code->append( new QString("File: " + uvc_prefix + "_types.e\n") );
    code->append( new QString("<'\n"));
    code->append( new QString("type " + uvc_prefix +"_env_name_t: [DEFAULT];\n"));
    
    if( enable_register_memory_model ) {
        code->append( new QString( "\n" ) );
        code->append( new QString( "// Provide a dedicated sub-type name for the register model\n" ) );
        code->append( new QString( "extend vr_ad_map_kind :[" + uvc_prefix.toUpper() + "];\n" ) );
    };
    // TODO: Implement the module user-type definitions
//    if( mod_model->type_count() != 0 ) {
//        code->append( new QString("// User-defined enumerated types below, if previously defined\n"));
//    };
//    for( int line_index = 0; line_index < mod_model->type_count(); ++line_index) {
//        properties = mod_model->get_type_line(line_index);
        
//        if( not properties->at(0)->isEmpty() ) {
//            code->append( new QString("type " + *mod_model->get_type_line(line_index)->at(0) ) );
//            code->append( new QString(" : [" + *mod_model->get_type_line(line_index)->at(1) + "];\n"));
//        };
//    };   // end of iterating over each element
    
    code->append( new QString("'>\n"));
    
    write_file(
        mod_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_types.e",
        code
    );
}   // end of create_module_types_file

void spec_e_module_code_generator::create_module_macros_file( spec_module_model *mod_model ) {
    QString uvc_prefix = mod_model->get_prefix();
    
    qDebug() << "Generating file " << (uvc_prefix + "_macros.e");
    
    QList <QString*>    *code = new QList <QString*>;
    
    //    code->append( get_file_header( mod_model ) );
    code->append( new QString("File: " + uvc_prefix + "_macros.e\n") );
    code->append( new QString("<'\n"));
    code->append( new QString("// Add any macros you may need in here...") );
    code->append( new QString("\n"));
    code->append( new QString("\n"));
    code->append( new QString("\n"));
    code->append( new QString("'>\n"));
    
    write_file(
        mod_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_macros.e",
        code
    );
}   // end of create_module_macros_file

void spec_e_module_code_generator::create_module_config_file( spec_module_model *mod_model ) {
    QString uvc_prefix = mod_model->get_prefix();
    
    qDebug() << "Generating file " << (uvc_prefix + "_config.e");
    
    QList <QString*>    *code = new QList <QString*>;
    
    //    code->append( get_file_header( mod_model ) );
    code->append( new QString("File: " + uvc_prefix + "_config.e\n") );
    code->append( new QString("<'\n"));
    
    // TODO: Determine what and how the configuration could look like
    // Configuration could be handled as a centralized API to tie together all the interface-level UVCs
    // Configuration could also be handled as each 
    
    code->append( new QString("\n"));
    code->append( new QString("'>\n"));
    
    write_file(
        mod_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_config.e",
        code
    );
}   // end of create_module_config_file

void spec_e_module_code_generator::create_module_reference_model_top_file( spec_module_model *mod_model ) {
    QString uvc_prefix = mod_model->get_prefix();
    
    qDebug() << "Generating file " << (uvc_prefix + "_ref_model_top.e");
    
    QList <QString*>    *code = new QList <QString*>;
    
    //    code->append( get_file_header( mod_model ) );
    code->append( new QString("File: " + uvc_prefix + "_ref_model_top.e\n") );
    code->append( new QString("<'\n"));
    code->append( new QString("// The reference model is usually a more complex representation with multiple components\n"));
    code->append( new QString("// or multiple aspects. For this reason, splitting up the reference model across multiple\n"));
    code->append( new QString("// files is mandated, which is performed below.\n"));
    code->append( new QString("\n"));
    code->append( new QString("import " + uvc_prefix + "/e/ref_model/" + uvc_prefix + "_ref_model_shell.e;\n"));
    code->append( new QString("\n"));
    code->append( new QString("'>\n"));
    
    write_file(
        mod_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_ref_model_top.e",
        code
    );
}   // end of create_module_reference_model_file

void spec_e_module_code_generator::create_module_reference_model_shell_file( spec_module_model *mod_model ) {
    QString uvc_directory   = mod_model->get_directory();
    QString uvc_prefix      = mod_model->get_prefix();
    
    QDir uvc_dir( uvc_directory );
    
    qDebug() << "Generating file " << (uvc_prefix + "_ref_model_shell.e");
    
    QList <QString*>    *code = new QList <QString*>;
    
    //    code->append( get_file_header( mod_model ) );
    code->append( new QString("File: " + uvc_prefix + "_ref_model_shell.e\n") );
    code->append( new QString("<'\n"));
    code->append( new QString("unit " + uvc_prefix + "_ref_model_shell_u {\n") );
    // TODO: Add TLM analysis ports from each instantiated monitor
    code->append( new QString("  // TODO: Add TLM analysis ports from each instantiated monitor\n"));
    code->append( new QString("\n"));
    code->append( new QString("\n"));
    code->append( new QString("};   // end of " + uvc_prefix + "_ref_model_shell_u\n"));
    code->append( new QString("'>\n"));
    
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/e/ref_model" );
    
    write_file(
        uvc_directory + "/" + uvc_prefix + "/e/ref_model/" + uvc_prefix + "_ref_model_shell.e",
        code
    );
}   // end of create_module_reference_model_file

void spec_e_module_code_generator::create_module_scoreboard_file(spec_module_model *mod_model) {
    QString             uvc_prefix                      = mod_model->get_prefix();
    
    bool                enable_register_memory_model    = mod_model -> get_enable_register_model();
    bool                enable_reference_model          = mod_model -> get_enable_reference_model();
    bool                enable_uvm_scoreboard           = 
            (mod_model -> get_enable_scoreboard()) and (mod_model -> get_enable_uvm_scoreboard());
    
    qDebug() << "Generating file " << (uvc_prefix + "_scoreboard.e");
    
    QList <QString*>    *code = new QList <QString*>;
    
    //    code->append( get_file_header( mod_model ) );
    code->append( new QString("File: " + uvc_prefix + "_scoreboard.e\n") );
    code->append( new QString("<'\n"));
    
    if( enable_uvm_scoreboard ) {
        code->append( new QString( "unit " + uvc_prefix + "_scoreboard_u like uvm_scoreboard {\n" ));
    } else {
        code->append( new QString( "unit " + uvc_prefix + "_scoreboard_u {\n" ));
    };   // end of instantiating custom scoreboard
    
    if( enable_reference_model ) {
        code->append( new QString("  // Instantiate the reference model shell that contains an API for calculating\n"));
        code->append( new QString("  // complex transformations.\n"));
        code->append( new QString("  p_ref_model: " + uvc_prefix + "_ref_model_shell_u is instance;\n"));
    };
    code->append( new QString("};   // end of " + uvc_prefix + "_scoreboard_u\n"));
    code->append( new QString("'>\n"));
    
    write_file(
        mod_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_scoreboard.e",
        code
    );
}   // end of create_module_scoreboard_file

void spec_e_module_code_generator::create_module_monitor_file(spec_module_model *mod_model) {
    QString                 uvc_prefix                      = mod_model->get_prefix();
    
    bool                    enable_reset                    = mod_model -> get_enable_reset_uvc();
    bool                    enable_register_memory_model    = mod_model -> get_enable_register_model();
    bool                    enable_reference_model          = mod_model -> get_enable_reference_model();
    bool                    enable_scoreboard               = mod_model -> get_enable_scoreboard();
    bool                    enable_uvm_scoreboard           = mod_model -> get_enable_uvm_scoreboard();
    
    QList<spec_code_model*> uvc_list                        = mod_model -> get_code_model_list();
    int                     mon_cnt                         = 0;
    spec_struct_data_node   node                            ;
    spec_field_node         *tlm_port                       ;
    QString                 tlm_port_string                 ;
    
    QList<QString*>         tlm_port_imp                    ;
    
    spec_msg("Generating file " << (uvc_prefix + "_monitor.e"));
    
    QList <QString*>    *code = new QList <QString*>;
    
    //    code->append( get_file_header( mod_model ) );
    code->append( new QString("File: " + uvc_prefix + "_monitor.e\n") );
    code->append( new QString("<'\n"));
    
    code->append( new QString( "unit " + uvc_prefix + "_monitor_u like uvm_monitor {\n"));
    
    if( enable_register_memory_model ) {
        code->append( new QString( "  // Instantiate the register/memory address map\n" ) );
        code->append( new QString( "  addr_map : "+ uvc_prefix.toUpper() +" vr_ad_map;\n" ) );
        code->append( new QString( "  \n" ) );
    };
    
    if( enable_reset ) {
        spec_reset_model    *res_model = mod_model->get_reset_model();
        
        code->append( new QString( "  // Have a reset monitor pointer to get reset access\n" ) );
        
        // Add multi-reset monitors. Each monitor has its associated domain name in upper case letters
        for( int reset_index = 0; reset_index < res_model->get_reset_domain_count(); ++reset_index ) {
            code->append( new QString( 
                "  !p_reset_" + res_model->get_reset_domain_name( reset_index ).toUpper() + "_mon: " +
                res_model->get_prefix() + "_monitor_u;\n"
                )
            );
        };
    };
    
    if( enable_scoreboard ) {
        if( enable_uvm_scoreboard ) {
            code->append( new QString( "  // Instantiate UVM scoreboard\n" ) );
            code->append( new QString( "  \n" ) );
        } else {
            code->append( new QString( "  // Instantiate custom scoreboard\n" ) );
            code->append( new QString( "  \n" ) );
        };
    };   // end of handling scoreboard instantiation
    
    if( enable_reference_model ) {
        code->append( new QString( "  // Instantiate a reference model shell\n" ) );
        code->append( new QString("  ref_model :" + uvc_prefix + "_ref_model_shell_u is instance;\n"));
        code->append( new QString( "  \n" ) );
    };
    
    // Add the TLM Analysis port of each monitor in here
    // <uvc_prefix><port_index>_tlm_imp : in interface_port of tlm_analysis of <port_data_type> using prefix=<uvc_prefix><port_index>_ is instance;
    for( spec_code_model* uvc : uvc_list ) {
        // Reset the driver count variable for each new UVC and get each new UVCs number of monitors... we need the active
        // and passive monitors to be connected properly
        mon_cnt = uvc->get_active_agent_count() + uvc->get_passive_agent_count();
        
        // Cycle through each instantiated/configured driver
        node = uvc->get_struct_like("uvm_monitor");
        for( spec_field_node* field : node.get_fields() ) {
            if( field->is_port() and field->get_port_kind() == "tlm_analysis") {
                if( mon_cnt != 0 ) {
                    code->append( new QString( "  // Instantiate the monitor's TLM port(s) from "+ uvc->get_prefix() + "\n"));
                } else {
                    code->append( new QString( "  // FIXME: It seems unlikely, but there seem to be no configured agents in the UVC " +uvc->get_prefix() ) );
                };
                for( int index = 0; index < mon_cnt;++index ) {
                    // Assemble the TLM port instantiation type
                    tlm_port_string = uvc->get_prefix() + QString::number(index) + "_tlm_imp: in interface_port of ";
                    tlm_port_string = tlm_port_string + field->get_port_kind() + " of " + field->get_port_data_type();
                    tlm_port_string = tlm_port_string + " using prefix=" + uvc->get_prefix() + QString::number(index) + "_";
                    
                    code->append( new QString( "  " + tlm_port_string + " is instance;\n" ) );
                    
                };
                code->append( new QString( "  \n" ) );
                
                // Assemble the TLM port imp method and store it in list.
                // Storing it for later file generation makes the file look cleaner
                for( int index = 0; index < mon_cnt; ++index ) {
                    tlm_port_string = uvc->get_prefix() + QString::number(index) +"_";
                    
                    if( field->get_port_kind() == "tlm_analysis" ) {
                        tlm_port_string = tlm_port_string + "write";
                    } else {
                        spec_msg( "There is currently only tlm_analysis port support!!!" );
                    };
                    
                    tlm_port_string = tlm_port_string + "( tr: " + field->get_port_data_type() + " ) is {";
                    
                    tlm_port_imp.append( new QString( "  " + tlm_port_string + "\n" ) );
                    tlm_port_imp.append( new QString( "    message(LOW, \"Received transaction on TLM port "+ uvc->get_prefix() + QString::number(index) + "_imp: \", tr);\n") );
                    tlm_port_imp.append( new QString( "    // TODO: Pass a deep_copy(tr) instance of the tr object for further processing\n") );
                    tlm_port_imp.append( new QString( "  };   // end of TLM received method\n") );
                    tlm_port_imp.append( new QString( "  \n" ) );
                };
            };
        };   // end of handling field being a TLM port
    };   // end of iterating over each UVCs TLM port
    
    if( enable_reset ) {
        // TODO: Each reset domain requires its own reset signalling mechanism
        spec_reset_model    *res_model          = mod_model->get_reset_model();
        int                 reset_domain_count  = res_model->get_reset_domain_count();
        
        code->append( new QString( "  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////\n" ) );
        code->append( new QString( "  // Reset Model " + res_model->get_prefix() + " UVC Configuration\n" ) );
        code->append( new QString( "  \n" ) );
        
        QString reset_domain_name;
        QString reset_active_flank;
        QString reset_off_flank;
        
        for( int index = 0; index < reset_domain_count; ++index ) {
            // Cycling through each reset domain
            
            reset_domain_name   = res_model->get_reset_domain_name(index);
            if( res_model->get_reset_domain_active_level(index) == "RESET_LO" ) {
                reset_active_flank  = "reset_fall_e";
                reset_off_flank     = "reset_rise_e";
            } else {
                reset_active_flank  = "reset_rise_e";
                reset_off_flank     = "reset_fall_e";
            };
            
            code->append( new QString( "  //--------------------------------------------------------------------------------------------------------------------\n" ) );
            code->append( new QString( "  // Handling Reset Domain --> " + reset_domain_name + " <--\n" ) );
            code->append( new QString( "  // These events get triggered whenever a reset signal changes its level\n" ) );
            code->append( new QString( "  event reset_asserted_" + reset_domain_name + "_e    is @p_reset_" + res_model->get_reset_domain_name( index ).toUpper() + "_mon.reset_fall_e;\n" ) );
            code->append( new QString( "  event reset_deasserted_" + reset_domain_name + "_e  is @p_reset_" + res_model->get_reset_domain_name( index ).toUpper() + "_mon.reset_rise_e;\n" ) );
            code->append( new QString( "  \n" ) );
            code->append( new QString( "  // This event handler is executed any time a reset gets asserted\n" ) );
            code->append( new QString( "  on reset_asserted_" + reset_domain_name + "_e {\n" ) );
            code->append( new QString( "    // TODO: Implement reset logic. Think of what needs to be reset or cleared in the testbench\n" ) );
            code->append( new QString( "    \n" ) );
            code->append( new QString( "  };   // end of reset_asserted_" + reset_domain_name + "_e event handler\n" ) );
            code->append( new QString( "  \n" ) );
            code->append( new QString( "  // This event handler is executed any time a reset gets deasserted\n" ) );
            code->append( new QString( "  on reset_deasserted_" + reset_domain_name + "_e {\n" ) );
            code->append( new QString( "    // TODO: Implement reset logic. Think of what needs to be reset or cleared in the testbench\n" ) );
            code->append( new QString( "    \n" ) );
            code->append( new QString( "  };   // end of reset_deasserted_" + reset_domain_name + "_e event handler\n" ) );
            code->append( new QString( "    \n" ) );
        };
        
        code->append( new QString( "  \n" ) );
        code->append( new QString( "  \n" ) );
    };  // end of reset model integration in module monitor
    
    code->append( new QString( "  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////\n" ) );
    code->append( new QString( "  // The following code contains the TLM inport '_write() method implementations. Each instantiated TLM port has an \n" ) );
    code->append( new QString( "  // pendant implemented _write method. The 'using prefix=<prefix>' corresponds witht he <prefix>_write() method.\n" ) );
    code->append( new QString( "  // Please keep in mind that each TLM inport needs to trigger the further processing of the received transaction.\n" ) );
    // Now write each of the TLM Analysis write_imp methods.
    for( QString* str : tlm_port_imp ) {
        code->append( str );
    };   // end of printing the TLM port impl methods
    
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  short_name():string is also {\n" ) );
    code->append( new QString( "    result = append( \"MOD_MON\" );\n" ) );
    code->append( new QString( "  };   // end of short_name()\n" ) );
    code->append( new QString( "};   // end of unit " + uvc_prefix + "_monitor_u\n" ) );
    code->append( new QString( "'>\n" ) );
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        mod_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_monitor.e",
        code
    );
}   // end of create_module_monitor_file


// This method does create the virtual driver based on the configuration of the given Module Model.
// If there are no drivers in a UVC, or none at all, the generator will create comments in the source file to point out
// that there may be something missing.
void spec_e_module_code_generator::create_module_virtual_driver_file( spec_module_model *mod_model ) {
    QString                     uvc_prefix                      = mod_model->get_prefix();
    
    bool                        enable_register_memory_model    = mod_model -> get_enable_register_model();
    bool                        enable_reset                    = mod_model -> get_enable_reset_uvc();
    QList<spec_code_model*>     uvc_list                        = mod_model -> get_code_model_list();
    
    spec_struct_data_node*      env_unit                        ;
    int                         drv_cnt                         = 0;
    int                         total_drv_cnt                   = 0;
    QString                     any_driver_string               ;
    QString                     any_sequence_string             ;
    
    spec_msg( "Generating file " << (uvc_prefix + "_virtual_driver.e") );
    
    QList <QString*>    *code = new QList <QString*>;
    
    //    code->append( get_file_header( mod_model ) );
    code->append( new QString( "File: " + uvc_prefix + "_virtual_driver.e\n") );
    code->append( new QString( "<'\n" ) );
    code->append( new QString( "sequence " + uvc_prefix + "_virtual_sequence_s using\n" ) );
    code->append( new QString( "  created_kind   = " + uvc_prefix + "_virtual_kind_t,\n"));
    code->append( new QString( "  created_driver = " + uvc_prefix + "_virtual_driver_u;\n"));
    code->append( new QString( "  \n"));
    code->append( new QString( "  \n"));
    code->append( new QString( "// The virtual driver has pointers to each concrete protocol driver\n"));
    code->append( new QString( "extend " + uvc_prefix + "_virtual_driver_u {\n"));
    
    if( enable_register_memory_model ) {
        code->append( new QString( "  \n"));
        code->append( new QString( "  // Provide the resgister sequence driver below to be layered on top of a given interface\n"));
        code->append( new QString( "  !rsd: vr_ad_sequenve_driver;\n"));
        code->append( new QString( "  \n"));
        code->append( new QString( "  \n"));
    };
    
    if( enable_reset ) {
        // TODO: Add the reset model
        spec_reset_model    *res_model = mod_model->get_reset_model();
        QString             res_prefix = res_model->get_prefix();
        
        code->append( new QString( "  // Now adding each reset domain's driver\n" ) );
        for( int reset_index = 0; reset_index < res_model->get_reset_domain_count();++reset_index ) {
            code->append( new QString( "  !p_" + res_prefix + "_" + res_model->get_reset_domain_name( reset_index).toUpper() + "_drv : " + res_prefix + "_driver_u;\n") );
        };
    };  // end of handling reset model enabled
    
    // Add a new driver for each instantiated concrete agent.
    // !p_<driver_name_u>: <driver_name_u>;
    for( spec_code_model* uvc : uvc_list ) {
        env_unit = uvc->get_env_node();
        // Reset the driver count variable for each new UVC
        drv_cnt = env_unit->get_active_agents_count();
        total_drv_cnt += drv_cnt;
        
        if( drv_cnt > 0 ) {
            QList<spec_struct_data_node*> agent_nodes = env_unit->get_active_agent_nodes();
            
            any_driver_string = *(uvc->get_all_like_structs_names("any_sequence_driver")).at(0);
            
            if( any_driver_string.isEmpty() ) {
                code->append( new QString( "  // The provided UVC: " + uvc_prefix + "  has no defined driver. Please add driver pointer manually." ) );
            } else {
                code->append( new QString( "  // Instantiate concrete drivers of UVC:  " + uvc->get_prefix()+ "\n"));
                for( int index = 0; index < drv_cnt;++index ) {
                    // Cycle through each instantiated/configured driver
                    code->append( new QString( "  !p_" + uvc->get_prefix() + "_drv" + QString::number(index) + " : " + any_driver_string + ";\n" ) );
                };
            };
        };
    };  // end of iterating over each code model's driver list
    
    if( total_drv_cnt == 0 ) {
        code->append( new QString( "  // TODO: There were no drivers configured. Check if that is intentional, or if testbench\n" ) );
        code->append( new QString( "  //       is really used without stimulating the Design Under Test.\n" ) );
    };
    
    code->append( new QString( "  \n"));
    code->append( new QString( "  event clock is only @sys.any;\n") );
    code->append( new QString( "  \n"));
    code->append( new QString( "  short_name(): string is also {\n"));
    code->append( new QString( "    result = \"VDRV\";\n"));
    code->append( new QString( "  };   // end of short_name()\n"));
    code->append( new QString( "};   // end of " + uvc_prefix + "_virtual_driver_u\n" ) );
    code->append( new QString( "\n"));
    code->append( new QString( "// Disable each of the integrated sequences\n" ) );
    
    if( enable_reset ) {
        code->append( new QString( "extend MAIN " + mod_model->get_reset_model()->get_prefix() +"_sequence_s {\n" ) );
        code->append( new QString( "  body()@driver.clock is only {};\n" ) );
        code->append( new QString( "};   // end of muting MAIN " + mod_model->get_reset_model()->get_prefix() +" sequence\n"));
        code->append( new QString( "\n"));
    };
    
    for( spec_code_model* uvc : uvc_list ) {
        any_sequence_string = uvc->get_struct_like("any_sequence").get_struct_name();
        // Do not mute sequence, if it doesn't exist
        if( any_sequence_string != "" ) {
            code->append( new QString( "extend MAIN " + any_sequence_string + " {\n"));
            code->append( new QString( "  body()@driver.clock is only {};\n"));
            code->append( new QString( "};   // end of muting MAIN " + any_sequence_string +" sequence\n"));
            code->append( new QString( "\n"));
        };
    };   // end of iterating over each UVC
    code->append( new QString( "\n"));
    code->append( new QString( "// Extending the base virtual sequence to provide objection to any started virtual sequence\n"));
    code->append( new QString( "extend " + uvc_prefix + "_virtual_sequence_s {\n" ) );
    code->append( new QString( "  // Use objection mechanism to handle graceful simulation end\n" ) );
    code->append( new QString( "  // Objections will only be triggered, when sequence is being started\n" ) );
    code->append( new QString( "  pre_body()@sys.any is first {\n" ) );
    code->append( new QString( "    driver.raise_objection( TEST_DONE );\n" ) );
    code->append( new QString( "  };\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  post_body()@sys.any is also {\n" ) );
    code->append( new QString( "    driver.drop_objection( TEST_DONE );\n" ) );
    code->append( new QString( "  };\n" ) );
    code->append( new QString( "};   // end of " + uvc_prefix + "_virtual_sequence_s\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "extend MAIN " + uvc_prefix + "_virtual_sequence_s {\n" ) );
    code->append( new QString( "  run_reset ()@driver.clock is empty;\n" ) );
    code->append( new QString( "  run_init  ()@driver.clock is empty;\n" ) );
    code->append( new QString( "  run_main  ()@driver.clock is empty;\n" ) );
    code->append( new QString( "  run_finish()@driver.clock is empty;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  body()@driver.clock is only {\n" ) );
    code->append( new QString( "    run_reset ();\n" ) );
    code->append( new QString( "    run_init  ();\n" ) );
    code->append( new QString( "    run_main  ();\n" ) );
    code->append( new QString( "    run_finish();\n" ) );
    code->append( new QString( "  };   // end of body()\n" ) );
    code->append( new QString( "};   // end of MAIN " + uvc_prefix + "_virtual_sequence_s\n" ) );
    
    code->append( new QString( "'>\n" ) );
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        mod_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_virtual_driver.e",
        code
    );
}   // end of create_module_virtual_driver_file

void spec_e_module_code_generator::create_module_sequence_lib_file( spec_module_model *mod_model ) {
    QString             uvc_prefix                      = mod_model->get_prefix();
    
    bool                enable_register_memory_model    = mod_model -> get_enable_register_model();
    
    qDebug() << "Generating file " << (uvc_prefix + "_sequence_library.e");
    
    QList <QString*>    *code = new QList <QString*>;
    
    //    code->append( get_file_header( mod_model ) );
    code->append( new QString( "File: " + uvc_prefix + "_sequence_library.e\n") );
    code->append( new QString( "<'\n" ) );
//    code->append( new QString( "extend MAIN " + uvc_prefix + "_virtual_sequence_s {" ) );
//    code->append( new QString( "  // The reset phase should contain only reset specific sequences\n" ) );
//    code->append( new QString( "  run_reset ()@driver.clock is empty;\n" ) );
//    code->append( new QString( "  // The init phase should contain anything that is needed for the bring-up\n" ) );
//    code->append( new QString( "  run_init  ()@driver.clock is empty;\n" ) );
//    code->append( new QString( "  // The main phase should contain the main traffic flow and scenarios\n" ) );
//    code->append( new QString( "  run_main  ()@driver.clock is empty;\n" ) );
//    code->append( new QString( "  // The finish phase ought to contain anything required for graceful simulation termination\n" ) );
//    code->append( new QString( "  run_finish()@driver.clock is empty;\n" ) );
//    code->append( new QString( "  \n" ) );
//    code->append( new QString( "  \n" ) );
//    code->append( new QString( "  body()@driver.clock is only {\n" ) );
//    code->append( new QString( "    message(NONE, \"Starting MAIN scenario...\");\n" ) );
//    code->append( new QString( "    \n" ) );
//    code->append( new QString( "    run_reset ();\n" ) );
//    code->append( new QString( "    run_init  ();\n" ) );
//    code->append( new QString( "    run_main  ();\n" ) );
//    code->append( new QString( "    run_finish();\n" ) );
//    code->append( new QString( "    \n" ) );
//    code->append( new QString( "    message(NONE, \"Done driving MAIN scenario...\");\n" ) );
//    code->append( new QString( "  };   // end of body()\n" ) );
//    code->append( new QString( "};   // end of " + uvc_prefix + "_virtual_sequence_s\n" ) );
    code->append( new QString( "'>\n" ) );
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        mod_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_sequence_library.e",
        code
    );
}   // end of create_module_sequence_lib_file

void spec_e_module_code_generator::create_module_env_file( spec_module_model *mod_model ) {
    QString                 uvc_prefix                      = mod_model->get_prefix();
    
    bool                    enable_register_memory_model    = mod_model -> get_enable_register_model();
    bool                    is_reset_enabled                = mod_model -> get_enable_reset_uvc();
    QList<spec_code_model*> uvc_list                        = mod_model -> get_code_model_list();
    QString                 driver_path;
    QString                 driver_path_calculated;
    int                     drv_cnt                         = 0;
    int                     total_drv_cnt                   = 0;
    spec_struct_data_node   *hierarchy_node                 ;
    spec_struct_data_node   *env_unit;
    
    
    spec_msg("Generating file " << (uvc_prefix + "_env.e"));
    
    QList <QString*>    *code = new QList <QString*>;
    
    //    code->append( get_file_header( mod_model ) );
    code->append( new QString( "File: " + uvc_prefix + "_env.e\n") );
    code->append( new QString( "<'\n" ) );
    code->append( new QString( "unit " + uvc_prefix + "_env_u like uvm_env {\n" ) );
    code->append( new QString( "  env_name      : " + uvc_prefix + "_env_name_t;\n" ) );
    code->append( new QString( "  \n" ) );
    
    if( is_reset_enabled ) {
        QString reset_prefix = mod_model->get_reset_model()->get_prefix();
        code->append( new QString(  "  // The reset UVC pointer is used to connect all reset domains throughout the UVC\n") );
        code->append( new QString( "  !p_" + reset_prefix + "_env : " + reset_prefix + "_env_u;\n" ) );
        code->append( new QString( "  \n" ) );
    };
    
    for( spec_code_model* uvc : uvc_list ) {
        code->append( new QString( "  !p_" + uvc->get_prefix() + "_env : " + uvc->get_struct_like("uvm_env").get_struct_name() + ";\n"));
    };
    code->append( new QString( "  \n"));
    code->append( new QString( "  // Now adding the module configuration here\n"));
    code->append( new QString( "  \n"));
    code->append( new QString( "  // The mod_monitor contains the module's functional model.\n"));
    code->append( new QString( "  // This object also contains the receiving TLM analysis entry points.\n"));
    code->append( new QString( "  mod_monitor : " + uvc_prefix + "_monitor_u is instance;\n"));
    code->append( new QString( "  \n"));
    code->append( new QString( "  // The virtual driver contains pointers to each of the module's concrete protocol drivers\n"));
    code->append( new QString( "  vdrv        : " + uvc_prefix + "_virtual_driver_u is instance;\n"));
    
    code->append( new QString( "  \n"));
    code->append( new QString( "  // We'll connect all the protocol sub-drivers to the virtual driver pointers in this phase\n"));
    code->append( new QString( "  connect_pointers() is also {\n"));
    
    if( is_reset_enabled ) {
        spec_reset_model    *res_model  = mod_model->get_reset_model();
        QString             res_prefix  = res_model->get_prefix();
        code->append( new QString( "    // First, we are connecting the reset monitors for each reset domain.\n"  ) );
        for( int reset_index = 0; reset_index < res_model->get_reset_domain_count(); ++reset_index ) {
           code->append( new QString( "    mod_monitor.p_reset_" + res_model->get_reset_domain_name(reset_index).toUpper()+ "_mon = " ) );
           code->append( new QString( "p_" + res_prefix + "_env.agents["+QString::number(reset_index)+"].mon;\n" ) );
           // vdrv.p_db_reset_SYSTEM_drv = p_db_apb_env.agents[0].as_a(ACTIVE db_reset_agent_u).drv;
           code->append( new QString(
                "    vdrv.p_" + res_model->get_prefix() + "_" + res_model->get_reset_domain_name( reset_index).toUpper() +  "_drv" " = p_" + 
                res_prefix + "_env.agents[" +QString::number(reset_index) + "].as_a( ACTIVE " + res_prefix + "_agent_u ).drv" + ";\n"
                ) 
            );
           
        };
    };
    for( spec_code_model* uvc : uvc_list ) {
        // Getting the env_node as the root interface UVC unit enables us to track the number of configured drivers
        env_unit = uvc->get_env_node();
        
        // Reset the driver count variable for each new UVC
        drv_cnt = env_unit -> get_active_agents_count();
        
        // Get the hierarchy path from the instantiated hierarchy to the driver object
        driver_path = uvc->get_single_hierarchy_path(
            env_unit,
            "get_like_name",
            "any_sequence_driver"
        );
        
        // Replace the env struct with the p_<uvc_prefix>_env_u name
        driver_path.replace(
            uvc->get_struct_like("uvm_env").get_struct_name(),
            "p_" + uvc->get_prefix() + "_env"
        );
        
        // Replace the index with the drv_cnt number
        code->append( new QString( "    // Connecting all drivers of the ACTIVE agents of UVC " + uvc->get_prefix()+ "\n"));
        for( int index = 0; index < drv_cnt; ++index ) {
            driver_path_calculated = driver_path;
            driver_path_calculated.replace(
                "[0]",
                "[" + QString::number(index + env_unit -> get_passive_agents_count()) +"]"
            );
            code->append( new QString( "    vdrv.p_" + uvc->get_prefix() + "_drv" + QString::number(index) + " = " + driver_path_calculated + ";\n" ) );
        };
        code->append( new QString( "    \n"));
    };  // end of traversing all of the UVCs
    
    code->append( new QString( "  \n"));
    code->append( new QString( "  };   // end of connect_pointers\n"));
    
    code->append( new QString( "};   // end of " + uvc_prefix + "_env_u\n" ) );
    code->append( new QString( "'>\n" ) );
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        mod_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_env.e",
        code
    );
}   // end of create_module_env_file

void spec_e_module_code_generator::create_module_objections_file( spec_module_model         *mod_model ) {
    QString             uvc_prefix                          = mod_model->get_prefix();
    
    qDebug() << "Generating file " << (uvc_prefix + "_objections.e");
    QList <QString*>    *code = new QList<QString*>;
    
    //    code->append( get_file_header( mod_model ) );
    code->append( new QString( "File: " + uvc_prefix + "_objections.e\n") );
    code->append( new QString( "==============================================================================================\n" ) );
    code->append( new QString( "== The objection mechanism is usually attached to the sequencing. The first time an \n" ) );
    code->append( new QString( "== objection is raised is when the objection mechanism assumes control of the simulation`s\n" ) );
    code->append( new QString( "== graceful termination mechanism.\n" ) );
    code->append( new QString( "==============================================================================================\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "<'\n" ) );
    code->append( new QString( "extend MAIN " + uvc_prefix + "_virtual_sequence_s {\n" ) );
    code->append( new QString( "  // The pre_body() time-consuming method is only called during start of the sequences. Hence only the MAIN\n" ) );
    code->append( new QString( "  // sequence is being used to kick-off the objection counter mechanism\n" ) );
    code->append( new QString( "  pre_body()@sys.any is first {\n" ) );
    code->append( new QString( "    driver.raise_objection( TEST_DONE );\n" ) );
    code->append( new QString( "  };\n" ) );
    code->append( new QString( "  " ) );
    code->append( new QString( "  // The post_body() time-consuming method will ensure that the objection is dropped at the end of the MAIN\n" ) );
    code->append( new QString( "  // virtual sequence\n" ) );
    code->append( new QString( "  post_body()@sys.any is also {\n" ) );
    code->append( new QString( "    driver.drop_objection( TEST_DONE );\n" ) );
    code->append( new QString( "  };\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "};   // end of " + uvc_prefix + "_virtual_sequence_s" ) );
    
    code->append( new QString( "// TODO: Check if any of the protocol monitors should implement an objection mechanism as well\n" ) );
    
    code->append( new QString( "'>\n" ) );
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        mod_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_objections.e",
        code
    );
}


void spec_e_module_code_generator::create_module_sve_config_file( spec_module_model *mod_model ) {
    QString                 uvc_prefix                  = mod_model->get_prefix();
    
    bool                    enable_clock_uvc            = mod_model -> get_enable_clock_uvc();
    bool                    enable_reset_uvc            = mod_model -> get_enable_reset_uvc();
    
    QList<spec_code_model*> uvc_list                    = mod_model -> get_code_model_list();
    
    
    spec_struct_data_node   *env_node                   ;
    spec_struct_data_node   *env_config_node            ;
    spec_struct_data_node   *agent_config_node          ;
    spec_struct_data_node   *port_node                  ;
    QList<spec_field_node*> fields                      ;
    QString                 assembled_string            ;
    QList<QString*>         config_struct_names         ;
    QList<QString>          subtype_struct_name         ;
    QList<spec_struct_data_node*>   complete_agent_list ;
    int                     counter                     = 0;
    QString                 agent_object_name           ;
    QString                 env_config_name             ;
    QString                 agent_config_name           ;
    QString                 domain_name                 ;
    QList<QString>          interface_name_list         ;
    bool                    has_domain_association      ;
    QList<QString>          config_line                 ;
    QList<QString*>         env_config_code             ;
    QList<QString*>         agent_config_code           ;
    QList<spec_struct_data_node*>   for_loop_concat_list;
    
    
    spec_msg("Generating file " << (uvc_prefix + "_full_config.e"));
    
    QList <QString*>    *code = new QList <QString*>;
    
    //    code->append( get_file_header( mod_model ) );
    code->append( new QString( "File: " + uvc_prefix + "_full_config.e\n") );
    code->append( new QString( "<'\n" ) );
    
    if( enable_clock_uvc ) {
        // Get the clock model
        spec_clock_model *clk_mod = mod_model->get_clock_model();
        int clock_domain_count = clk_mod->get_clock_domain_count();
        
        code->append( new QString( "////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////\n" ) );
        code->append( new QString( "// " + clk_mod->get_prefix() + " UVC Configuration\n" ) );
        code->append( new QString( "\n" ) );
        
        if( clock_domain_count > 0 ) {
//            code->append( new QString( "extend " + uvc_prefix.toUpper() + " " + clk_mod->get_prefix() +"_env_u {\n" ) );
            code->append( new QString( "extend " + clk_mod->get_prefix() +"_env_u {\n" ) );
            code->append( new QString( "  keep " + uvc_prefix.toUpper() + "_" + clk_mod->get_prefix().toUpper() +"_ENV_U_C is all of {\n") );
            code->append( new QString( "    agents.size() == " + QString::number( clock_domain_count ) + ";\n" ) );
            code->append( new QString( "    for each (agent) in agents {\n" ) );
            code->append( new QString( "      agent.conf.clock_domain == (index + 1).as_a( " + clk_mod->get_prefix() + "_domain_name_t );\n" ) );
            code->append( new QString( "    };\n" ) );
            code->append( new QString( "  };\n" ) );
            code->append( new QString( "};   // end of "  + uvc_prefix + " " + clk_mod->get_prefix() +"_env_u\n" ) );
            code->append( new QString( "\n" ) );
            
            for( int i = 0 ; i < clock_domain_count; ++i ) {
                // Get the current clock domain and make it UPPERCASE
                domain_name    = clk_mod->get_clock_domain_name(i).toUpper();
                
                code->append( new QString( "//----------------------------------------------------------------------------------------------------\n" ) );
                code->append( new QString( "// Configuring all components associated with CLOCK DOMAIN  --> " + domain_name + " <--\n") );
                code->append( new QString( "extend " + clk_mod->get_prefix() + "_domain_name_t: [ " + domain_name + " ];\n" ) );
                code->append( new QString( "extend " + domain_name + " " + clk_mod->get_prefix() + "_configuration_s {\n") );
                code->append( new QString( "  keep " + uvc_prefix.toUpper() + "_" + domain_name + "_" +clk_mod->get_prefix().toUpper() +"_CONFIGURATION_S_C is all of {\n") );
                code->append( new QString( "    enable_jitter == " + clk_mod->get_clock_domain_jitter(i).toUpper() + ";\n" ) );
                code->append( new QString( "    clk_shift     == " + clk_mod->get_clock_domain_shift(i) + ";\n" ) );
                code->append( new QString( "    clk_period    == " + clk_mod->get_clock_domain_period(i) + ";\n" ) );
                code->append( new QString( "    clk_init      == " + clk_mod->get_clock_domain_init(i) + ";\n" ) );
                code->append( new QString( "  };\n" ) );
                code->append( new QString( "};\n" ) );
                code->append( new QString( "\n" ) );
                code->append( new QString( "extend " + domain_name + " " + clk_mod->get_prefix() + "_signal_map_u {\n") );
                code->append( new QString( "  keep " + domain_name + "_CLOCK_DOMAIN_SPECIFIC_SIGNAL_MAP_C is all of {\n" ) );
                code->append( new QString( "    clk_seed_op             .hdl_path() == \"sn_seed\";\n" ) );
                code->append( new QString( "    clk_ip                  .hdl_path() == \"" + clk_mod->get_clock_domain_hdl_name(i) + "\";\n" ) );
                code->append( new QString( "    clk_jitter_enable_op    .hdl_path() == \"" + domain_name + "_clk_jitter_enable\";\n" ) );
                code->append( new QString( "    clk_period_op           .hdl_path() == \"" + domain_name + "_clk_period\";\n" ) );
                code->append( new QString( "    clk_shift_op            .hdl_path() == \"" + domain_name + "_clk_shift\";\n" ) );
                code->append( new QString( "    clk_init_op             .hdl_path() == \"" + domain_name + "_clk_init\";\n" ) );
                code->append( new QString( "  };\n" ) );
                
                code->append( new QString( "};   // end of clock domain " + clk_mod->get_clock_domain_name(i).toUpper() + " signal map\n" ) );
                
                domain_name                     = clk_mod->get_clock_domain_name(i);
//                QList<spec_struct_data_node*> for_loop_concat_list;
                
                for( spec_code_model *code_model : mod_model->get_code_model_list() ) {
                    // First we'll cycle through all PASSIVE agents, then through the ACTIVE agents
                    // TODO: Capture the agents in a loop prior to processing the actual code models!!!!!
                    for_loop_concat_list = code_model->get_passive_agent_nodes();
                    for_loop_concat_list.append( code_model->get_active_agent_nodes() );
                    
                    for( spec_struct_data_node *agent : for_loop_concat_list ) {
                        // Then we are going to check each agent if it has a matching clock domain name
                        if( agent->get_clock_domain_conf().toUpper() == domain_name ) {
                            // Now let' search the selected clock signal within the signal map...
                            for( int smp_child_index = 0; smp_child_index < agent->get_signal_map_node()->childCount(); ++smp_child_index ) {
                                port_node = agent->get_signal_map_node()->get_child( smp_child_index );
                                // Always reset the domain association in each iteration step to avoid initialization side-effects
                                has_domain_association = false;
                                if( port_node->get_like_name() == "any_event_port" ) {
                                    has_domain_association = port_node->get_event_port_widget()->is_associated_clock();
                                };
                                if( port_node->get_like_name() == "any_simple_port" ) {
                                    has_domain_association = port_node->get_simple_port_widget()->is_associated_clock();
                                };
                                spec_msg( "Clock Domain Assignment: " + port_node->get_like_name() );
                                if( has_domain_association ) {
                                    spec_msg( " --> Is assigned " + clk_mod->get_clock_domain_hdl_name(i));
                                    code->append( new QString(
                                                      "extend " + agent->get_agent_widget()->get_selected_interface_domain() + " " +
                                                      agent->get_struct_name().replace( "__", " ")  + " {\n" )
                                                  );
                                    code->append( new QString( 
                                                      "  keep " + agent->get_signal_map_node()->get_object_name() + "." +
                                                      port_node->get_object_name() + ".hdl_path() == \"" +
                                                      clk_mod->get_clock_domain_hdl_name(i) + "\"\n" )
                                                  );
                                    code->append( new QString( "};\n" ) );
                                    code->append( new QString( "\n" ) );
                                };
                            };  // end of iterating over each of the signal map children
                        };  // end of matching the current agent's configured clock domain against the processed clock domain
                    };  // end of iterating over each agent
                };  // end of iterating over each of the code models
            };  // end of iterating over each clock domain
        } else {
            code->append( new QString( " // FIXME: The CLOCK UVC generation was selected, but there were no declared CLOCK domains.\n" ) );
            code->append( new QString( " //        Your options are:\n" ) );
            code->append( new QString( " //            1.) Uncheck the CLOCK UVC generation\n" ) );
            code->append( new QString( " //            2.) Declare at least one CLOCK domain\n" ) );
            code->append( new QString( " //            3.) Configure your CLOCK domains directly in this code.\n" ) );
            code->append( new QString( " // \n" ) );
        };
        
        // We need to set each UVC's hdl_path() to point to the right clock in the clock domain
        
        // Writing nice delimiter comment make the sections easier to distinguish
        code->append( new QString( "////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////\n" ) );
        code->append( new QString( "\n" ) );
    };   // end of handling clock UVC generation
    
    if( enable_reset_uvc ) {
        // Get the reset model
        spec_reset_model    *res_mod            = mod_model->get_reset_model();
        int                 reset_domain_count  = res_mod->get_reset_domain_count();
        
        // Extracting and writing reset configuration
        code->append( new QString( "////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////\n" ) );
        code->append( new QString( "// " + res_mod->get_prefix() + " UVC Configuration\n" ) );
        code->append( new QString( "\n" ) );
        
        if( reset_domain_count > 0 ) {
            
            
//            code->append( new QString( "extend " + uvc_prefix.toUpper() + " " + res_mod->get_prefix() +"_env_u {\n" ) );
            code->append( new QString( "extend " + res_mod->get_prefix() +"_env_u {\n" ) );
            code->append( new QString( "  keep " + uvc_prefix.toUpper() + "_" + res_mod->get_prefix().toUpper() +"_ENV_U_C is all of {\n") );
            code->append( new QString( "    agents.size() == " + QString::number( reset_domain_count ) + ";\n" ) );
            code->append( new QString( "    for each (agent) in agents {\n" ) );
            code->append( new QString( "      agent.if_name == (index + 1).as_a( " + res_mod->get_prefix() + "_domain_name_t );\n" ) );
            code->append( new QString( "    };\n" ) );
            code->append( new QString( "  };\n" ) );
            code->append( new QString( "};   // end of "  + uvc_prefix + " " + res_mod->get_prefix() +"_env_u\n" ) );
            code->append( new QString( "\n" ) );
            
            
            // FIXME: There is a bug that causes only one of the reset UVCs to be attached to the reset properly.
            for_loop_concat_list.clear();
            if( for_loop_concat_list.count() == 0 ) {
                spec_msg( "No previously detected agents found" );
                for( spec_code_model *code_model : mod_model->get_code_model_list() ) {
                    for_loop_concat_list.append( code_model->get_passive_agent_nodes() );
                    for_loop_concat_list.append( code_model->get_active_agent_nodes() );
                };
            };
            
            for( spec_struct_data_node *agent : for_loop_concat_list ) {
                spec_msg( "Agent " + agent->get_struct_name() + " at " + agent->get_agent_widget()->get_selected_interface_domain() );
            };
            
//            for( spec_code_model *code_model : mod_model->get_code_model_list() ) {
//                // First we'll cycle through all PASSIVE agents, then through the ACTIVE agents
//                for_loop_concat_list = code_model->get_passive_agent_nodes();
//                for_loop_concat_list.append( code_model->get_active_agent_nodes() );
//            };
            
            
            for( int i = 0; i < reset_domain_count; ++i ) {
                domain_name = res_mod->get_reset_domain_name( i ).toUpper();
                
                code->append( new QString( "//----------------------------------------------------------------------------------------------------\n" ) );
                code->append( new QString( "// Configuring all components associated with RESET DOMAIN  --> " + domain_name + " <--\n") );
                code->append( new QString( "extend " + res_mod->get_prefix() + "_domain_name_t: [ " + domain_name + " ];\n" ) );
                code->append( new QString( "extend " + domain_name + " " + res_mod->get_prefix() + "_agent_config_s {\n") );
                code->append( new QString( "  keep " + uvc_prefix.toUpper() + "_" + domain_name + "_" +res_mod->get_prefix().toUpper() +"_CONFIGURATION_S_C is all of {\n") );
                code->append( new QString( "    reset_level         == " + res_mod->get_reset_domain_active_level(i) + ";\n" ) );
                code->append( new QString( "    reset_synchronicity == " + res_mod->get_reset_domain_sync_clock(i) + ";\n" ) );
                code->append( new QString( "  };\n" ) );
                code->append( new QString( "};\n" ) );
                code->append( new QString( "\n" ) );
                code->append( new QString( "extend " + domain_name + " " + res_mod->get_prefix() + "_signal_map_u {\n") );
                code->append( new QString( "  keep " + domain_name + "_RESET_DOMAIN_SPECIFIC_SIGNAL_MAP_C is all of {\n" ) );
                code->append( new QString( "    reset_iop.hdl_path() == \"" + domain_name.toUpper() + "_" + res_mod->get_reset_domain_hdl_name(i) + "\";\n" ) );
                if( not enable_clock_uvc ) {
                    code->append( new QString( "  // TODO: You may have to connect the clock event_port manually because you have not configured any clocks.\n" ) );
                    code->append( new QString( "  // clock_iep.hdl_path()  == \"<change_to_my_hdl_clock_signal>\";\n" ) );
                } else {
                    QString             agent_clock_domain;
                    spec_clock_model    *_clk_model = mod_model->get_clock_model();
                    
                    // Now get the appropriate clock domain and associate each agent's clock domain with the reset clock
                    for( spec_struct_data_node *agent : for_loop_concat_list ) {
                        if( agent->get_reset_domain_conf() == domain_name ) {
                            for( int clock_index = 0; clock_index < _clk_model->get_clock_domain_count(); ++clock_index ) {
                                agent_clock_domain = agent->get_clock_domain_conf();
                                if( _clk_model->get_clock_domain_name( clock_index ) == agent_clock_domain ) {
                                    code->append( new QString( "  clock_iep.hdl_path()  == \"" + _clk_model->get_clock_domain_hdl_name( clock_index) + "\";\n" ) );
                                    break;
                                };
                            };
                            break;
                        };
                    };  // end of iterating over each single agent
                };  // end of handling defined clock agents
                
                code->append( new QString( "  };\n" ) );
                code->append( new QString( "};   // end of reset domain " + domain_name + " signal map\n" ) );
                
                
                for( spec_struct_data_node *agent : for_loop_concat_list ) {
                    // Then we are going to check each agent if it has a matching clock domain name
                    if( agent->get_reset_domain_conf().toUpper() == domain_name ) {
                        // Now let' search the selected reset signal within the signal map...
                        for( int smp_child_index = 0; smp_child_index < agent->get_signal_map_node()->childCount(); ++smp_child_index ) {
                            port_node = agent->get_signal_map_node()->get_child( smp_child_index );
                            // Always reset the domain association in each iteration step to avoid initialization side-effects
                            has_domain_association = false;
                            if( port_node->get_like_name() == "any_event_port" ) {
                                has_domain_association = port_node->get_event_port_widget()->is_associated_reset();
                            };
                            if( port_node->get_like_name() == "any_simple_port" ) {
                                has_domain_association = port_node->get_simple_port_widget()->is_associated_reset();
                            };
                            
                            spec_msg( "Reset Domain Assignment: " + port_node->get_like_name() );
                            if( has_domain_association ) {
                                spec_msg( " --> Is assigned " + res_mod->get_reset_domain_hdl_name(i));
                                code->append( new QString(
                                                  "extend " + agent->get_agent_widget()->get_selected_interface_domain() + " " +
                                                  agent->get_struct_name().replace( "__", " ")  + " {\n" )
                                              );
                                code->append( new QString( 
                                                  "  keep " + agent->get_signal_map_node()->get_object_name() + "." +
                                                  port_node->get_object_name() + ".hdl_path() == \"" +
                                                  domain_name.toUpper() + "_" + res_mod->get_reset_domain_hdl_name(i) + "\"\n" )
                                              );
                                code->append( new QString( "};\n" ) );
                                code->append( new QString( "\n" ) );
                            };
                        };  // end of iterating over each of the signal map children
                    };  // end of matching the current agent's configured reset domain against the processed reset domain
                };  // end of iterating over each agent
            };  // end of iterating over each of the code models  
        } else {
            code->append( new QString( " // FIXME: The RESET UVC generation was selected, but there were no declared RESET domains.\n" ) );
            code->append( new QString( " //        Your options are:\n" ) );
            code->append( new QString( " //            1.) Uncheck the RESET UVC generation\n" ) );
            code->append( new QString( " //            2.) Declare at least one RESET domain\n" ) );
            code->append( new QString( " //            3.) Configure your RESET domains directly in this code.\n" ) );
            code->append( new QString( " // \n" ) );
        };   // end of displaying a code generation reminder
        
        // Writing nice delimiter comment make the sections easier to spot
        code->append( new QString( "////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////\n" ) );
        code->append( new QString( "\n" ) );
    };   // end of reset UVC generation
    
    // Iterate over each given uvc and dump its configuration
    for( spec_code_model *uvc : uvc_list ) {
        // First we'll get the information from the current env node
        env_node                = uvc->get_env_node();
        env_config_node         = env_node->get_env_configuration_node();
        if( env_config_node == nullptr ) {
            spec_msg( "The current environment " << env_node->get_object_name() << " of " << env_node->get_struct_name() << " does not have a configuration struct." );
        } else {
            env_config_name         = env_config_node->get_struct_name();
        };
        
        complete_agent_list     = env_node -> get_passive_agent_nodes();
        complete_agent_list.append( env_node -> get_active_agent_nodes() );
        agent_object_name       = "";
        counter                 = 0;
        
        // Configuring the number of agents and assigning their interface domain
        for( int child_index = 0; child_index < env_node->childCount(); ++child_index ) {
            if( env_node->get_child( child_index )->get_like_name() == "uvm_agent" ) {
                // Assign the number of agents as configured by the GUI
                agent_object_name = env_node->get_child( child_index )->get_object_name();
                
                // Remove the list index indicator from the naming scheme
                int trim_pos = agent_object_name.indexOf( "[" );
                if( trim_pos > 0 ) {
                    agent_object_name.remove( trim_pos, agent_object_name.count() - trim_pos);
                };
                // No need to continue searching once we got the agent name
                break;
            };
        };  // end of searching for agents
        
//        code->append( new QString( "extend " + uvc_prefix.toUpper() + " " + env_node->get_struct_name() + " {\n") );
        code->append( new QString( "extend " + env_node->get_struct_name() + " {\n") );
        code->append( new QString( "  keep CONCRETE_ENV_AGENT_CONFIG_C is all of {\n" ) );
        code->append( new QString( "    // First the total size of all agents has to be set\n" ) );
        code->append( new QString( "    " + agent_object_name + ".size() == " + QString::number(complete_agent_list.count()) +";\n" ) );
        code->append( new QString( "    for each (agent) using index (agent_index) in " + agent_object_name + " {\n" ) );
        
        for( spec_struct_data_node *agent : complete_agent_list ) {
            // Let's store each agent's interface name first
            domain_name         = agent->get_agent_widget()->get_selected_interface_domain();
            interface_name_list = domain_name.split( QRegularExpression("'"));
            agent_config_node   = agent->get_agent_configuration_node( env_config_name );
            
            code->append( new QString( "      (agent_index == " + QString::number(counter) + ") => all of {\n" ) );
            if( counter < env_node->get_passive_agents_count() ) {
                code->append( new QString( "        agent.active_passive == PASSIVE;\n") );
            } else {
                code->append( new QString( "        agent.active_passive == ACTIVE;\n") );
            };
            if( interface_name_list.count() == 2 ) {
                code->append( new QString( "        agent." + interface_name_list.at(1) + " == " + interface_name_list.at(0) + ";\n" ) );
            } else {
                code->append( new QString( "        // Could not determine the interface descriptor field. To ensure correct configuration \n" ) );
                code->append( new QString( "        // please add the interface name label constraint here: \n" ) );
                code->append( new QString( "        // agent.<interface_name_descriptor> == <interface_name>; \n" ) );
            };
            if( not agent->get_agent_widget()->get_hdl_path().isEmpty() ) {
                code->append( new QString( "        agent.hdl_path()     == \"" + agent->get_agent_widget()->get_hdl_path() +"\";\n" ) );
            };
            
            code->append( new QString( "      };   // end of configuring agent " + domain_name + "\n" ) );
            
            if( agent_config_node == nullptr ) {
                spec_msg( "There was no agent configuration struct defined for agent " << agent->get_object_name() << " of type " << agent->get_struct_name() );
            } else {
                // Each agent configuration is written separately and postponed for later that we can keep the configurations together
                agent_config_code.append( new QString( "extend " + agent_config_node->get_struct_name() + " {\n" ) );
                agent_config_code.append( new QString( "  // Traversing through all the table lines and only setting constraints on user-configured items\n" ) );
                agent_config_code.append( new QString( "  // ...\n" ) );
                
                // Extract the table information and use those values to configure the env_configuration
                for( int c_index = 0; c_index < agent_config_node->get_config_params_widget()->get_line_count(); ++c_index ) {
                    config_line = agent_config_node->get_config_params_widget()->get_line_at( c_index );
                    spec_msg( "[" << c_index << "]: " << config_line );
                    
                    agent_config_code.append( new QString( "  // Field: " + config_line.at(0) + "  :  " + config_line.at(1) +"\n" ) );
                    
                    if( not config_line.at(2).isEmpty() ) {
                        // Remove the enum type field indicator, by first splitting the list and using only the first element
                        QList<QString> split_config = config_line.at(2).split("'");
                        
                        agent_config_code.append( new QString( "  keep "+ config_line.at(0) + "  ==  " + split_config.at(0) +";\n" ) );
                    } else {
                        agent_config_code.append( new QString( "  // " + config_line.at(0) + "  ==  <" + config_line.at(1) + ">;   // <== has not been user-configured\n" ) );
                    };
                    agent_config_code.append( new QString( "  \n" ) );
                };
                
                agent_config_code.append( new QString( "};   // end of extending configuration for interface " + domain_name + "\n" ) );
                agent_config_code.append( new QString( "\n" ) );
                
                // We also need to configure the signal map!
                spec_struct_data_node *signal_map_node = agent->get_signal_map_node();
                agent_config_code.append( new QString( "extend " + agent->get_agent_widget()->get_selected_interface_domain() + " " + signal_map_node->get_struct_name() + " {\n" ) );
                for( int smp_index = 0; smp_index < signal_map_node->childCount(); ++smp_index ) {
                    port_node = signal_map_node->get_child( smp_index );
                    if( port_node->get_like_name() == "any_simple_port" ) {
                        if( not (port_node->get_simple_port_widget()->is_associated_reset() or port_node->get_simple_port_widget()->is_associated_clock()) ) {
                            agent_config_code.append( new QString( "  keep " + port_node->get_object_name() + ".hdl_path()  ==  \"" + port_node->get_simple_port_widget()->get_signal_name() + "\";\n" ) );
                        };
                    };
                    
                    if( port_node->get_like_name() == "any_event_port" ) {
                        if( not (port_node->get_event_port_widget()->is_associated_reset() or port_node->get_event_port_widget()->is_associated_clock()) ) {
                            agent_config_code.append( new QString( "  keep " + port_node->get_object_name() + ".hdl_path()  ==  \"" + port_node->get_event_port_widget()->get_signal_name() + "\";\n" ) );
                        };
                    };
                };  // end of iterating over each signal map node
                agent_config_code.append( new QString( "};   // end of " + agent->get_agent_widget()->get_selected_interface_domain() + " " + signal_map_node->get_struct_name() + "\n" ) );
            };
            
            ++counter;
        };  // end of iterating through all agents
        
        code->append( new QString( "    };   // end of iterating over each agent element\n" ) );
        code->append( new QString( "  };   // end of CONCRETE_ENV_AGENT_CONFIG_C\n" ) );
        code->append( new QString( "};   // end of extending " + uvc_prefix.toUpper() + " " +env_node->get_struct_name() + "\n" ) );
        code->append( new QString( "\n" ) );
        
        if( env_config_node != nullptr ) {
            // Writing the env_config settings
//            code->append( new QString( "extend " + uvc_prefix.toUpper() + " " + env_config_node->get_struct_name() + " {\n" ) );
            code->append( new QString( "extend " + env_config_node->get_struct_name() + " {\n" ) );
            if( env_config_node->get_config_params_widget() != nullptr ) {
                // Extract the information from the enviroment configuration file and then add it right here....
                for(int c_index = 0; c_index < env_config_node->get_config_params_widget()->get_line_count(); ++c_index ) {
                    config_line = env_config_node->get_config_params_widget()->get_line_at( c_index );
                    spec_msg( "[" << c_index << "]: " << config_line );
                    
                    code->append( new QString( "  // Field: " + config_line.at(0) + "  :  " + config_line.at(1) + "\n" ) );
                    
                    if( not config_line.at(2).isEmpty() ) {
                        code->append( new QString( "  keep "+ config_line.at(0) + "  ==  " + config_line.at(2) +";\n" ) );
                    } else {
                        code->append( new QString( "  // " + config_line.at(0) + "  ==  <" + config_line.at(1) + ">;   // <== has not been user-configured\n" ) );
                    };
                    code->append( new QString( "  \n" ) );
                };  // end of iterating over each detected environment config node field
            }  // end of handling properly configured env_node_config
            else {
                spec_msg( "No config node found in environment " << env_config_node->get_struct_name() );
            };
            
            code->append( new QString( "  \n" ) );
            code->append( new QString( "  \n" ) );
            code->append( new QString( "};   // end of configuring the " + uvc_prefix.toUpper() + " " + env_config_node->get_struct_name() + " configuration\n" ) );
            code->append( new QString( "\n") );
        };
        
        // Add the agent config code to each agent section, after the env section
        for( QString *str : agent_config_code ) {
            code->append( str );
        };
    };   // end of iterating over each module configuration
    
    code->append( new QString( "'>\n" ) );
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        mod_model->get_directory() + "/" + uvc_prefix + "/sve/" + uvc_prefix + "_full_config.e",
        code
    );
}   // end of create_module_sve_config_file

void spec_e_module_code_generator::create_module_smoke_test_file              ( spec_module_model         *mod_model ) {
    QString                         uvc_prefix  = mod_model -> get_prefix();
    QList<spec_code_model*>         uvc_list    = mod_model -> get_code_model_list();
    spec_struct_data_node   *env_unit    ;
    QString                 cur_uvc_prefix      ;
    QString                 current_struct_name ;
    QList<QString>          seq_object_names    ;
    QList<QString>          seq_struct_names    ;
    QList<QString>          seq_driver_obj_names;
    QList<QString>          seq_driver_unit_names    ;
    QList<int>              seq_agents_counts   ;
    QString                 any_driver_string   ;
    int                     drv_cnt             = 0;
    int                     total_drv_cnt       = 0;
    QList <QString*>        *code               = new QList <QString*>;
    QList<QString>          body_code           ;
    
    spec_msg("Generating file " + uvc_prefix + "_smoke_test.e");
    
    //    code->append( get_file_header( mod_model ) );
    code->append( new QString( "File: " + uvc_prefix + "_smoke_test.e\n") );
    code->append( new QString( "==============================================================================================\n" ) );
    code->append( new QString( "== In this testcase, each interface sends a single transaction over each ACTIVE interface \n" ) );
    code->append( new QString( "== By executing this test you can check if the setup to the DUT is done correctly.\n" ) );
    code->append( new QString( "==============================================================================================\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "<'\n" ) );
    code->append( new QString( "extend MAIN " + uvc_prefix + "_virtual_sequence_s {\n" ) );
    
    // In order to get all the sequence declarations we need to query them from the library
    code->append( new QString( "  // Each concrete interface has a SIMPLE sequence. By declaring the SIMPLE sequence \n" ) );
    code->append( new QString( "  // it is possible to run the same SIMPLE sequence kind on each interface for a smoke test\n" ) );
    
    for( spec_code_model *uvc : uvc_list ) {
        env_unit            = uvc->get_env_node();
        cur_uvc_prefix      = uvc -> get_prefix();
        current_struct_name = uvc -> get_struct_like( "any_sequence" ).get_struct_name();
        
        // Reset the driver count variable for each new UVC
        drv_cnt = env_unit->get_active_agents_count();
        total_drv_cnt += drv_cnt;
        
        if( drv_cnt > 0 ) {
            QList<spec_struct_data_node*> agent_nodes = env_unit->get_active_agent_nodes();
            
            any_driver_string = *(uvc->get_all_like_structs_names("any_sequence_driver")).at(0);
            
            if( any_driver_string.isEmpty() ) {
                code->append( new QString( "  // The provided UVC: " + uvc_prefix + "  has no defined driver. Please add driver pointer manually." ) );
            } else {
                // First the sequence needs to be instantiated
                if( current_struct_name != "" ) {
                    code->append( new QString( "  !simple_" + current_struct_name + " : " + current_struct_name + ";\n") );
                    
                    // Storing the sequence object and its type name in lists...
                    seq_object_names.append( "simple_" + current_struct_name );
                    seq_struct_names.append( current_struct_name );
                };
                
                code->append( new QString( "  // Instantiate concrete drivers of UVC:  " + uvc->get_prefix()+ "\n"));
                for( int index = 0; index < drv_cnt;++index ) {
                    // Cycle through each instantiated/configured driver
                    body_code.append( "    do simple_" + current_struct_name + " on driver.p_" + cur_uvc_prefix + "_drv" + QString::number(index) +";\n" );
                };
            };
        };
        
    };
    // First we'll declare all the sequence objects
    
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  // Now we are going to drive each of the sequences one after another on their respective drivers\n" ) );
    code->append( new QString( "  run_main()@driver.clock is {\n" ) );
    for( QString bcode : body_code ) {
        code->append( new QString(bcode) );
        
    };
    code->append( new QString( "  };   // end of run_main method\n" ) );
    
    code->append( new QString( "};   // end of " + uvc_prefix + "_virtual_sequence_s\n" ) );
    code->append( new QString( "'>\n" ) );
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        mod_model->get_directory() + "/" + uvc_prefix + "/tests/smoke_test.e",
        code
    );
}


void spec_e_module_code_generator::create_module_verilog_testbench_file        ( spec_module_model         *mod_model ) {
    QString                 uvc_prefix          = mod_model->get_prefix();
    QList <QString*>        *code               = new QList <QString*>;
    QList <QString*>        *initial_code       = new QList <QString*>;
    QString                 clock_domain_name   ;
    QString                 reset_domain_name   ;
    bool                    enable_clock_uvc    = mod_model -> get_enable_clock_uvc();
    bool                    enable_reset_uvc    = mod_model -> get_enable_reset_uvc();
    
    spec_msg("Generating file tb_top.sv");
    
    code->append( new QString( "`resetall\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "// File: "+ uvc_prefix + "/hdl_tb_top.sv\n" ) );
    code->append( new QString( "module " + mod_model->get_hdl_tb_top() + ";\n" ) );
    code->append( new QString( "  \n" ) );
    
    // Instantiate the clock domains first
    if( enable_clock_uvc ) {
        spec_clock_model    *clk_mod            = mod_model->get_clock_model();
        int                 clock_domain_count  = clk_mod->get_clock_domain_count();
        
        code->append( new QString( "  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////\n" ) );
        code->append( new QString( "  // CLOCK DOMAINS\n" ) );
        
        // Iterate over each defined clock domain
        for( int i = 0; i < clock_domain_count; ++i ) {
            clock_domain_name   = clk_mod->get_clock_domain_name(i).toUpper();
            
            // Create one common seed bus for all clock UVCs
            if( i == 0 ) {
                code->append( new QString( "  // Contains the 64 bit seed to enable different randomization offsets\n" ) );
                code->append( new QString( "  reg [63:0]        sn_seed;\n" ) );
                code->append( new QString( "  \n" ) );
            };
            
            code->append( new QString( "  // CLOCK DOMAIN ===> " + clock_domain_name + "\n" ) );
            code->append( new QString( "  // Domain clock signal\n" ) );
            code->append( new QString( "  wire              " + clk_mod->get_clock_domain_hdl_name(i) +";\n" ) );
            code->append( new QString( "  \n" ) );
            code->append( new QString( "  // Domain jitter enable signal\n" ) );
            code->append( new QString( "  reg               " + clock_domain_name + "_clk_jitter_enable;\n" ) );
            code->append( new QString( "  \n" ) );
            code->append( new QString( "  // Domain clock shift bus\n" ) );
            code->append( new QString( "  reg [31:0]        " + clock_domain_name + "_clk_shift;\n" ) );
            code->append( new QString( "  \n" ) );
            code->append( new QString( "  // Domain clock period bus\n" ) );
            code->append( new QString( "  reg [31:0]        " + clock_domain_name + "_clk_period;\n" ) );
            code->append( new QString( "  \n" ) );
            code->append( new QString( "  // Domain clock init value signal\n" ) );
            code->append( new QString( "  reg               " + clock_domain_name + "_clk_init;\n" ) );
            code->append( new QString( "  \n" ) );
            code->append( new QString( "  // Domain clock generator instance\n" ) );
            code->append( new QString( "  clock_generator clk_" + clk_mod->get_clock_domain_name(i).toUpper() + "_I0(\n" ) );
            code->append( new QString( "    .seed            ( sn_seed         ),\n" ) );
            code->append( new QString( "    .clk_jitter_en   ( " + clock_domain_name + "_clk_jitter_enable ),\n" ) );
            code->append( new QString( "    .clk_shift       ( " + clock_domain_name + "_clk_shift         ),\n" ) );
            code->append( new QString( "    .clk_period      ( " + clock_domain_name + "_clk_period        ),\n" ) );
            code->append( new QString( "    .clk_init        ( " + clock_domain_name + "_clk_init          ),\n" ) );
            code->append( new QString( "    .clk             ( " + clk_mod->get_clock_domain_hdl_name(i) + "           )\n" ) );
            code->append( new QString( "  );\n" ) );
            if( i < clock_domain_count-1 ) {
                code->append( new QString( "  \n" ) );
                code->append( new QString( "  \n" ) );
            };
        };   // end of iterating over each defined clock domain
        code->append( new QString( "  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////\n" ) );
    };   // end of generating clock UVC
    
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  \n" ) );
    
    // Instantiate the reset domains next
    if( enable_reset_uvc ) {
        spec_reset_model    *res_mod            = mod_model->get_reset_model();
        int                 reset_domain_count  = res_mod ->get_reset_domain_count();
        
        code->append( new QString( "  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////\n" ) );
        code->append( new QString( "  // RESET DOMAINS\n" ) );
        
        // Iterate over each define reset domain
        for( int i = 0; i < reset_domain_count; ++i ) {
            reset_domain_name = res_mod->get_reset_domain_name(i ).toUpper();
            code->append( new QString( "  // RESET DOMAIN ===> " + reset_domain_name + "\n" ) );
            code->append( new QString( "  reg           " + reset_domain_name + "_" + res_mod->get_reset_domain_hdl_name(i) + ";\n" ) );
            
            // Also generating code that is used in the initial block
            if( i == 0 ) {
                initial_code->append( new QString( "    // Initialize reset values for each domain\n" ) );
            };
            initial_code->append( new QString( "    " + reset_domain_name + "_" + res_mod->get_reset_domain_hdl_name(i) + " = 1'b" + res_mod->get_reset_domain_init_value(i) + ";\n" ) );
            if( i == reset_domain_count-1 ) {
                initial_code->append( new QString("    \n") );
                initial_code->append( new QString("    \n") );
            };
            code->append( new QString( "  \n" ) );
        };   // end of generating reset domain levels
        code->append( new QString( "  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////\n" ) );
    };   // end of generating reset uvc
    
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////\n" ) );
    code->append( new QString( "  // Add DUT specific signals in this section\n" ) );
    code->append( new QString( "  // Please note that all the signals are auto-generated/extracted from the GUI configuration!\n" ) );
    code->append( new QString( "  // TODO: Ensure the signals' polarity (wire or reg/logic) are properly indicated and change manually, if necessary!!!\n" ) );
    // TODO: Extract configuration information and provide instantiated Verilog signals here
    QList<spec_code_model*>         uvc_list            = mod_model -> get_code_model_list();
    QList<spec_struct_data_node*>   complete_agent_list ;
    spec_struct_data_node           *smp_node;
    spec_struct_data_node           *port_node;
    spec_event_port_widget          *event_port_widget;
    spec_simple_port_widget         *simple_port_widget;
    
    for( spec_code_model *uvc : uvc_list ) {
        complete_agent_list = uvc->get_env_node()->get_passive_agent_nodes();
        complete_agent_list.append( uvc->get_env_node()->get_active_agent_nodes() );
        code->append( new QString( "  //-----------------------------------------------------------------------------------------------\n" ) );
        code->append( new QString( "  // Signal Declaration for Interface UVC " + uvc->get_prefix().toUpper() + "\n") );
        for( spec_struct_data_node *agent : complete_agent_list ) {
            code->append( new QString( "  // UVC Agent Interface " + uvc->get_prefix().toUpper() + " -> " + agent->get_agent_widget()->get_selected_interface_domain() + " " + agent->get_object_name() + "\n") );
            // Now we are iterating over each agent... but for now we are completely ignoring any HDL path configuration options Ü
            for( int agent_child_index = 0; agent_child_index < agent->childCount();++agent_child_index ) {
                smp_node = agent->get_child( agent_child_index );
                if( smp_node->get_like_name() == "uvm_signal_map" ) {
                    for( int smp_child_index = 0; smp_child_index < smp_node->childCount(); ++smp_child_index ) {
                        port_node = smp_node->get_child( smp_child_index );
                        
                        if( port_node->get_like_name() == "any_simple_port" ) {
                            simple_port_widget = port_node->get_simple_port_widget();
                            
                            if(
                                not simple_port_widget->is_associated_clock()
                                    and 
                                not simple_port_widget->is_associated_reset()
                            ) {
                                // This determines if the generated signal is generated as either a wire or a reg
                                if( QString::compare( simple_port_widget->get_access_direction(), "Read Only Signal" ) == 0 ) {
                                    // Generating wire... can't write to this signal
                                    code->append( new QString( "  wire " ) );
                                } else {
                                    // Generating reg... writing is explicitly allowed to this signal
                                    code->append( new QString( "  reg  " ) );
                                    initial_code->append( new QString( "    " + simple_port_widget->get_signal_name() + " = 'b0;\n" ) );
                                };
                                
                                if( simple_port_widget->get_signal_type_width() > 1) {
                                    code->append( new QString( "[" + QString::number(simple_port_widget->get_signal_type_width()-1) + ":0] " ) );
                                };
                                
                                code->append( new QString( simple_port_widget->get_signal_name() + ";\n" ) );
                            };  // end of adding non-associated signal to port
                        };
                        
                        if( port_node->get_like_name() == "any_event_port" ) {
                            event_port_widget = port_node->get_event_port_widget();
                            
                            if(
                                not event_port_widget->is_associated_clock()
                                    and 
                                not event_port_widget->is_associated_reset()
                            ) {
                                
                                // Generating reg... writing is explicitly allowed to this signal
                                code->append( new QString( "  reg  " ) );
                                initial_code->append( new QString( "    " + event_port_widget->get_signal_name() + " = 'b0;\n" ) );
                                
                                code->append( new QString( event_port_widget->get_signal_name() + ";\n" ) );
                            };  // end of adding non-associated signal to port
                        };
                    };  // end of iterating each smp child node
                };  // end of handling UVM signal map
            };  // end of iterating over each of the agent's children
            code->append( new QString( "  \n") );
        };  // end of iterating over each agent
    };  // end of iterating over each UVC model

    code->append( new QString( "  \n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////\n" ) );
    code->append( new QString( "  // Add DUT and other component instantiations in this section\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  // Add signal initializations inside this block\n" ) );
    code->append( new QString( "  initial begin\n" ) );
    for( QString *str : *initial_code ) {
        code->append( str );
    };
    code->append( new QString( "    #1337;\n" ) );
    code->append( new QString( "    \n" ) );
    code->append( new QString( "  end\n" ) );
    code->append( new QString( "endmodule\n" ) );
    
    write_file(
        mod_model->get_directory() + "/" + uvc_prefix + "/hdl/tb_top.sv",
        code
    );
}   // end of create_module_verilog_testbench_file



void spec_e_module_code_generator::create_module_script_files                  ( spec_module_model         *mod_model ) {
    QString             uvc_prefix  = mod_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    QString             specman_path;
    
    QList<QString*>     str_list;
    int                 index       = 0;
    int                 elem_count  = 0;
    
    spec_msg("Generating file scripts/run_interactive");
    
    str_list    = mod_model->get_specman_paths();
    elem_count  = str_list.count();
    
    code->append( new QString( "#!/bin/bash \n" ) );
    code->append( new QString( "#echo \"SPECMAN_PATH is ${SPECMAN_PATH}\"  \n" ) );
    code->append( new QString( "export SPECMAN_PATH=\\\n" ) );
    // Add each of the SPECMAN_PATH configured strings
    for( QString *str: str_list ) {
        if( index < (elem_count-1) ) {
            code->append( new QString( *str + ":\\\n" ) );
        } else {
            code->append( new QString( *str + "\n" ) );
        };
        ++index;
    };
    code->append( new QString( "\n" ) );
    code->append( new QString( "export MODULE_UVC=" + mod_model->get_directory() + "\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "xrun \\\n" ) );
    code->append( new QString( "    -access rw\\\n" ) );
    code->append( new QString( "    -createdebugdb\\\n" ) );
    code->append( new QString( "    -snpath ${MODULE_UVC}\\\n" ) );
    if( mod_model->get_enable_clock_uvc() ) {
        code->append( new QString( "    ${MODULE_UVC}/" + mod_model->get_clock_model()->get_prefix() + "/hdl/clk_gen.sv\\\n" ) );
        code->append( new QString( "    -sntimescale 1" + mod_model->get_clock_model()->get_time_resolution() + "\\\n" ) );
    };
    code->append( new QString( "    ${MODULE_UVC}/" + uvc_prefix + "/hdl/tb_top.sv \\\n" ) );
    code->append( new QString( "    -snload ${MODULE_UVC}/" + uvc_prefix + "/e/" + uvc_prefix + "_top.e\\\n" ) );
    code->append( new QString( "    -snload ${MODULE_UVC}/" + uvc_prefix + "/sve/" + uvc_prefix + "_tb_instantiation.e\\\n" ) );
    code->append( new QString( "    -snload ${MODULE_UVC}/" + uvc_prefix + "/tests/smoke_test.e\\\n" ) );
    code->append( new QString( "    -input @\"probe -create [scope -tops] -depth all -tasks -functions -all -shm\"\\\n" ) );
    code->append( new QString( "    -gui\\\n" ) );
    code->append( new QString( "    $*\n" ) );
    
    write_file(
        mod_model->get_directory() + "/" + uvc_prefix + "/scripts/run_interactive",
        code
    );
    
    // Adding execution permission for user and group
    QFile script_file ( mod_model->get_directory() + "/" + uvc_prefix + "/scripts/run_interactive" );
    script_file.setPermissions(
        QFile::ReadUser     |
        QFile::WriteUser    |
        QFile::ExeUser      |
        QFile::ReadGroup    |
        QFile::WriteGroup   |
        QFile::ExeGroup
    );
}   // end of create_module_script_files

void spec_e_module_code_generator::create_module_dvt_build_file         ( spec_module_model    *mod_model ) {
    QString             uvc_prefix  = mod_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    QString             paths_str   ;
    
    spec_msg( "Generating file default.build" );
    
    code->append( new QString("## This file contains build setup to be used with Amiq DVT Plugin for Eclipse\n"));
    code->append( new QString("## You can grab DVT Eclipse from here:\n"));
    code->append( new QString("##   https://dvteclipse.com\n"));
    code->append( new QString("## \n"));
    code->append( new QString("## Documentation on the syntax of this file can be found here:\n"));
    code->append( new QString("##   https://dvteclipse.com/documentation/e/Build_Configurations.html\n"));
    code->append( new QString("## \n"));
    code->append( new QString("## ============================================================================\n"));
    code->append( new QString("## //                         Configuration File Setup                       \\\\\n"));
    code->append( new QString("## ============================================================================\n"));
    code->append( new QString("## \n"));
    code->append( new QString("+dvt_init+ius.irun \n"));
    paths_str = mod_model->get_directory();
    for( QString *str : mod_model->get_specman_paths() ) {
        paths_str.append( paths_str + ":" + *str);
    };
    code->append( new QString( "+dvt_setenv+SPECMAN_PATH=" + paths_str + "\n" ) );
    code->append( new QString( uvc_prefix + "/e/" + uvc_prefix + "_top.e\n") );
    code->append( new QString( uvc_prefix + "/sve/" + uvc_prefix + "_tb_instantiation.e\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "## Adding a simple smoke test to the environment\n") );
    code->append( new QString( "+dvt_test+" + uvc_prefix + "/tests/smoke_test.e\n" ) );
    code->append( new QString("## ============================================================================\n"));
    
    write_file(
        mod_model->get_directory() + "/.dvt/default.build",
        code
    );
}   // end of create_module_dvt_build_file

void spec_e_module_code_generator::create_module_instantiation_file( spec_module_model *mod_model ) {
    QString                 uvc_prefix                      = mod_model->get_prefix();
    
    bool                    enable_clock_uvc                = mod_model -> get_enable_clock_uvc();
    bool                    enable_reset_uvc                = mod_model -> get_enable_reset_uvc();
    
    QList<spec_code_model*> uvc_list                        = mod_model -> get_code_model_list();
    
    spec_struct_data_node   struct_data_node                ;
    QList<spec_field_node*> fields                          ;
    QString                 assembled_string                ;
    spec_struct_data_node   *hierarchy_node                 ;
    spec_struct_data_node   *env_node                       ;
    QString                 uvc_env_name                    ;
    QList<QString*>         config_struct_names             ;
    QList<QString>          subtype_struct_name             ;
    QString                 tlm_port_path                   ;
    QString                 tlm_port_path_calculated        ;
    QList<QString>          tlm_port_paths                  ;
    QList<QString>          connect_pointer_code            ;
    int                     uvc_agent_count                 ;
    int                     field_counter                   = 0;
    int                     agent_counter                   = 0;
    
    spec_msg("Generating file " << (uvc_prefix + "_tb_instantiation.e"));
    
    QList <QString*>    *code = new QList <QString*>;
    
    //    code->append( get_file_header( mod_model ) );
    code->append( new QString( "File: " + uvc_prefix + "_tb_instantiation.e\n") );
    code->append( new QString( "<'\n" ) );
    
    code->append( new QString( "// Import the reusable module-level UVC\n" ) );
    code->append( new QString( "import " + uvc_prefix + "/e/" + uvc_prefix + "_top.e;\n" ) );
    code->append( new QString( "// Import the configuration settings of the module UVC\n" ) );
    code->append( new QString( "import " + uvc_prefix + "/sve/" + uvc_prefix + "_full_config.e;\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "unit " + uvc_prefix + "_sve_env_u like uvm_env {\n" ) );
    
    if( enable_clock_uvc ) {
        code->append( new QString( "  // Instantiate the clock UVC\n" ) );
        code->append( new QString( "  clock_uvc : " + mod_model->get_clock_model()->get_prefix() + "_env_u is instance;\n") );
        code->append( new QString( "  \n") );
    };
    
    if( enable_reset_uvc ) {
        code->append( new QString( "  // Instantiate the reset UVC\n" ) );
        code->append( new QString( "  reset_uvc : " + mod_model->get_reset_model()->get_prefix() + "_env_u is instance;\n") );
        code->append( new QString( "  \n") );
    };
    
    code->append( new QString( "  // Instantiate each UVCs\n" ) );
    for( spec_code_model *uvc : uvc_list ) {
        // Get the current UVC's environment root node
        env_node        = uvc->get_env_node();
        
        // Get the number of agents in total... this is necessary, because both ACTIVE and PASSIVE agents contain
        // monitors that need to be connected to the module monitor.
        uvc_agent_count = env_node->get_active_agents_count() + env_node->get_passive_agents_count();
        
        uvc_env_name    = uvc->get_struct_like("uvm_env").get_struct_name();
        tlm_port_path   = uvc->get_single_hierarchy_path(
            env_node,
            "get_like_name",
            "uvm_agent"
        );
        // We are extracting the name up until uvm_agent so we can handle the named objects
        if( not tlm_port_path.contains( "[0]" ) ) {
            tlm_port_path.append("[0]");
        };
        tlm_port_path.append(".");
        if( env_node->get_active_agents_count() != 0 ) {
            tlm_port_path.append( uvc->get_active_agent_nodes().at(0)->get_monitor_node()->get_object_name() );
        } else {
            tlm_port_path.append( uvc->get_passive_agent_nodes().at(0)->get_monitor_node()->get_object_name() );
        };
        
        // Replace the env struct with the <uvc_prefix>_env_u name
        tlm_port_path.replace(
            uvc->get_struct_like("uvm_env").get_struct_name(),
            uvc->get_prefix() + "_env"
        );
        
        tlm_port_paths.append( "//   => Connecting monitor TLM ports of UVC  " + uvc->get_prefix() + "\n" );
        
        // The loop iterates over each of the configured agents and connects the TLM ports of each interface UVC with
        // the TLM inport in the module monitor
        for( int index = 0; index < uvc_agent_count;++index ) {
            tlm_port_path_calculated = tlm_port_path;
            // Replace the index with the agent_cnt number
            tlm_port_path_calculated.replace(
                "[0]",
                "[" + QString::number(index) + "]"
            );
            for( spec_field_node *field : uvc->get_struct_like("uvm_monitor").get_fields() ) {
                if( field->is_port() ) {
                    if( field->get_port_kind() == "tlm_analysis" ) {
                        // This contains the full path of the interface UVC
                        tlm_port_path_calculated = tlm_port_path_calculated + "." + field->get_field_name();
                        
                        // Now constructing the connection to the module's monitor
                        tlm_port_path_calculated = tlm_port_path_calculated + 
                                ".connect( " + uvc_prefix + "_env.mod_monitor." + uvc->get_prefix() + QString::number(index) + "_tlm_imp );\n";
                        
                        tlm_port_paths.append( tlm_port_path_calculated );
                    };
                };
            };   // end of iterating over each field
        };  // end of calculating the monitor TLM ports.
        
//        code->append( new QString( "  " + uvc->get_prefix() + "_env: " + uvc_prefix.toUpper() + " " +  uvc_env_name + " is instance;\n" ) );
        code->append( new QString( "  " + uvc->get_prefix() + "_env: " +  uvc_env_name + " is instance;\n" ) );
        // Generate the strings that are applied in the connect_pointers phase
        connect_pointer_code.append( "    " + uvc_prefix + "_env.p_" + uvc->get_prefix() + "_env  = " + uvc->get_prefix() + "_env;\n" );
    };   // end of iterating over each loaded UVC and instantiating it
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  // Instantiate the testbench\n" ) );
    code->append( new QString( "  " + uvc_prefix +"_env : " + uvc_prefix + "_env_u is instance;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  connect_pointers() is also {\n" ) );
    
    if( enable_reset_uvc ) {
        code->append( new QString( "    // Connecting the reset UVC to the module level testbench\n" ) );
        code->append( new QString( "    " + uvc_prefix  + "_env.p_" + mod_model->get_reset_model()->get_prefix() + "_env = reset_uvc;\n" ) );
    };
    
    // Now we need to connect all the UVC environment pointers in the module-level UVC
    for( QString str : connect_pointer_code ) {
        code-> append( new QString( str ) );
    };
    code->append( new QString( "  };   // end of connect_pointers\n") );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  connect_ports() is also {\n" ) );
    code->append( new QString( "    // Connect each UVCs TLM analysis port with the monitor TLM analysis implementation\n" ) );
    
    // Iterate over each of the ports and connect them in the pre-calculatd loop of connections
    for( QString str : tlm_port_paths ) {
        code->append( new QString( "    " + str ) );
    };   // end of iterating over each tlm_path
    code->append( new QString( "    \n" ) );
    code->append( new QString( "  };   // end of connect_ports\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "};   // end of " + uvc_prefix + "_sve_env_u\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "// Instantiate the concrete testbench under sys and also configure sys\n" ) );
    code->append( new QString( "extend sys {\n" ) );
    code->append( new QString( "  // This object instantiates the full testbench tree\n" ) );
    code->append( new QString( "  " + uvc_prefix + "_sve_env : " + uvc_prefix +"_sve_env_u is instance;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  keep " + uvc_prefix.toUpper() + "_SVE_ENV_C is all of {\n" ) );
    code->append( new QString( "    " + uvc_prefix + "_sve_env.hdl_path() == \"~/" + mod_model->get_hdl_tb_top() + "\";\n" ) );
    code->append( new QString( "  };   // end of constraint block " + uvc_prefix.toUpper() + "_SVE_ENV_C\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  // Configuring some Specman parameters\n" ) );
    code->append( new QString( "  setup() is also {\n" ) );
    code->append( new QString( "    // Increase the max ticks of the simulation time\n") );
    code->append( new QString( "    set_config( run        , tick_max           , MAX_INT  );\n") );
    code->append( new QString( "    \n") );
    code->append( new QString( "    // Set the default printing format to be hexadecimal\n") );
    code->append( new QString( "    set_config( print      , radix              , hex   );\n") );
    code->append( new QString( "    \n") );
    code->append( new QString( "    // Increase the number of characters a line may have to 2048\n") );
    code->append( new QString( "    set_config( print      , line_size          , 2048  );\n") );
    code->append( new QString( "    \n") );
    code->append( new QString( "    // Increase the number of items that will be shown when a list is printed\n") );
    code->append( new QString( "    set_config( databrowser, list_first_items   , 64k );\n") );
    code->append( new QString( "    \n") );
    code->append( new QString( "    // Set the coverage database to use UNICOV\n") );
    code->append( new QString( "    set_config( cover      , database_format    , ucd   );\n") );
    code->append( new QString( "    \n") );
    code->append( new QString( "    // Enable e code coverage\n") );
    code->append( new QString( "    set_config( cover      , block_coverage_mode, on    );\n") );
    // TODO: Add port unification
    
    
    code->append( new QString( "  };   // end of setup\n" ) );
    code->append( new QString( "};   // end of extend sys\n" ) );
    code->append( new QString( "'>\n" ) );
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        mod_model->get_directory() + "/" + uvc_prefix + "/sve/" + uvc_prefix + "_tb_instantiation.e",
        code
    );
}   // end of create_module_instantiation_file

void spec_e_module_code_generator::create_module_uvc( spec_module_model *mod_model ) {
    QString uvc_directory = mod_model->get_directory();
    QString uvc_prefix    = mod_model->get_prefix();
    
    spec_msg("Now generating MODULE UVC " << uvc_prefix << " in selected folder " << uvc_directory);
    
    QDir uvc_dir( uvc_directory );
    uvc_dir.mkdir( uvc_directory + "/.dvt"       );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix                 );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/doc"        );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/e"          );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/hdl"        );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/scripts"    );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/sve"        );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/tests"      );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/vplan"      );
    
    bool                enable_register_memory_model    = mod_model -> get_enable_register_model();
    bool                enable_clock_uvc                = mod_model -> get_enable_clock_uvc();
    bool                enable_reset_uvc                = mod_model -> get_enable_reset_uvc();
    bool                enable_reference_model          = mod_model -> get_enable_reference_model();
    bool                enable_scoreboard               = mod_model -> get_enable_scoreboard();
    bool                enable_uvm_scoreboard           = mod_model -> get_enable_uvm_scoreboard();
    
    // Creating files in folder uvc_directory + "/" + uvc_prefix + "/e"
    //==================================================================================================================
    create_module_top_file              ( mod_model );
    
    //==================================================================================================================
    create_module_defines_file          ( mod_model );
    
    //==================================================================================================================
    create_module_types_file            ( mod_model );
    
    //==================================================================================================================
    create_module_macros_file           ( mod_model );
    
    //==================================================================================================================
    create_module_config_file           ( mod_model );
    
    //==================================================================================================================
    if( enable_reference_model ) {
        create_module_reference_model_top_file      ( mod_model );
        create_module_reference_model_shell_file    ( mod_model );
    };
    
    //==================================================================================================================
    if( enable_scoreboard ) {
        create_module_scoreboard_file               ( mod_model );
    };
    
    //==================================================================================================================
    create_module_monitor_file          ( mod_model );
    
    //==================================================================================================================
    create_module_virtual_driver_file   ( mod_model );
    
    //==================================================================================================================
    create_module_sequence_lib_file     ( mod_model );
    
    //==================================================================================================================
    create_module_env_file              ( mod_model );
    
    //==================================================================================================================
    create_module_objections_file       ( mod_model );
    
    //==================================================================================================================
    //create_module_check_file( mod_model );
    
    //==================================================================================================================
    //create_module_coverage_file( mod_model );
    
    
    // uvc_directory + "/" + uvc_prefix + "/sve"
    //==================================================================================================================
    create_module_sve_config_file       ( mod_model );
    
    //==================================================================================================================
    create_module_instantiation_file    ( mod_model );
    
    
    // uvc_directory + "/" + uvc_prefix + "/tests"
    //==================================================================================================================
    create_module_smoke_test_file       ( mod_model );
    
    
    // uvc_directory + "/" + uvc_prefix + "/scripts"
    //=================================================================================================
    create_module_script_files          ( mod_model );
    
    // uvc_directory + "/" + uvc_prefix + "/hdl"
    //=================================================================================================
    create_module_verilog_testbench_file( mod_model );
    
    
    // uvc_directory + "/" + uvc_prefix + "/hdl"
    //=================================================================================================
    //create_module_vhdl_testbench_files( mod_model );
    
    
    // uvc_directory + "/" + uvc_prefix + "/.dvt
    //=================================================================================================
    create_module_dvt_build_file        ( mod_model );
}   // end of create_module_uvc
