// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_e_reset_uvc_generator.h"

spec_e_reset_uvc_generator::spec_e_reset_uvc_generator() {
    
}   // end of spec_e_reset_uvc_generator constructor


void spec_e_reset_uvc_generator::create_reset_top_file               ( spec_reset_model *res_model ) {
    QString             uvc_prefix  = res_model -> get_prefix();
    QList <QString*>    *code   = new QList <QString*>;
    
    qDebug() << "Generating file " << (uvc_prefix + "_top.e");
    
    // TODO: Add a customizable header string generator in here
    code -> append( new QString("File: " + uvc_prefix + "_top.e" + "\n") );
    code -> append( new QString("<'\n") );
    code -> append( new QString("// Importing the UVM-e library\n") );
    code -> append( new QString("import uvm_e/e/uvm_e_top;\n") );
    code -> append( new QString("\n") );
    code -> append( new QString("// Importing the reusable reset environment files\n") );
    code -> append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_types;\n") );
    code -> append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_agent_config;\n") );
    code -> append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_item;\n") );
    code -> append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_signal_map;\n") );
    code -> append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_monitor;\n") );
    code -> append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_driver;\n") );
    code -> append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_bfm;\n") );
    code -> append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_agent;\n") );
    code -> append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_env;\n") );
//    code -> append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_coverage;\n") );
//    code -> append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_reset_objections;\n") );
//    code -> append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_sequences;\n") );
    code -> append( new QString("'>\n") );
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        res_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_top.e",
        code
    );
}   // end of create_reset_top_file


void spec_e_reset_uvc_generator::create_reset_types_file             ( spec_reset_model *res_model ) {
    QString             uvc_prefix  = res_model -> get_prefix();
    QList <QString*>    *code   = new QList <QString*>;
    
    qDebug() << "Generating file " << (uvc_prefix + "_types.e");
    
    // TODO: Add a customizable header string generator in here
    code -> append( new QString("File: " + uvc_prefix + "_types.e" + "\n") );
    code -> append( new QString("<'\n") );
    code -> append( new QString("type " + uvc_prefix + "_env_name_t         : [NONE];\n") );
    code -> append( new QString("type " + uvc_prefix + "_domain_name_t: [NONE];\n") );
    code -> append( new QString("type " + uvc_prefix + "_active_level_t     : [RESET_HI, RESET_LO];\n") );
    code -> append( new QString("type " + uvc_prefix + "_synchronicity_t    : [RESET_SYNC, RESET_ASYNC];\n") );
    code -> append( new QString("'>\n"));
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        res_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_types.e",
        code
    );
}   // end of create_reset_types_file


void spec_e_reset_uvc_generator::create_reset_configuration_file     ( spec_reset_model *res_model ) {
    QString             uvc_prefix  = res_model -> get_prefix();
    QList <QString*>    *code   = new QList <QString*>;
    
    qDebug() << "Generating file " << (uvc_prefix + "_agent_config.e");
    
    // TODO: Add a customizable header string generator in here
    code -> append( new QString("File: " + uvc_prefix + "_agent_config.e" + "\n") );
    code -> append( new QString("<'\n") );
    code -> append( new QString("struct " + uvc_prefix + "_agent_config_s like uvm_config_params_s {\n") );
    code -> append( new QString("  if_name            : " + uvc_prefix + "_domain_name_t;\n") );
    code -> append( new QString("  has_checks         : bool;\n") );
    code -> append( new QString("  has_coverage       : bool;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  agent_id           : uint(bits: 32);\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  reset_level        : " + uvc_prefix + "_active_level_t;\n") );
    code -> append( new QString("  reset_synchronicity: " + uvc_prefix + "_synchronicity_t;\n") );
    code -> append( new QString("};   // end of " + uvc_prefix + "_agent_config_s\n") );
    code -> append( new QString("'>\n"));
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        res_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_agent_config.e",
        code
    );
}   // end of create_reset_configuration_file


void spec_e_reset_uvc_generator::create_reset_signal_map_file        ( spec_reset_model *res_model ) {
    QString             uvc_prefix  = res_model -> get_prefix();
    QList <QString*>    *code   = new QList <QString*>;
    
    qDebug() << "Generating file " << (uvc_prefix + "_signal_map.e");
    
    // TODO: Add a customizable header string generator in here
    code -> append( new QString("File: " + uvc_prefix + "_signal_map.e" + "\n") );
    code -> append( new QString("<'\n") );
    code -> append( new QString("unit " + uvc_prefix + "_signal_map_u like uvm_signal_map {\n") );
    code -> append( new QString("  ////////////////////////////////////////////////////////////////////////////\n") );
    code -> append( new QString("  // Fields\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  agent_config : " + uvc_prefix + "_agent_config_s;") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  if_name      : " + uvc_prefix + "_domain_name_t;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  clock_iep    : in    event_port         is instance;\n") );
    code -> append( new QString("  reset_iop    : inout simple_port of bit is instance;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  keep DEFAULT_RESET_SMP_HDL_PATH_C is all of {\n") );
    code -> append( new QString("    soft reset_iop.hdl_path() == \"reset\";\n") );
    code -> append( new QString("    soft clock_iep.hdl_path() == \"clock\";\n") );
    code -> append( new QString("    soft clock_iep.edge()     == rise;\n") );
    code -> append( new QString("  };   // end of DEFAULT_RESET_SMP_HDL_PATH_C constraint block\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  keep DEFAULT_RESET_SMP_BIND_C is all of {\n") );
    code -> append( new QString("    bind( clock_iep, external );\n") );
    code -> append( new QString("    bind( reset_iop, external );\n") );
    code -> append( new QString("  };   // end of DEFAULT_RESET_SMP_BIND_C constraint block\n") );
    code -> append( new QString("};   // end of " + uvc_prefix + "_reset_signal_map_u\n") );
    code -> append( new QString("'>\n"));
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        res_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_signal_map.e",
        code
    );
}   // end of create_reset_signal_map_file


void spec_e_reset_uvc_generator::create_reset_driver_file            ( spec_reset_model *res_model ) {
    QString             uvc_prefix  = res_model -> get_prefix();
    QList <QString*>    *code   = new QList <QString*>;
    
    qDebug() << "Generating file " << (uvc_prefix + "_driver.e");
    
    // TODO: Add a customizable header string generator in here
    code -> append( new QString("File: " + uvc_prefix + "_driver.e" + "\n") );
    code -> append( new QString("<'\n") );
    code -> append( new QString("sequence " + uvc_prefix + "_sequence_s using\n") );
    code -> append( new QString("  item           = " + uvc_prefix + "_item_s,\n") );
    code -> append( new QString("  created_kind   = " + uvc_prefix + "_sequence_kind_t,\n") );
    code -> append( new QString("  created_driver = " + uvc_prefix + "_driver_u;\n") );
    code -> append( new QString("\n") );
    code -> append( new QString("extend " + uvc_prefix + "_driver_u {\n") );
    code -> append( new QString("  if_name            : " + uvc_prefix + "_domain_name_t;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  !p_smp        : " + uvc_prefix + "_signal_map_u;\n") );
    code -> append( new QString("  !agent_config : " + uvc_prefix + "_agent_config_s;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  event clock is only @p_smp.clock_iep$;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  short_name(): string is also {\n") );
    code -> append( new QString("    result = \"DRV\";\n") );
    code -> append( new QString("  };   // end of short_name extension\n") );
    code -> append( new QString("};   // end of " + uvc_prefix + "_driver_u\n") );
    code -> append( new QString("'>\n"));
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        res_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_driver.e",
        code
    );
}   // end of create_reset_driver_file


void spec_e_reset_uvc_generator::create_reset_monitor_file           ( spec_reset_model *res_model ) {
    QString             uvc_prefix  = res_model -> get_prefix();
    QList <QString*>    *code   = new QList <QString*>;
    
    qDebug() << "Generating file " << (uvc_prefix + "_monitor.e");
    
    // TODO: Add a customizable header string generator in here
    code -> append( new QString("File: " + uvc_prefix + "_monitor.e" + "\n") );
    code -> append( new QString("<'\n") );
    code -> append( new QString("unit " + uvc_prefix + "_monitor_u like uvm_monitor {\n") );
    code -> append( new QString("  if_name            : " + uvc_prefix + "_domain_name_t;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  agent_config       : " + uvc_prefix + "_agent_config_s;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  has_checks         : bool;\n") );
    code -> append( new QString("  has_coverge        : bool;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  !p_smp             : " + uvc_prefix + "_signal_map_u;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  !reset_start_times : list of time;\n") );
    code -> append( new QString("  !reset_end_times   : list of time;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  event reset_rise_e is rise( p_smp.reset_iop$ )@sim;\n") );
    code -> append( new QString("  event reset_fall_e is fall( p_smp.reset_iop$ )@sim;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  // Detecting a rising edge can either be asserting or de-asserting the reset.\n") );
    code -> append( new QString("  on reset_rise_e {\n") );
    code -> append( new QString("    if( agent_config.reset_level == RESET_HI ) {\n") );
    code -> append( new QString("      reset_start_times.add( sys.time );\n") );
    code -> append( new QString("    };\n") );
    code -> append( new QString("    \n") );
    code -> append( new QString("    if( agent_config.reset_level == RESET_LO ) {\n") );
    code -> append( new QString("      reset_end_times.add( sys.time );\n") );
    code -> append( new QString("    };\n") );
    code -> append( new QString("    \n") );
    code -> append( new QString("  };   // end of handling event reset_rise_e\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  // Detecting a falling edge can either be asserting or de-asserting the reset.\n") );
    code -> append( new QString("  on reset_fall_e {\n") );
    code -> append( new QString("    if( agent_config.reset_level == RESET_HI ) {\n") );
    code -> append( new QString("      reset_end_times.add( sys.time );\n") );
    code -> append( new QString("    };\n") );
    code -> append( new QString("    \n") );
    code -> append( new QString("    if( agent_config.reset_level == RESET_LO ) {\n") );
    code -> append( new QString("      reset_start_times.add( sys.time );\n") );
    code -> append( new QString("    };\n") );
    code -> append( new QString("  };   // end of handling event reset_fall_e\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  short_name(): string is also {\n") );
    code -> append( new QString("    result = \"MON\";\n") );
    code -> append( new QString("  };   // end of extending built-in short_name\n") );
    code -> append( new QString("};   // end of " + uvc_prefix + "_monitor_u\n") );
    code -> append( new QString("'>\n"));
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        res_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_monitor.e",
        code
    );
}   // end of create_reset_monitor_file


void spec_e_reset_uvc_generator::create_reset_env_file               ( spec_reset_model *res_model ) {
    QString             uvc_prefix  = res_model -> get_prefix();
    QList <QString*>    *code   = new QList <QString*>;
    
    qDebug() << "Generating file " << (uvc_prefix + "_env.e");
    
    // TODO: Add a customizable header string generator in here
    code -> append( new QString("File: " + uvc_prefix + "_env.e" + "\n") );
    code -> append( new QString("<'\n") );
    code -> append( new QString("unit " + uvc_prefix + "_env_u like uvm_env {\n") );
    code -> append( new QString("  env_name  : " + uvc_prefix + "_env_name_t;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  agents    : list of " + uvc_prefix + "_agent_u is instance;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  keep RESET_AGENTS_C is all of {\n") );
    code -> append( new QString("    soft agents.size() == 1;\n") );
    code -> append( new QString("    for each (agent) in agents {\n") );
    code -> append( new QString("      agent.agent_config.agent_id == index;\n") );
    code -> append( new QString("    };\n") );
    code -> append( new QString("  };   // end of constraint block RESET_AGENTS_C\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  short_name(): string is also {\n") );
    code -> append( new QString("    result = append( env_name, \"_ENV\");\n") );
    code -> append( new QString("  };   // end of extending built-in short_example\n") );
    code -> append( new QString("};   // end of " + uvc_prefix + "_env_u\n") );
    code -> append( new QString("'>\n"));
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        res_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_env.e",
        code
    );
}   // end of create_reset_env_file


void spec_e_reset_uvc_generator::create_reset_agent_file             ( spec_reset_model *res_model ) {
    QString             uvc_prefix  = res_model -> get_prefix();
    QList <QString*>    *code   = new QList <QString*>;
    
    qDebug() << "Generating file " << (uvc_prefix + "_agent.e");
    
    // TODO: Add a customizable header string generator in here
    code -> append( new QString("File: " + uvc_prefix + "_agent.e" + "\n") );
    code -> append( new QString("<'\n") );
    code -> append( new QString("unit " + uvc_prefix + "_agent_u like uvm_agent {\n"));
    code -> append( new QString("  if_name     : " + uvc_prefix + "_domain_name_t;\n"));
    code -> append( new QString("  \n"));
    code -> append( new QString("  smp         : " + uvc_prefix + "_signal_map_u is instance;\n"));
    code -> append( new QString("  mon         : " + uvc_prefix + "_monitor_u    is instance;\n"));
    code -> append( new QString("  \n"));
    code -> append( new QString("  agent_config:  " + uvc_prefix + "_agent_config_s;\n"));
    code -> append( new QString("  \n"));
    code -> append( new QString("  keep MY_EX_AGENT_IF_NAME_C is all of {\n"));
    code -> append( new QString("    agent_config.if_name  == read_only( if_name );\n"));
    code -> append( new QString("    \n"));
    code -> append( new QString("    smp.if_name           == read_only( if_name );\n"));
    code -> append( new QString("    smp.agent_config      == read_only( agent_config );\n"));
    code -> append( new QString("    \n"));
    code -> append( new QString("    mon.if_name           == read_only( if_name );\n"));
    code -> append( new QString("    mon.agent_config      == read_only( agent_config );\n"));
    code -> append( new QString("  };  // end of MY_EX_AGENT_IF_NAME_C constraint group\n"));
    code -> append( new QString("  \n"));
    code -> append( new QString("  connect_pointers() is also {\n"));
    code -> append( new QString("    mon.p_smp = smp;\n"));
    code -> append( new QString("  };  // end of connect_pointers\n"));
    code -> append( new QString("  \n"));
    code -> append( new QString("  when ACTIVE {\n"));
    code -> append( new QString("    bfm: " + uvc_prefix + "_bfm_u    is instance;\n"));
    code -> append( new QString("    drv: " + uvc_prefix + "_driver_u is instance;\n"));
    code -> append( new QString("    \n"));
    code -> append( new QString("    keep MY_EX_ACTIVE_AGENT_IF_NAME_C is all of {\n"));
    code -> append( new QString("      bfm.if_name      == read_only( if_name );\n"));
    code -> append( new QString("      bfm.agent_config == read_only( agent_config );\n"));
    code -> append( new QString("      \n"));
    code -> append( new QString("      drv.if_name      == read_only( if_name );\n"));
    code -> append( new QString("      drv.agent_config == read_only( agent_config );\n"));
    code -> append( new QString("    };  // end of MY_EX_ACTIVE_AGENT_IF_NAME_C constraint group\n"));
    code -> append( new QString("    \n"));
    code -> append( new QString("    connect_pointers() is also {\n"));
    code -> append( new QString("      bfm.p_smp = smp;\n"));
    code -> append( new QString("      drv.p_smp = smp;\n"));
    code -> append( new QString("      bfm.p_drv = drv;\n"));
    code -> append( new QString("    };  // end of connect_pointers\n"));
    code -> append( new QString("  };  // end of when ACTIVE subtype\n"));
    code -> append( new QString("  \n") );
    code -> append( new QString("  short_name():string is also {\n") );
    code -> append( new QString("    result = append( if_name );\n") );
    code -> append( new QString("  };  // end of short_name\n") );
    code -> append( new QString("};  // end of " + uvc_prefix + "_agent_u\n"));
    code -> append( new QString("'>\n"));
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        res_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_agent.e",
        code
    );
}   // end of create_reset_agent_file


void spec_e_reset_uvc_generator::create_reset_bfm_file               ( spec_reset_model *res_model ) {
    QString             uvc_prefix  = res_model -> get_prefix();
    QList <QString*>    *code   = new QList <QString*>;
    
    qDebug() << "Generating file " << (uvc_prefix + "_bfm.e");
    
    // TODO: Add a customizable header string generator in here
    code -> append( new QString("File: " + uvc_prefix + "_bfm.e" + "\n") );
    code -> append( new QString("<'\n") );
    code -> append( new QString("unit " + uvc_prefix + "_bfm_u like uvm_bfm {\n") );
    code -> append( new QString("  agent_config : " + uvc_prefix + "_agent_config_s;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  if_name      : " + uvc_prefix + "_domain_name_t;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  !p_smp       : " + uvc_prefix + "_signal_map_u;\n") );
    code -> append( new QString("  !p_drv       : " + uvc_prefix + "_driver_u;\n") );
    code -> append( new QString("  !reset_item  : " + uvc_prefix + "_item_s;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  event clock_e is @p_smp.clock_iep$;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  drive()@clock_e is {\n") );
    code -> append( new QString("    // Reset operations are accompanied by objections to avoid premature simulation stop.\n") );
    code -> append( new QString("    raise_objection( TEST_DONE );\n") );
    code -> append( new QString("    \n") );
    code -> append( new QString("    var async_item: RESET_ASYNC " + uvc_prefix + "_item_s;\n") );
    code -> append( new QString("    var sync_item : RESET_SYNC  " + uvc_prefix + "_item_s;\n") );
    code -> append( new QString("    \n") );
    code -> append( new QString("    message(LOW, \"Reset Offset Phase ...\");\n") );
    code -> append( new QString("    \n") );
    code -> append( new QString("    if( reset_item is a RESET_ASYNC " + uvc_prefix +"_item_s ) {\n") );
    code -> append( new QString("      async_item = reset_item.as_a(RESET_ASYNC " + uvc_prefix + "_item_s);\n") );
    code -> append( new QString("      // Waiting for the offset time to pass\n") );
    code -> append( new QString("      wait delay(async_item.reset_duration_time);\n") );
    code -> append( new QString("    };\n") );
    code -> append( new QString("    \n") );
    code -> append( new QString("    if( reset_item is a RESET_SYNC " + uvc_prefix + "_item_s ) {\n") );
    code -> append( new QString("      sync_item = reset_item.as_a(RESET_SYNC " + uvc_prefix + "_item_s);\n") );
    code -> append( new QString("      // Waiting for the offset number of clock cycles\n") );
    code -> append( new QString("      wait [sync_item.reset_offset_count];\n") );
    code -> append( new QString("    };\n") );
    code -> append( new QString("    \n") );
    code -> append( new QString("    message(LOW, \"Assert Reset ...\");\n") );
    code -> append( new QString("    // Set the reset signal level according to the configured reset activity level\n") );
    code -> append( new QString("    if( agent_config.reset_level == RESET_HI ) {\n") );
    code -> append( new QString("      p_smp.reset_iop$ = 1'b1;\n") );
    code -> append( new QString("    };\n") );
    code -> append( new QString("    if( agent_config.reset_level == RESET_LO ) {\n") );
    code -> append( new QString("      p_smp.reset_iop$ = 1'b0;\n") );
    code -> append( new QString("    };\n") );
    code -> append( new QString("    \n") );
    code -> append( new QString("    message(LOW, \"Reset Asserted Phase...\");\n") );
    code -> append( new QString("    \n") );
    code -> append( new QString("    // Active RESET phase\n") );
    code -> append( new QString("    if( sync_item != NULL ) {\n") );
    code -> append( new QString("      // Simply wait for the number of clock edges\n") );
    code -> append( new QString("      wait [reset_item.as_a(RESET_SYNC " + uvc_prefix + "_item_s).reset_clock_edge_count];\n") );
    code -> append( new QString("    };\n") );
    code -> append( new QString("    \n") );
    code -> append( new QString("    if( async_item != NULL ) {\n") );
    code -> append( new QString("      // In RESET_ASYNC mode it is possible to de-assert the reset at any time\n") );
    code -> append( new QString("      wait delay(reset_item.as_a(RESET_ASYNC " + uvc_prefix + "_item_s).reset_duration_time);\n") );
    code -> append( new QString("    };\n") );
    code -> append( new QString("    \n") );
    code -> append( new QString("    message(LOW, \"De-asserting Reset...\");\n") );
    code -> append( new QString("    \n") );
    code -> append( new QString("    // Now de-assert the reset. Again distinguish according to the reset activity level\n") );
    code -> append( new QString("    if( agent_config.reset_level == RESET_HI ) {\n") );
    code -> append( new QString("      p_smp.reset_iop$ = 1'b0;\n") );
    code -> append( new QString("    };\n") );
    code -> append( new QString("    if( agent_config.reset_level == RESET_LO ) {\n") );
    code -> append( new QString("      p_smp.reset_iop$ = 1'b1;\n") );
    code -> append( new QString("    };\n") );
    code -> append( new QString("    \n") );
    code -> append( new QString("    // Reset operations are accompanied by objections to avoid premature simulation stop.\n") );
    code -> append( new QString("    drop_objection( TEST_DONE );\n") );
    code -> append( new QString("  };   // end of drive() method\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  request_and_drive_reset()@clock_e is {\n") );
    code -> append( new QString("    while( TRUE ) {\n") );
    code -> append( new QString("      reset_item = p_drv.get_next_item();\n") );
    code -> append( new QString("      drive();\n") );
    code -> append( new QString("      emit p_drv.item_done;\n") );
    code -> append( new QString("    };   // end of perpetual while loop\n") );
    code -> append( new QString("  };   // end of request_and_drive_reset\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  run() is also {\n") );
    code -> append( new QString("    start request_and_drive_reset();\n") );
    code -> append( new QString("  };   // end of run method extension\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  short_name(): string is also {\n") );
    code -> append( new QString("    result = \"BFM\";\n") );
    code -> append( new QString("  };   // end of short_name() extension\n") );
    code -> append( new QString("};   // end of " + uvc_prefix + "_bfm_u\n") );
    code -> append( new QString("'>\n"));
    code -> append( new QString("\n") );
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        res_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_bfm.e",
        code
    );
}   // end of create_reset_bfm_file


void spec_e_reset_uvc_generator::create_reset_coverage_file          ( spec_reset_model *res_model ) {
    QString             uvc_prefix  = res_model -> get_prefix();
    QList <QString*>    *code   = new QList <QString*>;

    qDebug() << "Generating file " << (uvc_prefix + "_coverage.e");
    // TODO: Add a customizable header string generator in here
    code -> append( new QString("File: " + uvc_prefix + "_coverage.e" + "\n") );
    code -> append( new QString("<'\n") );

    // TODO: Add generation logic!!!!!!

    code -> append( new QString("'>\n"));
    code -> append( new QString("\n") );
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );

    // Write the file to the user-declared directory
    write_file(
        res_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_coverage.e",
        code
    );
}   // end of create_reset_coverage_file
void spec_e_reset_uvc_generator::create_reset_defines_file           ( spec_reset_model *res_model ) {
    
}   // end of create_reset_defines_file


void spec_e_reset_uvc_generator::create_reset_item_file              ( spec_reset_model *res_model ) {
    QString             uvc_prefix  = res_model -> get_prefix();
    QList <QString*>    *code   = new QList <QString*>;
    
    qDebug() << "Generating file " << (uvc_prefix + "_item.e");
    
    // TODO: Add a customizable header string generator in here
    code -> append( new QString("File: " + uvc_prefix + "_item.e" + "\n") );
    code -> append( new QString("<'\n") );
    code -> append( new QString("struct " + uvc_prefix + "_item_s like any_sequence_item {\n") );
    code -> append( new QString("  // Indicates if reset is synchronous or asynchronous\n") );
    code -> append( new QString("  reset_synchronicity: " + uvc_prefix + "_synchronicity_t;\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  when RESET_SYNC'reset_synchronicity {\n") );
    code -> append( new QString("    // Indicates in how many triggering clock edges the reset will be asserted\n") );
    code -> append( new QString("    reset_offset_count    : uint(bits: 32);\n") );
    code -> append( new QString("    // Indicates the number of triggering clock edges\n") );
    code -> append( new QString("    reset_clock_edge_count: uint(bits: 32);\n") );
    code -> append( new QString("  };   // end of subtyping block RESET_SYNC'reset_synchronicity\n") );
    code -> append( new QString("  \n") );
    code -> append( new QString("  when RESET_ASYNC'reset_synchronicity {\n") );
    code -> append( new QString("    // Indicates the time offset for the reset to be asserted\n") );
    code -> append( new QString("    reset_offset_time     : time;\n") );
    code -> append( new QString("    // Indicates the time for how long the reset is maintained active\n") );
    code -> append( new QString("    reset_duration_time   : time;\n") );
    code -> append( new QString("  };   // end of subtyping block RESET_ASYNC'reset_synchronicity\n") );
    code -> append( new QString("};   // end of " + uvc_prefix + "_item_s\n") );
    code -> append( new QString("'>\n"));
    code -> append( new QString("\n"));
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        res_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_item.e",
        code
    );
}   // end of create_reset_item_file


void spec_e_reset_uvc_generator::create_reset_sequences_file         ( spec_reset_model *res_model ) {
    QString             uvc_prefix  = res_model -> get_prefix();
    QList <QString*>    *code   = new QList <QString*>;
    
    qDebug() << "Generating file " << (uvc_prefix + "_sequences.e");
    
    // TODO: Add a customizable header string generator in here
    code -> append( new QString("File: " + uvc_prefix + "_sequences.e" + "\n") );
    code -> append( new QString("<'\n") );
    code -> append( new QString("extend " + uvc_prefix + "_sequence_kind_t: [DEFAULT_SYNC];\n"));
    code -> append( new QString("extend DEFAULT_SYNC " + uvc_prefix + "_sequence_s {\n"));
    code -> append( new QString("  \n"));
    code -> append( new QString("  reset_offset_count    : uint(bits: 32);\n"));
    code -> append( new QString("  \n"));
    code -> append( new QString("  reset_clock_edge_count: uint(bits: 32);\n"));
    code -> append( new QString("  \n"));
    code -> append( new QString("  !sync_reset           : RESET_SYNC " + uvc_prefix + "_item_s;\n"));
    code -> append( new QString("  \n"));
    code -> append( new QString("  body()@driver.clock is only {\n"));
    code -> append( new QString("    message(LOW, \"Started SYNC reset. OFFSET: \",dec(reset_offset_count), \"  DURATION: \",dec(reset_clock_edge_count));\n"));
    code -> append( new QString("    \n"));
    code -> append( new QString("    do sync_reset keeping {\n"));
    code -> append( new QString("      .reset_offset_count     == reset_offset_count;\n"));
    code -> append( new QString("      .reset_clock_edge_count == reset_clock_edge_count;\n"));
    code -> append( new QString("    };\n"));
    code -> append( new QString("    \n"));
    code -> append( new QString("    message(LOW, \"Ended SYNC reset.\");\n"));
    code -> append( new QString("  };   // end of body method\n"));
    code -> append( new QString("};   // end of DEFAULT_SYNC " + uvc_prefix + "_sequence_s\n"));
    code -> append( new QString("  \n"));
    code -> append( new QString("extend " + uvc_prefix + "_sequence_kind_t: [DEFAULT_ASYNC];\n"));
    code -> append( new QString("extend DEFAULT_ASYNC " + uvc_prefix + "_sequence_s {\n"));
    code -> append( new QString("  \n"));
    code -> append( new QString("  reset_offset_time  : time;\n"));
    code -> append( new QString("  \n"));
    code -> append( new QString("  reset_duration_time: time;\n"));
    code -> append( new QString("  \n"));
    code -> append( new QString("  !async_reset       : RESET_ASYNC " + uvc_prefix + "_item_s;\n"));
    code -> append( new QString("  \n"));
    code -> append( new QString("  \n"));
    code -> append( new QString("  body()@driver.clock is only {\n"));
    code -> append( new QString("    message(LOW, \"Started ASYNC reset. OFFSET: \",dec(reset_offset_time), \"  DURATION: \",dec(reset_duration_time));\n"));
    code -> append( new QString("    \n"));
    code -> append( new QString("    do async_reset keeping {\n"));
    code -> append( new QString("      .reset_offset_time   == reset_offset_time;\n"));
    code -> append( new QString("      .reset_duration_time == reset_duration_time;\n"));
    code -> append( new QString("    };\n"));
    code -> append( new QString("    \n"));
    code -> append( new QString("    message(LOW, \"Ended ASYNC reset.\");\n"));
    code -> append( new QString("  };   // end of body\n"));
    code -> append( new QString("};   // end of DEFAULT_ASYNC " + uvc_prefix + "_sequence_s\n"));
    code -> append( new QString("'>\n"));
    code -> append( new QString("\n"));
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        res_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_sequences.e",
        code
    );
}   // end of create_reset_sequences_file

void spec_e_reset_uvc_generator::create_reset_testcase_file          ( spec_reset_model *res_model ) {
    
}   // end of create_reset_testcase_file

// NON e Files
void spec_e_reset_uvc_generator::create_reset_package_readme_file    ( spec_reset_model *res_model ) {
    
}   // end of create_reset_package_readme_file
void spec_e_reset_uvc_generator::create_reset_hdl_file               ( spec_reset_model *res_model ) {
    
}   // end of create_reset_hdl_file

void spec_e_reset_uvc_generator::create_reset_testcase_script_file   ( spec_reset_model *res_model ) {
    
}   // end of create_reset_testcase_script_file


void spec_e_reset_uvc_generator::create_reset_uvc(  spec_reset_model    *res_model ) {
    QString uvc_directory   = res_model -> get_directory();
    QString uvc_prefix      = res_model -> get_prefix();
    
    qDebug() << "Now generating RESET UVC " << uvc_prefix << " in selected folder " << uvc_directory;
    
    QDir uvc_dir( uvc_directory );
    
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix                 );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/.dvt"       );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/e"          );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/scripts"    );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/demo"       );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/hdl"        );
    
    // Creating files in folder uvc_directory + "/" + uvc_prefix + "/e"
    //==================================================================================================================
    create_reset_top_file                   ( res_model );
    
    //==================================================================================================================
    create_reset_types_file                 ( res_model );
    
    //==================================================================================================================
    create_reset_signal_map_file            ( res_model );
    
    //==================================================================================================================
    create_reset_item_file                  ( res_model );

    //==================================================================================================================
    create_reset_driver_file                ( res_model );
    
    //==================================================================================================================
    create_reset_bfm_file                   ( res_model );

    //==================================================================================================================
    create_reset_monitor_file               ( res_model );
    
    //==================================================================================================================
    create_reset_env_file                   ( res_model );
    
    //==================================================================================================================
    create_reset_configuration_file         ( res_model );
    
    //==================================================================================================================
    create_reset_agent_file                 ( res_model );
    
    //==================================================================================================================
    create_reset_package_readme_file        ( res_model );
    
    //==================================================================================================================
    create_reset_hdl_file                   ( res_model );
    
    //==================================================================================================================
    create_reset_testcase_file              ( res_model );
    
    //==================================================================================================================
    create_reset_testcase_script_file       ( res_model );
}   // end of create_reset_uvc
