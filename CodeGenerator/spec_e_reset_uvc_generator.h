// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_E_RESET_UVC_GENERATOR_H
#define SPEC_E_RESET_UVC_GENERATOR_H

#include <CodeGenerator/spec_e_code_generator.h>

class spec_e_reset_uvc_generator : public spec_e_code_generator {
    public:
        //==============================================================================================================
        // Constructor
        spec_e_reset_uvc_generator();
        
        //==============================================================================================================
        // Methods
        
        void    create_reset_uvc                    ( spec_reset_model  *res_model );
        
    private:
        //==============================================================================================================
        // Methods
        
        void    create_reset_top_file               ( spec_reset_model *res_model );
        void    create_reset_types_file             ( spec_reset_model *res_model );
        void    create_reset_configuration_file     ( spec_reset_model *res_model );
        void    create_reset_signal_map_file        ( spec_reset_model *res_model );
        void    create_reset_driver_file            ( spec_reset_model *res_model );
        void    create_reset_monitor_file           ( spec_reset_model *res_model );
        void    create_reset_env_file               ( spec_reset_model *res_model );
        void    create_reset_agent_file             ( spec_reset_model *res_model );
        void    create_reset_bfm_file               ( spec_reset_model *res_model );
        void    create_reset_coverage_file          ( spec_reset_model *res_model );
        void    create_reset_defines_file           ( spec_reset_model *res_model );
        void    create_reset_item_file              ( spec_reset_model *res_model );
        void    create_reset_sequences_file         ( spec_reset_model *res_model );
        
        void    create_reset_testcase_file          ( spec_reset_model *res_model );
        
        // NON e Files
        void    create_reset_package_readme_file    ( spec_reset_model *res_model );
        void    create_reset_hdl_file               ( spec_reset_model *res_model );
        
        void    create_reset_testcase_script_file   ( spec_reset_model *res_model );
};

#endif // SPEC_E_RESET_UVC_GENERATOR_H
