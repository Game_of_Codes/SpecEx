// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_E_CODE_GENERATOR_H
#define SPEC_E_CODE_GENERATOR_H

#include <math.h>

#include <QComboBox>
#include <QDir>
#include <QFile>
#include <QLineEdit>
#include <QList>
#include <QObject>
#include <QString>

#include <BaseClasses/spec_logger.h>

#include "CodeModel/spec_clock_model.h"
#include "CodeModel/spec_reset_model.h"
#include "CodeModel/spec_interface_model.h"
#include "CodeModel/spec_module_model.h"
#include "CodeModel/spec_struct_data_node.h"
#include "BaseClasses/spec_tablewidget.h"


class spec_e_code_generator : public QObject {
    Q_OBJECT
    
    public:
        explicit spec_e_code_generator   (QObject *parent = nullptr);
        
        
        
    signals:
        
    public slots:
        
    protected:
        void                write_file                  ( QString file_name, QList <QString*> *codeList );
        
        QList <QString*>    get_file_header             ( spec_interface_model    *if_model );
        
        QString             get_interface_struct_member ( QList <QString*>        *properties );
        
        QString             get_interface_signal_port   ( QList <QString*>        *properties );
        
    private:
        QString                             c_line(const QChar *str, int size = -1);
        QString                             c_line_t(QString str);
        
};

#endif // SPEC_E_CODE_GENERATOR_H
