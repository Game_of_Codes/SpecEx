// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_e_interface_code_generator.h"

spec_e_interface_code_generator::spec_e_interface_code_generator() {
    
};

void spec_e_interface_code_generator::create_interface_top_file(spec_interface_model    *if_model) {
    QString uvc_prefix = if_model->get_prefix();
    
    spec_msg( "Generating file " << (uvc_prefix + "_top.e") );
    
    QList <QString*>    *code  = new QList <QString*>;
    
    // TODO: Add a customizable header string generator in here
    code->append( get_file_header( if_model ) );
    code->append( new QString("File: " + uvc_prefix + "_top.e" + "\n") );
    code->append( new QString("<'\n" ) );
    code->append( new QString("import uvm_e/e/uvm_e_top;\n") );
    code->append( new QString("\n") );
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_types;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_macros;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_env_cfg;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_agent_cfg;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_item;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_signal_map;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_monitor;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_monitor_protocol;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_driver;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_bfm;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_bfm_protocol;"  + "\n"));
    if( if_model->get_scenario_count() > 0 ) {
        // Only import the generated scenarios, if we have some defined :)
        code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_bfm_scenario_protocol;\n"));
    };
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_agent;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_env;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_checks;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_sequences;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_coverage;"  + "\n"));
    code->append( new QString("'>\n"));
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_top.e",
        code
    );
};   // end of create_interface_top_file()

void spec_e_interface_code_generator::create_interface_types_file     (spec_interface_model    *if_model) {
    QString uvc_prefix = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    QList <QString*>    *properties;
    
    spec_msg( "Generating file " << (uvc_prefix + "_types.e"));
    
    
    // TODO: Add a customizable header string generator in here
    code->append( get_file_header( if_model ) );
    code->append( new QString("File: " + uvc_prefix + "_types.e\n") );
    code->append( new QString("<'\n"));
    code->append( new QString("type " + uvc_prefix + "_env_name_t     : [DEFAULT];\n"));
    code->append( new QString("type " + uvc_prefix + "_if_name_t      : [NONE, IF0, IF1, IF2, IF3, IF4, IF5, IF6, IF7, IF8, IF9, IF10];\n"));
    code->append( new QString("\n"));
    
    if( if_model->get_scenario_count() > 0 ) {
        // Only create the new scenario type, if there were some scenarios defined
        code->append( new QString("// Captures the names of each user-defined scenario\n") );
        code->append( new QString("type " + uvc_prefix + "_scenario_name_t: [NONE];\n") );
    };
    
    if( if_model->type_count() != 0 ) {
        code->append( new QString("// User-defined enumerated types below, if previously defined\n"));
    };
    for( int line_index = 0; line_index < if_model->type_count(); ++line_index) {
        properties = if_model->get_type_line(line_index);
        
        if( not properties->at(0)->isEmpty() ) {
            code->append( new QString("type " + *if_model->get_type_line(line_index)->at(0) ) );
            code->append( new QString(" : [" + *if_model->get_type_line(line_index)->at(1) + "];\n"));
        };
    };   // end of iterating over each element
    
    code->append( new QString("'>\n"));
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_types.e",
        code
    );
};   // end of create_interface_types_file()

void spec_e_interface_code_generator::create_interface_macros_file(spec_interface_model    *if_model) {
    QString uvc_prefix = if_model->get_prefix();
    QList <QString*>    *code  = new QList <QString*>;
    
    spec_msg( "Generating file " << (uvc_prefix + "_macros.e") );
    
    code->append( get_file_header( if_model ) );
    code->append( new QString("File: " + uvc_prefix + "_macros.e\n") );
    code->append( new QString("<'\n"));
    code->append( new QString("// Add any macros you may need in here...") );
    code->append( new QString("\n"));
    code->append( new QString("\n"));
    code->append( new QString("\n"));
    code->append( new QString("'>\n"));
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_macros.e",
        code
    );
};   // end of create_interface_macros_file()

void spec_e_interface_code_generator::create_interface_agent_cfg_file( spec_interface_model    *if_model ) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    spec_msg( "Generating file " << (uvc_prefix + "_agent_cfg.e") );
    
    code->append( get_file_header( if_model ) );
    code->append( new QString("File: " + uvc_prefix + "_agent_cfg.e\n") );
    code->append( new QString("<'\n"));
    code->append( new QString("struct " + uvc_prefix + "_agent_cfg_s like uvm_config_params_s {\n"));
    code->append( new QString("  if_name: " + uvc_prefix + "_if_name_t;\n"));
    code->append( new QString("  \n") );
    code->append( new QString("  // TLM Analysis Ports are connected -> empty if they are not used\n") );
    code->append( new QString("  // To enable the monitor TLM port, constrain this field to TRUE\n") );
    code->append( new QString("  // and connect the TLM port to a target port\n") );
    code->append( new QString("  use_connected_tlm_port: bool;\n") );
    code->append( new QString("  keep soft use_connected_tlm_port == FALSE;\n") );
    code->append( new QString("  \n"));
    
    // Iterate over each given line and create the new string.
    for( int line_index = 0; line_index < (if_model->agent_count()); ++line_index ) {
        // Finally add the computed string to the codeStringList
        code->append(
                    new QString( get_interface_struct_member( if_model->get_agent_line(line_index) ) ) 
        );
    };
    
    code->append( new QString("};\n"));
    code->append( new QString("'>\n"));
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_agent_cfg.e",
        code
    );
};   // end of create_interface_agent_cfg_file()

void spec_e_interface_code_generator::create_interface_env_cfg_file( spec_interface_model    *if_model ) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    spec_msg( "Generating file " << (uvc_prefix + "_env_cfg.e") );
    
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( if_model ) );
    code->append( new QString("File: " + uvc_prefix + "_env_cfg.e\n") );
    code->append( new QString("<'\n"));
    
    code->append( new QString("struct " + uvc_prefix + "_env_cfg_s like uvm_config_params_s {\n"));
    code->append( new QString("  env_name: " + uvc_prefix + "_env_name_t;\n"));
    
    // Iterate over each given line and create the new string.
    for( int line_index = 0; line_index < (if_model->global_count()); ++line_index ) {
        // Finally add the computed string to the codeStringList
        code->append(
                    new QString( get_interface_struct_member( if_model->get_global_line(line_index) ) ) 
        );
    };
    
    code->append( new QString("};\n"));
    code->append( new QString("'>\n"));
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_env_cfg.e",
        code
    );
};   // end of create_interface_env_cfg_file()

void spec_e_interface_code_generator::create_interface_item_file( spec_interface_model    *if_model ) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    spec_msg( "Generating file " << (uvc_prefix + "_item.e") );
    
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( if_model ) );
    
    code->append( new QString("File: " + uvc_prefix + "_item.e\n") );
    code->append( new QString("<'\n"));
    code->append( new QString("struct " + uvc_prefix + "_item_s like any_sequence_item {\n"));
    code->append( new QString("  if_name: " + uvc_prefix + "_if_name_t;\n"));
    
    if( if_model->item_count() > 0 ) {
        code->append( new QString("  \n") );
        code->append( new QString("  // The subsequent fields are user-defined.\n") );
    };
    for( int line_index = 0; line_index < (if_model->item_count()); ++line_index ) {
        // Finally add the computed string to the codeStringList
        code->append(
            new QString( get_interface_struct_member( if_model->get_item_line(line_index) ) ) 
        );
    };   // end of iterating through each data item declaration
    
    // Only generate scenarios, if they are defined
    if( if_model->get_scenario_count() > 0 ) {
        QList<QString*>                     signal_line;
        QString                             *signal_name;
        QString                             signal_type;
        QString                             signal_width;
        QString                             assembled_type_string;
        
        QList<QString*>                     signal_list;
        QList<QString*>                     field_list;
        QList<QString*>                     method_list;
        QList<QString*>                     for_each_list;
        QList<QString*>                     size_list;
        
        // We are only interested in all the signals that are defined in each table
        
        for( int signal_index = 0; signal_index < if_model->signal_count(); ++signal_index ) {
            signal_line     = *if_model->get_signal_line( signal_index );
            signal_name     = signal_line.at(0);
            signal_type     = *signal_line.at(1);
            signal_width    = *signal_line.at(2);
            
            
            // Calculating the type string facilitates easier generation of the code
            if( signal_type == "uint" or signal_type == "int" or signal_type == "longuint" or signal_type == "longint"  ) {
                if( signal_width.toInt() == 1 ) {
                    assembled_type_string = "bit";
                } else {
                    assembled_type_string = signal_type + "(bits: " + signal_width + ")";
                };
            } else {
                assembled_type_string = signal_type;
            };
            
            // First we'll assemble and store all the signal declarations properly
            // The form of this expression will be something like this:
            //      <name>_l : list of <type>;   // <type> may be any integral type or a single 'bit'
            signal_list.append( new QString( *signal_name + "_l : list of " + assembled_type_string + ";\n" ) );
            
            // Second we'll assemble the generation field used to drive the stimulus generation
            //      !<name>_gen : <type>;
            field_list.append( new QString( "!" + *signal_name + "_gen : " + assembled_type_string + ";\n" ) );
            
            // Third, we'll assemble the method that is used in the constraining process
            if( assembled_type_string == "bit" ) {
                method_list.append( new QString( "get_" + *signal_name + "_bit( i : int ): bit is empty;\n" ) );
            } else {
                method_list.append( new QString( "get_" + *signal_name + "_word( i : int ): " + assembled_type_string + " is empty;\n" ) );
            };
            
            // Fourth, we'll assemble the for each constraining operation
            if( assembled_type_string == "bit" ) {
                for_each_list.append( new QString( "for each in " + *signal_name + "_l { it == get_" + *signal_name + "_bit( index ) };\n" ) );
            } else {
                for_each_list.append( new QString( "for each in " + *signal_name + "_l { it == get_" + *signal_name + "_word( index ) };\n" ) );
            };
            
            // Fifth, now assemble the size() constraints
            size_list.append( new QString( *signal_name + "_l.size() == 0;\n" ) );
        };  // end of iterating over each scenario list
        
        code->append( new QString("  //////////////////////////////////////////////////////////////////////////////\n") );
        code->append( new QString("  // SCENARIO GENERATION DISCLAIMER:\n") );
        code->append( new QString("  //   This code is NOT best practices, because it does not provide the bit-level\n") );
        code->append( new QString("  //   to transaction abstraction layer.\n") );
        code->append( new QString("  //   The scenario generation is intended to quickly get protocol stimulus\n") );
        code->append( new QString("  //   validated and not to reach coverage closure.\n") );
        code->append( new QString("  \n") );
        code->append( new QString("  // Indicates if a user-defined scenario is applied if field value is not NONE\n") );
        code->append( new QString("  user_scenario : " + uvc_prefix + "_scenario_name_t;\n") );
        code->append( new QString("  \n") );
        code->append( new QString("  // Declaration of the signal bit-level stimulus lists\n") );
        for(QString *str : signal_list ) {
            // Please keep in mind that the EOL character is already in the string itself
            code->append( new QString( "  " + *str ) );
        };
        code->append( new QString("  \n") );
        code->append( new QString("  \n") );
        code->append( new QString("  // Declaration of all fields that are used to generate the randomized values\n") );
        for(QString *str : field_list ) {
            // Please keep in mind that the EOL character is already in the string itself
            code->append( new QString( "  " + *str ));
        };
        code->append( new QString("  \n") );
        code->append( new QString("  \n") );
        code->append( new QString("  // Declaring constraint API  methods used to model the values of the scenario wave model\n") );
        for(QString *str : method_list ) {
            // Please keep in mind that the EOL character is already in the string itself
            code->append( new QString( "  " + *str ));
        };
        code->append( new QString("  \n") );
        code->append( new QString("  \n") );
        code->append( new QString("  // This constraint is used to enable user-stimulus and also regular transaction based stimulus\n") );
        code->append( new QString("  keep USER_SCENARIO_C is all of {\n") );
        code->append( new QString("    // By default, if there is no user-defined scenario, then the bit-level\n") );
        code->append( new QString("    // stimulus will not be generated.\n") );
        code->append( new QString("    soft user_scenario == NONE;\n") );
        code->append( new QString("    \n") );
        code->append( new QString("    // These constraints will set the actual stimulus\n") );
        for(QString *str : for_each_list ) {
            // Please keep in mind that the EOL character is already in the string itself
            code->append( new QString( "    " + *str ));
        };
        code->append( new QString("    \n") );
        code->append( new QString("    read_only( user_scenario == NONE ) => all of {\n") );
        for(QString *str : size_list ) {
            // Please keep in mind that the EOL character is already in the string itself
            code->append( new QString( "      " + *str ));
        };
        code->append( new QString("    };  // end of constraining the NONE scenario\n") );
        code->append( new QString("  };  // end of USER_SCENARIO_C\n") );
    };  // end of handling the scenario generation code
    
    code->append( new QString("};  // end of " + uvc_prefix + "_item_s declaration\n"));
    code->append( new QString("'>\n"));
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_item.e",
        code
    );
};   // end of create_interface_item_file

void spec_e_interface_code_generator::create_interface_signal_map_file( spec_interface_model    *if_model ) {
    QString             uvc_prefix      = if_model->get_prefix();
    QList <QString*>    clk_properties  = if_model->get_clock_properties();
    QString             *clk_tb         = clk_properties.at(0);
    QString             *clk_hdl        = clk_properties.at(1);
    QString             *clk_edge       = clk_properties.at(2);
    
    QList <QString*>    res_properties = if_model->get_reset_properties();
    QString             *res_tb         = res_properties.at(0);
    QString             *res_hdl        = res_properties.at(1);
    QString             *res_level      = res_properties.at(2);
    
    QList <QString*>    *code           = new QList <QString*>;
    
    spec_msg( "Generating file " << (uvc_prefix + "_signal_map.e") );
    
    // Creating the Clock Events
    QString             edge_string;
    
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( if_model ) );
    
    code->append( new QString("File: " + uvc_prefix + "_signal_map.e\n") );
    code->append( new QString("<'\n"));
    
    code->append( new QString("unit " + uvc_prefix + "_signal_map_u like uvm_signal_map {\n"));
    code->append( new QString("  if_name      : " + uvc_prefix + "_if_name_t;\n"));
    code->append( new QString("  \n"));
    code->append( new QString("  env_config   : " + uvc_prefix + "_env_cfg_s;\n") );
    code->append( new QString("  agent_config : " + uvc_prefix + "_agent_cfg_s;\n") );
    code->append( new QString("  \n"));
    code->append( new QString("  " + *clk_tb + ": in event_port is instance;\n"));
    code->append( new QString("  keep soft " + *clk_tb + ".hdl_path() == \"" + *clk_hdl + "\";\n") );
    
    if( QString::compare( *clk_edge, tr("Asynchronous"), Qt::CaseSensitive ) == 0 ) {
        // TODO: Figure how to make the clock event_port trigger asynchronously
    } else {
        if( QString::compare( *clk_edge, tr("Rising Edge"), Qt::CaseSensitive ) == 0 ) {
            edge_string = tr("rise");
        };
        if( QString::compare( *clk_edge, tr("Falling Edge"), Qt::CaseSensitive ) == 0 ) {
            edge_string = tr("fall");
        };
        if( QString::compare( *clk_edge, tr("Rising and Falling Edge"), Qt::CaseSensitive ) == 0 ) {
            edge_string = tr("change");
        };
        code->append( new QString("  keep " + *clk_tb + ".edge() == " + edge_string + ";\n"));
        code->append( new QString("  keep soft bind( " + *clk_tb + ", external );\n\n") );
    };
    
    // Creating the Reset 
    code->append( new QString("  " + *res_tb + ": inout simple_port of bit is instance;\n"));
    code->append( new QString("  keep soft " + *res_tb + ".hdl_path() == \"" + *res_hdl + "\";\n"));
    code->append( new QString("  keep bind( " + *res_tb + ", external );\n\n"));
    
    if( QString::compare( *res_level, tr("Active-Low"), Qt::CaseSensitive ) == 0 ) {
        edge_string = tr("fall");
    } else {
        edge_string = tr("rise");
    };
    
    code->append( new QString("  event reset_start is " + edge_string + "(" + *res_tb + "$)@sim;\n"));
    if( QString::compare( *res_level, tr("Active-Low"), Qt::CaseSensitive ) == 0 ) {
        edge_string = tr("rise");
    } else {
        edge_string = tr("fall");
    };
    code->append( new QString("  event reset_end   is " + edge_string + "(" + *res_tb + "$)@sim;\n\n"));
    
    // Each signal name has to be given a name. If no name was provided for a given line in the table, then no signal
    // will be generated
    for( int line_index = 0; line_index < if_model->signal_count(); ++line_index) {
        code->append(
            new QString( get_interface_signal_port( if_model->get_signal_line(line_index) ) )
        );
    };   // end of iterating over each element
    code->append( new QString("};\n"));
    code->append( new QString("'>\n"));
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_signal_map.e",
        code
    );
};   // end of create_interface_signal_map_file

void spec_e_interface_code_generator::create_interface_monitor_file( spec_interface_model    *if_model ) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    QString             *clk_tb     = if_model->get_clock_properties().at(0);
    
    spec_msg( "Generating file " << (uvc_prefix + "_monitor.e") );
    
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( if_model ) );
    
    code->append( new QString("File: " + uvc_prefix + "_monitor.e\n") );
    code->append( new QString("<'\n"));
    code->append( new QString("unit " + uvc_prefix + "_monitor_u like uvm_monitor {\n") );
    code->append( new QString("  if_name        : " + uvc_prefix + "_if_name_t;\n\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  env_config     : " + uvc_prefix + "_env_cfg_s;\n\n") );
    code->append( new QString("  agent_config   : " + uvc_prefix + "_agent_cfg_s;\n\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  !p_smp         : " + uvc_prefix + "_signal_map_u;\n\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  // The monitor transaction item contains the semtic logic of the protocol\n") );
    code->append( new QString("  !mon_item      : " + uvc_prefix + "_item_s;\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  event clock is @p_smp." + *clk_tb + "$;\n") );
    code->append( new QString("  event reset is @p_smp.reset_start;\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  event item_detected_e;\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  monitor_item_ap : out interface_port of tlm_analysis of " + uvc_prefix + "_item_s is instance;\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  // Enables the use of the monitor's TLM analysis port.\n") );
    code->append( new QString("  use_connected_tlm_port: bool;\n") );
    code->append( new QString("  keep use_connected_tlm_port == read_only( agent_config.use_connected_tlm_port );\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  \n") );
    code->append( new QString("  monitor()@clock is empty;\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  when FALSE'use_connected_tlm_port {\n"));
    code->append( new QString("    connect_ports() is also {\n") );
    code->append( new QString("      monitor_item_ap.connect( empty );\n") );
    code->append( new QString("    };\n") );
    code->append( new QString("  };\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  on reset {\n") );
    code->append( new QString("    rerun();\n") );
    code->append( new QString("  };\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  run() is also {\n") );
    code->append( new QString("    start monitor();\n") );
    code->append( new QString("  };\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  short_name():string is also {\n") );
    code->append( new QString("    result = \"MON\";\n") );
    code->append( new QString("  };\n") );
    code->append( new QString("};\n") );
    code->append( new QString("'>\n"));
    
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_monitor.e",
        code
    );
};   // end of create_interface_monitor_file

void spec_e_interface_code_generator::create_interface_monitor_protocol_file( spec_interface_model    *if_model ) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    spec_msg( "Generating file " << (uvc_prefix + "_monitor_protocol.e") );
    
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( if_model ) );
    
    code->append( new QString("File: " + uvc_prefix + "_monitor_protocol.e\n") );
    code->append( new QString("<'\n"));
    code->append( new QString("extend " + uvc_prefix + "_monitor_u {\n"));
    code->append( new QString("  // TODO: Implement the low-level monitor protocol in monitor() TCM\n"));
    code->append( new QString("  monitor()@clock is {\n"));
    code->append( new QString("    while( TRUE ) {\n"));
    code->append( new QString("      // ...\n"));
    code->append( new QString("      wait;\n"));
    code->append( new QString("    };// ...\n"));
    code->append( new QString("  };\n"));
    code->append( new QString("};\n"));
    code->append( new QString("'>\n"));
    
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_monitor_protocol.e",
        code
    );
};   // end of create_interface_monitor_file

void spec_e_interface_code_generator::create_interface_driver_file( spec_interface_model    *if_model ) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    QString             *clk_tb     = if_model->get_clock_properties().at(0);
    
    spec_msg( "Generating file " << (uvc_prefix + "_driver.e") );
    
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( if_model ) );
    
    code->append( new QString("File: " + uvc_prefix + "_driver.e\n") );
    code->append( new QString("<'\n"));
    code->append( new QString("sequence " + uvc_prefix + "_seq_s using\n"));
    code->append( new QString("  item           = " + uvc_prefix + "_item_s,\n"));
    code->append( new QString("  created_kind   = " + uvc_prefix + "_sequence_kind_t,\n"));
    code->append( new QString("  created_driver = " + uvc_prefix + "_driver_u;\n"));
    code->append( new QString("  \n"));
    code->append( new QString("extend " + uvc_prefix + "_driver_u {\n"));
    code->append( new QString("  if_name      : " + uvc_prefix + "_if_name_t;\n") );
    code->append( new QString("  \n"));
    code->append( new QString("  env_config   : " + uvc_prefix + "_env_cfg_s;\n") );
    code->append( new QString("  agent_config : " + uvc_prefix + "_agent_cfg_s;\n") );
    code->append( new QString("  \n"));
    code->append( new QString("  !p_smp       : " + uvc_prefix + "_signal_map_u;\n") );
    code->append( new QString("  \n"));
    code->append( new QString("  event clock is only @p_smp." + *clk_tb + "$;\n"));
    code->append( new QString("  \n"));
    code->append( new QString("  short_name():string is also {\n") );
    code->append( new QString("    result = \"DRV\";\n") );
    code->append( new QString("  };\n") );
    code->append( new QString("};\n"));
    code->append( new QString("'>\n"));
    
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_driver.e",
        code
    );
};   // end of create_interface_driver_file

void spec_e_interface_code_generator::create_interface_bfm_file(spec_interface_model    *if_model) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    QString             *clk_tb     = if_model->get_clock_properties().at(0);
    
    spec_msg( "Generating file " << (uvc_prefix + "_bfm.e") );
    
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( if_model ) );
    
    code->append( new QString("File: " + uvc_prefix + "_bfm.e\n") );
    code->append( new QString("<'\n") );
    code->append( new QString("unit " + uvc_prefix + "_bfm_u like uvm_bfm {\n") );
    code->append( new QString("  if_name      : "+ uvc_prefix + "_if_name_t;\n") );
    code->append( new QString("  \n"));
    code->append( new QString("  env_config   : " + uvc_prefix + "_env_cfg_s;\n") );
    code->append( new QString("  agent_config : " + uvc_prefix + "_agent_cfg_s;\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  !p_smp       : " + uvc_prefix + "_signal_map_u;\n") );
    code->append( new QString("  !p_drv       : " + uvc_prefix + "_driver_u;\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  event clock is @p_smp." + *clk_tb + "$;\n") );
    code->append( new QString("  event reset is @p_smp.reset_start;\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  protocol( tr : " + uvc_prefix + "_item_s )@clock is empty;\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  get_and_drive_item()@clock is {\n") );
    code->append( new QString("    var tr_item: " + uvc_prefix + "_item_s;\n") );
    code->append( new QString("    \n") );
    code->append( new QString("    while( TRUE ) {\n") );
    code->append( new QString("      tr_item = p_drv.get_next_item();\n") );
    code->append( new QString("      protocol( tr_item );\n") );
    code->append( new QString("      emit p_drv.item_done;\n") );
    code->append( new QString("    };\n") );
    code->append( new QString("  };\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  on reset {\n") );
    code->append( new QString("    rerun();\n") );
    code->append( new QString("  };\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  run() is also {\n") );
    code->append( new QString("    start get_and_drive_item();\n") );
    code->append( new QString("  };\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  short_name():string is also {\n") );
    code->append( new QString("    result = \"BFM\";\n") );
    code->append( new QString("  };\n") );
    code->append( new QString("};\n") );
    code->append( new QString("'>\n") );
    
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_bfm.e",
        code
    );
};   // end of create_interface_bfm_file

void spec_e_interface_code_generator::create_interface_bfm_protocol_file( spec_interface_model    *if_model ) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    spec_msg( "Generating file " << (uvc_prefix + "_bfm_protocol.e") );
    
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( if_model ) );
    
    code->append( new QString("File: " + uvc_prefix + "_bfm_protocol.e\n") );
    code->append( new QString("<'\n"));
    code->append( new QString("extend " + uvc_prefix + "_bfm_u {\n") );
    code->append( new QString("  // TODO: Implement the specific protocol\n") );
    code->append( new QString("  protocol( tr: " + uvc_prefix + "_item_s )@clock is {\n") );
    code->append( new QString("    // ...\n") );
    code->append( new QString("  };\n") );
    code->append( new QString("};\n") );
    code->append( new QString("'>\n"));
    
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_bfm_protocol.e",
        code
    );
};   // end of create_interface_bfm_protocol_file

void spec_e_interface_code_generator::create_interface_bfm_scenario_protocol( spec_interface_model      *if_model ) {
    QString                     uvc_prefix  = if_model->get_prefix();
    QList <QString*>            *code       = new QList <QString*>;
    QList<QString*>             signal_line;
    QString                     signal_name;
    int                         signal_count = if_model->signal_count();
    spec_interface_wave_table   *scenario_table = if_model->get_scenario_table( 0 );
    
    spec_msg( "Generating file " << (uvc_prefix + "_bfm_scenario_protocol.e") );
    
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( if_model ) );
    code->append( new QString("File: " + uvc_prefix + "_bfm_scenario_protocol.e\n") );
    code->append( new QString("<'\n"));
    code->append( new QString("extend " + uvc_prefix + "_bfm_u {\n"));
    code->append( new QString("  // The protcol() method for scenarios is being executed before transactions\n"));
    code->append( new QString("  protocol( tr: " + uvc_prefix + "_item_s )@clock is first {\n"));
    code->append( new QString("    if( tr.user_scenario != NONE ) {\n"));
    code->append( new QString("      message(LOW,\"Starting scenario \", tr.user_scenario );\n"));
    code->append( new QString("      \n") );
    code->append( new QString("      // Each scenario can be of a different length, hence we are calculating the maximum\n") );
    code->append( new QString("      // number of elements over the list of given stimulus lists. READ accesses will always be 0\n") );
    code->append( new QString("      // and all WRITE accesses will have the same lengths.\n") );
    code->append( new QString("      var time_steps_l : list of int;\n") );
    for( int signal_index = 0; signal_index < signal_count; ++signal_index ) {
        // The first two rows are reserved for clock and reset only
        signal_name = scenario_table->get_signal_name( signal_index+2 );
        code->append( new QString("      if( not tr." + signal_name + "_l.is_empty() ) {\n" ) );
        code->append( new QString("        time_steps_l.add( tr." + signal_name + "_l.size() );\n") );
        code->append( new QString("      };\n"));
        code->append( new QString("      \n") );
    };
    code->append( new QString("      var time_step_count : int = time_steps_l.max(it);\n"));
    code->append( new QString("      for time_step from 0 to (time_step_count-1) {\n"));
    code->append( new QString("        // We're assigning/driving only the signals, marked as drivers.\n"));
    code->append( new QString("        // READ marked signals are empty lists, as such, we will skip driving those\n"));
    for( int signal_index = 0; signal_index < signal_count; ++signal_index ) {
        // The first two rows are reserved for clock and reset only
        signal_name = scenario_table->get_signal_name( signal_index+2 );
        code->append( new QString("        if( not tr." + signal_name + "_l.is_empty() ) {\n"));
        code->append( new QString("          p_smp." + signal_name + "$ = tr." + signal_name + "_l[time_step];\n") );
        code->append( new QString("        };\n"));
    };
    code->append( new QString("        wait;  // for the next clock cycle\n"));
    code->append( new QString("      };  // end if iterating over each of the time_steps to drive the signals\n"));
    code->append( new QString("      message(LOW,\"Finished scenario \", tr.user_scenario );\n"));
    code->append( new QString("      \n"));
    code->append( new QString("      // Avoid running the transaction to bit-level BFM protocol loop\n"));
    code->append( new QString("      return;\n"));
    code->append( new QString("    };  // end of driving scenario stimulus\n"));
    code->append( new QString("  };  // end of protocol() method\n"));
    code->append( new QString("};  // end of " + uvc_prefix + "_bfm_u\n"));
    code->append( new QString("'>\n"));
    code->append( new QString("    "));
    
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_bfm_scenario_protocol.e",
        code
    );
}

void spec_e_interface_code_generator::create_interface_agent_file( spec_interface_model    *if_model ) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    spec_msg( "Generating file " << (uvc_prefix + "_agent.e") );
    
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( if_model ) );
    code->append( new QString("File: " + uvc_prefix + "_agent.e\n") );
    code->append( new QString("<'\n"));
    
    code->append( new QString("unit " + uvc_prefix + "_agent_u like uvm_agent {\n"));
    code->append( new QString("  if_name     : " + uvc_prefix + "_if_name_t;\n"));
    code->append( new QString("  \n"));
    code->append( new QString("  smp         : " + uvc_prefix + "_signal_map_u is instance;\n"));
    code->append( new QString("  mon         : " + uvc_prefix + "_monitor_u    is instance;\n"));
    code->append( new QString("  \n"));
    code->append( new QString("  env_config  :  " + uvc_prefix + "_env_cfg_s;\n"));
    code->append( new QString("  agent_config:  " + uvc_prefix + "_agent_cfg_s;\n"));
    code->append( new QString("  \n"));
    code->append( new QString("  keep MY_EX_AGENT_IF_NAME_C is all of {\n"));
    code->append( new QString("    agent_config.if_name  == read_only( if_name );\n"));
    code->append( new QString("    \n"));
    code->append( new QString("    smp.if_name           == read_only( if_name );\n"));
    code->append( new QString("    smp.env_config        == read_only( env_config );\n"));
    code->append( new QString("    smp.agent_config      == read_only( agent_config );\n"));
    code->append( new QString("    \n"));
    code->append( new QString("    mon.if_name           == read_only( if_name );\n"));
    code->append( new QString("    mon.env_config        == read_only( env_config );\n"));
    code->append( new QString("    mon.agent_config      == read_only( agent_config );\n"));
    code->append( new QString("  };  // end of MY_EX_AGENT_IF_NAME_C constraint group\n"));
    code->append( new QString("  \n"));
    code->append( new QString("  connect_pointers() is also {\n"));
    code->append( new QString("    mon.p_smp = smp;\n"));
    code->append( new QString("  };  // end of connect_pointers\n"));
    code->append( new QString("  \n"));
    code->append( new QString("  when ACTIVE {\n"));
    code->append( new QString("    bfm: " + uvc_prefix + "_bfm_u    is instance;\n"));
    code->append( new QString("    drv: " + uvc_prefix + "_driver_u is instance;\n"));
    code->append( new QString("    \n"));
    code->append( new QString("    keep MY_EX_ACTIVE_AGENT_IF_NAME_C is all of {\n"));
    code->append( new QString("      bfm.if_name      == read_only( if_name );\n"));
    code->append( new QString("      bfm.env_config   == read_only( env_config );\n"));
    code->append( new QString("      bfm.agent_config == read_only( agent_config );\n"));
    code->append( new QString("      \n"));
    code->append( new QString("      drv.if_name      == read_only( if_name );\n"));
    code->append( new QString("      drv.env_config   == read_only( env_config );\n"));
    code->append( new QString("      drv.agent_config == read_only( agent_config );\n"));
    code->append( new QString("    };  // end of MY_EX_ACTIVE_AGENT_IF_NAME_C constraint group\n"));
    code->append( new QString("    \n"));
    code->append( new QString("    connect_pointers() is also {\n"));
    code->append( new QString("      bfm.p_smp = smp;\n"));
    code->append( new QString("      drv.p_smp = smp;\n"));
    code->append( new QString("      bfm.p_drv = drv;\n"));
    code->append( new QString("    };  // end of connect_pointers\n"));
    code->append( new QString("  };  // end of when ACTIVE subtype\n"));
    code->append( new QString("  \n") );
    code->append( new QString("  short_name():string is also {\n") );
    code->append( new QString("    result = append( if_name );\n") );
    code->append( new QString("  };  // end of short_name\n") );
    code->append( new QString("};  // end of " + uvc_prefix + "_agent_u\n"));
    
    code->append( new QString("'>\n"));
    
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_agent.e",
        code
    );
};   // end of create_interface_agent_file

void spec_e_interface_code_generator::create_interface_env_file( spec_interface_model    *if_model ) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    spec_msg( "Generating file " << (uvc_prefix + "_env.e") );
    
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( if_model ) );
    
    code->append( new QString("File: " + uvc_prefix + "_env.e\n") );
    code->append( new QString("<'\n") );
    code->append( new QString("unit " + uvc_prefix + "_env_u like uvm_env {\n") );
    code->append( new QString("  env_name   : " + uvc_prefix + "_env_name_t;\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  env_config : " + uvc_prefix + "_env_cfg_s;\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  agents     : list of " + uvc_prefix + "_agent_u is instance;\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  keep MY_EX_ENV_DEFAULT_C is all of {\n") );
    code->append( new QString("    env_config.env_name == read_only( env_name );\n") );
    code->append( new QString("    soft agents.size()  == 1;\n") );
    code->append( new QString("    \n") );
    code->append( new QString("    for each (agent) in agents {\n") );
    code->append( new QString("      agent.env_config == read_only( env_config );\n") );
    code->append( new QString("    };\n") );
    code->append( new QString("  };\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  short_name():string is also {\n") );
    code->append( new QString("    result = append(env_name,\"_ENV\");\n") );
    code->append( new QString("  };\n") );
    code->append( new QString("};\n") );
    code->append( new QString("'>\n") );
    
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_env.e",
        code
    );
};   // end of create_interface_env_file

void spec_e_interface_code_generator::create_interface_checks_file( spec_interface_model    *if_model ) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    spec_msg( "Generating file " << (uvc_prefix + "_checks.e") );
    
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( if_model ) );
    
    code->append( new QString("File: " + uvc_prefix + "_checks.e\n") );
    code->append( new QString("<'\n"));
    
    code->append( new QString("'>\n"));
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_checks.e",
        code
    );
};   // end of create_interface_checks_file

void spec_e_interface_code_generator::create_interface_seq_lib_file( spec_interface_model    *if_model ) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    int                 scenario_count  = if_model -> get_scenario_count();
    
    spec_msg( "Generating file " << (uvc_prefix + "_sequences.e") );
    
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( if_model ) );
    
    code->append( new QString( "File: " + uvc_prefix + "_sequences.e\n" ) );
    code->append( new QString( "<'\n" ));
    code->append( new QString( "// Adding the Objection mechanism to the MAIN sequence.\n" ) );
    code->append( new QString( "// Only the MAIN sequence executes the pre_body and post_body methods, hence only there we'll\n" ) );
    code->append( new QString( "// raise and drop objections via the driver unit.\n" ) );
    code->append( new QString( "extend MAIN " + uvc_prefix + "_seq_s {\n" ) );
    code->append( new QString( "  pre_body()@sys.any is only {\n" ) );
    code->append( new QString( "    driver.raise_objection( TEST_DONE );\n" ) );
    code->append( new QString( "  };\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  post_body()@sys.any is only {\n" ) );
    code->append( new QString( "    driver.drop_objection( TEST_DONE );\n" ) );
    code->append( new QString( "  };\n" ) );
    code->append( new QString( "};  // end of extending MAIN " + uvc_prefix +"_seq_s\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "\n" ) );
    
    if( scenario_count > 0 ) {
        spec_interface_wave_table           *scenario_table;
        QString                             scenario_name;
        int                                 signal_count    = 0;
        int                                 time_step_count = 0;
        QString                             signal_name;
        QString                             signal_type;
        QString                             signal_width;
        QString                             assembled_type_string;
        QString                             assembled_action_string;
        bool                                is_signal_write;
        bool                                is_signal_bit;
        QList<int>                          signal_flow_list;
        
        for( int scenario_index = 0; scenario_index < scenario_count;++scenario_index ) {
            // First we'll get the scenario name
            scenario_table  = if_model->get_scenario_table(scenario_index);
            scenario_name   = scenario_table->get_scenario_name();
            signal_count    = scenario_table->rowCount();
            time_step_count = scenario_table->get_time_step_count();
            code->append( new QString("extend " + uvc_prefix + "_scenario_name_t:[" + scenario_name + "];\n") );
            code->append( new QString("extend " + scenario_name + "'user_scenario " + uvc_prefix + "_item_s {\n") );
            code->append( new QString("  // Scenario description consists of constraining the number of time steps and\n") );
            code->append( new QString("  // defining the values using case actions for each of the signals\n") );
            code->append( new QString("  keep " + scenario_name.toUpper() + "_C is all of {\n") );
            code->append( new QString("    // The size constraints reflect the number of time steps\n") );
            for( int signal_index = 2; signal_index < signal_count; ++signal_index ) {
                signal_name     = scenario_table->get_signal_name( signal_index );
                is_signal_write = scenario_table->is_signal_write( signal_index );
                
                // Only signals that are being written are constrained explicitly to the step size.
                // However, signals that are marked as read signals are explicitly constrained to be empty listss
                code->append( new QString("    " + signal_name + "_l.size() == " +QString::number( (is_signal_write? time_step_count : 0) ) + ";\n") );
            };  // end of iterating over each signal
            code->append( new QString("  };  // end of constraint block " + scenario_name.toUpper() + "_C\n") );
            code->append( new QString("  \n") );
            for( int signal_index = 2; signal_index < signal_count; ++signal_index ) {
                signal_name         = scenario_table->get_signal_name( signal_index );
                is_signal_write     = scenario_table->is_signal_write( signal_index );
                signal_flow_list    = scenario_table->get_signal_flow( signal_index );
                
                // Only driven signals are being constrained
                if( is_signal_write ) {
                    signal_type     = *(if_model->get_signal_line( signal_index-2 )->at(1));
                    signal_width    = *(if_model->get_signal_line( signal_index-2 )->at(2));
                    is_signal_bit   = false;
                    
                    // Calculating the type string facilitates easier generation of the code
                    if( signal_type == "uint" or signal_type == "int" or signal_type == "longuint" or signal_type == "longint"  ) {
                        if( signal_width.toInt() == 1 ) {
                            assembled_type_string   = "bit";
                            is_signal_bit           = true;
                        } else {
                            assembled_type_string = signal_type + "(bits: " + signal_width + ")";
                        };
                    } else {
                        assembled_type_string = signal_type;
                        is_signal_bit               = (signal_type == "bit");
                    };
                    
                    // Declare the method header, depending whether it is a bit or bus
                    if( is_signal_bit ) {
                        code->append( new QString( "  get_" + signal_name + "_bit( i : int ): bit is {\n" ) );
                    } else {
                        code->append( new QString( "  get_" + signal_name + "_word( i : int ): " + assembled_type_string + " is {\n" ) );
                    };
                    
                    code->append( new QString( "    case( i ) {\n" ) );
                    
                    for( int step_index = 0; step_index < signal_flow_list.count(); ++step_index ) {
                        // Gathering all the information and assemble the action assignment
                        assembled_action_string = QString::number(step_index) + ": { ";
                        
                        if( is_signal_bit ) {
                            assembled_action_string += "result = 1'b";
                            if( signal_flow_list.at(step_index) == spec_wave_state::lvl_0 or signal_flow_list.at(step_index) == spec_wave_state::trans_10 ) {
                                assembled_action_string += "0";
                            } else {
                                assembled_action_string += "1";
                            };
                            assembled_action_string += ";";
                        } else {
                            if( signal_flow_list.at(step_index) == spec_wave_state::data_change ) {
                                assembled_action_string += "gen " + signal_name + "_gen;";
                            };
                        };
                        
                        assembled_action_string += " };\n";
                        code->append( new QString( "      " + assembled_action_string) );
                    };  // end of generating the action assignment flow
                    
                    code->append( new QString( "    };  // end of case( i ) action\n" ) );
                    
                    // Only assign the result, if a multi-bit value is used
                    if( not is_signal_bit ) {
                        code->append( new QString( "    result = " + signal_name + "_gen;\n" ) );
                    };
                    
                    // Closing the method
                    code->append( new QString("  };\n") );
                    code->append( new QString("  \n") );
                };  // end of handling driven signals
            };  // end of constructing each of the scenario case action methods
            
            code->append( new QString("};  // end of extending " + scenario_name + " " + uvc_prefix + "_item_s\n") );
        };  // end of iterating over each scenario
    } else {
        // In case the scenario count is 0, then we'll simply add a dummy sequence in comments
        code->append( new QString("// TODO: Build the sequence library in this file using the following commented code\n"));
        code->append( new QString("//       as a template\n"));
        code->append( new QString("//extend " + uvc_prefix + "_sequence_kind_t: [MY_NEW_SEQ_NAME];\n"));
        code->append( new QString("//extend MY_NEW_SEQ_NAME " + uvc_prefix + "_seq_s {\n"));
        code->append( new QString("//  !my_item: "+ uvc_prefix + "_item_s;\n"));
        code->append( new QString("//  \n"));
        code->append( new QString("//  body()@driver.clock is only {\n"));
        code->append( new QString("//    // do my_item;\n"));
        code->append( new QString("//  };   // end of procedural sequence control\n"));
        code->append( new QString("//};   // end of MY_NEW_SEQ_NAME sequence\n"));
    };
    
    
    
    // TODO: Build a sequence scenario that defines the scenario
    
    code->append( new QString("'>\n"));
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_sequences.e",
        code
    );
};   // end of create_interface_seq_lib_file

void spec_e_interface_code_generator::create_interface_coverage_file( spec_interface_model    *if_model ) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    qDebug() << "Generating file " << (uvc_prefix + "_coverage.e");
    
    bool is_list;
    bool is_time;
    bool is_no_cover_type;
    int  bit_width;
    int  list_depth;
    
    QString item_name;
    QString item_type;
    QString item_width;
    QString is_item_list;
    
    QList <QString*>    *item_properties;
    
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( if_model ) );
    
    code->append( new QString("File: " + uvc_prefix + "_coverage.e\n") );
    code->append( new QString("<'\n"));
    code->append( new QString("extend " + uvc_prefix + "_monitor_u {\n"));
    code->append( new QString("\n") );
    code->append( new QString("  cover item_detected_e is {\n") );
    
    for( int line_index = 0; line_index < if_model->item_count();++line_index) {
        // Check the type and bit-width of the item.
        // If it is a uint with up to 4 bits, then only create the item <NAME>;
        // If the uint vector is > than 4, then use the 'num_of_buckets' option to avoid ungradability
        item_properties = if_model->get_item_line( line_index );
        item_name       = *item_properties->at(2);
        item_type       = *item_properties->at(3);
        item_width      = *item_properties->at(4);
        list_depth      = (item_properties->at(5))->toInt();
        is_list         = list_depth > 0;
        
        if( not item_name.isEmpty() and not item_width.isEmpty()  ) {
            // Only add a coverage item, if there is a given name and type
            is_time          = QString::compare( item_type, tr("time") ) == 0;
            is_no_cover_type = ( is_list or is_time );
            bit_width        =  (item_width.isEmpty()) ? (0) : (item_width.toInt());
            
            if( not is_no_cover_type ) {
                code->append( new QString("    item " + item_name +  " : " + item_type ));
                
                if( bit_width != 0 ) {
                    code->append( new QString("(bits: " + item_width + ") "));
                };
                
                code->append( new QString(" = mon_item." + item_name));
                
                if( bit_width > 4 ) {
                    // Aside from MIN and MAX values, the coverage range will be divided into 4 ranges between MAX and 
                    // MIN values
                    unsigned long range   = (1 << (bit_width-2));
                    
                    // Create min value bucket
                    code->append( new QString(" using ranges = {\n") );
                    code->append( new QString("      range([0]);\n") );
                    code->append( new QString("      range([1.."+ QString::number(range-1) + "]);\n") );
                    code->append( new QString("      range([" + QString::number(  range) + ".." + QString::number((2*range)-1) +"]);\n") );
                    code->append( new QString("      range([" + QString::number(2*range) + ".." + QString::number((3*range)-1) +"]);\n") );
                    code->append( new QString("      range([" + QString::number(3*range) + ".." + QString::number((4*range)-2) +"]);\n") );
                    code->append( new QString("      range([" + QString::number( (4*range)-1 ) + "])\n") );
                    code->append( new QString("    };   // end of range group\n") );
                } else {
                    code->append( new QString( ";\n"));
                };
                
            };   // end of handling coverable types
        };   // end of handling coverage item generation
    };   // end of iterating over each data item
    
    code->append( new QString("  \n") );
    code->append( new QString("  };   // end of cover item_detected_e\n") );
    
    code->append( new QString("};   // end of extend " + uvc_prefix + "_monitor_u\n") );
    code->append( new QString("'>\n"));
    
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_coverage.e",
        code
    );
};   // end of create_interface_coverage_file

void spec_e_interface_code_generator::create_interface_package_readme_file( spec_interface_model    *if_model ) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    qDebug() << "Generating file PACKAGE_README.txt";
    
    code->append( new QString("* Title: Interface eVC Package Information\n"));
    code->append( new QString("* Name: " + uvc_prefix + "\n"));
    code->append( new QString("* Version: 0.1\n"));
    code->append( new QString("* Description:\n"));
    code->append( new QString("  TODO: Add meaningful information on the UVC and the protocol that is being \n"));
    code->append( new QString("  implemented.\n"));
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/PACKAGE_README.txt",
        code
    );
};   // end of create_interface_package_readme_file

void spec_e_interface_code_generator::create_interface_verilog_testbench_file (spec_interface_model    *if_model) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    QString             *clock_hdl  = if_model->get_clock_properties().at(1);
    QString             *reset_hdl  = if_model->get_reset_properties().at(1);
    
    QList <QString*>    *signal_properties;
    QString             *signal_name;
    QString             *signal_width;
    QString             *signal_access;
    int                 signal_bit_width;
    
    spec_msg( "Generating file tb_top.sv" );
    
//    code->append( new QString(""));
    code->append( new QString("// File: " + uvc_prefix +"/hdl/tb_top.sv\n"));
    code->append( new QString("module tb_top;\n"));
    code->append( new QString("  reg " + *clock_hdl + ";\n"));
    code->append( new QString("  reg " + *reset_hdl + ";\n"));
    code->append( new QString("  \n"));
    code->append( new QString("  // Basic clock generation\n"));
    code->append( new QString("  always begin\n"));
    code->append( new QString("    #5 " + *clock_hdl + " <= ~" + *clock_hdl + ";\n"));
    code->append( new QString("  end\n"));
    code->append( new QString("  \n"));
    code->append( new QString(""));
    for( int line_index = 0; line_index < if_model->signal_count();++line_index) {
        // Process each given signal property line
        signal_properties   = if_model->get_signal_line( line_index );
        signal_name         = signal_properties->at(4);
        signal_width        = signal_properties->at(2);
        signal_access       = signal_properties->at(3);
        
        if( QString::compare( *signal_access, "HDL Read" ) == 0 ) {
            code->append( new QString("  wire "));
        } else {
            code->append( new QString("  reg "));
        };
        
        if( not signal_width->isEmpty() ) {
            signal_bit_width = signal_width->toInt();
            signal_bit_width -= 1;
            if( signal_bit_width > 0 ) {
                code->append( new QString( "[" + QString::number( signal_bit_width ) + ":0] " ) );
            };
        };
        code->append( new QString( *signal_name + ";\n"));
    };   // end of iterating over each signal name
    code->append( new QString("\n"));
    code->append( new QString("\n"));
    code->append( new QString("\n"));
    code->append( new QString("  // Initialize all values\n"));
    code->append( new QString("  initial begin\n"));
    code->append( new QString("    " + *clock_hdl + " = 1'b0;\n"));
    code->append( new QString("    " + *reset_hdl + " = 1'b0;\n"));
    
    for( int line_index = 0; line_index < if_model->signal_count();++line_index) {
        signal_properties   = if_model->get_signal_line( line_index );
        signal_name         = signal_properties->at(4);
        signal_access       = signal_properties->at(3);
        
        if( QString::compare( *signal_access, "HDL Read" ) != 0 ) {
            code->append( new QString("    " + *signal_name + " = 0;\n"));
        };
    };
    
    code->append( new QString("    #23;\n"));
    code->append( new QString("    " + *reset_hdl + " = 1'b1;\n"));
    code->append( new QString("    #1000;\n"));
    code->append( new QString("    $finish();\n"));
    code->append( new QString("  end\n"));
    code->append( new QString("endmodule\n"));
    
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/hdl/tb_top.sv",
        code
    );
};   // end of create_interface_verilog_testbench_file

void spec_e_interface_code_generator::create_interface_vhdl_testbench_file    (spec_interface_model    *if_model) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    QString             *clock_hdl  = if_model->get_clock_properties().at(1);
    QString             *reset_hdl  = if_model->get_reset_properties().at(1);
    
    QList <QString*>    *signal_properties;
    QString             *signal_name;
    QString             *signal_width;
    QString             *signal_access;
    int                 signal_bit_width;
    
    spec_msg( "Generating file tb_top.vhdl" );
    
    code -> append( new QString( "-- This VHDL file contains the top-level testbench as VHDL flavor\n" ) );
    code -> append( new QString( "library ieee;\n" ) );
    code -> append( new QString( "  use ieee.std_logic_1164.all;\n" ) );
    code -> append( new QString( "  use ieee.numeric_std_unsigned.all;\n" ) );
    code -> append( new QString( "  use ieee.numeric_std.all;\n" ) );
    code -> append( new QString( "  use work.all;\n" ) );
    code -> append( new QString( "\n" ) );
    code -> append( new QString( "-- Declare an empty testbench top entity\n" ) );
    code -> append( new QString( "entity TB_TOP is\n" ) );
    code -> append( new QString( "end entity TB_TOP;\n" ) );
    code -> append( new QString( "\n" ) );
    code -> append( new QString( "-- This contains the actual TB_TOP implementation\n" ) );
    code -> append( new QString( "architecture TBA of TB_TOP is\n" ) );
    
    // The clock and reset will always be created...
    code -> append( new QString( "  -- Declaring the clock signal and initializing it to 0\n" ));
    code -> append( new QString( "  signal " + *clock_hdl + " : std_logic := '0';\n" ) );
    code -> append( new QString( "  -- Declaring the reset signal and initializing it to 0\n" ));
    code -> append( new QString( "  signal " + *reset_hdl + " : std_logic := '0';\n" ) );
    code -> append( new QString( "  \n" ) );
    code -> append( new QString( "  -- Signals used by the protocol\n" ) );
    for( int line_index = 0; line_index < if_model->signal_count(); ++line_index ) {
        signal_properties   = if_model->get_signal_line( line_index );
        signal_name         = signal_properties->at(4);
        signal_width        = signal_properties->at(2);
        signal_access       = signal_properties->at(3);
        
        if( not signal_width->isEmpty() ) {
            signal_bit_width = signal_width -> toInt();
            signal_bit_width -= 1;
            if( signal_bit_width > 0 ) {
                // In case the signal is a a vector, we're going to generate a line that matches this kind of form:
                //        signal <vec_name> : std_logic_vector( 3 downto 0 ) := "0000";
                code -> append( new QString(
                    "  signal " + *signal_name + " : std_logic_vector( " + QString::number(signal_bit_width) + " downto 0 ) := \""
                    )
                );
                for( int bit_index = 0; bit_index <= signal_bit_width;++bit_index ) {
                    code -> append( new QString( "0" ) );
                };
                code -> append( new QString( "\";\n" ) );
            };
        } else {
            // For a single-bit signal we'll create a line that matches this form:
            //        signal <bit_name> : std_logic := '0';
            code -> append( new QString( "  signal " + *signal_name + " : std_logic := '0';\n" ) );
        };
    };  // end of declaring architecture signals
    code -> append( new QString( "begin\n" ) );
    code -> append( new QString( "  -- Provide a simple clock with 2x5ns = 10 ns interval cycle\n" ) );
    code -> append( new QString( "  " + *clock_hdl + " <= not " + *clock_hdl + " after 5 ns;\n" ) );
    code -> append( new QString( "  -- Assert the reset after 23 ns\n" ) );
    code -> append( new QString( "  " + *reset_hdl + " <= '1' after 23 ns;\n" ) );
    code -> append( new QString( "end architecture;\n" ) );
    code -> append( new QString( "\n" ) );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/hdl/tb_top.vhdl",
        code
    );
};   // end of create_interface_vhdl_testbench_file

void spec_e_interface_code_generator::create_interface_example_config_file    (
    spec_interface_model    *if_model
) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    spec_msg("Generating file single_active_interface.e");
    
    //    code->append( new QString(""));
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( if_model ) );
    
    code->append( new QString("// File: " + uvc_prefix +"/examples/single_active_interface.e\n"));
    code->append( new QString("<'\n"));
    code->append( new QString("extend " + uvc_prefix + "_env_name_t: [SINGLE_ACTIVE_INTERFACE];\n"));
    code->append( new QString("\n") );
    code->append( new QString("extend " + uvc_prefix + "_env_u {\n"));
    code->append( new QString("  // Set the newly given interface name.\n"));
    code->append( new QString("  // Recommended to avoid base class pollution.\n"));
    code->append( new QString("  keep env_name == SINGLE_ACTIVE_INTERFACE;\n"));
    code->append( new QString("  \n") );
    code->append( new QString("  keep SINGLE_ACTIVE_INTERFACE_C is all of {\n") );
    code->append( new QString("    // In this example, we only use a single agent\n") );
    code->append( new QString("    agents.size() == 1;\n") );
    code->append( new QString("    \n") );
    code->append( new QString("    // Also each agent needs to be configured. The most flexible way to achieve this\n") );
    code->append( new QString("    // is to iterate over each agent list element and perform the constraining\n") );
    code->append( new QString("    for each (agent) using index (agent_index) in agents {\n") );
    code->append( new QString("      // Assign each agent's interface name, based on the current list element index\n") );
    code->append( new QString("      agent.if_name                              == agent_index.as_a( " + uvc_prefix + "_if_name_t );\n") );
    code->append( new QString("      // Make sure the agent is ACTIVE to drive sequences. Constraint to PASSIVE for not driving\n") );
    code->append( new QString("      agent.active_passive                       == ACTIVE;\n") );
    code->append( new QString("      // In this example, we are not using a TLM port, because we do not have a counterpart\n") );
    code->append( new QString("      // that we would be able to connect it to\n") );
    code->append( new QString("      agent.agent_config.use_connected_tlm_port  == FALSE;\n") );
    code->append( new QString("    };  // end of iterating over each agent\n") );
    code->append( new QString("  };  // end of constraint block SINGLE_ACTIVE_INTERFACE_C\n") );
    code->append( new QString("};  // end of extend " + uvc_prefix + "_env_u\n") );
    
    code->append( new QString("\n") );
    code->append( new QString("// The instantiation hierarchy of EVERY testbench starts under the pre-defined object sys\n") );
    code->append( new QString("extend sys {\n") );
    code->append( new QString("  // Instantiate the root of the defined interface UVC environment\n") );
    code->append( new QString("  example_" + uvc_prefix + "_env : " + uvc_prefix + "_env_u is instance;\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  // To properly connect the HDL to the e domain, the HDL path property should point to\n") );
    code->append( new QString("  // to the HDL testbench top-level\n") );
    code->append( new QString("  keep TB_TOP_HDL_PATH_C is example_" + uvc_prefix + "_env.hdl_path() == \"~/tb_top\";\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  // It is also recommended to configure Specman with a few default settings that will\n") );
    code->append( new QString("  // improve debugging significantly using the predefined setup() method\n") );
    code->append( new QString("  setup() is also {\n") );
    code->append( new QString("    // Increase the max ticks of the simulation time\n") );
    code->append( new QString("    set_config( run, tick_max, MAX_INT);\n") );
    code->append( new QString("    \n") );
    
    code->append( new QString("    // Set the default printing format to be hexadecimal\n") );
    code->append( new QString("    set_config( print, radix, hex);\n") );
    code->append( new QString("    \n") );
    
    code->append( new QString("    // Increase the number of characters a line may have to 2048\n") );
    code->append( new QString("    set_config( print, line_size, 2048);\n") );
    code->append( new QString("    \n") );
    
    code->append( new QString("    // Increase the number of items that will be shown when a list is printed\n") );
    
    code->append( new QString("    set_config( databrowser, list_first_items, 64k);\n") );
    code->append( new QString("    \n") );
    
    code->append( new QString("    // Set the coverage database to use UNICOV\n") );
    code->append( new QString("    set_config( cover, database_format, ucd);\n") );
    code->append( new QString("    \n") );
    
    code->append( new QString("    // Enable e code coverage... this has to be done before loading files -> see script file\n") );
    code->append( new QString("//    set_config( cover, block_coverage_mode, on);\n") );
    code->append( new QString("    \n") );
    code->append( new QString("  };  // end of setup() extension\n") );
    code->append( new QString("};  // end of extending sys\n") );
    code->append( new QString("'>\n") );
    
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/examples/single_active_interface.e",
        code
    );
};  // end of create_interface_example_config_file

void spec_e_interface_code_generator::create_interface_scenario_traffic_file(spec_interface_model *if_model) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    spec_msg("Generating file examples/scenario_traffic.e");
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( if_model ) );
    
    code->append( new QString("// File: " + uvc_prefix +"/examples/scenario_traffic.e\n"));
    code->append( new QString("<'\n"));
    code->append( new QString("extend MAIN " + uvc_prefix + "_seq_s {\n"));
    code->append( new QString("  // The scenario item can assume any of the user-defined scenario stimulus types\n") );
    code->append( new QString("  !scenario_item : "+uvc_prefix + "_item_s;\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  // This constraint ensures that there will always be at least one of the defined scenarios driven\n") );
    code->append( new QString("  keep scenario_item.user_scenario != NONE;\n") );
    code->append( new QString("  \n") );
    code->append( new QString("  body()@driver.clock is only {\n") );
    code->append( new QString("    message(LOW, \"Starting SEQUENCE \", kind, \"...\");\n") );
    code->append( new QString("    // Let's simply generate about 20 scenarios\n") );
    code->append( new QString("    for scenario_index from 0 to 19 {\n") );
    code->append( new QString("      do scenario_item;\n") );
    code->append( new QString("    };  // end of iterating over generating each scenario\n") );
    code->append( new QString("    message(LOW, \"...SEQUENCE \", kind, \" ended!\");\n") );
    code->append( new QString("  };  // end of body() method\n") );
    code->append( new QString("};  // end of extending MAIN " + uvc_prefix + "_seq_s\n") );
    code->append( new QString("'>\n") );
    
    // TODO: Add a customizable footer string generator in here
//    code->append( get_footer_string() );
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/examples/scenario_traffic.e",
        code
    );
}   // end of create_interface_scenario_traffic_file

void spec_e_interface_code_generator::create_interface_script_files           (
    spec_interface_model    *if_model
) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    spec_msg("Generating file scripts/run_interactive");
    
    code->append( new QString("#!/bin/bash \n") );
    code->append( new QString("#echo \"SPECMAN_PATH is ${SPECMAN_PATH}\"  \n") );
    code->append( new QString("export INTERFACE_UVC=" + if_model->get_directory() + "\n") );
    code->append( new QString("echo \"(INFO) Running the eVC using \\${INTERFACE_UVC} path ${INTERFACE_UVC}\"\n") );
    code->append( new QString("  \n") );
    code->append( new QString("xrun \\\n") );
    code->append( new QString("     -gui \\\n") );
    code->append( new QString("     -snpath ${INTERFACE_UVC}\\\n") );
    code->append( new QString("     -snset \"set config cover -block_coverage_mode=on\"\\\n") );
    code->append( new QString("     ${INTERFACE_UVC}/" + uvc_prefix + "/hdl/tb_top.sv \\\n") );
    code->append( new QString("     -snload ${INTERFACE_UVC}/" + uvc_prefix + "/e/" + uvc_prefix + "_top.e \\\n") );
    code->append( new QString("     -snload ${INTERFACE_UVC}/" + uvc_prefix + "/examples/single_active_interface.e \\\n") );
    if( if_model->get_scenario_count() > 0 ) {
        code->append( new QString("     -snload ${INTERFACE_UVC}/" + uvc_prefix + "/examples/scenario_traffic.e \\\n") );
    };
    code->append( new QString("     $*\n") );
    code->append( new QString("\n") );
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/scripts/run_interactive",
        code
    );
    
    // Adding execution permission for user and group
    QFile script_file_verilog ( if_model->get_directory() + "/" + uvc_prefix + "/scripts/run_interactive" );
    script_file_verilog.setPermissions(
        QFile::ReadUser     |
        QFile::WriteUser    |
        QFile::ExeUser      |
        QFile::ReadGroup    |
        QFile::WriteGroup   |
        QFile::ExeGroup
    );
    
    // Also writing the VHDL run script in here:
    code->clear();
    spec_msg("Generating file scripts/run_interactive_vhdl");
    
    code->append( new QString("#!/bin/bash \n") );
    code->append( new QString("#echo \"SPECMAN_PATH is ${SPECMAN_PATH}\"  \n") );
    code->append( new QString("export INTERFACE_UVC=" + if_model->get_directory() + "\n") );
    code->append( new QString("  \n") );
    code->append( new QString("xrun \\\n") );
    code->append( new QString("     -gui \\\n") );
    code->append( new QString("     -snpath ${INTERFACE_UVC}\\\n") );
    code->append( new QString("     -snset \"set config cover -block_coverage_mode=on\"\\\n") );
    code->append( new QString("     -v200x \\\n") );
    code->append( new QString("     -top tb_top \\\n") );
    code->append( new QString("     ${INTERFACE_UVC}/" + uvc_prefix + "/hdl/tb_top.vhdl \\\n") );
    code->append( new QString("     -snload ${INTERFACE_UVC}/" + uvc_prefix + "/e/" + uvc_prefix + "_top.e \\\n") );
    code->append( new QString("     -snload ${INTERFACE_UVC}/" + uvc_prefix + "/examples/single_active_interface.e \\\n") );
    code->append( new QString("     $*\n") );
    code->append( new QString("\n") );
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/scripts/run_interactive_vhdl",
        code
    );
    
    // Adding execution permission for user and group
    QFile script_file_vhdl ( if_model->get_directory() + "/" + uvc_prefix + "/scripts/run_interactive_vhdl" );
    script_file_vhdl.setPermissions(
        QFile::ReadUser     |
        QFile::WriteUser    |
        QFile::ExeUser      |
        QFile::ReadGroup    |
        QFile::WriteGroup   |
        QFile::ExeGroup
    );
    
};  // end of create_interface_script_file

void spec_e_interface_code_generator::create_interface_dvt_build_file         ( spec_interface_model    *if_model ) {
    QString             uvc_prefix  = if_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    spec_msg("Generating file default.build");
    
    code->append( new QString("## This file contains build setup to be used with Amiq DVT Plugin for Eclipse\n"));
    code->append( new QString("## You can grab DVT Eclipse from here:\n"));
    code->append( new QString("##   https://dvteclipse.com\n"));
    code->append( new QString("## \n"));
    code->append( new QString("## Documentation on the syntax of this file can be found here:\n"));
    code->append( new QString("##   https://dvteclipse.com/documentation/e/Build_Configurations.html\n"));
    code->append( new QString("## \n"));
    code->append( new QString("## ============================================================================\n"));
    code->append( new QString("## //                         Configuration File Setup                       \\\\\n"));
    code->append( new QString("## ============================================================================\n"));
    code->append( new QString("## \n"));
    code->append( new QString("+dvt_init+ius.irun \n"));
    code->append( new QString("+dvt_setenv+SPECMAN_PATH=" + if_model->get_directory() + "\n"));
    code->append( new QString("e/" + uvc_prefix + "_top.e\n"));
    code->append( new QString("examples/single_active_interface.e\n"));
    if( if_model->get_scenario_count() > 0 ) {
        code->append( new QString("+dvt_test+examples/scenario_traffic.e\n"));
    };
    code->append( new QString("## ============================================================================\n"));
    
    write_file(
        if_model->get_directory() + "/" + uvc_prefix + "/.dvt/default.build",
        code
    );
};   // end of create_interface_dvt_build_file

void spec_e_interface_code_generator::create_interface_uvc         ( spec_interface_model    *if_model ) {
    QString uvc_directory   = if_model->get_directory();
    QString uvc_prefix      = if_model->get_prefix();
    
    spec_msg("Now generating UVC " << uvc_prefix << " in selected folder " << uvc_directory);
    
    QDir uvc_dir( uvc_directory );
    
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix                 );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/.dvt"       );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/e"          );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/scripts"    );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/examples"   );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/hdl"        );
    
    //=================================================================================================
    // This method call wraps generating the code string and writing the file.
    create_interface_top_file               ( if_model );
    
    //=================================================================================================
    create_interface_types_file             ( if_model );
    
    //=================================================================================================
    create_interface_macros_file            ( if_model );
    
    //=================================================================================================
    create_interface_agent_cfg_file         ( if_model );
    
    //=================================================================================================
    create_interface_env_cfg_file           ( if_model);
    
    //=================================================================================================
    create_interface_item_file              ( if_model );
    
    //=================================================================================================
    create_interface_signal_map_file        ( if_model );
    
    //=================================================================================================
    create_interface_monitor_file           ( if_model );
    
    //=================================================================================================
    create_interface_monitor_protocol_file  ( if_model );
    
    //=================================================================================================
    create_interface_driver_file            ( if_model );
    
    //=================================================================================================
    create_interface_bfm_file               ( if_model );
        
    //=================================================================================================
    create_interface_bfm_protocol_file      ( if_model );
    
    //=================================================================================================
    if( if_model->get_scenario_count() > 0 ) {
        // Only create this file, if there were scenarios defined
        create_interface_bfm_scenario_protocol( if_model );
    };
    
    //=================================================================================================
    create_interface_agent_file             ( if_model );
    
    //=================================================================================================
    create_interface_env_file               ( if_model );
    
    //=================================================================================================
    create_interface_checks_file            ( if_model );
    
    //=================================================================================================
    create_interface_seq_lib_file           ( if_model );
    
    //=================================================================================================
    create_interface_coverage_file          ( if_model );
    
    //=================================================================================================
    create_interface_package_readme_file    ( if_model );
    
    //=================================================================================================
    create_interface_verilog_testbench_file ( if_model );

    //=================================================================================================
    create_interface_vhdl_testbench_file    ( if_model );
    
    //=================================================================================================
    create_interface_example_config_file    ( if_model );
    
    //=================================================================================================
    if( if_model->get_scenario_count() > 0 ) {
        create_interface_scenario_traffic_file( if_model );
    };
    
    //=================================================================================================
    create_interface_script_files           ( if_model );
    
    //=================================================================================================
    create_interface_dvt_build_file         ( if_model );
    
    //=================================================================================================
    
//    foreach( QString* s, *debugStringList ) {
//        qDebug() << *s;
//    };
};   // end of create_interface_uvc
