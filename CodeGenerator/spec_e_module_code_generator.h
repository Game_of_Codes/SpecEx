// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_E_MODULE_CODE_GENERATOR_H
#define SPEC_E_MODULE_CODE_GENERATOR_H

#include <CodeGenerator/spec_e_code_generator.h>

class spec_e_module_code_generator : public spec_e_code_generator {
    public:
        //==============================================================================================================
        // Constructor
        spec_e_module_code_generator();
        
        //==============================================================================================================
        // Methods
        
        void create_module_uvc                              ( spec_module_model         *mod_model );
        
    private:
        //==============================================================================================================
        // Methods
        
        void    create_module_top_file                      ( spec_module_model         *mod_model );
        void    create_module_defines_file                  ( spec_module_model         *mod_model );
        void    create_module_types_file                    ( spec_module_model         *mod_model );
        void    create_module_macros_file                   ( spec_module_model         *mod_model );
        void    create_module_config_file                   ( spec_module_model         *mod_model );
        void    create_module_reference_model_top_file      ( spec_module_model         *mod_model );
        void    create_module_reference_model_shell_file    ( spec_module_model         *mod_model );
        void    create_module_scoreboard_file               ( spec_module_model         *mod_model );
        void    create_module_monitor_file                  ( spec_module_model         *mod_model );
        void    create_module_virtual_driver_file           ( spec_module_model         *mod_model );
        void    create_module_sequence_lib_file             ( spec_module_model         *mod_model );
        void    create_module_env_file                      ( spec_module_model         *mod_model );
        void    create_module_objections_file               ( spec_module_model         *mod_model );
        
        void    create_module_sve_config_file               ( spec_module_model         *mod_model );
        void    create_module_instantiation_file            ( spec_module_model         *mod_model );
        void    create_module_smoke_test_file              ( spec_module_model         *mod_model );
        
        void    create_module_verilog_testbench_file        ( spec_module_model         *mod_model );
        void    create_module_script_files                  ( spec_module_model         *mod_model );
        void    create_module_dvt_build_file                ( spec_module_model         *mod_model );
};

#endif // SPEC_E_MODULE_CODE_GENERATOR_H
