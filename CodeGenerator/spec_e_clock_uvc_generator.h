// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_E_CLOCK_UVC_GENERATOR_H
#define SPEC_E_CLOCK_UVC_GENERATOR_H

#include <CodeGenerator/spec_e_code_generator.h>

class spec_e_clock_uvc_generator : public spec_e_code_generator {
    public:
        //==============================================================================================================
        // Constructor
        spec_e_clock_uvc_generator();
        
        //==============================================================================================================
        // Methods
        
        void    create_clock_uvc                        ( spec_clock_model    *clk_model );
        
    private:
        //==============================================================================================================
        // Methods
        
        void    create_clock_top_file                   ( spec_clock_model    *clk_model );
        void    create_clock_types_file                 ( spec_clock_model    *clk_model );
        void    create_clock_signal_map_file            ( spec_clock_model    *clk_model );
        void    create_clock_static_driver_file         ( spec_clock_model    *clk_model );
        void    create_clock_monitor_file               ( spec_clock_model    *clk_model );
        void    create_clock_env_file                   ( spec_clock_model    *clk_model );
        void    create_clock_configuration_file         ( spec_clock_model    *clk_model );
        void    create_clock_agent_file                 ( spec_clock_model    *clk_model );
        
        void    create_clock_package_readme_file        ( spec_clock_model    *clk_model );
        
        void    create_clock_clk_gen_hdl_file           ( spec_clock_model    *clk_model );
        
        void    create_clock_single_clock_example_file  ( spec_clock_model    *clk_model );
        void    create_clock_multi_clock_example_file   ( spec_clock_model    *clk_model );
        
        void    create_clock_single_clock_hdl_file      ( spec_clock_model    *clk_model );
        void    create_clock_multi_clock_hdl_file       ( spec_clock_model    *clk_model );
        
        void    create_clock_single_clock_script_file   ( spec_clock_model    *clk_model );
        void    create_clock_multi_clock_script_file    ( spec_clock_model    *clk_model );
};

#endif // SPEC_E_CLOCK_UVC_GENERATOR_H
