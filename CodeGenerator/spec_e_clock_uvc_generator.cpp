// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_e_clock_uvc_generator.h"

spec_e_clock_uvc_generator::spec_e_clock_uvc_generator() {
    
};   // end of spec_e_clock_uvc_generator

void spec_e_clock_uvc_generator::create_clock_types_file                   ( spec_clock_model    *clk_model ) {
    QString uvc_prefix = clk_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    qDebug() << "Generating file " << (uvc_prefix + "_types.e");
    
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( clk_model ) );
    code->append( new QString("File: " + uvc_prefix + "_types.e\n") );
    code->append( new QString("<'\n"));
    code->append( new QString("type " + uvc_prefix + "_env_name_t    : [DEFAULT];\n"));
    code->append( new QString("type " + uvc_prefix + "_domain_name_t : [NONE];\n"));
    code->append( new QString("'>\n"));
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        clk_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_types.e",
        code
    );
};   // end of create_clock_types_file

void spec_e_clock_uvc_generator::create_clock_top_file                 ( spec_clock_model    *clk_model ) {
    QString uvc_prefix = clk_model->get_prefix();
    
    qDebug() << "Generating file " << (uvc_prefix + "_top.e");
    
    QList <QString*>    *code  = new QList <QString*>;
    
    // TODO: Add a customizable header string generator in here
//    code->append( get_file_header( clk_model ) );
    code->append( new QString("File: " + uvc_prefix + "_top.e" + "\n") );
    code->append( new QString("<'\n" ) );
    code->append( new QString("import uvm_e/e/uvm_e_top;\n") );
    code->append( new QString("\n") );
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_types;"  + "\n"));;
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_configuration;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_signal_map;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_monitor;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_static_driver;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_agent;"  + "\n"));
    code->append( new QString("import " + uvc_prefix + "/e/" + uvc_prefix + "_env;"  + "\n"));
    code->append( new QString("'>\n"));
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        clk_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_top.e",
        code
    );
};   // end of create_clock_top_file

void spec_e_clock_uvc_generator::create_clock_signal_map_file            ( spec_clock_model    *clk_model ) {
    QString uvc_prefix = clk_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    qDebug() << "Generating file " << (uvc_prefix + "_signal_map.e");
    
    // TODO: Add a customizable header string generator in here
    //    code->append( get_file_header( clk_model ) );
    code->append( new QString( "File: " + uvc_prefix + "_signal_map.e\n" ) );
    code->append( new QString( "<'\n" ) );
    code->append( new QString( "unit " + uvc_prefix + "_signal_map_u like uvm_signal_map {\n" ) );
    code->append( new QString( "  // The clock domaing field indicates the name of the clock\n" ) );
    code->append( new QString( "  clock_domain          : " + uvc_prefix + "_domain_name_t;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  clk_seed_op           : out simple_port of longuint       is instance;\n" ) );
    code->append( new QString( "  clk_ip                : in  simple_port of bit            is instance;\n" ) );
    code->append( new QString( "  clk_jitter_enable_op  : out simple_port of bit            is instance;\n" ) );
    code->append( new QString( "  clk_period_op         : out simple_port of uint(bits: 32) is instance;\n" ) );
    code->append( new QString( "  clk_shift_op          : out simple_port of uint(bits: 32) is instance;\n" ) );
    code->append( new QString( "  clk_init_op           : out simple_port of bit            is instance;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  keep CLOCK_SIGNAL_MAP_DEFAULT_C is all of {\n" ) );
    code->append( new QString( "    soft  clk_seed_op           .hdl_path() == \"sn_seed\";\n" ) );
    code->append( new QString( "    soft  clk_ip                .hdl_path() == \"clk\";\n" ) );
    code->append( new QString( "    soft  clk_jitter_enable_op  .hdl_path() == \"clk_jitter_en\";\n" ) );
    code->append( new QString( "    soft  clk_period_op         .hdl_path() == \"clk_period\";\n" ) );
    code->append( new QString( "    soft  clk_shift_op          .hdl_path() == \"clk_shift\";\n" ) );
    code->append( new QString( "    soft  clk_init_op           .hdl_path() == \"clk_init\";\n" ) );
    code->append( new QString( "    \n" ) );
    code->append( new QString( "    bind( clk_seed_op           , external );\n" ) );
    code->append( new QString( "    bind( clk_ip                , external );\n" ) );
    code->append( new QString( "    bind( clk_jitter_enable_op  , external );\n" ) );
    code->append( new QString( "    bind( clk_period_op         , external );\n" ) );
    code->append( new QString( "    bind( clk_shift_op          , external );\n" ) );
    code->append( new QString( "    bind( clk_init_op           , external );\n" ) );
    code->append( new QString( "  };   // end of CLOCK_SIGNAL_MAP_DEFAULT_C\n" ) );
    code->append( new QString( "};   // end of " + uvc_prefix + "_signal_map_u\n" ) );
    code->append( new QString("'>\n"));
    
    write_file(
        clk_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_signal_map.e",
        code
    );
};
void spec_e_clock_uvc_generator::create_clock_static_driver_file         ( spec_clock_model    *clk_model ) {
    QString uvc_prefix = clk_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    qDebug() << "Generating file " << (uvc_prefix + "_static_driver.e");
    
    // TODO: Add a customizable header string generator in here
    //    code->append( get_file_header( clk_model ) );
    code->append( new QString( "File: " + uvc_prefix + "_static_driver.e\n" ) );
    code->append( new QString( "<'\n" ) );
    code->append( new QString( "unit " + uvc_prefix + "_static_driver_u like uvm_base_unit {\n" ));
    code->append( new QString( "  // Pointer to the clock generator signal map\n" ) );
    code->append( new QString( "  !p_smp: " + uvc_prefix + "_signal_map_u;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  // Contains the configuration for the current interface's clocking parameters\n" ) );
    code->append( new QString( "  clk_conf: " + uvc_prefix + "_configuration_s;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  // This clock driver is not capable of changin clock speeds during the simulation.\n" ) );
    code->append( new QString( "  // Such a clock UVC has to be developed to suit the project's needs.\n" ) );
    code->append( new QString( "  run() is first {\n" ) );
    code->append( new QString( "    // Set the given Specman seed\n" ) );
    code->append( new QString( "    p_smp.clk_seed_op$ = covers.get_seed();\n" ) );
    code->append( new QString( "    \n" ) );
    code->append( new QString( "    // Set the jitter enable bit for this clock interface\n" ) );
    code->append( new QString( "    p_smp.clk_jitter_enable_op$ = (clk_conf.enable_jitter? 1'b1 : 1'b0);\n" ) );
    code->append( new QString( "    // Set the clock period for this interface\n" ) );
    code->append( new QString( "    p_smp.clk_period_op$ = clk_conf.clk_period;\n" ) );
    code->append( new QString( "    // Set the clock shift for the clock generator to drive relatively delayed clock to each other\n" ) );
    code->append( new QString( "    p_smp.clk_shift_op$  = clk_conf.clk_shift;\n" ) );
    code->append( new QString( "    // Set the clock initial level\n" ) );
    code->append( new QString( "    p_smp.clk_init_op$ = clk_conf.clk_init;\n" ) );
    code->append( new QString( "  };   // end of run() method\n" ) );
    code->append( new QString( "};   // end of " + uvc_prefix + "_static_driver_u\n" ));
    code->append( new QString( "'>\n" ) );
    
    write_file(
        clk_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_static_driver.e",
        code
    );
};
void spec_e_clock_uvc_generator::create_clock_monitor_file               ( spec_clock_model    *clk_model ) {
    QString uvc_prefix = clk_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    qDebug() << "Generating file " << (uvc_prefix + "_monitor.e");
    
    // TODO: Add a customizable header string generator in here
    //    code->append( get_file_header( clk_model ) );
    code->append( new QString( "File: " + uvc_prefix + "_monitor.e\n" ) );
    code->append( new QString( "<'\n" ) );
    code->append( new QString( "unit " + uvc_prefix + "_monitor_u like uvm_monitor {\n" ) );
    code->append( new QString( "  clock_domain  : " + uvc_prefix + "_domain_name_t;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  !p_smp        : " + uvc_prefix + "_signal_map_u;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  // TODO: This monitor does not yet have any kind of functionality implemented\n" ) );
    code->append( new QString( "};   // end of " + uvc_prefix + "_monitor_u\n" ) );
    
    
    code->append( new QString( "'>\n" ) );
    
    write_file(
        clk_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_monitor.e",
        code
    );
};
void spec_e_clock_uvc_generator::create_clock_env_file                   ( spec_clock_model    *clk_model ) {
    QString uvc_prefix = clk_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    qDebug() << "Generating file " << (uvc_prefix + "_env.e");
    
    // TODO: Add a customizable header string generator in here
    //    code->append( get_file_header( clk_model ) );
    code->append( new QString( "File: " + uvc_prefix + "_env.e\n" ) );
    code->append( new QString( "<'\n" ) );
    code->append( new QString( "unit " + uvc_prefix + "_env_u like uvm_env {\n" ) );
    code->append( new QString( "  name: " + uvc_prefix + "_env_name_t;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  // To cover multiple clock domains, instantiate one agent per clock\n" ) );
    code->append( new QString( "  agents: list of " + uvc_prefix + "_agent_u is instance;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  keep ENV_DEFAULT_C is all of {\n" ) );
    code->append( new QString( "    soft agents.size() == 1;\n" ) );
    code->append( new QString( "  };   // end of ENV_DEFAULT_C\n" ) );
    code->append( new QString( "};   // end of " + uvc_prefix + "_env_u\n" ) );
    
    code->append( new QString( "'>\n" ) );
    
    write_file(
        clk_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_env.e",
        code
    );
};
void spec_e_clock_uvc_generator::create_clock_configuration_file         ( spec_clock_model    *clk_model ) {
    QString uvc_prefix = clk_model->get_prefix();
    
    qDebug() << "Generating file " << (uvc_prefix + "_configuration.e");
    
    QList <QString*>    *code  = new QList <QString*>;
    
    // TODO: Add a customizable header string generator in here
    //    code->append( get_file_header( clk_model ) );
    code->append( new QString( "File: " + uvc_prefix + "_configuration.e" + "\n" ) );
    code->append( new QString( "<'\n" ) );
    code->append( new QString( "struct " + uvc_prefix + "_configuration_s like uvm_config_params_s {\n" ) );
    code->append( new QString( "  clock_domain : " + uvc_prefix + "_domain_name_t;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  // When set, this field enabled the clock jitter generation for each clock signal\n" ) );
    code->append( new QString( "  enable_jitter: bool;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  // These fields are added to allow for randomization of the phase relationship between various clocks\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  // This field contains the clock shift time in\n" ) );
    code->append( new QString( "  clk_shift : uint(bits: 32);\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  // This field contains the clock period time\n" ) );
    code->append( new QString( "  clk_period : uint(bits: 32);\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  // This field contains the clock initial level\n" ) );
    code->append( new QString( "  clk_init   : bit;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  keep CLOCK_DEFAULT_C is all of {\n" ) );
    code->append( new QString( "    soft enable_jitter == FALSE;\n" ) );
    code->append( new QString( "    soft clk_shift     in [32'd1..32'd9];\n" ) );
    code->append( new QString( "    soft clk_period    == 32'd10;\n" ) );
    code->append( new QString( "  };   // end of CLOCK_DEFAULT_C\n" ) );
    code->append( new QString( "};   // end of " + uvc_prefix + "_configuration_s\n" ) );
    code->append( new QString( "'>\n" ) );
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        clk_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_configuration.e",
        code
    );
};
void spec_e_clock_uvc_generator::create_clock_agent_file                 ( spec_clock_model    *clk_model ) {
    QString uvc_prefix = clk_model->get_prefix();
    
    qDebug() << "Generating file " << (uvc_prefix + "_agent.e");
    
    QList <QString*>    *code  = new QList <QString*>;
    
    // TODO: Add a customizable header string generator in here
    //    code->append( get_file_header( clk_model ) );
    code->append( new QString( "File: " + uvc_prefix + "_agent.e" + "\n" ) );
    code->append( new QString( "<'\n" ) );
    code->append( new QString( "unit " + uvc_prefix + "_agent_u like uvm_agent {\n" ) );
    code->append( new QString( "  clock_domain  : " + uvc_prefix + "_domain_name_t;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  conf          : " + uvc_prefix + "_configuration_s;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  // Signal map instantiation\n" ) );
    code->append( new QString( "  smp           : " + uvc_prefix + "_signal_map_u     is instance;\n" ) );
    code->append( new QString( "  // Monitor instantiation\n" ) );
    code->append( new QString( "  mon           : " + uvc_prefix + "_monitor_u is instance;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  keep AGENT_C is all of {\n" ) );
    code->append( new QString( "         clock_domain     == read_only( conf.clock_domain );\n" ) );
    code->append( new QString( "         mon.clock_domain == read_only( conf.clock_domain );\n" ) );
    code->append( new QString( "         smp.clock_domain == read_only( conf.clock_domain );\n" ) );
    code->append( new QString( "    soft active_passive   == ACTIVE;\n" ) );
    code->append( new QString( "  };   // end of AGENT_C\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  when ACTIVE'active_passive {\n" ) );
    code->append( new QString( "    // Static driver instantiation. This driver does not use any sequencing\n" ) );
    code->append( new QString( "    drv_static: " + uvc_prefix + "_static_driver_u is instance;\n" ) );
    code->append( new QString( "    \n" ) );
    code->append( new QString( "    keep ACTIVE_AGENT_C is all of {\n" ) );
    code->append( new QString( "      drv_static.clk_conf == read_only( conf );\n" ) );
    code->append( new QString( "    };   // end of ACTIVE_AGENT_C\n" ) );
    code->append( new QString( "    \n" ) );
    code->append( new QString( "    connect_pointers() is also {\n" ) );
    code->append( new QString( "      // Connect the driver's smp pointer with the actual signal map\n" ) );
    code->append( new QString( "      drv_static.p_smp = smp;\n" ) );
    code->append( new QString( "    };   // end of connect_pointers() method\n" ) );
    code->append( new QString( "  };   // end of ACTIVE'active_passive subtyping\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  connect_pointers() is also {\n" ) );
    code->append( new QString( "    // Connect the monitor's signal map with the actual signal map\n" ) );
    code->append( new QString( "    mon.p_smp = smp;\n" ) );
    code->append( new QString( "  };   // end of connect_pointers() method\n" ) );
    code->append( new QString( "};   // end of " + uvc_prefix + "_agent_u\n" ) );
    code->append( new QString( "'>\n" ) );
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        clk_model->get_directory() + "/" + uvc_prefix + "/e/" + uvc_prefix + "_agent.e",
        code
    );
};

void spec_e_clock_uvc_generator::create_clock_package_readme_file        ( spec_clock_model    *clk_model ) {
    
};

void spec_e_clock_uvc_generator::create_clock_clk_gen_hdl_file           ( spec_clock_model    *clk_model ) {
    QString             uvc_prefix  = clk_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    qDebug() << "Generating file clk_gen.sv";
    
    //    code->append( new QString(""));
    code->append( new QString( "// File: " + uvc_prefix +"/hdl/clk_gen.sv\n"));
    code->append( new QString( "module clock_generator(\n"));
    code->append( new QString( "  input wire [63: 0] seed,\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  input wire         clk_jitter_en,\n" ) );
    code->append( new QString( "  input wire [31: 0] clk_shift,\n" ) );
    code->append( new QString( "  input wire [31: 0] clk_period,\n" ) );
    code->append( new QString( "  input wire         clk_init,\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  output reg         clk\n" ) );
    code->append( new QString( ");\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  // Contains Specman seed\n" ) );
    code->append( new QString( "  reg [63: 0] sn_seed;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  // This task toggles the clk, based on the clk_half_period and the \n" ) );
    code->append( new QString( "  // jitter control parameters\n" ) );
    code->append( new QString( "  task clk_task();\n" ) );
    code->append( new QString( "    real    jitter_value;\n" ) );
    code->append( new QString( "    real    jitter_sum;\n" ) );
    code->append( new QString( "    \n" ) );
    code->append( new QString( "    #clk_shift ;\n" ) );
    code->append( new QString( "    \n" ) );
    code->append( new QString( "    // Only inject jitter if it is enabled\n" ) );
    code->append( new QString( "    if( clk_jitter_en == 1'b1 ) begin\n" ) );
    code->append( new QString( "      while( 1 ) begin\n" ) );
    code->append( new QString( "        // Limit the drift to a maximum of +/-1.5 time units\n" ) );
    code->append( new QString( "        if( jitter_sum >= 1.5 ) begin\n" ) );
    code->append( new QString( "          jitter_value = real'(clk_period)/2.0 - 0.1;\n" ) );
    code->append( new QString( "        end\n" ) );
    code->append( new QString( "        else if( jitter_sum <= -1.5 ) begin\n" ) );
    code->append( new QString( "          jitter_value = real'(clk_period)/2.0 + 0.1;\n" ) );
    code->append( new QString( "        end\n" ) );
    code->append( new QString( "        \n" ) );
    code->append( new QString( "        // Use the jitter sum variable to track drifts and limt them\n" ) );
    code->append( new QString( "        jitter_sum += (jitter_value - real'(clk_period)/2.0);\n" ) );
    code->append( new QString( "        \n" ) );
    code->append( new QString( "        #(jitter_value) clk = ~clk;\n" ) );
    code->append( new QString( "      end   // of jitter clock generator\n" ) );
    code->append( new QString( "    end else begin\n" ) );
    code->append( new QString( "      while( 1 ) begin\n" ) );
    code->append( new QString( "        #(clk_period/2) clk = ~clk;\n" ) );
    code->append( new QString( "      end\n" ) );
    code->append( new QString( "    end   // of non-jitter clock generator\n" ) );
    code->append( new QString( "  endtask : clk_task\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  function real get_next_edge_time( int mean_value, int deviation_value);\n" ) );
    code->append( new QString( "    // Declare a temp variable to store the distribution value\n" ) );
    code->append( new QString( "    real dist_normal;\n" ) );
    code->append( new QString( "    // Calculate the distribution\n" ) );
    code->append( new QString( "    dist_normal = $dist_normal( sn_seed[31:0], mean_value, deviation_value*100) / 100.0;\n" ) );
    code->append( new QString( "    // Cut-off any values below +/- 0.2\n" ) );
    code->append( new QString( "    if( dist_normal <= -0.2 ) begin\n" ) );
    code->append( new QString( "      return -0.2;\n" ) );
    code->append( new QString( "    end else if( dist_normal >= 0.2 ) begin\n" ) );
    code->append( new QString( "      return 0.2;\n" ) );
    code->append( new QString( "    end else begin\n" ) );
    code->append( new QString( "      return dist_normal;\n" ) );
    code->append( new QString( "    end\n" ) );
    code->append( new QString( "  endfunction : get_next_edge_time\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  initial begin\n" ) );
    code->append( new QString( "    // Simple delta delay to allow for values to be set by verification environment\n" ) );
    code->append( new QString( "    #0.0001 ;\n" ) );
    code->append( new QString( "    \n" ) );
    code->append( new QString( "    // seed has been passed from the e environment\n" ) );
    code->append( new QString( "    sn_seed = seed;\n" ) );
    code->append( new QString( "    \n" ) );
    code->append( new QString( "    clk = clk_init;\n" ) );
    code->append( new QString( "    \n" ) );
    code->append( new QString( "    fork\n" ) );
    code->append( new QString( "      begin\n" ) );
    code->append( new QString( "        clk_task();\n" ) );
    code->append( new QString( "      end\n" ) );
    code->append( new QString( "    join_none\n" ) );
    code->append( new QString( "  end\n" ) );
    
    code->append( new QString( "\n" ) );
    
    code->append( new QString( "endmodule\n" ) );
    
    write_file(
        clk_model->get_directory() + "/" + uvc_prefix + "/hdl/clk_gen.sv",
        code
    );
};   // end of create_clock_clk_gen_hdl_file

void spec_e_clock_uvc_generator::create_clock_single_clock_example_file  ( spec_clock_model    *clk_model ) {
    QString             uvc_prefix  = clk_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    qDebug() << "Generating file demo_single_clock.e";
    
    // TODO: Add a customizable header string generator in here
    //    code->append( get_file_header( clk_model ) );
    code->append( new QString( "File: " + uvc_prefix + "/demo/demo_single_clock.e" + "\n" ) );
    code->append( new QString( "<'\n" ) );
    
    code->append( new QString( "import " + uvc_prefix + "/e/" + uvc_prefix + "_top;\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "extend " + uvc_prefix + "_name_t        : [TB_NAME];\n" ) );
    code->append( new QString( "extend " + uvc_prefix + "_domain_name_t : [SINGLE_CLK];\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "extend sys {\n" ) );
    code->append( new QString( "  clk_single_env: " + uvc_prefix + "_env_u is instance;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  keep clk_single_env.hdl_path() == \"~/tb_top_single_clock\";\n" ) );
    code->append( new QString( "};   // end of extend sys\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "extend SINGLE_CLK " + uvc_prefix + "_configuration_s {\n" ) );
    code->append( new QString( "  keep TEST_CONFIG_SINGLE_CLOCK_C is all of {\n" ) );
    code->append( new QString( "    enable_jitter.reset_soft();\n" ) );
    code->append( new QString( "    clk_shift.reset_soft();\n" ) );
    code->append( new QString( "    clk_period.reset_soft();\n" ) );
    code->append( new QString( "    \n" ) );
    code->append( new QString( "    clk_period in [32'd5..32'd15];\n" ) );
    code->append( new QString( "    clk_shift <= read_only( clk_period );\n" ) );
    code->append( new QString( "  };   // end of TEST_CONFIG_SINGLE_CLOCK_S\n" ) );
    code->append( new QString( "};   // end of SINGLE_CLK " + uvc_prefix + "_configuration_s\n" ) );
    
    code->append( new QString( "'>\n" ) );
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        clk_model->get_directory() + "/" + uvc_prefix + "/demo/demo_single_clock.e",
        code
    );
};
void spec_e_clock_uvc_generator::create_clock_multi_clock_example_file   ( spec_clock_model    *clk_model ) {
    QString             uvc_prefix  = clk_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    qDebug() << "Generating file demo_multi_clock.e";
    
    // TODO: Add a customizable header string generator in here
    //    code->append( get_file_header( clk_model ) );
    code->append( new QString( "File: " + uvc_prefix + "/demo/demo_multi_clock.e" + "\n" ) );
    code->append( new QString( "<'\n" ) );
    
    code->append( new QString( "import " + uvc_prefix + "/e/" + uvc_prefix + "_top;\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "extend " + uvc_prefix + "_name_t        : [MULTI_CLOCK_ENV];\n" ) );
    code->append( new QString( "extend " + uvc_prefix + "_domain_name_t : [CLOCK_DOMAIN_0, CLOCK_DOMAIN_1, CLOCK_DOMAIN_2];\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "extend sys {\n" ) );
    code->append( new QString( "  clk_multi_env: " + uvc_prefix + "_env_u is instance;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  keep clk_multi_env.hdl_path() == \"~/tb_top_multi_clock\";\n" ) );
    code->append( new QString( "};   // end of sys\n" ) );
    
    code->append( new QString( "extend MULTI_CLK_ENV " + uvc_prefix + "_env_u {\n" ) );
    code->append( new QString( "  keep MULTI_AGENTS_C is all of {\n" ) );
    code->append( new QString( "    agents.size() == 3;\n" ) );
    code->append( new QString( "    \n" ) );
    code->append( new QString( "    for each (agent) in agents {\n" ) );
    code->append( new QString( "      (index == 0) => (it.clock_domain == CLOCK_DOMAIN_0);\n" ) );
    code->append( new QString( "      (index == 1) => (it.clock_domain == CLOCK_DOMAIN_1);\n" ) );
    code->append( new QString( "      (index == 2) => (it.clock_domain == CLOCK_DOMAIN_2);\n" ) );
    code->append( new QString( "    };   // end of iterating over each agent\n" ) );
    code->append( new QString( "  };   // end of MULTI_AGENTS_C\n" ) );
    code->append( new QString( "};   // end of SINGLE_CLK " + uvc_prefix + "_configuration_s\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "extend CLOCK_DOMAIN_0 " + uvc_prefix + "_configuration_s {\n" ) );
    code->append( new QString( "  keep CLOCK_DOMAIN_CONF_0_C is all of {\n" ) );
    code->append( new QString( "    enable_jitter.reset_soft();\n" ) );
    code->append( new QString( "    clk_shift.reset_soft();\n" ) );
    code->append( new QString( "    clk_period.reset_soft();\n" ) );
    code->append( new QString( "    \n" ) );
    code->append( new QString( "    clk_period == 20;\n" ) );
    code->append( new QString( "    clk_shift <= read_only( clk_period );\n" ) );
    code->append( new QString( "  };   // end of CLOCK_DOMAIN_CONF_0_C\n" ) );
    code->append( new QString( "};   // end of CLOCK_DOMAIN_0 " + uvc_prefix + "_configuration_s\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "extend CLOCK_DOMAIN_0 " + uvc_prefix + "_smp_u {\n" ) );
    code->append( new QString( "  keep CLOCK_DOMAIN_0_HDL_PATH_C is all of {\n" ) );
    code->append( new QString( "    clk_seed_op .hdl_path() == \"sn_seed_0\";\n" ) );
    code->append( new QString( "    clk_iop     .hdl_path() == \"clk_0\";\n" ) );
    code->append( new QString( "    clk_jitter  .hdl_path() == \"clk_jitter_en_0\";\n" ) );
    code->append( new QString( "    clk_period  .hdl_path() == \"clk_period_0\";\n" ) );
    code->append( new QString( "    clk_shift_op.hdl_path() == \"clk_shift_0\";\n" ) );
    code->append( new QString( "    clk_init_op .hdl_path() == \"clk_init_0\";\n" ) );
    code->append( new QString( "  };   // end of CLOCK_DOMAIN_0_HDL_PATH_C\n" ) );
    code->append( new QString( "};   // end of CLOCK_DOMAIN_0 " + uvc_prefix + "_smp_u\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "extend CLOCK_DOMAIN_1 " + uvc_prefix + "_configuration_s {\n" ) );
    code->append( new QString( "  keep CLOCK_DOMAIN_CONF_1_C is all of {\n" ) );
    code->append( new QString( "    enable_jitter.reset_soft();\n" ) );
    code->append( new QString( "    clk_shift.reset_soft();\n" ) );
    code->append( new QString( "    clk_period.reset_soft();\n" ) );
    code->append( new QString( "    \n" ) );
    code->append( new QString( "    clk_period == 40;\n" ) );
    code->append( new QString( "    clk_shift <= read_only( clk_period );\n" ) );
    code->append( new QString( "  };   // end of CLOCK_DOMAIN_CONF_1_C\n" ) );
    code->append( new QString( "};   // end of CLOCK_DOMAIN_1 " + uvc_prefix + "_configuration_s\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "extend CLOCK_DOMAIN_1 " + uvc_prefix + "_smp_u {\n" ) );
    code->append( new QString( "  keep CLOCK_DOMAIN_1_HDL_PATH_C is all of {\n" ) );
    code->append( new QString( "    clk_seed_op .hdl_path() == \"sn_seed_1\";\n" ) );
    code->append( new QString( "    clk_iop     .hdl_path() == \"clk_1\";\n" ) );
    code->append( new QString( "    clk_jitter  .hdl_path() == \"clk_jitter_en_1\";\n" ) );
    code->append( new QString( "    clk_period  .hdl_path() == \"clk_period_1\";\n" ) );
    code->append( new QString( "    clk_shift_op.hdl_path() == \"clk_shift_1\";\n" ) );
    code->append( new QString( "    clk_init_op .hdl_path() == \"clk_init_1\";\n" ) );
    code->append( new QString( "  };   // end of CLOCK_DOMAIN_1_HDL_PATH_C\n" ) );
    code->append( new QString( "};   // end of CLOCK_DOMAIN_1 " + uvc_prefix + "_smp_u\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "extend CLOCK_DOMAIN_2 " + uvc_prefix + "_configuration_s {\n" ) );
    code->append( new QString( "  keep CLOCK_DOMAIN_CONF_2_C is all of {\n" ) );
    code->append( new QString( "    enable_jitter.reset_soft();\n" ) );
    code->append( new QString( "    clk_shift.reset_soft();\n" ) );
    code->append( new QString( "    clk_period.reset_soft();\n" ) );
    code->append( new QString( "    \n" ) );
    code->append( new QString( "    clk_period == 11;\n" ) );
    code->append( new QString( "    clk_shift <= read_only( clk_period );\n" ) );
    code->append( new QString( "  };   // end of CLOCK_DOMAIN_CONF_2_C\n" ) );
    code->append( new QString( "};   // end of CLOCK_DOMAIN_2 " + uvc_prefix + "_configuration_s\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "extend CLOCK_DOMAIN_2 " + uvc_prefix + "_smp_u {\n" ) );
    code->append( new QString( "  keep CLOCK_DOMAIN_2_HDL_PATH_C is all of {\n" ) );
    code->append( new QString( "    clk_seed_op .hdl_path() == \"sn_seed_2\";\n" ) );
    code->append( new QString( "    clk_iop     .hdl_path() == \"clk_2\";\n" ) );
    code->append( new QString( "    clk_jitter  .hdl_path() == \"clk_jitter_en_2\";\n" ) );
    code->append( new QString( "    clk_period  .hdl_path() == \"clk_period_2\";\n" ) );
    code->append( new QString( "    clk_shift_op.hdl_path() == \"clk_shift_2\";\n" ) );
    code->append( new QString( "    clk_init_op .hdl_path() == \"clk_init_2\";\n" ) );
    code->append( new QString( "  };   // end of CLOCK_DOMAIN_2_HDL_PATH_C\n" ) );
    code->append( new QString( "};   // end of CLOCK_DOMAIN_2 " + uvc_prefix + "_smp_u\n" ) );
    code->append( new QString( "\n" ) );
    
    code->append( new QString( "'>\n" ) );
    // TODO: Add a customizable footer string generator in here
    //    code->append( get_footer_string() );
    
    // Write the file to the user-declared directory
    write_file(
        clk_model->get_directory() + "/" + uvc_prefix + "/demo/demo_multi_clock.e",
        code
    );
};

void spec_e_clock_uvc_generator::create_clock_single_clock_hdl_file      ( spec_clock_model    *clk_model ) {
    QString             uvc_prefix  = clk_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    qDebug() << "Generating file tb_single_clock.sv";
    
    //    code->append( new QString(""));
    code->append( new QString( "// File: " + uvc_prefix +"/clock_demo/tb_single_clock.sv\n"));
    code->append( new QString( "\n"));
    code->append( new QString( "`resetall\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "module tb_top_single_clock;\n" ) );
    code->append( new QString( "  wire clk;\n" ) );
    code->append( new QString( "  reg [63: 0] sn_seed;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  reg         clk_jitter_en;\n" ) );
    code->append( new QString( "  reg [31: 0] clk_shift;\n" ) );
    code->append( new QString( "  reg [31: 0] clk_period;\n" ) );
    code->append( new QString( "  reg         clk_init;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  clock_generator clk_I0(\n" ) );
    code->append( new QString( "    .seed            ( sn_seed       ),\n" ) );
    code->append( new QString( "    .clk_jitter_en   ( clk_jitter_en ),\n" ) );
    code->append( new QString( "    .clk_shift       ( clk_shift     ),\n" ) );
    code->append( new QString( "    .clk_period      ( clk_period    ),\n" ) );
    code->append( new QString( "    .clk_init        ( clk_init      ),\n" ) );
    code->append( new QString( "    .clk             ( clk           )\n" ) );
    code->append( new QString( "  );\n" ) );
    code->append( new QString( "endmodule\n" ) );
    code->append( new QString( "\n" ) );
    
    write_file(
        clk_model->get_directory() + "/" + uvc_prefix + "/demo/tb_single_clock.sv",
        code
    );
};
void spec_e_clock_uvc_generator::create_clock_multi_clock_hdl_file       ( spec_clock_model    *clk_model ) {
    QString             uvc_prefix  = clk_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    qDebug() << "Generating file tb_multi_clock.sv";
    
    //    code->append( new QString(""));
    code->append( new QString( "// File: " + uvc_prefix +"/demo/tb_multi_clock.sv\n"));
    code->append( new QString( "\n"));
    code->append( new QString( "`resetall\n" ) );
    code->append( new QString( "\n" ) );
    code->append( new QString( "module tb_top_multi_clock;\n" ) );
    code->append( new QString( "  wire clk_0;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  reg [63: 0] sn_seed_0;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  reg         clk_jitter_en_0;\n" ) );
    code->append( new QString( "  reg [31: 0] clk_shift_0;\n" ) );
    code->append( new QString( "  reg [31: 0] clk_period_0;\n" ) );
    code->append( new QString( "  reg         clk_init_0;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  wire clk_1;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  reg [63: 0] sn_seed_1;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  reg         clk_jitter_en_1;\n" ) );
    code->append( new QString( "  reg [31: 0] clk_shift_1;\n" ) );
    code->append( new QString( "  reg [31: 0] clk_period_1;\n" ) );
    code->append( new QString( "  reg         clk_init_1;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  wire clk_2;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  reg [63: 0] sn_seed_2;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  reg         clk_jitter_en_2;\n" ) );
    code->append( new QString( "  reg [31: 0] clk_shift_2;\n" ) );
    code->append( new QString( "  reg [31: 0] clk_period_2;\n" ) );
    code->append( new QString( "  reg         clk_init_2;\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  clock_generator clk_I0(\n" ) );
    code->append( new QString( "    .seed            ( sn_seed_0       ),\n" ) );
    code->append( new QString( "    .clk_jitter_en   ( clk_jitter_en_0 ),\n" ) );
    code->append( new QString( "    .clk_shift       ( clk_shift_0     ),\n" ) );
    code->append( new QString( "    .clk_period      ( clk_period_0    ),\n" ) );
    code->append( new QString( "    .clk_init        ( clk_init_0      ),\n" ) );
    code->append( new QString( "    .clk             ( clk_0           )\n" ) );
    code->append( new QString( "  );\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  clock_generator clk_I1(\n" ) );
    code->append( new QString( "    .seed            ( sn_seed_1       ),\n" ) );
    code->append( new QString( "    .clk_jitter_en   ( clk_jitter_en_1 ),\n" ) );
    code->append( new QString( "    .clk_shift       ( clk_shift_1     ),\n" ) );
    code->append( new QString( "    .clk_period      ( clk_period_1    ),\n" ) );
    code->append( new QString( "    .clk_init        ( clk_init_1      ),\n" ) );
    code->append( new QString( "    .clk             ( clk_1           )\n" ) );
    code->append( new QString( "  );\n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  \n" ) );
    code->append( new QString( "  clock_generator clk_I2(\n" ) );
    code->append( new QString( "    .seed            ( sn_seed_2       ),\n" ) );
    code->append( new QString( "    .clk_jitter_en   ( clk_jitter_en_2 ),\n" ) );
    code->append( new QString( "    .clk_shift       ( clk_shift_2     ),\n" ) );
    code->append( new QString( "    .clk_period      ( clk_period_2    ),\n" ) );
    code->append( new QString( "    .clk_init        ( clk_init_2      ),\n" ) );
    code->append( new QString( "    .clk             ( clk_2           )\n" ) );
    code->append( new QString( "  );\n" ) );
    code->append( new QString( "endmodule\n" ) );
    code->append( new QString( "\n" ) );
    
    write_file(
        clk_model->get_directory() + "/" + uvc_prefix + "/demo/tb_multi_clock.sv",
        code
    );
};

void spec_e_clock_uvc_generator::create_clock_single_clock_script_file   ( spec_clock_model    *clk_model ) {
    QString             uvc_prefix  = clk_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    qDebug() << "Generating file scripts/run_single_clock";
    
    code->append( new QString("#!/bin/csh\n") );
    code->append( new QString("echo \"SPECMAN_PATH is ${SPECMAN_PATH}\"  \n") );
    code->append( new QString("setenv CLOCK_UVC " + clk_model->get_directory() + "\n") );
    code->append( new QString("  \n") );
    code->append( new QString("xrun \\\n") );
    code->append( new QString("     -nosncomp \\\n") );
    code->append( new QString("     -snpath ${CLOCK_UVC} \\\n") );
    code->append( new QString("     ${CLOCK_LIBRARY}/"+ uvc_prefix + "/demo/demo_single_clock.e \\\n") );
    code->append( new QString("     ${CLOCK_LIBRARY}/"+ uvc_prefix + "/demo/tb_single_clock.sv \\\n") );
    code->append( new QString("     ${CLOCK_LIBRARY}/"+ uvc_prefix + "/hdl/clk_gen.sv \\\n") );
    code->append( new QString("     -access rw \\\n") );
    code->append( new QString("     -createdebugdb \\\n") );
    code->append( new QString("     -gui \\\n") );
    code->append( new QString("     \\\n") );
    code->append( new QString("\\\n") );
    
    write_file(
        clk_model->get_directory() + "/" + uvc_prefix + "/scripts/run_single_clock",
        code
    );
    // Adding execution permission for user and group
    QFile script_file ( clk_model->get_directory() + "/" + uvc_prefix + "/scripts/run_single_clock" );
    script_file.setPermissions(
        QFile::ReadUser     |
        QFile::WriteUser    |
        QFile::ExeUser      |
        QFile::ReadGroup    |
        QFile::WriteGroup   |
        QFile::ExeGroup
    );
};
void spec_e_clock_uvc_generator::create_clock_multi_clock_script_file    ( spec_clock_model    *clk_model ) {
    QString             uvc_prefix  = clk_model->get_prefix();
    QList <QString*>    *code       = new QList <QString*>;
    
    qDebug() << "Generating file scripts/run_multi_clock";
    
    code->append( new QString("#!/bin/csh\n") );
    code->append( new QString("echo \"SPECMAN_PATH is ${SPECMAN_PATH}\"  \n") );
    code->append( new QString("setenv CLOCK_UVC " + clk_model->get_directory() + "\n") );
    code->append( new QString("  \n") );
    code->append( new QString("xrun \\\n") );
    code->append( new QString("     -nosncomp \\\n") );
    code->append( new QString("     -snpath ${CLOCK_UVC} \\\n") );
    code->append( new QString("     ${CLOCK_LIBRARY}/"+ uvc_prefix + "/demo/demo_multi_clock.e \\\n") );
    code->append( new QString("     ${CLOCK_LIBRARY}/"+ uvc_prefix + "/demo/tb_multi_clock.sv \\\n") );
    code->append( new QString("     ${CLOCK_LIBRARY}/"+ uvc_prefix + "/hdl/clk_gen.sv \\\n") );
    code->append( new QString("     -access rw \\\n") );
    code->append( new QString("     -debug_opts verisium_interactive \\\n") );
    code->append( new QString("     \\\n") );
    code->append( new QString("\\\n") );
    
    write_file(
        clk_model->get_directory() + "/" + uvc_prefix + "/scripts/run_multi_clock",
        code
    );
    // Adding execution permission for user and group
    QFile script_file ( clk_model->get_directory() + "/" + uvc_prefix + "/scripts/run_multi_clock" );
    script_file.setPermissions(
        QFile::ReadUser     |
        QFile::WriteUser    |
        QFile::ExeUser      |
        QFile::ReadGroup    |
        QFile::WriteGroup   |
        QFile::ExeGroup
    );
};


void spec_e_clock_uvc_generator::create_clock_uvc                        ( spec_clock_model    *clk_model ) {
    QString uvc_directory = clk_model->get_directory();
    QString uvc_prefix    = clk_model->get_prefix();
    
    qDebug() << "Now generating CLOCK UVC " << uvc_prefix << " in selected folder " << uvc_directory;
    
    QDir uvc_dir( uvc_directory );
    
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix                 );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/.dvt"       );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/e"          );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/scripts"    );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/demo"       );
    uvc_dir.mkdir( uvc_directory + "/" + uvc_prefix + "/hdl"        );
    
    // Creating files in folder uvc_directory + "/" + uvc_prefix + "/e"
    //==================================================================================================================
    create_clock_top_file                   ( clk_model );
    
    //==================================================================================================================
    create_clock_types_file                 ( clk_model );
    
    //==================================================================================================================
    create_clock_signal_map_file            ( clk_model );
    
    //==================================================================================================================
    create_clock_static_driver_file         ( clk_model );
    
    //==================================================================================================================
    create_clock_monitor_file               ( clk_model );
    
    //==================================================================================================================
    create_clock_env_file                   ( clk_model );
    
    //==================================================================================================================
    create_clock_configuration_file         ( clk_model );
    
    //==================================================================================================================
    create_clock_agent_file                 ( clk_model );
    
    //==================================================================================================================
    create_clock_package_readme_file        ( clk_model );
    
    //==================================================================================================================
    create_clock_clk_gen_hdl_file           ( clk_model );
    
    //==================================================================================================================
    create_clock_single_clock_example_file  ( clk_model );
    
    //==================================================================================================================
    create_clock_multi_clock_example_file   ( clk_model );
    
    //==================================================================================================================
    create_clock_single_clock_hdl_file      ( clk_model );
    
    //==================================================================================================================
    create_clock_multi_clock_hdl_file       ( clk_model );
    
    //==================================================================================================================
    create_clock_single_clock_script_file   ( clk_model );
    
    //==================================================================================================================
    create_clock_multi_clock_script_file    ( clk_model );
};   // end of create_clock_uvc
