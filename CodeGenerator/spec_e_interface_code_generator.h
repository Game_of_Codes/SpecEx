// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_E_INTERFACE_CODE_GENERATOR_H
#define SPEC_E_INTERFACE_CODE_GENERATOR_H

#include <CodeGenerator/spec_e_code_generator.h>

class spec_e_interface_code_generator : public spec_e_code_generator {
    public:
        //==============================================================================================================
        // Constructor
        spec_e_interface_code_generator();
        
        //==============================================================================================================
        // Methods
        
        void create_interface_uvc                       ( spec_interface_model      *if_model  );
        
    private:
        //==============================================================================================================
        // Methods
        
        void    create_interface_top_file               ( spec_interface_model    *if_model );
        void    create_interface_types_file             ( spec_interface_model    *if_model );
        void    create_interface_macros_file            ( spec_interface_model    *if_model );
        void    create_interface_agent_cfg_file         ( spec_interface_model    *if_model );
        void    create_interface_env_cfg_file           ( spec_interface_model    *if_model );
        void    create_interface_item_file              ( spec_interface_model    *if_model );
        void    create_interface_signal_map_file        ( spec_interface_model    *if_model );
        void    create_interface_monitor_file           ( spec_interface_model    *if_model );
        
        // TODO: This method should be connected with the data-set of the waveform tool
        void    create_interface_monitor_protocol_file  ( spec_interface_model    *if_model );
        
        void    create_interface_driver_file            ( spec_interface_model    *if_model );
        void    create_interface_bfm_file               ( spec_interface_model    *if_model );
        void    create_interface_bfm_protocol_file      ( spec_interface_model    *if_model );
        void    create_interface_bfm_scenario_protocol  ( spec_interface_model    *if_model );
        void    create_interface_agent_file             ( spec_interface_model    *if_model );
        void    create_interface_env_file               ( spec_interface_model    *if_model );
        void    create_interface_checks_file            ( spec_interface_model    *if_model );
        void    create_interface_seq_lib_file           ( spec_interface_model    *if_model );
        void    create_interface_coverage_file          ( spec_interface_model    *if_model );
        void    create_interface_package_readme_file    ( spec_interface_model    *if_model );
        void    create_interface_verilog_testbench_file ( spec_interface_model    *if_model );
        void    create_interface_vhdl_testbench_file    ( spec_interface_model    *if_model );
        void    create_interface_example_config_file    ( spec_interface_model    *if_model );
        void    create_interface_scenario_traffic_file  ( spec_interface_model    *if_model );
        void    create_interface_script_files           ( spec_interface_model    *if_model );
        void    create_interface_dvt_build_file         ( spec_interface_model    *if_model );
};

#endif // SPEC_E_INTERFACE_CODE_GENERATOR_H
