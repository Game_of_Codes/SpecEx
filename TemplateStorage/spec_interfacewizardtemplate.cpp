// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_interfacewizardtemplate.h"

spec_InterfaceWizardTemplate::spec_InterfaceWizardTemplate(QObject *parent) : QObject(parent) {
    
};

void    spec_InterfaceWizardTemplate::appendInterfaceValues(
    QString name,
    QString type,
    QString width,
    QString direction,
    QString hdlName
) {
    spec_DataContainer *newContainer = new spec_DataContainer();
    newContainer->appendString( name      );
    newContainer->appendString( type      );
    newContainer->appendString( width     );
    newContainer->appendString( direction );
    newContainer->appendString( hdlName   );
    
    interfaceSignalList->append( newContainer );
};

void    spec_InterfaceWizardTemplate::appendDataItemValues(
        bool isDNG,
        bool isPhysical,
        QString name, 
        QString type,
        QString width,
        bool isListOf,
        QString defaultName
) {
    spec_DataContainer *newContainer = new spec_DataContainer();
    newContainer->appendBool    ( isDNG       );
    newContainer->appendBool    ( isPhysical  );
    newContainer->appendString  ( name        );
    newContainer->appendString  ( type        );
    newContainer->appendString  ( width       );
    newContainer->appendBool    ( isListOf    );
    newContainer->appendString  ( defaultName );
    
    dataItemList->append( newContainer );
};

void    spec_InterfaceWizardTemplate::appendGlobalConfigurationValues(
        bool isDNG,
        bool isPhysical,
        QString name, 
        QString type,
        QString width,
        bool isListOf,
        QString defaultName
) {
    spec_DataContainer *newContainer = new spec_DataContainer();
    newContainer->appendBool    ( isDNG       );
    newContainer->appendBool    ( isPhysical  );
    newContainer->appendString  ( name        );
    newContainer->appendString  ( type        );
    newContainer->appendString  ( width       );
    newContainer->appendBool    ( isListOf    );
    newContainer->appendString  ( defaultName );

    globalConfigList->append( newContainer );
};

void    spec_InterfaceWizardTemplate::appendInterfaceConfigurationValues(
        bool isDNG,
        bool isPhysical,
        QString name, 
        QString type,
        QString width,
        bool isListOf,
        QString defaultName
) {
    spec_DataContainer *newContainer = new spec_DataContainer();
    newContainer->appendBool    ( isDNG       );
    newContainer->appendBool    ( isPhysical  );
    newContainer->appendString  ( name        );
    newContainer->appendString  ( type        );
    newContainer->appendString  ( width       );
    newContainer->appendBool    ( isListOf    );
    newContainer->appendString  ( defaultName );

    interfaceConfigList->append( newContainer );
};

QList <spec_DataContainer*>   spec_InterfaceWizardTemplate::getSignalList() {
    return *interfaceSignalList;
};

spec_DataContainer*           spec_InterfaceWizardTemplate::getSignalElement(int index) {
    return interfaceSignalList->at( index );
};

QList <spec_DataContainer*>   spec_InterfaceWizardTemplate::getDataItemList() {
    return *dataItemList;
};

spec_DataContainer*           spec_InterfaceWizardTemplate::getDataItemElement(int index) {
    return dataItemList->at( index );
};

QList <spec_DataContainer*>   spec_InterfaceWizardTemplate::getGlobalConfigList() {
    return *globalConfigList;
};

spec_DataContainer*           spec_InterfaceWizardTemplate::getGlobalConfigElement(int index) {
    return globalConfigList->at( index );
};

QList <spec_DataContainer*>   spec_InterfaceWizardTemplate::getAgentConfigList() {
    return *interfaceConfigList;
};

spec_DataContainer*           spec_InterfaceWizardTemplate::getAgentConfigElement(int index) {
    return interfaceConfigList->at( index );
};
