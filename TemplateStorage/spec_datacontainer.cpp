// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_datacontainer.h"

spec_DataContainer::spec_DataContainer(QObject *parent) : QObject(parent) {
    
};

void            spec_DataContainer::appendString( QString       str         ) {
    stringList->append(str);
};

void            spec_DataContainer::appendBool  ( bool          boolValue   ) {
    boolList->append(boolValue);
};

void            spec_DataContainer::appendUInt  ( unsigned int  uIntValue   ) {
    uIntList->append(uIntValue);
};

QString         spec_DataContainer::getString   ( unsigned int  index ) {
    return stringList->at(index);
};

bool            spec_DataContainer::getBool     ( unsigned int  index ) {
    return boolList->at(index);
};

unsigned int    spec_DataContainer::getUInt     ( unsigned int  index ) {
    return uIntList->at(index);
};
