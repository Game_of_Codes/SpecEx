// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_INTERFACEWIZARDTEMPLATE_H
#define SPEC_INTERFACEWIZARDTEMPLATE_H

#include <QDebug>
#include <QObject>

#include <TemplateStorage/spec_datacontainer.h>

class spec_InterfaceWizardTemplate : public QObject {
        Q_OBJECT
    public:
        explicit spec_InterfaceWizardTemplate(QObject *parent = nullptr);
        
        void    appendInterfaceValues(
                    QString name,
                    QString type,
                    QString width,
                    QString direction,
                    QString hdlName
                );
        
        void    appendDataItemValues(
                    bool    isDNG,
                    bool    isPhysical,
                    QString name,
                    QString type,
                    QString width,
                    bool    isListOf,
                    QString defaultName
                );
        
        void    appendGlobalConfigurationValues(
                    bool    isDNG,
                    bool    isPhysical,
                    QString name,
                    QString type,
                    QString width,
                    bool    isListOf,
                    QString defaultName
                );
        
        void    appendInterfaceConfigurationValues(
                    bool    isDNG,
                    bool    isPhysical,
                    QString name,
                    QString type,
                    QString width,
                    bool    isListOf,
                    QString defaultName
                );
        
        QList <spec_DataContainer*>   getSignalList           ();
        QList <spec_DataContainer*>   getDataItemList         ();
        QList <spec_DataContainer*>   getGlobalConfigList     ();
        QList <spec_DataContainer*>   getAgentConfigList      ();
        
        spec_DataContainer*           getSignalElement        (int index);
        spec_DataContainer*           getDataItemElement      (int index);
        spec_DataContainer*           getGlobalConfigElement  (int index);
        spec_DataContainer*           getAgentConfigElement   (int index);
        
    signals:
        
    public slots:
        
    private:
        QList <spec_DataContainer*>   *interfaceSignalList    = new QList<spec_DataContainer*>();
        QList <spec_DataContainer*>   *dataItemList           = new QList<spec_DataContainer*>();
        QList <spec_DataContainer*>   *globalConfigList       = new QList<spec_DataContainer*>();
        QList <spec_DataContainer*>   *interfaceConfigList    = new QList<spec_DataContainer*>();
        
};

#endif // SPEC_INTERFACEWIZARDTEMPLATE_H
