// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_DATACONTAINER_H
#define SPEC_DATACONTAINER_H

#include <QDebug>
#include <QList>
#include <QObject>
#include <QString>

class spec_DataContainer : public QObject {
        Q_OBJECT
    public:
        explicit spec_DataContainer(QObject   *parent = nullptr);
        void            appendString( QString       str         );
        void            appendBool  ( bool          boolValue   );
        void            appendUInt  ( unsigned int  uIntValue   );
        
        QString         getString   ( unsigned int  index );
        bool            getBool     ( unsigned int  index );
        unsigned int    getUInt     ( unsigned int  index );
        
    signals:
        
    public slots:
        
    private:
        QList <QString>         *stringList = new QList<QString>();
        QList <bool>            *boolList = new QList<bool>();
        QList <unsigned int>    *uIntList = new QList<unsigned int>();
        
};

#endif // SPEC_DATACONTAINER_H
