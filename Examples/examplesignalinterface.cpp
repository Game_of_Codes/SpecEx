// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "examplesignalinterface.h"

ExampleSignalInterface::ExampleSignalInterface(QObject *parent) : QObject(parent) {
    
    company_name_string = tr("my");
    protocol_name_string = tr("spp");
    
    // Defining the interface protocol signals
    //  name            , type          , width     , direction     ,hdlName
    interfaceWizardExample->appendInterfaceValues(
        tr("data")      , tr("uint")    , tr("8")   , tr("inout")   , tr("tb_data")
    );
    interfaceWizardExample->appendInterfaceValues(
        tr("valid")     , tr("uint")    , tr("1")   , tr("inout")   , tr("tb_valid")
    );
    interfaceWizardExample->appendInterfaceValues(
        tr("suspend")   , tr("uint")    , tr("1")   , tr("inout")   , tr("tb_suspend")
    );
    
    // Defining the interface protocol transaction type
    //  isDNG   , isPhysical    , name          , type          , width     , isListOf  ,   defaultName
    interfaceWizardExample->appendDataItemValues(
        false   , true          , tr("address") , tr("uint")    , tr("2")   , false     , ""
    );
    interfaceWizardExample->appendDataItemValues(
        false   , true          , tr("length")  , tr("uint")    , tr("6")   , false     , ""
    );
    interfaceWizardExample->appendDataItemValues(
        false   , true          , tr("payload") , tr("uint")    , tr("8")   , true      , ""
    );
    interfaceWizardExample->appendDataItemValues(
        false   , true          , tr("parity")  , tr("uint")    , tr("8")   , false     , ""
    );
    
    interfaceWizardExample->appendDataItemValues(
        true   , false          , tr("start_time")  , tr("time")    , tr("")   , false     , ""
    );
    
    interfaceWizardExample->appendDataItemValues(
        true   , false          , tr("end_time")  , tr("time")    , tr("")   , false     , ""
    );
    
    // Defining the interface protocol global configuration type
    //  isDNG   , isPhysical    , name          , type          , width     , isListOf  ,   defaultName
//    interfaceWizardExample->appendGlobalConfigurationValues(
//        false   , true          , tr("address") , tr("uint")    , tr("2")   , false     , ""
//    );
    
    // Defining the interface protocol per agent type
    //  isDNG   , isPhysical    , name          , type          , width     , isListOf  ,   defaultName
//    interfaceWizardExample->appendInterfaceConfigurationValues(
//        false   , true          , tr("address") , tr("uint")    , tr("2")   , false     , ""
//    );
};


