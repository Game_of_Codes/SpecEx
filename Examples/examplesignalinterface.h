// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef EXAMPLESIGNALINTERFACE_H
#define EXAMPLESIGNALINTERFACE_H

#include <QObject>
#include <QString>

#include "TemplateStorage/spec_interfacewizardtemplate.h"

class ExampleSignalInterface : public QObject {
    Q_OBJECT
    
    public:
        explicit ExampleSignalInterface(QObject *parent = nullptr);
        spec_InterfaceWizardTemplate  *interfaceWizardExample = new spec_InterfaceWizardTemplate();
        
        QString company_name_string;
        QString protocol_name_string;
        
    signals:
        
    public slots:
};

#endif // EXAMPLESIGNALINTERFACE_H
