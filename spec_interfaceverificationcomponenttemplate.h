// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_INTERFACEVERIFICATIONCOMPONENTTEMPLATE_H
#define SPEC_INTERFACEVERIFICATIONCOMPONENTTEMPLATE_H

#include <QDebug>
#include <QObject>

#include <CodeModel/spec_struct_data_node.h>
#include <CodeModel/spec_type_node.h>

class spec_InterfaceVerificationComponentTemplate : public QObject {
    Q_OBJECT
    
    public:
        explicit spec_InterfaceVerificationComponentTemplate(QObject *parent = nullptr);
        
        QString                 groupName       ;
        QString                 interfaceName   ;
        
        QList <spec_type_node> *typeList       = new QList<spec_type_node>();
        spec_struct_data_node   *envCfgStruct   = new spec_struct_data_node();
        spec_struct_data_node   *agentCfgStruct = new spec_struct_data_node();
        spec_struct_data_node   *smpUnit        = new spec_struct_data_node();
        spec_struct_data_node   *monUnit        = new spec_struct_data_node();
        spec_struct_data_node   *bfmUnit        = new spec_struct_data_node();
        spec_struct_data_node   *drvUnit        = new spec_struct_data_node();
        spec_struct_data_node   *agentUnit      = new spec_struct_data_node();
        spec_struct_data_node   *envUnit        = new spec_struct_data_node();
        
        
    signals:
        
    public slots:
        void                    createSmpUnit();
        
};

#endif // SPEC_INTERFACEVERIFICATIONCOMPONENTTEMPLATE_H
