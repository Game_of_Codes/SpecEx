// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_mod_gen.h"

spec_mod_gen::spec_mod_gen(QObject *parent) : QObject(parent) {
    // The p_ui pointer to the ui element is assigned directly through the
    // spec_mainwindow.cpp: instantiate_gui_elements()
//    p_ui = qobject_cast<spec_main_window*>(parent)->get_ui_pointer();
}   // end of spec_mod_gen constructor

void spec_mod_gen::initialize() {
    
    // Create the one column table
    lib_table = new spec_mod_lib_table_widget(
        new QList<QString> {"Loaded UVC Library"}
    );
    lib_table->init_widget_data_model(
        new QList<QString> { "string" }
    );
    lib_table -> setParent              ( p_ui->tb_designer_lib_table_scroll_widget_content );
    lib_table -> setColumnWidth         ( 0, 230 );
    lib_table -> resize                 ( 230, 221 );
    lib_table -> removeRow              ( 0 );
    lib_table -> setSelectionBehavior   ( QAbstractItemView::SelectRows );
    lib_table -> setSelectionMode       ( QAbstractItemView::SingleSelection );
    
    // Create the hierarchy tree
    uvc_tree = new spec_mod_lib_tree_widget(
        new QList<QString> { "Object", "Type", " " }
    );
    uvc_tree -> setParent   ( p_ui->tb_designer_hier_tree_scroll_widget_content );
    uvc_tree -> resize      ( 711, 631 );
    uvc_tree -> header() -> resizeSection   ( 0, 325 );
    uvc_tree -> header() -> resizeSection   ( 1, 325 );
    uvc_tree -> header() -> resizeSection   ( 2,  50 );
    
    // Create the hierarchy tree item view
    uvc_tree_item_table = new spec_mod_lib_table_widget(
        new QList<QString> {"Name", "Type", "!", "%", "List Of"}
    );
    uvc_tree_item_table->init_widget_data_model(
        new QList<QString> {"string", "string", "bool", "bool", "bool"}
    );
//    uvc_tree_item_table->setParent( p_ui->tb_designer_hier_field_view_scroll_area );
    uvc_tree_item_table->resize(470, 640);
    
    // Instantiate the clock configuration dialog
    mod_gen_clock_config_dialog = new spec_mod_clock_configuration_dialog();
    mod_gen_clock_config_dialog -> setModal( true );
    mod_gen_clock_config_dialog -> setWindowTitle( "Clock Configuration" );
    
    // Instantiate the clock configuration dialog
    mod_gen_reset_config_dialog = new spec_mod_reset_configuration_dialog();
    mod_gen_reset_config_dialog -> setModal( true );
    mod_gen_reset_config_dialog -> setWindowTitle( "Reset Configuration" );
    
    // Instantiate the configuration dialog
    mod_gen_configuration_dialog = new spec_mod_uvc_configuration_dialog();
    mod_gen_configuration_dialog -> setModal( true );
    mod_gen_configuration_dialog -> setWindowTitle( "UVC Configuration" );
    
    // Instantiate the message box
    message_box = new QMessageBox();
    
    // Create the new module code model
    mod_data = new spec_module_model();
    
    // Reset and Clock UVC configuration has to be explicitly turned on
    p_ui -> tb_designer_configure_clock_button -> setDisabled( true );
    p_ui -> tb_designer_configure_reset_button -> setDisabled( true );
    
    // Furthermore, disable the simulation button upon start-up, because a simulation can only be run if a
    // code base exists
    p_ui->tb_designer_simulate_button           -> setEnabled( false );
    
    // Create and attach the process logging mechanism
    process_dialog  = new spec_process_viewer();
    process         = new QProcess( this );
    process_dialog->set_process( process );
    process_dialog->initialize();
    process_dialog->setModal( false );
    
    // Connecting GUI elements
    connect(
        p_ui -> tb_designer_clock_uvc_check_box         , &QCheckBox::stateChanged,
        this                                            , &spec_mod_gen::change_clock_config_check_state
    );
    connect(
        p_ui -> tb_designer_reset_uvc_check_box         , &QCheckBox::stateChanged,
        this                                            , &spec_mod_gen::change_reset_config_checked_state
    );
    
    connect(
        p_ui -> tb_designer_enable_register_check_box   , &QCheckBox::stateChanged,
        this                                            , &spec_mod_gen::change_register_checked_state
    );
    
    connect(
        p_ui -> tb_designer_enable_scoreboard_check_box , &QCheckBox::stateChanged,
        this                                            , &spec_mod_gen::change_scoreboard_checked_state
    );
    
    connect(
        p_ui->tb_designer_add_specman_path_button       , &QPushButton::pressed,
        this                                            , &spec_mod_gen::add_specman_path
    );
    
    connect(
        p_ui->tb_designer_remove_specman_path_button    , &QPushButton::pressed,
        this                                            , &spec_mod_gen::remove_specman_path
    );
    
    connect(
        p_ui->tb_designer_add_lib_button                , &QPushButton::pressed,
        this                                            , &spec_mod_gen::add_uvc
    );
    
    connect(
        p_ui->tb_designer_remove_lib_button             , &QPushButton::pressed,
        this                                            , &spec_mod_gen::remove_uvc
    );
    
    connect(
        p_ui->tb_designer_config_uvc_button             , &QPushButton::pressed,
        this                                            , &spec_mod_gen::show_uvc_config_dialog
    );
    
    connect(
        p_ui->tb_designer_configure_clock_button        , &QPushButton::pressed,
        this                                            , &spec_mod_gen::show_clock_config_dialog
    );
    
    connect(
        p_ui->tb_designer_configure_reset_button        , &QPushButton::pressed,
        this                                            , &spec_mod_gen::show_reset_config_dialog
    );
    
    connect(
        lib_table                                       , &spec_mod_lib_table_widget::itemSelectionChanged,
        this                                            , &spec_mod_gen::show_lib_table_selection
    );
    
    connect(
        mod_gen_configuration_dialog                    , &spec_mod_uvc_configuration_dialog::ok_button_pressed,
        this                                            , &spec_mod_gen::update_uvc_configuration
    );
    
    connect(
        p_ui->tb_designer_simulate_button               , &QPushButton::pressed,
        this                                            , &spec_mod_gen::simulate_module_uvc
    );
    
    // Connecting the code model with the generation enginer
    connect(
        p_ui->tb_designer_generate_button               , &QPushButton::pressed,
        this                                            , &spec_mod_gen::generate_module_uvc
    );
    
    // TODO: DELETE THIS LINE AFTER GUI IS DESIGNED
//    p_ui->tb_designer_config_uvc_button->setEnabled( false );
//    p_ui->tb_designer_config_uvc_button->setToolTip( "This feature is under development." );
    
//    spec_mod_uvc_configuration_dialog *test_dialog = new spec_mod_uvc_configuration_dialog();
//    test_dialog->test_build();
//    test_dialog->show();
}   // end of initialize


// This method creates a new file parser object and connects all the necessary signal slots
void spec_mod_gen::init_file_parser() {
    // Create a new file-parser
    e_file_parser = new spec_file_parser();
    
    // Connecting the e File Parser with GUI elements
    connect(
        e_file_parser                                   , &spec_file_parser::loading_uvc_done,
        this                                            , &spec_mod_gen::show_mod_uvc
    );
    
    // Connecting the e File Parser with the code model
    connect(
        e_file_parser                                   , &spec_file_parser::top_file,
        this                                            , &spec_mod_gen::parser_top_file
    );
    
    connect(
        e_file_parser                                   , &spec_file_parser::type_declaration,
        this                                            , &spec_mod_gen::parser_type_slot
    );
    
    connect(
        e_file_parser                                   , &spec_file_parser::struct_declaration,
        this                                            , &spec_mod_gen::parser_struct_slot
    );
    
    connect(
        e_file_parser                                   , &spec_file_parser::field_declaration,
        this                                            , &spec_mod_gen::parser_field_slot
    );
    
    connect(
        e_file_parser                                   , &spec_file_parser::method_declaration,
        this                                            , &spec_mod_gen::parser_method_slot
    );
    
    connect(
        e_file_parser                                   , &spec_file_parser::constraint_declaration,
        this                                            , &spec_mod_gen::parser_constraint_slot
    );
    
}   // end of init_file_parser

//======================================================================================================================
// Slots
//======================================================================================================================
// GUI Slots

void spec_mod_gen::change_clock_config_check_state( int state ) {
    if( state == Qt::Checked ) {
        p_ui -> tb_designer_configure_clock_button -> setDisabled( false );
    } else {
        p_ui -> tb_designer_configure_clock_button -> setDisabled( true );
    }
}   // end of change_clock_config_check_state

void spec_mod_gen::change_reset_config_checked_state( int state ) {
    if( state == Qt::Checked ) {
        p_ui -> tb_designer_configure_reset_button -> setDisabled( false );
    } else {
        p_ui -> tb_designer_configure_reset_button -> setDisabled( true );
    }
}   // end of change_reset_config_checked_state

void spec_mod_gen::change_register_checked_state( int state ) {
    if( state == Qt::Checked ) {
        p_ui -> tb_designer_register_address_line_edit  -> setDisabled( false );
        p_ui -> tb_designer_register_data_line_edit     -> setDisabled( false );
    } else {
        p_ui -> tb_designer_register_address_line_edit  -> setDisabled( true );
        p_ui -> tb_designer_register_data_line_edit     -> setDisabled( true );
    };
}   // end of change_register_checked_state

void spec_mod_gen::change_scoreboard_checked_state( int state ) {
    p_ui -> tb_designer_enable_uvm_scoreboard_check_box->setDisabled(
        p_ui -> tb_designer_enable_scoreboard_check_box -> checkState() == Qt::Unchecked
    );
}   // end of change_scoreboard_checked_state

void spec_mod_gen::add_specman_path() {
    // Add a new SPECMAN_PATH will not check if the path is already in the list.
    // This is the user's choice and will not be validated
    
    QString new_path = QFileDialog::getExistingDirectory(
        nullptr,
        "Select SPECMAN_PATH Folder",
        "",
        QFileDialog::ShowDirsOnly
    );
    
    // Do not add an empty path
    if( new_path == "" ) {
        return;
    };
    // The path is added to the widget directly and upon generating the UVC,
    // the path will be handed over to the mod_data model
    p_ui->tb_designer_path_list_widget->addItem( QString(new_path) );
}   // end of add_specman_path

void spec_mod_gen::remove_specman_path() {
    // Removing a _selected_ SPECMAN_PATH has to check if there really is a selection active.
    // If nothingis selected, then remove nothing
    QList <QListWidgetItem*> selected_items = p_ui->tb_designer_path_list_widget->selectedItems();
    
    // Check if there was an item selected
    if( selected_items.count() > 0 ) {
        int sel_row = p_ui->tb_designer_path_list_widget->row(
            selected_items.at(0)
        );
        
        if( sel_row >= 0 ) {
            p_ui->tb_designer_path_list_widget->takeItem( sel_row );
        };
    };  // end of handling selected items
}   // end of remove_specman_path

// Gets called whenever a new UVC is added to the lib_table library
void spec_mod_gen::add_uvc() {
    // Create a new file parser. Creating it every time we add a new UVC is necessary, otherwise the file-parser
    // will not reload a previously loaded UVC
    init_file_parser();
    
    // Create a new "current UVC" that receives the parser's signals
    uvc_component = new spec_code_model();
    
    // Chose UVC_top.e and load model in lib table.
    e_file_parser -> load_uvc();
    spec_msg( "Done executing method. uvc_component pointer is " << uvc_component );
}   // end of add_uvc() slot

// Gets called whenever a UVC gets removed from the lib_table library
void spec_mod_gen::remove_uvc() {
    spec_msg( "lib_table.count() == " << lib_table->rowCount() );
    spec_msg( "uvc_library_hierarchies.count() == " << uvc_library_hierarchies.count() );
    
    if( lib_table->selectedItems().count() > 0 ) {
        // Removing the last row in the lib_table causes a crash at removeRow... hence we avoid that case (TODO FIXME)
//        if( lib_table->rowCount() == 1 ) {
            
//        } else {
        // The deletion of elements is not completely done, even here.... TODO figure out why
            int sel_row = lib_table->selectedItems().at(0)->row();
            lib_table->removeRow( sel_row );
            uvc_library.takeAt( sel_row );
            uvc_library_hierarchies.takeAt( sel_row );
//        };
    };
    
    spec_msg( "lib_table.count() == " << lib_table->rowCount() );
    spec_msg( "uvc_library_hierarchies.count() == " << uvc_library_hierarchies.count() );
    
}   // end of remove_uvc() slot


void spec_mod_gen::set_clock_model() {
    // Instantiate the clock data model
    clk_data = new spec_clock_model();
    clk_data                -> set_group_name    ( mod_data->get_group_name() );
    clk_data                -> set_project_name  ( QString( "clock" ) );  // This is hard-coded to be the clock UVC ;-)
    
    // Extract the configured values from the clock config dialog
    for( int i = 0; i < mod_gen_clock_config_dialog->get_clock_config_count(); ++i ) {
        clk_data->add_clock_domain(
            mod_gen_clock_config_dialog->get_clock_config_at( i )
        );
    };
    
    // Get the time-resolution from the GUI
    clk_data                -> set_time_resolution( mod_gen_clock_config_dialog ->get_clock_time_scale() );
    
    // Assign the clock data model to the module data model
    mod_data                -> set_clock_model( clk_data );
}

void spec_mod_gen::set_reset_model() {
    // Instantiate the reset data model
    res_data = new spec_reset_model();
    res_data -> set_group_name( mod_data->get_group_name() );
    res_data -> set_project_name( QString("reset") );
    
    // Extract the configured values from the reset config dialog
    for( int i = 0; i < mod_gen_reset_config_dialog->get_reset_config_count(); ++i ) {
        res_data->add_reset_domain(
            mod_gen_reset_config_dialog->get_reset_config_at( i )
        );
    };
    // Assign the reset data model to the module data model
    mod_data                -> set_reset_model( res_data );
}

// This method is called whenever a user presses the "Configure UVC" button
void spec_mod_gen::show_uvc_config_dialog() {
    // Check if there is a loaded model...
    if( lib_table->rowCount() == 0 ) {
        // ... and show a message box that we can't configure anything, if nothing is loaded
        message_box->setText("Please load a UVC component (*top.e) before trying to configure it.");
        message_box->show();
    } else {
        // ... in case a model was loaded, the currently selected model from the list of loaded models is picked
        int sel_row = lib_table->currentRow();
        
        if( sel_row < 0 ) {
            // If nothing was selected, then just show the first loaded UVC
            sel_row = 0;
        };
        
        // Pass the full code model to the configuration to the configuration dialog
        mod_gen_configuration_dialog -> set_module_model( mod_data );
        
        // Pass the model of the currently selected eVC to the dialog
        mod_gen_configuration_dialog -> set_root_node( uvc_tree );
        
        // Determine if the clock and reset UVCs shall be generated
        mod_data -> set_enable_clock_uvc( ( p_ui -> tb_designer_clock_uvc_check_box->checkState() == Qt::Checked) );
        mod_data -> set_enable_reset_uvc( ( p_ui -> tb_designer_reset_uvc_check_box->checkState() == Qt::Checked) );
        
        // Pass the clock and reset model to the configuration dialog
        if( mod_data -> get_enable_clock_uvc() ) {
            set_clock_model();
        };
        if( mod_data -> get_enable_reset_uvc() ) {
            set_reset_model();
        };
        
        // ... and SHOWIME ...
        mod_gen_configuration_dialog -> show();
    };
}   // end of show_config_dialog

// This method is called upon closing the UVC configuration dialog.
// The configured model is replacing the previously loaded and unconfigured model.
void spec_mod_gen::update_uvc_configuration    (
    spec_module_model           *model,
    spec_mod_lib_tree_widget    *conf_uvc_tree
) {
    spec_msg( "Processing the conf_uvc_tree " << conf_uvc_tree << static_cast<spec_struct_data_node*>(conf_uvc_tree->topLevelItem(0))->get_struct_name() );
    // Pass the updated model back to the current model
    mod_data = model;
    
    // Update the tree.. need to remove the old top level item and add a copy of the updated item
    uvc_tree -> takeTopLevelItem(0);
    uvc_tree -> addTopLevelItem(
        new spec_struct_data_node(static_cast <spec_struct_data_node*>( conf_uvc_tree->topLevelItem(0) ))
    );
    // Update the library with the configuration, based on the currently selected table row
    int sel_row = lib_table->currentRow();
    if( sel_row == -1 ) {
        // If there was no selected row, then only the last element in the lib_table could've been selected
        sel_row = lib_table->rowCount()-1;
    };
    // Update the model in the stored library
    uvc_library_hierarchies.replace(
        sel_row,
        static_cast<spec_struct_data_node*>(uvc_tree->topLevelItem(0))
    );
    
    // Only show the depth up until the level of agents
    uvc_tree->expandToDepth(1);
}

void spec_mod_gen::show_clock_config_dialog() {
    mod_gen_clock_config_dialog -> show();
}   // end of show_clock_config_dialog

void spec_mod_gen::show_reset_config_dialog() {
    mod_gen_reset_config_dialog -> show();
}   // endof show_reset_config_dialog

// Gets called whenever a UVC library gets selected from the UVC list
void spec_mod_gen::show_lib_table_selection() {
    spec_msg("Called");
    
    // Always remove the currently showing UVC tree
    // Remove the only tree element from the library.
    // IMPORTANT: DO NOT EVER USE delete OR clear() since this will remove ALL DATA STRUCTURES!!!!!
    uvc_tree->takeTopLevelItem(0);
    
    // Get the current selection of the library.
    int sel_row = lib_table->currentRow();
    if( sel_row >= 0 ) {
        // We can only show the UVC if one has been loaded
        uvc_tree->addTopLevelItem(
            uvc_library_hierarchies.at( sel_row )
        );
        // And make sure all the nodes are showing
        uvc_tree->expandToDepth(1);
    };
    
    // DEBUG CODE
//    static_cast<spec_struct_data_node*>( uvc_tree->topLevelItem(0) )->node_visitor(
//        static_cast<spec_struct_data_node*>( uvc_tree->topLevelItem(0) )
//    );
}   // end of show_lib_table_selection

// Gets called whenever a selection change within a UVCs hierarchy view is detected
void spec_mod_gen::show_hier_tree_selection() {
//    qDebug() << "spec_mod_gen::show_hier_tree_selection(): " << "called";
    
    // In case a new UVC library is loaded or we are switching between UVCs, then the 
    // selected item in the uvc_tree is null and would cause a segfault, if not caught properly
    if( uvc_tree->selectedItems().isEmpty() ) {
        return;
    };
    
//    spec_struct_data_node *cur_node = static_cast<spec_struct_data_node*> (uvc_tree->selectedItems().at(0));
//    spec_struct_data_node *par_node;
    
}   // end of show_hier_tree_selection

void spec_mod_gen::set_module_model_config() {
//    qDebug() << "spec_mod_gen::set_module_model_config(): " << " called!";
    
}   // end of set_module_model_config


// This method builds and shows the UVC tree in the right-hand-side tree viewer.
void spec_mod_gen::show_mod_uvc() {
    spec_msg("Called with uvc_component " << uvc_component );
    
    bool                    has_uvm_env = false;
    QString                 env_name;
    
    spec_struct_data_node   uvm_env = uvc_component->get_struct_like("uvm_env");
    env_name    = uvm_env.get_struct_name();
    
    if( env_name != "" ) {
        // set flag that building the tree can be performed
        has_uvm_env = true;
        
        // Also add the new code model to the module data code list
        mod_data -> add_code_model( uvc_component );
        
//        spec_msg("Looking at struct " << uvm_env.get_struct_name() );
        
        // Build the tree using the struct's uvm_env derived environment
        uvc_component->build_new_hierarchy( env_name );
        
        // Adding the current component into the list of uvc_components
        uvc_library             .append( uvc_component );
        uvc_library_hierarchies .append( uvc_component->get_hierarchy_tree() );
        
        // Only show a single UVC tree. With this condition, we'll ensure that only one tree shows at a time.
        if( uvc_tree->topLevelItemCount() == 0 ) {
            uvc_tree->addTopLevelItem( uvc_library_hierarchies.last() );
        } else {
            // Remove the previously shown tree without NULLing every existing data structure
            uvc_tree->takeTopLevelItem(0);
            // Put the selected tree in place for visual browsing
            uvc_tree->addTopLevelItem( uvc_library_hierarchies.last() );
        };
        
        uvc_tree->expandToDepth(1);
        
        // This adds the name of the UVC package to the library container
        lib_table->add_row( env_name );
    };
}   // end of show_mod_uvc

void spec_mod_gen::simulate_module_uvc() {
    process_dialog->setWindowTitle( "SpecEx - Simulation" );
    process_dialog->show();
    
    QByteArray  output;
    QString     folder  = mod_data->get_directory();
    QString     prefix  = mod_data->get_prefix();
    QString     command =
            "xrun -gui -snload " + folder + "/" + prefix + "/e/" + prefix + "_top.e " +
            "-snload " + folder + "/" +prefix + "/sve/" + prefix + "_tb_instantiation.e ";
    command.append( "-snpath " + folder + " " );
    for( QString *sn_path : mod_data->get_specman_paths() ) {
        command.append( "-snpath " + *sn_path + " " );
    };  // end of iterating over each detected SPECMAN_PATH variable
    if( mod_data->get_enable_reset_uvc() ) {
        command.append(
            folder + "/" + mod_data->get_clock_model()->get_prefix() + "/hdl/clk_gen.sv "
        );
    };
    command.append(
        "-access rw -createdebugdb " + folder + "/" + mod_data->get_prefix() + "/hdl/tb_top.sv " +
        "-input @\"probe -create [scope -tops] -depth all -tasks -functions -all -shm\" "
    );
    command.append(
        "-snload " + folder + "/" + mod_data->get_prefix() + "/tests/smoke_test.e "
    );
    
    // Reset the queue, just in case some previous commands are still in there
    process_dialog->reset_command_queue();
    // Then create a new folder
    process_dialog->queue_command( "mkdir _sim_", folder );
    // Run the simulator command
    process_dialog->queue_command( command, folder + "/_sim_" );
    // Remove the simulation folder after simulation is done
    process_dialog->queue_command( "rm -rf _sim_", folder);
    
    // actually launch the series of commands
    process_dialog->start_command_queue();
}   // end of simulate_module_uvc

void spec_mod_gen::generate_module_uvc() {
    QList<QString>  incomplete_elements;
    if( p_ui->tb_designer_group_name_line_edit->text().isEmpty() ) {
        incomplete_elements.append( "Enter a Group Name in text field." );
    };
    if( p_ui->tb_designer_project_name_line_edit->text().isEmpty() ) {
        incomplete_elements.append( "Enter a Project Name in text field." );
    };
    for( spec_code_model *m : mod_data -> get_code_model_list() ) {
//    for( spec_code_model *m : uvc_library ) {
        spec_msg( "Checking " + m->get_prefix() );
        incomplete_elements.append( m->get_incomplete_config_elements() );
    };
    spec_msg( incomplete_elements );
    if( incomplete_elements.count() > 0 ) {
        // TODO: Show non-modal message Box which configurations are incomplete
        QMessageBox *warning_message_box = new QMessageBox();
        warning_message_box->setWindowTitle("Module Generation Validation");
        QString     msg_box_string =
                "Incomplete Configuration.\n\
Please resolve the detected issues listed below and then click generate again.\n\
You may leave this window open and close it once you are done fixing the issues.\n";
        for( int index = 0; index < incomplete_elements.count();++index ) {
            msg_box_string.append( "[" + QString::number(index+1) + "]: " + incomplete_elements.at(index) + "\n" );
            msg_box_string.append( "\n" );
        };
        warning_message_box -> setModal( false );
        warning_message_box -> setText( msg_box_string );
        warning_message_box->show();
        return;
    };
    
    //////////////////////////////////////////////////////////
    // Get the information provided previously
    
    mod_data -> set_group_name      ( p_ui -> tb_designer_group_name_line_edit -> text() );
    mod_data -> set_project_name    ( p_ui -> tb_designer_project_name_line_edit -> text() );
    
    mod_data -> set_enable_clock_uvc( ( p_ui -> tb_designer_clock_uvc_check_box->checkState() == Qt::Checked) );
    mod_data -> set_enable_reset_uvc( ( p_ui -> tb_designer_reset_uvc_check_box->checkState() == Qt::Checked) );
    
    if( p_ui -> tb_designer_enable_register_check_box->checkState() == Qt::Checked ) {
        mod_data -> set_enable_register_model( true );
        mod_data -> set_register_max_addr_width( (p_ui -> tb_designer_register_address_line_edit->text()).toUInt() );
        mod_data -> set_register_max_data_width( (p_ui -> tb_designer_register_data_line_edit->text()).toUInt() );
    } else {
        mod_data -> set_enable_register_model   ( false );
        mod_data -> set_register_max_addr_width( 0 );
        mod_data -> set_register_max_data_width( 0 );
    };
    
    bool enable_scoreboard = (p_ui->tb_designer_enable_scoreboard_check_box->checkState() == Qt::Checked);
    mod_data->set_enable_scoreboard( enable_scoreboard );
    
    if( enable_scoreboard ) {
        mod_data->set_enable_uvm_scoreboard(
            p_ui->tb_designer_enable_uvm_scoreboard_check_box->checkState() == Qt::Checked
        );
    };
    
    // Get user folder selection
    QString uvc_directory = QFileDialog::getExistingDirectory(
        nullptr,
        tr("Select Output Folder"),
        "",
        QFileDialog::ShowDirsOnly
    );
    // Ensure not to generate the module UVC if aborted by user
    if( uvc_directory.isEmpty()  ) {
        message_box->setText(
            "Module UVC generation aborted."
        );
        message_box->show();
        return;
    };
    mod_data->set_directory( uvc_directory );
    
    // Assign all used SPECMAN_PATHs to the code generation model
    QList<QString*> existing_paths;
    for( int i = 0; i < p_ui->tb_designer_path_list_widget->count(); ++i ) {
        existing_paths.append( new QString(p_ui->tb_designer_path_list_widget->item( i )->text()) );
    };
    mod_data->set_all_specman_paths(
        existing_paths
    );
    
    // Add the Clock UVC if requested by user
    if( mod_data -> get_enable_clock_uvc() ) {
        // Create a new clock model
        set_clock_model();
        
        // set its location of where it will be generated
        clk_data -> set_directory     ( uvc_directory );
        
        // ... and create the clock UVC
        clock_code_generator    -> create_clock_uvc( clk_data );
    };
    
    // Add the Reset UVC if requested by user
    if( mod_data -> get_enable_reset_uvc() ) {
        // Create a new reset model
        set_reset_model();
        
        // set its location of where it will be generated
        res_data                -> set_directory( uvc_directory );
        
        // ... and create the reset UVC
        reset_code_generator    -> create_reset_uvc( res_data );
    };
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Extract and populate the module UVC data from, the GUI
    
    // Create new instance of code generator
    module_code_generator = new spec_e_module_code_generator();
    module_code_generator->create_module_uvc( mod_data );
    
    // Also have a confirmation dialog here that shows where the UVC was generated
    message_box->setTextFormat( Qt::RichText );
    message_box->setText(
        "The module UVC code can be found here:<br><br><a href=" + uvc_directory + "/" + mod_data->get_prefix() + ">"+
        uvc_directory + "/" + mod_data->get_prefix() + "</a>"
    );
    message_box->show();
    
    if( QSysInfo::productType() != "windows" ) {
        // Enable the simulation button
        p_ui -> tb_designer_simulate_button->setEnabled( true );
    };
}

QString spec_mod_gen::get_connected_field_enclosing_struct(
        spec_struct_data_node   data_node,
        QList<QString>          expr_list
        ) {
    spec_msg("");
    
    QString         result;
    QList<QString>  stripped_expr_list;
    int             last_dot_index = expr_list.lastIndexOf( "." );
    
    // Only perform the checking of the enclosing unit, if a traversal operator was found
    if( last_dot_index != -1 ) {
        // Since we already know from which element we need to strip away the trailer, we'll simply do that
        for( int index = expr_list.count()-1 ; index >= last_dot_index; --index) {
            expr_list.takeLast();
        };   // stripping elements
        
        result = get_connected_field_type( data_node, expr_list );
        
        return result;
    } else {
        return data_node.get_struct_name();
    };
}   // end of get_connected_field_enclosing_struct

QString spec_mod_gen::get_connected_field_type(
        spec_struct_data_node   data_node,
        QList<QString>          expr_list
        ) {
    spec_msg("");
    
    // First, we'll extract the data and field nodes from the current UVC that is being loaded
    QList<spec_field_node*>     fld_nodes           = data_node.get_fields();
    spec_struct_data_node       path_data_node      ;
    QString                     result              = "";
    
    // Cycle through each list element
    for( QString str : expr_list ) {
        // And only perform tracing on 
        if( not (str == "read_only" or str == "value" or str == "(" or str == ")") ) {
            if( not (str == ".") ) {
                for( spec_field_node *fld : fld_nodes ) {
                    if( (*fld).get_field_name() == str ) {
                        result = (*fld).get_data_type();
                        break;
                    };
                };   // end of iterating over each field
            } else {
                // Now handle the path traversing
                data_node = uvc_component->get_struct( result );
            };
        } else {
            
        };
    };   // end of iterating over a full expression
    
    return result;
}   // end of get_connected_field


//======================================================================================================================
// Parser Slots

// Gets triggered right after the file selection has been done
void spec_mod_gen::parser_top_file(
        QStringList         *file_paths,
        QString             file_name
) {
//    qDebug() << "spec_mod_gen::parser_top_file() " << " Received file_paths " << file_paths;
//    qDebug() << "spec_mod_gen::parser_top_file() " << " Received file_name " << file_name;
    
    uvc_component->set_top_file( QString( file_name ) );
    
    int paths_count = p_ui->tb_designer_path_list_widget->count();
    
    QVector<QString> existing_paths;
    
    // Extract all the currently defined paths from the GUI list
    for( int i = 0; i< paths_count; ++i ) {
        existing_paths.append( p_ui->tb_designer_path_list_widget->item( i )->text() );
    };
    
    // If there are no paths previously defined then add all the detected file paths to the list
    if( existing_paths.isEmpty() ) {
        for( QString path : *file_paths ) {
            p_ui->tb_designer_path_list_widget->addItem( path );
        };
    } else {
        // However, if there are any previously defined lists, then we want to avoid duplicating
        // any entries.
        for( int i = 0; i < file_paths->count();++i ) {
            // Only add paths that have not been previously added
            if( not existing_paths.contains( file_paths->at(i) ) ) {
                 p_ui->tb_designer_path_list_widget->addItem( file_paths->at(i) );
            };
        };
    };   // end of handling already existing paths
}   // end of parser_top_file slot


// Gets triggered when the parser detects a 'type'
void spec_mod_gen::parser_type_slot(
        QString             parent,
        QList<QString*>     properties
        ) {
    spec_msg("");
    uvc_component->add_enum_type( parent, properties );
}   // end of parser_type_slot

// Gets triggered when the parser detects a 'struct' or 'unit'
void spec_mod_gen::parser_struct_slot(
        QString             parent,
        QList<QString*>     properties
        ) {
    spec_msg("");
    
    if( *(properties.at(3)) == "extend" and uvc_component->has_struct_declaration( parent ) ) {
        spec_msg("(Not creating a new struct)");
        return;
        // TODO: This condition may have to be changed further to also allow sub-typing
    };
    
    // update the current UVC CODE model
    uvc_component->add_struct( parent, properties );
    
    // Update the GUI model (only needed when displaying the class declarations
}   // end of parser_struct_slot

// Gets triggered when the parser detects a field
void spec_mod_gen::parser_field_slot(
        QString             struct_name,
        QString             field_name,
        QList <QString*>    field_properties
        ) {
    spec_msg("");
    
    uvc_component->add_struct_field(
        struct_name, field_name, field_properties
    );
    
}   // end of parser_field_slot

// Gets triggered when the parser detects a method
void spec_mod_gen::parser_method_slot(
        QString             struct_name,
        QString             field_name,
        QList <QString*>    field_properties
        ) {
    spec_msg("");
    
}   // end of parser_method_slot

void spec_mod_gen::parser_constraint_slot(
        QString             *struct_name,
        QString             *constraint_name,
        QList <QString*>    constraint_properties
        ) {
    spec_msg( "Caught the emit constraint signal" );
    spec_struct_data_node   *node = uvc_component->get_struct_pointer(*struct_name);
    node->add_constraint_node( *constraint_name, constraint_properties );
}   // end of parser_constraint_slot


