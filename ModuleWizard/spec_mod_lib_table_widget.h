// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_MOD_LIB_TABLE_WIDGET_H
#define SPEC_MOD_LIB_TABLE_WIDGET_H

#include <QObject>
#include <QWidget>

#include "BaseClasses/spec_tablewidget.h"

class spec_mod_lib_table_widget : public spec_table_widget {
    Q_OBJECT
    
    public:
        // TODO: Learn and understand why we need to provide a constructor body in the header and perhaps how to override this behavior?
        spec_mod_lib_table_widget( QList<QString> *headerStringList ) : spec_table_widget(headerStringList) {
            
        }
        spec_mod_lib_table_widget( spec_mod_lib_table_widget *other );
    
};   // end of spec_mod_lib_table_widget class

#endif // SPEC_MOD_LIB_TABLE_WIDGET_H
