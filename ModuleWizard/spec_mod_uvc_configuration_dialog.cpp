// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_mod_uvc_configuration_dialog.h"

spec_mod_uvc_configuration_dialog::spec_mod_uvc_configuration_dialog() {
    // Create objects
    current_uvc_group_box               = new QGroupBox                 ();
    configured_uvc_group_box            = new QGroupBox                 ();
    
    ok_button                           = new QPushButton               ();
    abort_button                        = new QPushButton               ();
    
    // Ensure that each instantiation has its agent counters set to zero in the beginning
    active_agents_count                 = 0;
    passive_agents_count                = 0;
    
    // Setting the properties of each object
    current_uvc_group_box               -> setTitle     ( "Selected UVC Hierarchy" );
    current_uvc_group_box               -> setGeometry  ( 10, 10, 710, 700 );
    current_uvc_group_box               -> setParent    ( this );
    
    configured_uvc_group_box            -> setTitle     ( "Object/Class Configuration " );
    configured_uvc_group_box            -> setGeometry  ( 720, 10, 700, 700 );
    configured_uvc_group_box            -> setParent    ( this );
    
    ok_button                           -> setText      ( "Ok" );
    ok_button                           -> setGeometry  ( 1120, 720, 100,  30 );
    ok_button                           -> setParent    ( this );
    
    abort_button                        -> setText      ( "Abort" );
    abort_button                        -> setGeometry  ( 1320, 720, 100,  30 );
    abort_button                        -> setParent    ( this );
    
    connect(
        ok_button       , &QPushButton::pressed,
        this            , &spec_mod_uvc_configuration_dialog::update_model
    );
    
    connect(
        abort_button    , &QPushButton::pressed,
        this            , &spec_mod_uvc_configuration_dialog::hide
    );
    
//    this->move(0, 0);
}   // end of spec_mod_gen_config_dialog

void spec_mod_uvc_configuration_dialog::set_module_model(
    spec_module_model         *model
) {
    spec_msg( "Called with model " << model  );
    mod_model       = model;
}

// This method gets called when the OK button is pressed.
// This method updates the respective code model's agent counter, to keep the state consistent across
// multiple code model configurations
void spec_mod_uvc_configuration_dialog::update_model() {
    int model_index = 0;
    spec_struct_data_node   *current_top_level_item = static_cast<spec_struct_data_node*>(current_uvc_tree_widget->topLevelItem(0) );
    for( spec_code_model *code_model : mod_model->get_code_model_list() ) {
        spec_msg( "Iterating over code_model " << code_model );
        if( code_model -> get_hierarchy_tree()->get_struct_name() == current_top_level_item->get_struct_name() ) {
            // Extracting the current module's configured agent count offset
            code_model  -> set_passive_agent_count          ( passive_agents_count );
            code_model  -> set_active_agent_count           ( active_agents_count );
            
            // Get the node of the uvc_tree_widget. This guarantees data consistency with passing the data directly to the
            // code model.
            code_model  -> set_hierarchy( current_top_level_item );
            mod_model   -> update_code_model( model_index, code_model );
        };  // end of handling code model agent counts
        ++model_index;
    };  // end of looking through each model
    
    // Finally, let's update the proposal for the HDL TB top testbench name
    mod_model   -> set_hdl_tb_top( current_top_level_item->get_env_widget()->get_hdl_tb_top() );
    
    emit ok_button_pressed( mod_model, current_uvc_tree_widget );
    this->hide();
}

void spec_mod_uvc_configuration_dialog::set_root_node(
    spec_mod_lib_tree_widget           *uvc_tree
) {
//    spec_msg( "Assigning " << uvc_tree->topLevelItem(0) << static_cast<spec_struct_data_node*>(uvc_tree->topLevelItem(0))->get_struct_name() );
    // Always create new object to purge the previous tree remnants
    current_uvc_tree_widget             = new spec_mod_lib_tree_widget  (
        new QList<QString> { "Object Name", "Type" }
    );
    
    
    // Assign the parent widget to be the current UVC group box widget
    current_uvc_tree_widget -> setGeometry  ( 5, 20, 700, 670 );
    current_uvc_tree_widget -> setColumnWidth( 0, 300 );
    current_uvc_tree_widget -> setParent( current_uvc_group_box );
    
    spec_struct_data_node *copied_top_node = new spec_struct_data_node( static_cast<spec_struct_data_node*>( uvc_tree->topLevelItem(0) ) );
    copied_top_node->init_node_widget();
    
    current_uvc_tree_widget -> addTopLevelItem( copied_top_node );
    
    // Also expand all the tree widget items
    current_uvc_tree_widget -> expandToDepth(2);
    
    // And connect the item selector within the tree
    connect(
        current_uvc_tree_widget , &spec_tree_widget::itemSelectionChanged,
        this                    , &spec_mod_uvc_configuration_dialog::show_selected_item
    );
    
    // Ensure that the environment widget is shown.
    // To do so, we are getting the top-level item, which is always an environment class
    // and set the tree's currently selected item to be the first top item.
    current_uvc_tree_widget->setCurrentItem(current_uvc_tree_widget->topLevelItem(0));
    
    cur_uvc_node = static_cast<spec_struct_data_node*>(current_uvc_tree_widget->topLevelItem(0));
    // To avoid having to initialize each node widget manually, calling the init_node_widget method is doing that for us
//    cur_uvc_node -> init_node_widget();
    cur_uvc_node -> show_node_widget( configured_uvc_group_box );
    
    env_widget  = static_cast<spec_struct_data_node*>(current_uvc_tree_widget->topLevelItem(0))->get_env_widget();
    if( not mod_model->get_hdl_tb_top().isEmpty() ) {
        env_widget->set_hdl_tb_top( mod_model->get_hdl_tb_top() );
    };
    
    // Initialize the agent counters
    for( spec_code_model *code_model : mod_model->get_code_model_list() ) {
        if( code_model -> get_hierarchy_tree()->get_struct_name() == cur_uvc_node->get_struct_name() ) {
            // Extracting the current module's configured agent count offset. In case there are no initialized
            // agents previously, then simply assume 0, otherwise get the already configured counter value.
            passive_agents_count = (
                (code_model->get_passive_agent_count() <= 0)?
                    0 :
                    code_model->get_passive_agent_count()
            );
            active_agents_count = (
                (code_model->get_active_agent_count() <= 0)?
                    0 :
                    code_model->get_active_agent_count()
            );
        };  // end of handling code model agent counts
    };  // end of looking through each model
    
    // These offsets will be calculated later together with the agent counter values
    passive_agents_start_index  = 0;
    passive_agents_end_index    = 0;
    active_agents_start_index   = 0;
    active_agents_end_index     = 0;
    
    // Now connect the widget's agent count notifiers
    connect(
        env_widget              , &spec_env_widget::agent_count_change,
        this                    , &spec_mod_uvc_configuration_dialog::update_agent_instances
    );
    connect(
        env_widget              , &spec_env_widget::active_agent_count_change,
        this                    , &spec_mod_uvc_configuration_dialog::update_active_agent_instances
    );
}

// This method slot calls the selected item.
void spec_mod_uvc_configuration_dialog::show_selected_item() {
    // First we'll need to retrieve the currently selected tree item as a specialized spec_struct_data_node
    // and then we have access to each specialized field and method
    cur_uvc_node = static_cast<spec_struct_data_node*> (current_uvc_tree_widget->currentItem());
    
    // Let's hide each widget first
    for( QObject* widget : configured_uvc_group_box->children() ) {
        static_cast<QWidget*>(widget)->hide();
    };
    
    // Then show the current widget, based on the current Struct Data Node information
    cur_uvc_node-> show_node_widget( configured_uvc_group_box );
    
    // Since the widget is already actively shown, it may be necessary to update some GUI elements
    // afterwards, like the enumerated types
    if( cur_uvc_node->get_like_name() == "uvm_agent" ) {
        QList <QString> domains;
        
        // Add the new clock domain list, if a clock model exists
        if( mod_model -> get_clock_model() != nullptr ) {
            for( int index = 0; index < mod_model -> get_clock_model() -> get_clock_domain_count(); ++index ) {
                domains . append( mod_model -> get_clock_model() -> get_clock_domain_name( index) );
            };
            cur_uvc_node -> set_clock_domain_list( domains );
        };
        
        // Check if there was a declared model
        if( mod_model -> get_reset_model() != nullptr ) {
            // Remove the elements from any previous operation
            domains.clear();
            for( int index = 0; index < mod_model -> get_reset_model() -> get_reset_domain_count(); ++index ) {
                domains . append( mod_model -> get_reset_model() -> get_reset_domain_name( index) );
            };
            cur_uvc_node -> set_reset_domain_list( domains );
        };
        
        QList <QString>     enum_values;
        for( spec_field_node *fld : cur_uvc_node->get_fields() ) {
            for( spec_code_model *model : mod_model->get_code_model_list() ) {
                if( fld->get_field_name() == "active_passive" ) {
                    // Do not add the uvm_active_passive field to the list of possibly interface names
                    continue;
                };
                enum_values = model->get_enum_type_field_values( fld->get_data_type(),fld->get_field_name() );
                
                cur_uvc_node -> set_enum_type_selector( fld -> get_field_name(), enum_values );
            };  // end of iterating over each loaded code model
        };
    };  // end of handling an agent node
    
    if( cur_uvc_node->get_like_name() == "uvm_env" ) {
        QList <QString>     enum_values;
        for( spec_field_node *fld : cur_uvc_node->get_fields() ) {
            for( spec_code_model *model : mod_model->get_code_model_list() ) {
                
                enum_values = model->get_enum_type_field_values( fld->get_data_type(),fld->get_field_name() );
                
                cur_uvc_node -> set_enum_type_selector( fld -> get_field_name(), enum_values );
            };  // end of iterating over each loaded code model
        };
    };
    
    
    if( cur_uvc_node->get_like_name() == "uvm_config_params" or cur_uvc_node->get_like_name() == "uvm_config_params_s" ) {
        QList<spec_field_node*> field_nodes = cur_uvc_node->get_fields();
        QList<QString>          enum_values;
        for( spec_field_node *fld : cur_uvc_node->get_fields() ) {
            for( spec_code_model *model : mod_model->get_code_model_list() ) {
                enum_values = model->get_enum_type_field_values( fld->get_data_type(), fld->get_field_name() );
//                qDebug() << "spec_mod_uvc_configuration_dialog::show_selected_item(): " << fld->get_field_name() << "  has value  " << enum_values;
                cur_uvc_node -> set_enum_type_selector( fld -> get_field_name(), enum_values );
                
            };
        };  // end of iterating over each field within the uvm_config_params object
    };  // end of handling uvm_config_params
    
    if( cur_uvc_node->get_like_name() == "any_event_port" ) {
        qDebug() << "Any Simple Port with hdl_path()  " << cur_uvc_node->get_hdl_path();
        
    };
    
    if( cur_uvc_node->get_like_name() == "any_simple_port" ) {
        qDebug() << "Any Simple Port with hdl_path()  " << cur_uvc_node->get_hdl_path();
        
//        QString info_string;
//        qDebug() << "=================================================================================================\n";
//        qDebug() << "|| Field: " + cur_uvc_node->get_object_name() + "\n";
//        qDebug() << "-------------------------------------------------------------------------------------------------\n";
//        qDebug() << "|| HDL Path: " + cur_uvc_node-> get_hdl_path() + "\n";
//        info_string += "|| Fields  : " +  + "\n";
//        qDebug() << "=================================================================================================\n";
        // TODO: ADD THE BUILT-IN FIELDS TO THE SIMPLE PORT CONSTRUCTOR
        // add attributes in spec_code_model.cpp
        
        
    };
    
    if( cur_uvc_node->get_like_name() == "any_interface_port" ) {
        
    };
    
    // This method call is used to update the enumerated data types within the field table
    cur_uvc_node -> update_widget();
    
    // (TODO_DEL?) In case we need the previous node information, let's simply keep the one
    prev_uvc_node = static_cast<spec_struct_data_node*> (current_uvc_tree_widget->selectedItems().at(0));
}

void spec_mod_uvc_configuration_dialog::update_agent_instances( int value ) {
    // Update the passive agent tree
    update_agent_process( value, false );
}

void spec_mod_uvc_configuration_dialog::update_active_agent_instances( int value ) {
    // Update teh active agent tree
    update_agent_process( value, true );
}

void spec_mod_uvc_configuration_dialog::update_agent_process(
    int                 value,
    bool                is_active_agent
) {
    // Variable contain comparison target values
    QString     comp_like_name;
    QString     comp_when_name;
    
    int         comp_agent_count    = (is_active_agent? active_agents_count : passive_agents_count);
    
    // Update the tree view to add or remove elements
    // element naming is "[index]"
    
    // Getting the currently selected environment element node. This node can only be an environment because only
    // this node view is capable of changing the value of the agents.
    // This node does contain all the information that a spec_struct_data_node can provide (not just QTreeWidgetItem)
    spec_struct_data_node   *env_node = static_cast<spec_struct_data_node*> (current_uvc_tree_widget->currentItem());
    
    // As a next step we need to get full-blown pointers to the PASSIVE agent node
    //  Scanning through the hierarchy to get the PASSIVE agent struct node that may be located anywhere in the hierarchy
    
    // First, an iterator is created on the current tree widget
    QTreeWidgetItemIterator it( current_uvc_tree_widget );
    
    spec_struct_data_node   *traversal_node;
    spec_struct_data_node   *new_node;
    QString                 node_like_name;
    QString                 node_when_name;
    QString                 node_object_name;
    
    int                     agent_count_diff                    = 0;
    int                     last_agent_child_index              = 0;
    int                     cur_agent_index                     = 0;
    bool                    is_uvm_compliant_agent              = false;
    bool                    is_uvm_compliant_agent_configured   = false;
    
    // Set the compare search string for either active or passive agent
    if( is_active_agent ) {
        comp_like_name = "uvm_agent";
        comp_when_name = "ACTIVE";
    } else {
        comp_like_name = "uvm_agent";
        comp_when_name = "STATEMENT";
    };
    
    // Then the traversal commences
    while( *it ) {
        traversal_node      = static_cast<spec_struct_data_node*>( *it );
        node_like_name      = traversal_node->get_like_name();
        node_when_name      = traversal_node->get_when_name();
        node_object_name    = traversal_node->get_object_name();
        
        // This code is used to sanitize the names to avoid multi-dimensional list names of agents
        if( node_object_name.contains( "[" ) ) {
            node_object_name.remove(
                node_object_name.indexOf( "[" ),
                node_object_name.size() - node_object_name.indexOf( "[" )
            );
        };  // end of removing the square bracket names
        
        // Print the current data node
//        qDebug() << " --> node " << node_like_name << " when_name " << node_when_name;
        
        // A UVM compliant agent must be:
        //  1.) derived from the uvm_agent unit
        //  2.) either ACTIVE or PASSIVE
        //  3.) instantiated directly within the selected uvm_env item
        is_uvm_compliant_agent = (
            (node_like_name == comp_like_name)
                    and
            node_when_name.contains( QRegularExpression(QString(comp_when_name) ) )
                    and
            traversal_node->parent() == env_node
        );
        
        // Only handle UVM compliant structures
        if( is_uvm_compliant_agent ) {
//            qDebug() << "value  " + QString::number(value) + "   passive_agents_count  " + QString::number(comp_agent_count);
            
            // Calculate the difference between the new value and the previous passive agents_count
            agent_count_diff = value - comp_agent_count;
//            qDebug() << " --> agent_count_diff " << QString::number(agent_count_diff);
//            qDebug() << "passive_start -> passive_end   |||     active_start -> active_end";
            // Let's set the value to an absolute index, which will be 
            if(
               (passive_agents_start_index == 0) and (passive_agents_end_index == 0) and
               (active_agents_start_index  == 0) and (active_agents_end_index == 0)
               ) {
                // When all the values are 0, then the range index has to be calculated from the current counts
                // Current agent element count ranges
                // Passive agents start index will always stay a the initially determined indes
                passive_agents_start_index  = env_node->indexOfChild( *it );
                if( is_active_agent ) {
                    // We need to decrement by one, because the active agent index of the iterated item (*it) is the
                    // active index and not the passive index. Hence fixing this by decrementing by one is the way to go.
                    --passive_agents_start_index;
                };
//                spec_msg( traversal_node->get_object_name() << " : " << traversal_node->get_struct_name() );
                // Passive agents end index will be any of the newly added/subtracted elements
                passive_agents_end_index    = ((passive_agents_count <= 1)?(passive_agents_start_index):(passive_agents_start_index + passive_agents_count-1));
                
                // ACTIVE agents start is always passive_agents_end +1
                active_agents_start_index   = passive_agents_end_index+1;
                
                // ACTIVE agents end is calculated by the element difference
                active_agents_end_index     = ((active_agents_count <= 1)? (active_agents_start_index) : (active_agents_start_index+ active_agents_count-1));
            };
            
//            qDebug() << "      " << passive_agents_start_index << "     ->      " << passive_agents_end_index<< "      |||          " << active_agents_start_index << "       ->     " << active_agents_end_index<< "     ";
//            qDebug() << "====================================================================";
            // Target agent element ranges
            
            // We need to add elements if the difference is greater than 0 and remove element otherwise
            if( agent_count_diff > 0 ) {
                if( comp_agent_count == 0 ) {
                    // We'll set the current element with the proper string
                    traversal_node -> setText( 0, node_object_name + "[" + QString::number(0) + "]");
                    traversal_node -> set_object_name( node_object_name + "[" + QString::number(0) + "]" );
                    
                    // Reflect the changes in the model... do not change the ranges
                    comp_agent_count++;
                    agent_count_diff--;
                };  // end of handling special case of adding the first element
                
                // are we still greater than zero?
                // Well then... let's add more elements
                if( agent_count_diff > 0 ) {
                    // Use these intermediate variables to calculate 
                    //   ... the new agent's named index number (new_agent_index)
                    //   ... the offset of where the new agents have to be added.
                    int new_agent_index;
                    
                    // Since we don't want to duplicate the calculation logic, we'll get each agent range's last element
                    // index
                    if( is_active_agent ) {
                        cur_agent_index = active_agents_end_index+1;
                        new_agent_index = active_agents_end_index - active_agents_start_index + 1;
                    } else {
                        cur_agent_index = passive_agents_end_index+1;
                        new_agent_index = passive_agents_end_index - passive_agents_start_index + 1;
                    };
                    
//                    qDebug() << "ADD from index " << QString::number(cur_agent_index) << "  to " << 
//                                QString::number(cur_agent_index+agent_count_diff-1);
                    
                    // Adding the nodes
                    for( int add_index = cur_agent_index; add_index < (cur_agent_index+ agent_count_diff); ++add_index) {
//                        spec_msg("add_index: " << add_index << "  new_node["<<new_agent_index<<"]");
                        // Create the new node, based on the curren traversal node's object type, set the proper node label
                        // and add it to the end of the agent range list
                        new_node = new spec_struct_data_node( traversal_node );
                        new_node -> setText( 0, QString( node_object_name + "[" + QString::number(new_agent_index) + "]") );
                        new_node -> set_object_name( QString( node_object_name + "[" + QString::number(new_agent_index) + "]") );
                        new_node -> set_widget_nullptr();
                        env_node -> insertChild( add_index, new_node );
                        
//                        spec_msg("adding new agent[" << QString::number(new_agent_index) << "] at " << add_index);
                        new_agent_index++;
                    };  // end of adding new elements
                };  // end of adding node elements (agent_count_diff > 0)
            } else {
                // First, let's check if the new value is set to 0. Then we'll remove all nodes, except the last one
                // and rename it back to just agents
                if( value == 0 ) {
                    // Put the "uninitiailzed" text back into the tree
                    traversal_node->setText( 0 , node_object_name );
                    agent_count_diff++;
                    comp_agent_count = 0;
                };
                
                // Remove every child that is a PASSIVE agent, except for the first one
                int remove_agent_start_index;
                if( is_active_agent ) {
                    cur_agent_index             = active_agents_end_index;
                    remove_agent_start_index    = ((value == 0 or value == 1)? (active_agents_start_index) : (active_agents_end_index + agent_count_diff));
                } else {
                    cur_agent_index             = passive_agents_end_index;
                    remove_agent_start_index    = ((value == 0 or value == 1)? (passive_agents_start_index) : (passive_agents_end_index + agent_count_diff));
                };
                
//                qDebug() << "remove from index " << QString::number(cur_agent_index) << "  down to " << 
//                            QString::number(remove_agent_start_index);
                
                for( int remove_index = cur_agent_index; remove_index > remove_agent_start_index; --remove_index) {
//                    qDebug() << "removing new agent at " << remove_index;
                    env_node -> takeChild( remove_index );
                };
                
            };  // end of removing node elements
            
            // After the nodes are added or removed, we'll adjust the ranges
            if( is_active_agent ) {
                // Ensure that we are not erroneously indexing anything below the start index
                if( active_agents_end_index + agent_count_diff >= active_agents_start_index ) {
                    active_agents_end_index     += agent_count_diff;
                };
                
                active_agents_count = value;
            } else {
                // Ensure that we are not erroneously indexing anything below the start index
                if( passive_agents_end_index + agent_count_diff >= passive_agents_start_index ) {
                    passive_agents_end_index    += agent_count_diff;
                    active_agents_start_index   += agent_count_diff;
                    active_agents_end_index     += agent_count_diff;
                };
                
                passive_agents_count = value;
            };
//            qDebug() << "      " << passive_agents_start_index << "       ->      " << passive_agents_end_index<< "        |||          " << active_agents_start_index << "       ->     " << active_agents_end_index<< "     ";
            
            // Ensure to set the flag to flag a "non-UVM compliant" UVC detected
            is_uvm_compliant_agent_configured = true;
            
            // Exit the loop to avoid unnecessary CPU cycles wasted on finding agents
            break;
        };   // end of handling detected PASSIVE uvm_agent
        
        // Move to the next element
        ++it;
    };   // end of traversing over each tree widget item
    
    // Inform the user that their code does not meet the requirements to configure the UVC
    if( not is_uvm_compliant_agent_configured ) {
        QMessageBox *message_box = new QMessageBox();
        message_box->setTextFormat( Qt::RichText );
        message_box->setText(
            "The loaded UVC agent can not be configured.\nPlease ensure that the agent is:<br>\
             1.) ... derived from <b>uvm_agent</b><br> \
             2.) ... instantiated <b>as a list</b> directly in the UVCs environment<br>\
             3.) ... instantiated directly <b>within the uvm_env</b> derived unit"
        );
        message_box->show();
    };
}   // end of update_agent_process
