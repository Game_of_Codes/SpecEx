// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_MOD_GEN_H
#define SPEC_MOD_GEN_H

#include <QHeaderView>
#include <QMessageBox>
#include <QObject>
#include <QProcess>

#include <BaseClasses/spec_logger.h>

//#include <spec_mainwindow.h> // <-- circular dependency!!!
#include <ui_spec_mainwindow.h>
#include <FileParser/spec_file_parser.h>
#include <CommonDialogs/spec_process_viewer.h>
#include <CodeGenerator/spec_e_clock_uvc_generator.h>
#include <CodeGenerator/spec_e_reset_uvc_generator.h>
#include <CodeGenerator/spec_e_module_code_generator.h>
#include <ModuleWizard/spec_mod_lib_table_widget.h>
#include <ModuleWizard/spec_mod_lib_tree_widget.h>
#include <ModuleWizard/spec_mod_uvc_configuration_dialog.h>
#include <ModuleWizard/spec_mod_clock_configuration_dialog.h>
#include <ModuleWizard/spec_mod_reset_configuration_dialog.h>

#include <Widgets/spec_env_widget.h>
#include <Widgets/spec_agent_widget.h>
#include <Widgets/spec_event_port_widget.h>
#include <Widgets/spec_simple_port_widget.h>
#include <Widgets/spec_tlm_port_widget.h>

#include <BaseClasses/spec_logger.h>


// Use forward declaration to avoid circular dependency
class spec_main_window;

class spec_mod_gen : public QObject {
    Q_OBJECT
    
    public:
        explicit spec_mod_gen(QObject *parent = nullptr);
        
        void    initialize();
        
        //==============================================================================================================
        // Main Window UI object
        Ui::spec_MainWindow                 *p_ui;
        
        spec_logger                         *logger = new spec_logger();
        
    signals:
        
    public slots:
        // GUI element slots
        
        void    add_specman_path            ();
        void    remove_specman_path         ();
        void    add_uvc                     ();
        void    remove_uvc                  ();
        void    show_hier_tree_selection    ();
        
        void    show_mod_uvc                ();
        
        void    show_lib_table_selection    ();
        void    show_uvc_config_dialog      ();
        void    show_clock_config_dialog    ();
        void    show_reset_config_dialog    ();
        
        void    set_module_model_config     ();
        
        void    update_uvc_configuration    (
            spec_module_model *model,
            spec_mod_lib_tree_widget    *conf_uvc_tree
        );
        
        // Parser-related slots
        void    parser_top_file(
                QStringList         *file_paths,
                QString             file_name
                );
        
        void    parser_type_slot(
                QString             parent,
                QList<QString*>     properties
                );
        
        void    parser_struct_slot(
                QString             parent,
                QList<QString*>     properties
                );
        
        void    parser_field_slot(
                QString             struct_name,
                QString             field_name,
                QList <QString*>    field_properties
                );
        
        void    parser_method_slot(
                QString             struct_name,
                QString             field_name,
                QList <QString*>    field_properties
                );
        
        void    parser_constraint_slot(
                QString             *struct_name,
                QString             *constraint_name,
                QList <QString*>    constraint_properties
                );
        
        void    simulate_module_uvc ();
        void    generate_module_uvc ();
        
    private slots:
        void change_register_checked_state      ( int state );
        void change_scoreboard_checked_state    ( int state );
        void change_clock_config_check_state    ( int state );
        void change_reset_config_checked_state  ( int state );
        
        
    private:
        //==============================================================================================================
        // UI Objects
        
        // One column table that contains elements to select a module UVC configuration library
        spec_mod_lib_table_widget           *lib_table;
        // Tree view that shows the library tree structure from the "uvm_env" inherited type
        spec_mod_lib_tree_widget            *uvc_tree;
        // Table view that shows all fields of the currently selected uvc_tree item
        spec_mod_lib_table_widget           *uvc_tree_item_table;
        
        // The message box is used to provide GUI feedback to the user
        QMessageBox                         *message_box;
        
        // The process and process viewer are used to drive launch a single validation simulation within a GUI
        // with proper log-file output
        QProcess                            *process;
        spec_process_viewer                 *process_dialog;
        
        // Configuration dialogs
        spec_mod_uvc_configuration_dialog   *mod_gen_configuration_dialog;
        spec_mod_clock_configuration_dialog *mod_gen_clock_config_dialog;
        spec_mod_reset_configuration_dialog *mod_gen_reset_config_dialog;
        
        // Storing configuration options
        bool                                has_clock_uvc = false;
        bool                                has_reset_uvc = false;
        
        //==============================================================================================================
        // Model Containers
        spec_code_model                     *uvc_component;
        spec_struct_data_node               *uvc_component_hierarchy;
        QList<spec_code_model*>             uvc_library;
        QList<spec_struct_data_node*>       uvc_library_hierarchies;
        
        spec_module_model                   *mod_data;
        spec_clock_model                    *clk_data;
        spec_reset_model                    *res_data;
        
        //==============================================================================================================
        // File parser
        spec_file_parser                    *e_file_parser;
        
        //==============================================================================================================
        // Code Generators
        spec_e_module_code_generator        *module_code_generator;
        spec_e_clock_uvc_generator          *clock_code_generator;
        spec_e_reset_uvc_generator          *reset_code_generator;
        
        
        void                                init_file_parser    ();
        void                                set_clock_model     ();
        void                                set_reset_model     ();
        
        QString                             get_connected_field_enclosing_struct(
                spec_struct_data_node   data_node,
                QList<QString>          expr_list
                );
        QString                             get_connected_field_type (
                spec_struct_data_node   data_node,
                QList<QString>          expr_list
                );
        
};   // end of spec_mod_gen

#endif // SPEC_MOD_GEN_H
