// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_mod_reset_configuration_dialog.h"

spec_mod_reset_configuration_dialog::spec_mod_reset_configuration_dialog() {
    // Instantiate each of the objects
    add_button                  = new QPushButton();
    del_button                  = new QPushButton();
    ok_button                   = new QPushButton();
    add_button                  = new QPushButton();
    
    number_of_reset_label       = new QLabel();
    
    reset_config_scroll_area    = new QScrollArea();
    reset_config_table_widget   = new spec_table_widget(
        new QList<QString> { "Domain Name", "HDL Name", "Active Level", "Sync To Clock", "Init Value" }
    );
    reset_config_table_widget->init_widget_data_model(
        new QList<QString> { "string", "string", "combo_reset_active", "combo_reset_sync", "string" }
    );
    reset_config_table_widget   -> setColumnWidth( 0, 200 );
    reset_config_table_widget   -> setColumnWidth( 1, 200 );
    reset_config_table_widget   -> setColumnWidth( 2, 150 );
    reset_config_table_widget   -> setColumnWidth( 3, 150 );
    reset_config_table_widget   -> setColumnWidth( 4, 120 );
    
    // Layout of GUI elements
    number_of_reset_label       -> setGeometry  ( 20, 20, 450, 40 );
    number_of_reset_label       -> setText      ( "Reset Properties: Each line represents a single reset domain" );
    number_of_reset_label       -> setParent    ( this );
    
    reset_config_scroll_area    -> setGeometry  ( 20, 80, 1000, 500 );
    reset_config_scroll_area    -> setParent    ( this );
    
    reset_config_table_widget   -> setGeometry  ( 0, 0, 1000, 500 );
    reset_config_table_widget   -> setParent    ( reset_config_scroll_area );
    
    // If RESET UVC generation is selected, the user has to specifically declare each row explicitly
    reset_config_table_widget->removeRow( 0 );
    
    add_button                  -> setIcon      ( QIcon(":images/plus_only.png") );
    add_button                  -> setGeometry  ( 20, 600, 50, 50 );
    add_button                  -> setParent    ( this );
    
    del_button                  -> setIcon      ( QIcon(":images/minus_only.png") );
    del_button                  -> setGeometry  ( 75, 600, 50, 50 );
    del_button                  ->setParent     ( this );
    
    ok_button                   -> setText      ( "Ok" );
    ok_button                   -> setGeometry  ( 450, 600, 100, 50 );
    ok_button                   -> setParent    ( this );
    
    // Connecting the elemnents
    connect(
        add_button                  , &QPushButton::pressed,
        this                        , &spec_mod_reset_configuration_dialog::add_row
    );
    
    connect(
        del_button                  , &QPushButton::pressed,
        reset_config_table_widget   , &spec_table_widget::delete_selected_row
    );
    
    connect(
        ok_button                   , &QPushButton::pressed,
        this                        , &QDialog::hide
    );
}

void spec_mod_reset_configuration_dialog::add_row() {
    int row_index = reset_config_table_widget->rowCount();
    reset_config_table_widget   -> add_row("");
    
    QComboBox   *init_combo_box     = new QComboBox();
    init_combo_box  -> addItems( {"0", "1"} );
    
    reset_config_table_widget   -> setCellWidget( row_index, 4, init_combo_box );
}   // end of add_row

int spec_mod_reset_configuration_dialog::get_reset_config_count() {
    return reset_config_table_widget->rowCount();
}

QList<QString*> spec_mod_reset_configuration_dialog::get_reset_config_at( int index ) {
    QList<QString*> result;
    
    // result[0] == Domain Name
    result.append( new QString( reset_config_table_widget->item( index, 0 )->text() ) );
    
    // result[1] == HDL Name
    result.append( new QString( reset_config_table_widget->item( index, 1 )->text() ) );
    
    // result[2] == Reset ACTIVE level
    QComboBox *temp_combo_box = qobject_cast<QComboBox*>( reset_config_table_widget->cellWidget( index, 2) );
    
    if( temp_combo_box->currentText() == "Active-Low" ) {
        result.append( new QString( "RESET_LO" ) );
    } else {
        result.append( new QString( "RESET_HI" ) );
    };
    
    // result[3] == Reset Synchronous to clock
    temp_combo_box = qobject_cast<QComboBox*>( reset_config_table_widget->cellWidget( index, 3) );
    
    if( temp_combo_box->currentText() == "Asynchronous" ) {
        result.append( new QString( "RESET_ASYNC" ) );
    } else {
        result.append( new QString( "RESET_SYNC" ) );
    };
    
    // result[4] == Reset init value
    result.append( new QString( static_cast<QComboBox*>(reset_config_table_widget->cellWidget( index, 4 ))->currentText() ) );
    
    return result;
}
