// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_mod_clock_configuration_dialog.h"

spec_mod_clock_configuration_dialog::spec_mod_clock_configuration_dialog() {
    // Instantiate each of the objects
    add_button                  = new QPushButton();
    del_button                  = new QPushButton();
    ok_button                   = new QPushButton();
    number_of_clock_label       = new QLabel();
    
    time_resolution_label       = new QLabel( "Selected Time Scale: " );
    time_resolution_combo_box   = new QComboBox();
    
    time_resolution_combo_box   -> addItems( { "fs", "ps", "ns", "us", "ms", "s", "(none)" } );
    
    clock_config_scroll_area    = new QScrollArea();
    clock_config_table_widget   = new spec_table_widget(
        new QList<QString> { "Domain Name", "HDL Name", "Jitter", "Period", "Shift", "Init Value" }
    );
    // Change the table widgets to reflect the number elements to be QSpinBoxes!!!!
    clock_config_table_widget->init_widget_data_model(
        new QList<QString> { "string"     , "string"  , "string"  , "string", "string", "string"  }
    );
    clock_config_table_widget ->setColumnWidth( 1, 225 );
    clock_config_table_widget ->setColumnWidth( 2,  80 );
    clock_config_table_widget ->setColumnWidth( 3,  80 );
    clock_config_table_widget ->setColumnWidth( 4,  80 );
    clock_config_table_widget ->setColumnWidth( 5, 150 );
    
    // Layout of GUI elements
    time_resolution_label       -> setGeometry  ( 20, 20, 200, 40 );
    time_resolution_label       -> setParent    ( this );
    
    time_resolution_combo_box   -> setGeometry  ( 220, 20, 100, 40 );
    time_resolution_combo_box   -> setFont      ( QFont("Courier New") );
    time_resolution_combo_box   -> setParent    ( this );
    
    clock_config_scroll_area    -> setGeometry  ( 20, 80, 1000, 500 );
    clock_config_scroll_area    -> setParent    ( this );
    
    clock_config_table_widget   -> setGeometry  ( 0, 0, 1000, 500 );
    clock_config_table_widget   -> setParent    ( clock_config_scroll_area );
    
    add_button                  -> setIcon      ( QIcon(":images/plus_only.png") );
    add_button                  -> setGeometry  ( 20, 600, 50, 50 );
    add_button                  -> setParent    ( this );
    
    del_button                  -> setIcon      ( QIcon(":images/minus_only.png") );
    del_button                  -> setGeometry  ( 75, 600, 50, 50 );
    del_button                  ->setParent     ( this );
    
    ok_button                   -> setText      ( "Ok" );
    ok_button                   -> setGeometry  ( 450, 600, 100, 50 );
    ok_button                   -> setParent    ( this );
    
    // If CLOCK UVC generation is selected, the user has to specifically declare each row explicitly
    clock_config_table_widget->removeRow( 0 );
    
    // Connecting the elemnents
    connect(
        add_button                  , &QPushButton::pressed,
        this                        , &spec_mod_clock_configuration_dialog::add_row
    );
    
    connect(
        del_button                  , &QPushButton::pressed,
        clock_config_table_widget   , &spec_table_widget::delete_selected_row
    );
    
    connect(
        ok_button                   , &QPushButton::pressed,
        this                        , &QDialog::hide
    );
    
}   // end of constructor

void spec_mod_clock_configuration_dialog::add_row() {
    int row_index = clock_config_table_widget->rowCount();
    clock_config_table_widget->add_row("");
    
    QCheckBox   *jitter_check_box   = new QCheckBox();
    QSpinBox    *period_spin_box    = new QSpinBox();
    QSpinBox    *shift_spin_box     = new QSpinBox();
    QComboBox   *init_combo_box     = new QComboBox();
    
    period_spin_box -> setMinimum( 0 );
    period_spin_box -> setMaximum( 1000 );
    shift_spin_box  -> setMinimum( 0 );
    shift_spin_box  -> setMaximum( 1000 );
    init_combo_box  -> addItems( {"0", "1"} );
    
    clock_config_table_widget->setCellWidget( row_index, 2, jitter_check_box );
    clock_config_table_widget->setCellWidget( row_index, 3, period_spin_box );
    clock_config_table_widget->setCellWidget( row_index, 4, shift_spin_box );
    clock_config_table_widget->setCellWidget( row_index, 5, init_combo_box );
}   // end of add_row

int spec_mod_clock_configuration_dialog::get_clock_config_count() {
    return clock_config_table_widget->rowCount();
}   // end of get_clock_config_count

QList<QString*> spec_mod_clock_configuration_dialog::get_clock_config_at( int index ) {
    QList<QString*> result;
    
    // result[0] == Clock Domain Name
    result.append( new QString( clock_config_table_widget->item( index, 0 )->text() ) );
    
    // result[1] == Clock HDL Name
    result.append( new QString( clock_config_table_widget->item( index, 1 )->text() ) );
    
    // result[2] == Jitter Enabled
    if( static_cast<QCheckBox*>(clock_config_table_widget->cellWidget(index, 2))->checkState() == Qt::Checked ) {
        result.append( new QString( "true" ) );
    } else {
        result.append( new QString( "false" ) );
    };
    
    // result[3] == Period
    result.append( new QString( QString::number(static_cast<QSpinBox*>(clock_config_table_widget->cellWidget(index, 3 ))->value()) ) );
    // result[4] == Shift
    result.append( new QString( QString::number(static_cast<QSpinBox*>(clock_config_table_widget->cellWidget( index, 4 ))->value()) ) );
    
    // result{5] == Init Value
    result.append( new QString( static_cast<QComboBox*>(clock_config_table_widget->cellWidget( index, 5 ))->currentText() ) );
    
    return result;
}   // end of get_clock_config_at

QString spec_mod_clock_configuration_dialog::get_clock_time_scale    () {
    return time_resolution_combo_box->currentText();
}
