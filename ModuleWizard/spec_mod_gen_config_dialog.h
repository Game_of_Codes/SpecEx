// ----------------------------------------------------------------------------
//   Copyright 2018 Daniel Bayer
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_MOD_GEN_CONFIG_DIALOG_H
#define SPEC_MOD_GEN_CONFIG_DIALOG_H

#include <QCheckBox>
#include <QDebug>
#include <QDialog>
#include <QDialogButtonBox>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QScrollArea>

#include <ModuleWizard/spec_mod_lib_table_widget.h>
#include <ModuleWizard/spec_mod_lib_tree_widget.h>


class spec_mod_gen_config_dialog : public QDialog {
    Q_OBJECT
    
    public:
        spec_mod_gen_config_dialog();
        
        QPushButton                 *ok_button;
//        QString             get_group_name();
//        QString             get_project_name();
//        bool                get_has_clock();
//        bool                get_has_reset();
//        bool                get_use_register_model();
//        unsigned int        get_register_max_addr_width()   const;
//        unsigned int        get_register_max_data_width()   const;
//        bool                get_create_scoreboard();
//        bool                get_use_uvm_scoreboard()        const;
        
    public slots:
//        virtual void        accept();
//        void                show_uvc_tree();
        
    signals:
//        void                ok_button_pressed();
        
    private slots:
//        void                change_register_line_edit_state( int state );
        
    private:
        QGroupBox                   *current_uvc_group_box;
        QScrollArea                 *current_uvc_scroll_area;
        spec_mod_lib_tree_widget    *current_uvc_tree_widget;
        
        
        QGroupBox                   *configured_uvc_group_box;
        spec_mod_lib_table_widget   *config_table_widget;
        
//        QGroupBox           *prefix_group_box;
//        QLabel              *prefix_group_group_label;
//        QLabel              *prefix_group_project_label;
//        QLineEdit           *prefix_group_group_line_edit;
//        QLineEdit           *prefix_group_project_line_edit;
        
//        QCheckBox           *has_clock_uvc_check_box;
//        QCheckBox           *has_reset_uvc_check_box;
        
//        QGroupBox           *register_model_group_box;
//        QCheckBox           *use_register_model_check_box;
//        QLabel              *register_max_addr_width_label;
//        QLineEdit           *register_max_addr_width_line_edit;
//        QLabel              *register_max_data_width_label;
//        QLineEdit           *register_max_data_width_line_edit;
        
//        QCheckBox           *create_scoreboard_check_box;
//        QCheckBox           *use_uvm_scoreboard_check_box;
        
//        QDialogButtonBox    *config_dialog_button_box;
};

#endif // SPEC_MOD_GEN_CONFIG_DIALOG_H
