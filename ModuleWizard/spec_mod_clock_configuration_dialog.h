// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_MOD_CLOCK_CONFIGURATION_DIALOG_H
#define SPEC_MOD_CLOCK_CONFIGURATION_DIALOG_H

#include <QCheckBox>
#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QScrollArea>
#include <QSpinBox>

#include<BaseClasses/spec_logger.h>
#include <BaseClasses/spec_tablewidget.h>

class spec_mod_clock_configuration_dialog : public QDialog {
    Q_OBJECT
    
    public:
        spec_mod_clock_configuration_dialog();
        
        QPushButton         *add_button;
        QPushButton         *del_button;
        QPushButton         *ok_button;
        
        int                 get_clock_config_count  ();
        QList<QString*>     get_clock_config_at     ( int index );
        QString             get_clock_time_scale    ();
        
        
    public slots:
        void                add_row();
        
    private:
        QLabel              *number_of_clock_label;
        QLabel              *time_resolution_label;
        QComboBox           *time_resolution_combo_box;
        
        QScrollArea         *clock_config_scroll_area;
        spec_table_widget   *clock_config_table_widget;
};

#endif // SPEC_MOD_CLOCK_CONFIGURATION_DIALOG_H
