// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_MOD_LIB_TREE_WIDGET_H
#define SPEC_MOD_LIB_TREE_WIDGET_H

#include <QObject>
#include <QWidget>

#include "BaseClasses/spec_treewidget.h"

class spec_mod_lib_tree_widget : public spec_tree_widget {
    Q_OBJECT
    
    public:
        spec_mod_lib_tree_widget(QList <QString> *header_string_list) : spec_tree_widget(header_string_list){
            
        }
};

#endif // SPEC_MOD_LIB_TREE_WIDGET_H
