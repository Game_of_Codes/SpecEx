// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_MOD_UVC_CONFIGURATION_DIALOG_H
#define SPEC_MOD_UVC_CONFIGURATION_DIALOG_H

#include <QCheckBox>
#include <QDialog>
#include <QDialogButtonBox>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QMessageBox>
#include <QPushButton>
#include <QScrollArea>

#include <BaseClasses/spec_logger.h>

#include <ModuleWizard/spec_mod_lib_table_widget.h>
#include <ModuleWizard/spec_mod_lib_tree_widget.h>
#include <CodeModel/spec_code_model.h>
#include <CodeModel/spec_module_model.h>

class spec_mod_uvc_configuration_dialog : public QDialog {
    Q_OBJECT
    
    public:
        spec_mod_uvc_configuration_dialog();
        
        void                        set_module_model(
            spec_module_model       *model
        );
        
        void                        set_root_node(
            spec_mod_lib_tree_widget           *uvc_tree//spec_code_model         *code_model
        );
        
        QPushButton                 *ok_button;
        QPushButton                 *abort_button;
        
    public slots:
        void                        show_selected_item              ();
        void                        update_agent_instances          ( int value );
        void                        update_active_agent_instances   ( int value );
        
    signals:
        void                        ok_button_pressed               (
            spec_module_model           *model,
            spec_mod_lib_tree_widget    *conf_tree
        );
        
    private slots:
        void                        update_model();
        
        
    private:
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Object Declarations
        
        QGroupBox                   *current_uvc_group_box;
        spec_mod_lib_tree_widget    *current_uvc_tree_widget;
        
        QGroupBox                   *configured_uvc_group_box;
        spec_mod_lib_table_widget   *config_table_widget;
        
        // Code model, which will get the changes
        spec_module_model           *mod_model;
        spec_struct_data_node       *hierarchy;
        
        spec_env_widget             *env_widget;
        
        spec_struct_data_node       *cur_uvc_node;
        spec_struct_data_node       *prev_uvc_node;
        
        int                         passive_agents_count;
        int                         active_agents_count;
        int                         passive_agents_start_index;
        int                         passive_agents_end_index;
        int                         active_agents_start_index;
        int                         active_agents_end_index;
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Method Declarations
        
        
        void                        set_node(
            spec_struct_data_node   *node,
            int                     stack_depth
        );
        
        void                        update_agent_process(
            int                 value,
            bool                is_active_agent
        );
};

#endif // SPEC_MOD_UVC_CONFIGURATION_DIALOG_H
