// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------


#ifndef SPEC_MOD_RESET_CONFIGURATION_DIALOG_H
#define SPEC_MOD_RESET_CONFIGURATION_DIALOG_H

#include <QCheckBox>
#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QScrollArea>
#include <QSpinBox>

#include <BaseClasses/spec_logger.h>
#include <BaseClasses/spec_tablewidget.h>

class spec_mod_reset_configuration_dialog : public QDialog {
    Q_OBJECT
    
    public:
        spec_mod_reset_configuration_dialog();
        
        QPushButton         *add_button;
        QPushButton         *del_button;
        QPushButton         *ok_button;
        
        int                 get_reset_config_count();
        QList<QString*>     get_reset_config_at( int index );
        
    public slots:
        void                add_row();
        
    private:
        QLabel              *number_of_reset_label;
        
        QScrollArea         *reset_config_scroll_area;
        spec_table_widget   *reset_config_table_widget;
};

#endif // SPEC_MOD_RESET_CONFIGURATION_DIALOG_H
