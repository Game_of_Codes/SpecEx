// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_MAINWINDOW_H
#define SPEC_MAINWINDOW_H

// Include Qt Libraries

#include <QFileDialog>
#include <QList>
#include <QMainWindow>
#include <QMouseEvent>
#include <QTableWidget>
#include <QString>

#include "ui_spec_mainwindow.h"
#include "ui_spec_type_dialog.h"

#include <InterfaceWizard/spec_if_gen.h>
#include <ModuleWizard/spec_mod_gen.h>
#include <EBrowser/spec_e_browser.h>

#include <BaseClasses/spec_logger.h>

//#include <Examples/examplesignalinterface.h>

namespace Ui {
    class spec_MainWindow;
}

class spec_main_window : public QMainWindow {
    Q_OBJECT

    public:
        explicit spec_main_window(QWidget *parent = 0);
        ~spec_main_window();
        
        void mousePressEvent(QMouseEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
        void mouseReleaseEvent(QMouseEvent *event);
        
        void updateWidgetGeometry();
        
        // This method returns a pointer to the UI object
        Ui::spec_MainWindow*    get_ui_pointer();
        
    public slots:
        
    private:
        // Main Window UI object
        Ui::spec_MainWindow                 *ui;
        
        QPoint                              _initial;
        QPoint                              _current;
        
        int                                 _initTreeViewX;
        int                                 _initTreeViewY;
        int                                 _initTreeViewWidth;
        int                                 _initTreeViewHeight;
        
        int                                 _initTableViewX;
        int                                 _initTableViewY;
        int                                 _initTableViewWidth;
        int                                 _initTableViewHeight;
        
        void                                resizeEvent(QResizeEvent *event);
        
        // NEW Classes
        spec_mod_gen                        *mod_wizard;
        spec_if_gen                         *if_wizard;
        spec_e_browser                      *e_browser;
        
//        // Adding the e file parser
//        spec_file_parser    *fp;
        
//        spec_code_model     *declared_structs = new spec_code_model();
        
//        // This instance is used in the module browser tab to validate and identify that the parser
//        // correctly determined the loaded e details (types, structs, struct members, TLM ports)
//        spec_tree_widget    *tree_widget = new spec_tree_widget(
//            new QList <QString> {
//                tr("Object Name"), tr("Struct Name")
//            }
//        );
        
//        // This instance table widget is connected with the tree_widget. Any selected tree_widget item will
//        // cause the table to be updated
//        spec_table_widget   *module_browser_table_widget = new spec_table_widget(
//            new QList <QString> {
//                tr("Name"), tr("Type"), tr("Width"), tr("!"), tr("%"), tr("List Of")
//            }
//        );
        
    private slots:
        void    init_gui_elements   ();
        
        void    show_message_about  ();
        
        
//        void update_display_module_uvc_browser();
};

#endif // SPEC_MAINWINDOW_H
