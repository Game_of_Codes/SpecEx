// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_FILE_PARSER_H
#define SPEC_FILE_PARSER_H

#include <QDir>
#include <QFile>
#include <QFileDialog>
#include <QObject>
#include <QRegularExpression>
#include <QString>
#include <QSysInfo>
#include <QProcessEnvironment>

#include <BaseClasses/spec_logger.h>

// SystemVerilog Lexer/Parser 
// --> Using the ANTLR approach does not seem to be platform independent, since the generated code works for Linux, but not windows.
// --> As possible workaround, it would be required to build ANTLR against the Cpp target on Windows and maintain those
// --> generated files as well as the Linux generated files.
//#include <iostream>
//#include <antlr4-runtime.h>
//#include <list>
//#include <fstream>
//
//#include "VerilogLanguageLexer.h"
//#include "VerilogLanguageParser.h"
//#include "HeaderPrinter.h"
//#include "VerilogHeaderPort.h"


class spec_file_parser : public QObject {
    Q_OBJECT
        
    public:
        explicit    spec_file_parser(QObject *parent = nullptr);
        
        void        read_file(
                QString dir,
                QString file
                );
        QString     remove_comment(
                QString line
                );
        QString     remove_delimiter(
            QString token
        );
        QString     remove_character(
            QString character,
            QString token
        );
        
        QStringList string_processor(
            QStringList *tokens
        );
        
        QStringList pre_processor(
            QStringList *tokens
        );
        
        QStringList define_as_processor(
            QStringList *tokens
        );
        
        void        parse_struct_members(
            QString     struct_name,
            QStringList tokens
        );
        void        parse_struct_field(
            QString     struct_name,
            QStringList tokens
        );
        void        parse_struct_method(
            QString     struct_name,
            QStringList tokens
        );
        void        parse_struct_constraint(
            QString     struct_name,
            QStringList tokens
        );
        void        parse_struct_event(
            QString     struct_name,
            QStringList tokens
        );
        void        parse_struct_cover(
            QString     struct_name,
            QStringList tokens
        );
        void        parse_constraint_expression(
            QString     struct_name,
            QString     constraint_name,
            QStringList tokens
        );
        
        
    public slots:
        void        read_sv_interface();
        
    signals:
        void        top_file(
            QStringList         *folder_names,
            QString             file_name
        );
        void        type_declaration(
            QString             type_name,
            QList <QString*>    type_properties
        );
        void        struct_declaration(
            QString             struct_name,
            QList <QString*>    struct_properties
        );
        
        void        field_declaration(
            QString             struct_name,
            QString             field_name,
            QList <QString*>    field_properties
        );
        
        void        method_declaration(
            QString             struct_name,
            QString             method_name,
            QList <QString*>    method_properties
        );
        
        void        constraint_declaration(
            QString             *struct_name,
            QString             *constraint_name,
            QList <QString*>    constraint_properties
        );
        
        void        loading_uvc_done();
        
//        void        build_instance_tree(  ; << // TODO: Add this when extend sys is performed!
        
    public slots:
        void    load_uvc();
        
        void    clear_parsed_data();
        
        void    add_specman_path(
                QString path
                );
        void    remove_specman_path(
                unsigned int index
                );
        
    private:
        QProcessEnvironment process_environment;
        QStringList         *file_search_paths      = new QStringList();
        QStringList         *imported_files         = new QStringList();
        
        // Both lists will always have to be the same size
        QList<QString>      *pre_processor_names    = new QList<QString>();
        QList<QString>      *pre_processor_values   = new QList<QString>();
        QStringList         expr_operators          = {
            "=>",
            ">", "==", "<", ">=", "<=", "!="
            "in", "not", "or", "and"
        };
};

#endif // SPEC_FILE_PARSER_H
