// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_file_parser.h"

spec_file_parser::spec_file_parser(QObject *parent) : QObject(parent) {
    // Initialize and set the SPECMAN_PATH, if it has been previously set in the
    // environment
    process_environment = QProcessEnvironment::systemEnvironment();
    QString             sn_path_str;
    QList<QString>      path_string_list;
    QString             product_type = QSysInfo::productType();
    
    if( process_environment.keys().contains( "SPECMAN_PATH" ) ) {
        // Capture the SPECMAN_PATH variable and change the directory character, if we are in Windows
        sn_path_str = process_environment.value( "SPECMAN_PATH" );
        if( product_type == "windows" ) {
            sn_path_str = sn_path_str.replace(QRegularExpression("\\\\"), "/");
            // In case there are multiple paths defined, then those are separated either by semicolon (Windows)
            // or colon (Linux)
            path_string_list = sn_path_str.split( QRegularExpression(";") );
        } else {
            path_string_list = sn_path_str.split( QRegularExpression(":") );
        };
        
        for( QString str : path_string_list ) {
            if( not str.endsWith("/") ) {
                str.append("/");
            };
            file_search_paths->append(  str );
        };
        
        
//        if( file_search_paths->isEmpty() ) {
//            file_search_paths->append(  sn_path_str );
//        };
    };   // end of checking if SPECMAN_PATH is present
}   // end of spec_file_parser

void spec_file_parser::load_uvc() {
    QString file_name = QFileDialog::getOpenFileName(
        nullptr,
        tr("Select e File"),
        "",
        tr("e-Files (*.e)")
    );
    
    if( file_name == "" ) {
        // Abort processing if the file name was empty.
        return;
    };
    
    QString directory_name = file_name.left( file_name.lastIndexOf("/") );
    qDebug() << "Directory name  " << directory_name;
    qDebug() << "File Name       " << file_name;
    
    if( not file_search_paths->contains( directory_name ) ) {
        file_search_paths->append( directory_name + "/" );
        
        directory_name = directory_name.left( directory_name.lastIndexOf("/") );
        file_search_paths->append( directory_name  + "/" );
        
        directory_name = directory_name.left( directory_name.lastIndexOf("/") );
        file_search_paths->append( directory_name  + "/" );
    };
    
    emit top_file( file_search_paths, file_name );
    for(QString dir : *file_search_paths) {
        qDebug() << "dir " << dir;
    };
    
    // Ensure the current file is recognized as imported
    imported_files->append( file_name );
    
    // Start the reading process
    read_file(directory_name, file_name);
    
    
    qDebug() << "=======================================================\nImported the following files:";
    for(QString file: *imported_files) {
        qDebug() << "\t" << file;
    };
    qDebug() << "=======================================================\n";
    // Now trigger the event that the complete code base has been loaded
    emit loading_uvc_done();
}   // end of load_uvc

void spec_file_parser::clear_parsed_data() {
    imported_files          -> clear();
    pre_processor_names     -> clear();
    pre_processor_values    -> clear();
}   // end of clear_parsed_data

// This method reads a given e file
void spec_file_parser::read_file(
        QString dir,
        QString file
) {
    qDebug() << "Starting to read file " << (file);
    
    QFile top_file( file );
    
    if( !top_file.open(QIODevice::ReadOnly | QIODevice::Text) ) {
        qDebug() << "Can't read file " <<  file;
        if( !top_file.exists() ) {
            qDebug()  << " ==> File does not exist";
        };
    } else {
        QTextStream             top_in(&top_file);
        
        QString                 line;
        QStringList             token_line;
        QStringList             tokenized_file_lines;
//        QList <unsigned int>    token_line_number;
        
        unsigned int    l_index     = 1;        // start counting at line 1
        
        bool            is_delim_start_found;
        bool            is_delim_end_found;
        
        // Continue until the end of the file has been reached
        while( not top_in.atEnd() ) {
            line = top_in.readLine();
            
            is_delim_start_found = (QString::compare("<'", line) == 0);
            
            // Only start processing code if the code delimiter line was found
            if( is_delim_start_found ) {
                is_delim_end_found = false;
//                qDebug() << l_index << ": \t" << line;
                // Continue to cycle over code until the end of code delimiter was found
                while( not is_delim_end_found and not top_in.atEnd()) {
                    line = top_in.readLine();
                    l_index++;
                    
                    // Remove all the comments in the given line
                    line = remove_comment( line );
                    
                    // Only process non-empty lines
                    if( not line.isEmpty() ) {
                        // Expand each special character by adding space to it. This allows for easier token handling
                        line = line.replace( "\"", " \" " );
                        line = line.replace( "\{", " \{ " );
                        line = line.replace( "\}", " \} " );
                        line = line.replace( "\(", " \( " );
                        line = line.replace( "\)", " \) " );
                        line = line.replace( "\[", " \[ " );
                        line = line.replace( "\]", " \] " );
                        line = line.replace( "\;", " \; " );
                        line = line.replace("-", " - " );
                        line = line.replace( "\\\\", " \\\\ " );
                        // String regexp:     '"' .*? ~('\\\"' | '\\n' | '\\t')  '"'
                        // StackOverflow:     (["'])(?:(?=(\\?))\2.)*?\1
                        // Split to remove all the white-space characters and remove them
                        token_line = line.split(
                            QRegularExpression( "(\\s+|\\b)" ),
                            QString::SkipEmptyParts
                        );
                        
//                        qDebug() << "token_line " << token_line;
                        
                        if( not token_line.isEmpty() ) {
                            tokenized_file_lines.append( token_line );
//                            token_line_number.append( l_index );
                        };
                    };   // end of processing non-empty lines after removing comments
                    
                    
                    // Ensure to detect the code delimiter
                    is_delim_end_found   = (QString::compare("'>", line) == 0);
                    // Also disable reading in code that is declared between two closing -> opening code delimiters
                    is_delim_start_found = not is_delim_end_found;
                };   // end of processing each code-line
                if( not is_delim_end_found and top_in.atEnd() ) {
                    qDebug() << "End of file reached without end-of-code delimiter";
                };
            };   // end of processing actual code
            l_index++;
        };   // end of reading file line-by-line
        
//        qDebug() << "Fully tokenized code \n" << tokenized_file_lines;
        
        // Run pre-processor
        
        tokenized_file_lines = string_processor     ( &tokenized_file_lines );
        tokenized_file_lines = pre_processor        ( &tokenized_file_lines );
        tokenized_file_lines = define_as_processor  ( &tokenized_file_lines );
        
        // Variables that are needed for token parsing process
        // Token Index indicates the current token index
                 int    token_index                 = 0;
        
        // The bracket balance is 0, if there are no open brackets.
        // In case a closing bracket is missing, then the balance is > 0
        // In case a opening bracket is missing, then the balance is < 0
                 int    statement_bracket_balance   = 0;
        
        // The struct name is the root of a given parsing object
        QString         struct_name;
        // The struct like inheritance name will be empty, if the parsed struct was not inherited
        QString         struct_like_name;
        // The struct unit/struct indicator contains which type it is
        QString         struct_is_unit;
        
        
        // Fields that are required for processing the "import" statement
        bool            is_import               = false;
                 int    import_start_index      = 0;
        int             import_slash_index      = 0;
        QStringList     import_token_list;
        QString         import_directory;
        QString         import_traverse_directory;
        QString         import_file;
        
        // Variables that are required for processing the "struct" or "unit" statement
        bool            is_struct_unit          = false;
        bool            is_unit                 = false;
        bool            has_like                = false;
                 int    struct_unit_start_index = 0;
        QStringList     struct_unit_token_list;
        
        // Variables that are required for processing the type declaration statement
        bool            is_type                 = false;
                 int    type_start_index        = 0;
        QStringList     type_token_list;
        QString         type_name;
        QString         type_values;
        
        // Variables that are required for processing the extend statement
        bool            is_extend               = false;
                 int    extend_start_index      = 0;
        QStringList     extend_token_list;
        bool            is_extend_type          = false;
        bool            is_extend_struct_unit   = false;
        
        // TODO: Check validity of sequence statement
        // Variables that are required for processing the sequence statement
        bool            is_sequence             = false;
                 int    sequence_start_index    = 0;
        QStringList     sequence_token_list;
        
        
        // Variables that are required for processing the package declaration statement
        bool            is_package              = false;
                 int    package_start_index     = 0;
        QString         package_name;
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // PARSE LOOP 1: Iterate over each identified token
        //  This loop scans the full toke stream (per file) for statements.
        // Re-initialize the token_index
//        token_index = 0;
        
        for(QString token : tokenized_file_lines) {
//            qDebug() << "[" << token_index << "] --> " << token << " has quotation marks -> "<<token.contains("\"");
            
            if( token == "import" ) {
                is_import           = true;
                import_start_index  = token_index;
                qDebug() << "import detected at " << import_start_index;
            };
            if( token == "struct" or token == "unit" ) {
                is_struct_unit          = true;
                struct_unit_start_index = token_index;
                qDebug() << "struct/unit declaration detected at " << struct_unit_start_index;
            };
            if( token == "type" ) {
                is_type             = true;
                type_start_index    = token_index;
                qDebug() << "Type declaration detected at " << type_start_index;
            };
            if( token == "extend" ) {
                is_extend           = true;
                extend_start_index  = token_index;
                qDebug() << "Extend statement detected at " << extend_start_index;
            };
            if( token == "sequence" ) {
                is_sequence         = true;
                sequence_start_index= token_index;
                qDebug() << "Sequence statement detected at " << sequence_start_index;
            };
            if( token == "package" ) {
                is_package = true;
                package_start_index = token_index;
                qDebug() << "package detected at " << package_start_index;
            };
            
            // Handling "import"
            if( is_import == true ) {
                // Check if the end-of-line was detected
                // Matching has to be done against ending with delimiter because
                // the semi-colon character may either be shown directly with the name
                // or as a separate character (white-space)                
                if( token ==  ";" ) {
                    // Iterate from the import keyword to the detected EOL character
                    for (int index = import_start_index; index <= token_index; ++index) {
                        
                        // Remove import from list of strings
                        if( tokenized_file_lines.at(index) != "import" ) {
                            // Check if the end of the import has occurred
                            if( tokenized_file_lines.at(index) == ";" ) {
                                import_directory    = QString("");
                                import_file         = QString("");
                                
                                // If a relative directory was provided, then we'll capture the file and directory
                                // separately.
                                if( import_slash_index != 0 ) {
                                    for (int dir_index = 0; dir_index <= import_slash_index; ++dir_index) {
                                        import_directory += import_token_list.at(dir_index);
                                    };
                                };
                                import_file = QString( import_token_list.last() + ".e" );
                            } else {
                                if( tokenized_file_lines.at(index) != "." ) {
                                    // Only add the directory and the file into the string list
                                    import_token_list.append(tokenized_file_lines.at(index));
                                };
                            };
                        };   // end of not adding import branch
                        // Track the position at which a directory hierarchy character is detected
                        if( tokenized_file_lines.at(index) == "/" ) {
                            import_slash_index = import_token_list.count()-1;
                        };
                        if( tokenized_file_lines.at(index) == "." ) {
                            index++;
                        };
                    };   // end of traversing through the import tokens
                    
                    // Iterate over each of the previously determine file-paths.
                    for(QString directory: *file_search_paths) {
                        // Since each SPECMAN_PATH must be searched for the import file, in order to proceed, we need
                        // to ensure the file actually exists
                        import_traverse_directory = directory + import_directory;
                        
                        if( QFileInfo::exists( import_traverse_directory + import_file ) ) {
                            // Ensure that already loaded files will not be re-imported. This is to avoid running into
                            // any recursive or circular import statements that may be scattered across various files
                            if( not imported_files->contains( import_traverse_directory + import_file ) ) {
                                // Track the full file and path that are going to be loaded
                                imported_files->append( import_traverse_directory + import_file );
                                
                                // Read and parse the file
                                read_file( import_traverse_directory, ( import_traverse_directory + import_file ) );
                            } else {
                                qDebug() << "File already loaded ... skipping loading it again: " << (import_traverse_directory + import_file);
                            };
                        };   // end of handling only loading existing files
                    };
                    
                    // Reset import counter values
                    is_import           = false;
                    import_start_index  = 0;
                    import_slash_index  = 0;
                    import_token_list.clear();
                    import_file         = QString("");
                    import_directory    = QString("");
                };   // end of "import detected"
            };   // end of handling "import statement"
            
            // Handling "struct" and "unit"
            if( is_struct_unit == true ) {
//                qDebug() << struct_cnt << ":  " << token;
                
                // The bracket matching is done via a balancing scheme.
                // Error checking could also be applies here to catch superfluous brackets or a premature end-of-file
                if( token == "{" ) {
                    statement_bracket_balance++;
                };
                if( token == "}" ) {
                    statement_bracket_balance--;
                };
                if( (token == "{") and (statement_bracket_balance == 1) ) {
                    // Set the index to the actual first character in the block and not the curly bracket
                    struct_unit_start_index = token_index+1;
                };
                // Set Flags
                if( token == "unit" ) {
                    is_unit         = true;
                    struct_is_unit  = "true";
                    struct_name     = tokenized_file_lines.at(token_index+1);
                };
                if( token == "struct" ) {
                    is_unit         = false;
                    struct_is_unit  = "false";
                    struct_name     = tokenized_file_lines.at(token_index+1);
                };
                if( token == "like" ) {
                    has_like        = true;
                    struct_like_name = tokenized_file_lines.at(token_index+1);
                };
                
                // Handle complete unit/struct detection
                if( (statement_bracket_balance == 0) and (token == ";" ) ) {
                    // Assemble the sanitized struct member list here.
                    // Sanitized means, that only the content between the opening { and closing bracket } will be 
                    // assembled.
                    // Also detecting the struct's name, like inheritance (TODO: and package) affiliation will be handled here
                    for (int index = struct_unit_start_index; index <= token_index; ++index) {
                        struct_unit_token_list.append( tokenized_file_lines.at(index) );
//                        qDebug() << "[" << index << "]: " << tokenized_file_lines.at(index);
                    };
                    
                    if( struct_unit_token_list.last().contains("}") ) {
                        struct_unit_token_list.removeLast();
                    };
                    
                    emit struct_declaration(
                        struct_name,
                        QList <QString*> {
                            &struct_name     ,  // contains the struct's name
                            &package_name    ,  // contains the name of the package, but will be empty if no package given
                            &struct_like_name,  // contains the like inheritance name
                            &struct_is_unit  ,  // contains whether the current struct is a unit or a struct type
                            new QString("STATEMENT")
                        }
                    );
                
                    // Parse for struct members in given struct
                    parse_struct_members(struct_name, struct_unit_token_list);
                
                    // Reset values/counters
                    struct_unit_token_list.clear();
                    is_struct_unit  = false;
                    is_unit         = false;
                    has_like        = false;
                };
            };   // end of handling struct statements
            
            // Handling "type"
            if( is_type == true ) {
                // Increment and decrement the bracket balance specifically for the type value declarations
                if( token == "[" ) {
                    statement_bracket_balance++;
                    type_start_index = token_index;
                };
                if( token == "]" ) {
                    statement_bracket_balance--;
                };
                
                // Get the name, which is always at the position after the type keyword
                if( token == "type" ) {
                    type_name = tokenized_file_lines.at(token_index+1);
                };
                
                //TODO: Add logic to handle sized types and named sub-types
                // Enter the type pasing of the values
                if( token == ";" and statement_bracket_balance == 0 ) {
                    for (int index = type_start_index; index < token_index; ++index) {
                        if( not (tokenized_file_lines.at(index) == "[" or tokenized_file_lines.at(index) == "]") ) {
                            type_values += tokenized_file_lines.at(index);
                        };
                        type_token_list.append( tokenized_file_lines.at(index) );
                    };
                    
                    qDebug() << "Type declaration " << type_name << ": [" << type_values << "]";
                    emit type_declaration(
                        type_name,
                        QList <QString*> {
                            &type_values
                        }
                    );
                    
                    // Reset values/counters
                    is_type             = false;
                    type_start_index    = 0;
                    type_token_list.clear();
                    type_name           = tr("");
                    type_values         = tr("");
                };   // end of handling end-of-type declaration
            };   // end of handling "type"
            
            // Detecting "extend"
            if( is_extend == true ) {
                // Option 1: The extend is applied to a struct or unit. In that case, an opening curly bracket is the
                // definite giveaway
                if( token == "{" and not is_extend_type and not is_extend_struct_unit) {
                    is_extend_struct_unit   = true;
                    is_extend_type          = false;
                    struct_name = tokenized_file_lines.at(token_index-1);
                };
                
                // Option 2: The extend is applied to a type. In that case, no curly brackets are in the stream.
                if( token == ":" and not is_extend_struct_unit and not is_extend_type) {
                    is_extend_type          = true;
                    is_extend_struct_unit   = false;
                    
                    // Get the name, which is always at the position after the type keyword
                    type_name = tokenized_file_lines.at(token_index-1);
                };
                
                if( is_extend_type == true ) {
                    // Increment and decrement the bracket balance specifically for the type value declarations
                    if( token == "[" ) {
                        statement_bracket_balance++;
                        type_start_index = token_index;
                    };
                    if( token == "]" ) {
                        statement_bracket_balance--;
                    };
                    
                    //TODO: Add logic to handle sized types and named sub-types
                    // Enter the type pasing of the values
                    if( token == ";" and statement_bracket_balance == 0 ) {
                        for (int index = type_start_index; index < token_index; ++index) {
                            if( not (tokenized_file_lines.at(index) == "[" or tokenized_file_lines.at(index) == "]") ) {
                                type_values += tokenized_file_lines.at(index);
                            };
                            type_token_list.append( tokenized_file_lines.at(index) );
                        };
                        
                        qDebug() << "Extended TYPE declaration " << type_name << ": [" << type_values << "]";
                        emit type_declaration(
                            type_name,
                            QList <QString*> {
                                &type_values
                            }
                        );
                        // Reset the extend type indicator
                        is_extend           = false;
                        is_extend_type      = false;
                        type_start_index    = 0;
                        type_token_list.clear();
                        type_name           = tr("");
                        type_values         = tr("");
                    };
                };   // end of handling extend type 
                
                // The parsing process of the struct/unit extension is similar to the struct/unit declaration process
                if( is_extend_struct_unit ) {
//                    qDebug() << "extend_struct_unit_token --> " << token;
                    // TODO: IMPLEMENT THE EXTENSION DETECTION OF FIELDS IN HERE
                    // The bracket matching is done via a balancing scheme.
                    // Error checking could also be applies here to catch superfluous brackets or a premature end-of-file
                    if( token == "{" ) {
                        if( statement_bracket_balance == 0 ) {
                            struct_name = tokenized_file_lines.at(token_index-1);
                            struct_is_unit = "extend";
                            emit struct_declaration(
                                struct_name,
                                QList <QString*> {
                                    &struct_name,       // contains the struct's name
                                    &package_name,      // contains the name of the package, but will be empty if no package given
                                    &struct_like_name,  // contains the like inheritance name
                                    &struct_is_unit,    // contains whether the current struct is a unit or a struct type
                                    new QString("EXTEND_WHEN")
                                }
                            );
                        };
                        
                        statement_bracket_balance++;
                    };
                    if( token == "}" ) {
                        statement_bracket_balance--;
                    };
                    if( (token == "{") and (statement_bracket_balance == 1) ) {
                        // Set the index to the actual first character in the block and not the curly bracket
                        extend_start_index = token_index+1;
                    };
                    
                    // Handle complete unit/struct detection
                    if( (statement_bracket_balance == 0) and (token == ";" ) ) {
                        for (int index = extend_start_index; index <= token_index; ++index) {
                            extend_token_list.append( tokenized_file_lines.at(index) );
//                            qDebug() << "[" << index << "]: " << tokenized_file_lines.at(index);
                        };
                        
                        if( extend_token_list.last().contains("}") ) {
                            extend_token_list.removeLast();
                        };
                        
                        // Parse for struct members in given struct
                        parse_struct_members(struct_name, extend_token_list);
                        
                        // Reset values/counters
                        is_extend_struct_unit   = false;
                        is_extend               = false;
                    };   // end of handling extensions
                };   // end of processing extend statements
            };   // end of extend processing
            
            // Detecting "sequence"
            if( is_sequence == true and (token_index > 0) ) {
                if( tokenized_file_lines.at( token_index-1 ) == "sequence" ) {
                    // This declares a sequence struct
                    emit struct_declaration(
                        token                       ,
                        QList <QString*> {
                            &token                      , // contains the sequence struct name
                            &package_name               , // contains the name of the package
                            new QString("any_sequence") , // contains the predinfed sequence inheritance name "any_sequence"
                            new QString("false")        , // contains that the sequence is not a unit
                            new QString("STATEMENT")
                        }
                    );
                };   // end of signalling sequence struct declaration
                if( tokenized_file_lines.at( token_index-1) == "created_kind" ) {
                    
                };
                if( tokenized_file_lines.at( token_index-1 ) == "=" ) {
                    if( tokenized_file_lines.at( token_index-2 ) == "created_driver" ) {
                        // This declares the sequence driver unit
                        emit struct_declaration(
                            token                       ,
                            QList <QString*> {
                                &token                              , // contains the sequence struct name
                                &package_name                       , // contains the name of the package
                                new QString("any_sequence_driver")  , // contains the predinfed sequence inheritance name "any_sequence"
                                new QString("true")                 , // contains that the sequence is not a unit
                                new QString("STATEMENT")
                            }
                        );
                    };
                };
                if( token == ";" ) {
                    is_sequence             = false;
                    sequence_start_index    = 0;
                    qDebug() << "TODO: Sequence statement detected, needs additional parser support!";
                };
            };   // end of sequence processing
            
            // Detection of "package"
            if( is_package == true ) {
                
                // Handle end-of-package declaration
                if( token == ";" ) {
                    is_package = false;
                    
                    package_name = tokenized_file_lines.at(token_index-1);
                    qDebug() << "Package detected: " << package_name;
                    // Reset values/counters
                    package_start_index = 0;
                };
            };   // end of package processing
            
            // Always increment the token index
            token_index++;
        };   // end of iterating over each tokenized character
    };   // end of handling file open access
    
//    qDebug() << "List of imported files: " << *imported_files;
//    qDebug() << "SPECMAN_PATH value: " << *file_search_paths;
}   // end of read_file

// This method extracts and tokenizes full string blocks
QStringList spec_file_parser::string_processor(
    QStringList *tokens
) {
    QStringList result;
    
    int     token_index         = 0;
    int     string_start_index  = 0;
    int     string_end_index    = 0;
    bool    is_in_string       = false;
    
    QString         code_string;
    
    for( QString token : *tokens ) {
//        qDebug() << "[" << token_index << "]: " << token;
        
        // Detected the start of 
        if( token == "\"" ) {
//            qDebug() << "\t==> Detected \" character";
            
            if( tokens->at(token_index-1) == "\\" ) {
//                qDebug() << "Escape character detected, not togggling in_string";
                code_string += (token + " ");
            } else {
                is_in_string = not is_in_string;
                
                // Depending whether or not we're just starting the string parsing, we want to track the start and end indices
                if( is_in_string ) {
//                    qDebug() << "Started string...";
                    string_start_index  = token_index;
                    code_string         = "\"";
                } else {
                    string_end_index    = token_index;
                    // Remove the last whitespace character. Without this, the string detection always has one more 
                    // space character at the end.
                    code_string.chop(1);
                    result.append( code_string + "\"" );
//                    qDebug() << "\t ==> Detected character string: " << (code_string + "\"");
                };
            };
        } else {
            // We will keep every single token as is, while we are not within a string
            if( not is_in_string ) {
                result.append( token ); 
            } else {
                code_string += (token + " ");
            };
        };   // end of handling adding tokens
        
        // Increment the token index counter
        ++token_index;
    };   // end of iterating over each token and treating strings as one token
    
//    qDebug() << "CODE_STRING " << code_string;
    
    return result;
}   // end of string_processor()

// This method handles #if[n]def, #undef and #define pre-processor directives
QStringList spec_file_parser::pre_processor(
    QStringList *tokens
) {
    QStringList         result;
    
             int        token_index             = 0;
    
             int        pp_bracket_balance      = 0;
    
    bool                is_pound                = false;
    bool                is_branch_ignored       = false;
    bool                is_enable_else          = false;
    bool                is_capture_token        = true;
    bool                is_capture_next_token   = false;
    
    QStringList         branch_types;
    QStringList         branch_names;
    QList <int>         branch_bracket_indices;
    QList <int>         branch_start_index    ;
    QList <bool>        branch_ignore         ;
    
    
    // Variables needed for parsing define
    bool                is_define               = false;
             int        define_start_index      = 0;
    QString             define_name            ;
    QString             define_value           ;
    
    bool                is_undef                = false;
             int        undef_start_index       = 0;
    QString             *undef_token;
             int        undef_list_name_index   = 0;
    
    bool                is_ifdef                = false;
    bool                is_ifndef               = false;
    bool                is_else                 = false;
    
    
    
    // Iterate over each token in the token string
    for(QString token : *tokens) {
//        qDebug() <<  "Branch is " << (is_branch_ignored?tr(""): tr("not")) << " ignored -> [" << token_index << "]: " << token;
        
        // Ensure that ignored branches will ignore preprocessor directives
        if( branch_ignore.isEmpty() ) {
            is_branch_ignored   = false;
        } else {
            is_branch_ignored   = branch_ignore.last();
        };
        
        // This pound detection is used to identify true pre-procerssing directives
        if( token ==  "#" and not is_branch_ignored ) {
            is_pound                = true;
            is_capture_token        = false;
            is_capture_next_token   = false;
        };
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Handling  #define
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // This will either add a "flag" variable (without a value)
        // or add a "replacement" variable (with value)
        if( is_pound and token == "define" and not is_branch_ignored) {
            is_define           = true;
            define_start_index  = token_index+1;   // remove the define keyword from the actual parsing
            is_pound            = false;
            is_capture_token    = false;
        };
        if( is_define ) {
            if( token == ";" ) {
                // The end of a define is marked by a semi-colon.
                // Since a #define may define more complex code-blocks, a tokenized replacement code will be captured
                // and stored
                for (int index = define_start_index; index < token_index; ++index) {
                    // There are two kinds of #define types.
                    //   1.) Using just a single name -> flag
                    if( index == define_start_index ) {
                        // Add flag/single value
                        define_name = tokens->at(index);
                    } else {
                        //  2.) Using a name and value directive for replacing with blocks
                        // Add replacement/multi value
                        if( index == (define_start_index+1) ) {
                            define_value = tokens->at(index);
                        } else {
                            define_value.append( (" " + tokens->at(index)));
                        };
                    };
                };   // end of for loop
                
                if( define_name.isEmpty() ) {
                    pre_processor_names->append( QString("") );
                } else {
                    pre_processor_names->append( define_name );
                };
                // add the define name/value to the list
                
                if( define_value.isEmpty() ) {
                    pre_processor_values->append( QString("") );
                } else {
                    pre_processor_values->append( define_value );
                };
                
                qDebug() << "\t ==> #define <== " << define_name << "  with replacement value " << define_value;
                
                // Reset values/counters
                is_define               = false;
                define_start_index      = 0;
                define_name.clear();
                define_value.clear();
                is_capture_next_token   = true;
            };   // end of handling "end-of-define"
        };   // end of handling #define
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Handling  #undef
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // This will remove a variable previously defined. Applies to
        // "flag" and "replacement" values
        if( is_pound and token == "undef" and not is_branch_ignored) {
            is_undef            = true;
            undef_start_index   = token_index+1;
            is_pound            = false;
        };
        if( is_undef ) {
            if( token.contains( ";" ) ) {
                // since there is only a name provided, only remove the name from the pre-processor list
                undef_token             = new QString( tokens->at(undef_start_index) );
                undef_list_name_index   = pre_processor_names->indexOf( *undef_token );
                
                // Check that the #define to be removed has been really declared
                if( undef_list_name_index != std::numeric_limits<int>::max() ) {
                    pre_processor_names->removeAt( undef_list_name_index );
                    pre_processor_values->removeAt( undef_list_name_index );
                    
//                    qDebug() << "\t ==> #undef <== " << *undef_token;
                } else {
                    qDebug() << "\t ==> #undef <== " << *undef_token << " ERROR: Was not previously declared.";
                };
                
                // Reset values/counters
                is_undef                = false;
                is_capture_next_token   = true;
            };
        };   // end of handling #undef
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Handling  #ifdef
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // This enables the conditional branch enablement, if the subsequent flag is defined
        if( is_pound and token == "ifdef" and not is_branch_ignored ) {
            is_ifdef            = true;
            is_pound            = false;
            is_enable_else      = false;
        };
        if( is_ifdef ) {
            if( token == "{" ) {
//                qDebug() << "#ifdef stack at {} level " << pp_bracket_balance << " :\n"
//                    << "\t bracket_indices " << branch_bracket_indices << "\n"
//                    << "\t branch_types " << branch_types << "\n"
//                    << "\t branch_names " << branch_names << "\n"
//                    << "\t branch_ignore " << branch_ignore << "\n";
                
                // The 'then' keyword is optional
                if( tokens->at(token_index -1) == "then" ) {
                    branch_names.append( tokens->at(token_index - 2) );
                } else {
                    branch_names.append( tokens->at(token_index - 1) );
                };
                // Keep track of current branch properties
                branch_bracket_indices.append( pp_bracket_balance );
                branch_types.append( "ifdef" );
                branch_start_index.append(token_index+1);
                branch_ignore.append(
                    not pre_processor_names->contains( branch_names.last() )
                );
                
                // Reset values
                is_ifdef                = false;
                is_capture_next_token   = not branch_ignore.last();
                is_pound                = (tokens->at(token_index+1) == "else");
//                qDebug() << "\t ==> #ifdef <== " << tokens->at(token_index - 2) << "  is ignored " << branch_ignore.last();
            };
        };   // end of handling #ifdef directive found
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Handling  #ifndef
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // This enables the conditional branch removal, if teh subsequent flag is defined
        if( is_pound and token == "ifndef" and not is_branch_ignored ) {
            is_ifndef           = true;
            is_pound            = false;
            is_enable_else      = false;
        };
        if( is_ifndef ) {
            if( token == "{" ) {
//                qDebug() << "#ifndef stack at {} level " << pp_bracket_balance << " :\n"
//                    << "\t bracket_indices " << branch_bracket_indices << "\n"
//                    << "\t branch_types " << branch_types << "\n"
//                    << "\t branch_names " << branch_names << "\n"
//                    << "\t branch_ignore " << branch_ignore << "\n";
                
                // The 'then' keyword is optional
                if( tokens->at(token_index -1) == "then" ) {
                    branch_names.append( tokens->at(token_index - 2) );
                } else {
                    branch_names.append( tokens->at(token_index - 1) );
                };
                // Keep track of current branch properties
                branch_bracket_indices.append( pp_bracket_balance );
                branch_types.append( "ifndef" );
                branch_start_index.append(token_index+1);
                branch_ignore.append(
                    pre_processor_names->contains( branch_names.last() )
                );
                
                // Reset values
                is_ifndef               = false;
                is_capture_next_token   = not branch_ignore.last();
                is_pound                = (tokens->at(token_index+1) == "else");
//                qDebug() << "\t ==> #ifndef <== " << tokens->at(token_index - 2) << "  is ignored " << branch_ignore.last();
            };   // end of handling end of current is_ifndef layer
        };   // end of handling #ifndef
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Handling  #else
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if( is_pound and token == "else") {
            is_else             = true;
            is_pound            = false;
        };
        if( is_else ) {
            if( token == "{" ) {
//                qDebug() << "#else stack at {} level " << pp_bracket_balance << " :\n"
//                << "\t bracket_indices " << branch_bracket_indices << "\n"
//                << "\t branch_types " << branch_types << "\n"
//                << "\t branch_names " << branch_names << "\n"
//                << "\t branch_ignore " << branch_ignore << "\n";
                
                // Keep track of current branch properties
                branch_bracket_indices  .append( pp_bracket_balance );
                branch_types            .append( "else" );
                branch_names            .append( "" );
                branch_start_index      .append( token_index+1 );
                branch_ignore           .append( not is_enable_else );
                
                // Reset values
                is_else                 = false;
                is_branch_ignored       = not is_enable_else;
                is_capture_next_token   = true;
                
//                qDebug() << "\t ==> #else <== branch will " << (branch_ignore.last()? tr("not"): tr("")) << " be used ";
            };   // end of handling end of current is_ifdef layer
        };   // end of handling #else
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Handling  End-Of-Branch
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // This main branch will always be entered if there are still pending brackets and a closing curly bracket is
        // detected.
        // This branch handles #ifdef, #ifndef and #else
        
        if( token.contains( "}" ) and not branch_bracket_indices.isEmpty() ) {
//            qDebug()    << "{ --> [" << (pp_bracket_balance-1) << "]<-- } Nesting level" << " \n" 
//                    << "\t LAST bracket index " << branch_bracket_indices.last() << "\n"
//                    << "\t LAST type          " << branch_types.last() << "\n"
//                    << "\t LAST name          " << branch_names.last() << "\n"
//                    << "\t LAST is ignore     " << branch_ignore.last();
            
//            qDebug() << "REMOVE stack at {} level " << pp_bracket_balance << " :\n"
//                    << "\t bracket_indices " << branch_bracket_indices << "\n"
//                    << "\t branch_types " << branch_types << "\n"
//                    << "\t branch_names " << branch_names << "\n"
//                    << "\t branch_ignore " << branch_ignore << "\n";
            
            if( branch_types.last() == "ifdef" or branch_types.last() == "ifndef" ) {
                is_enable_else = branch_ignore.last();
            } else {
                is_enable_else = false;
            };
            
//            qDebug() << "==> next #else branch " << (is_enable_else? tr(""):tr("not")) << " enabled";
            
            // Remove the last element
            branch_bracket_indices.removeLast();
            branch_types.removeLast();
            branch_names.removeLast();
            branch_start_index.removeLast();
            branch_ignore.removeLast();
            is_capture_token        = false;
            is_capture_next_token   = true;
//            qDebug() << "Remaining branches " << branch_bracket_indices.count();
        };
        
        if( is_capture_next_token and not is_branch_ignored and token.contains(QRegExp("\\b")) ) {
            is_capture_token = true;
            is_capture_next_token = false;
        };
        
        if( is_capture_token and not is_branch_ignored) {
//            qDebug() << "\tis_capture_token       " << token;
            result.append(token);
        };
        
        if( is_capture_next_token and not is_branch_ignored ) {
//            qDebug() << "\tis_next_capture_token: " << token;
            is_capture_next_token   = false;
            is_capture_token        = true;
        };
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Handling  Bracket Balance Tracking
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Increment and decrement the curly bracket counters to keep track on the current declaration level of the
        // code block.
        if( token.contains( "{" ) ) {
            pp_bracket_balance++;
        };
        if( token.contains( "}" ) ) {
            pp_bracket_balance--;
        };
        
        // Increment the current token counter
        token_index++;
    };   // end of iterating over each token
//    qDebug() << "PREPROCESSOR: \n" << result;
    
    return result;
}   // end of pre_processor

// Handles detecting "define as" declarations, matching expressions and code replacement
QStringList spec_file_parser::define_as_processor(
    QStringList *tokens
) {
    // Is used to concatenate the string that is part of the source code
    QStringList         result;
    
             int        token_index                 = 0;
    
    // Variables needed to parse "define as" macros
    bool                is_define_as                = false;
             int        define_as_start_index       = 0;
             int        define_as_bracket_balance   = 0;
    QString             define_as_tag           ;
    QString             define_as_syntax_level  ;
    
    // Iterate over each token in the token string
    for(QString token : *tokens) {
        
        // Detecting e complex macro defines.
        // These are also handled within the pre-processor and after text replacement
        // will be part of the source code
        if( token == "<" and tokens->at( token_index-1) == "define" ) {
            is_define_as            = true;
            define_as_start_index   = token_index;
        };
        
        // Ensure that no define as macro code is treated as regular e code
        if( is_define_as == false and token != "define" ) {
            result.append( token );
        };
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // Handling  define <> "" as [ computed] { ... } Macros
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if( is_define_as == true ) {
            if( token == "{" ) {
                define_as_bracket_balance++;
            };
            if( token == "}" ) {
                define_as_bracket_balance--;
            };
            
            if( token == "{" and define_as_bracket_balance == 1 ) {
                // This is the start of the define as declaration
                define_as_start_index = token_index;
            };
            
            if( (define_as_bracket_balance == 0) and (token == "}") ) {
                for( int index = define_as_start_index; index <= token_index; ++index ) {
//                    qDebug() << "  " << tokens->at( index );
                };
                is_define_as            = false;
                define_as_start_index   = 0;
            };   // end of capturing the complete define as replacement block
        };   // end of handling define as macro processing
        
        // Increment the token counter
        token_index++;
    };   // end of iterating over each token in the token-string list
    
    return result;
}   // end of define_as_processor

void spec_file_parser::parse_struct_members(
    QString     struct_name,
    QStringList tokens
) {
//    qDebug() << "spec_file_parser::parse_struct_members(" << struct_name << " , " << tokens << ")";
    // Possible struct_members are:
    //  -> field        / 0
    //  -> method       / 1
    //  -> constraint   / 2
    //  -> event        / 3
    //  -> coverage     / 4
    //  -> on           / 5
    //  -> when         / 6
    
    bool is_field       = false;
    bool is_method      = false;
    bool is_constraint  = false;
    bool is_event       = false;
    bool is_coverage    = false;
    bool is_on          = false;
    bool is_when        = false;
    
             int    token_index                     = 0;
             int    struct_method_bracket_balance   = 0;
    
    
    QStringList     field_token_list;
             int    field_start_index               = 0;
    bool            is_field_const;
    bool            is_field_no_gen;
    bool            is_field_phyiscal;
    
    QStringList     method_token_list;
             int    method_start_index              = 0;
    bool            is_method_end                   = false;
    
    QStringList     constraint_token_list;
             int    constraint_start_index          = 0;
             int    constraint_bracket_balance      = 0;
    
    QStringList     event_token_list;
             int    event_start_index               = 0;
             int    event_bracket_balance           = 0;
    
    QStringList     cover_token_list;
             int    cover_start_index               = 0;
             int    cover_bracket_balance           = 0;
    
    QStringList     on_token_list;
             int    on_start_index                  = 0;
             int    on_bracket_balance              = 0;
    
    QStringList     when_token_list;
             int    when_start_index                = 0;
    QString         when_struct_string              = tr("");
             int    when_bracket_balance            = 0;
    
    // Iterate over each struct member
    for(QString token : tokens) {
//        qDebug()  << "spec_file_parser::parse_struct_members(): " << token_index << " -> " << token;
        if( is_when == false ) {
            // Check if the current token is any of the field indicating keywords
            is_field_const      = (token == "const" ) or (is_field == true);
            is_field_no_gen     = (token == "!"     ) or (is_field == true);
            is_field_phyiscal   = (token == "%"     ) or (is_field == true);
            
            // Only set the start index for the token stream upon encountering the first matching character
            if(
                (is_field_const || is_field_no_gen || is_field_phyiscal)
                                and
                (is_field == false) and (is_constraint == false) and (is_coverage == false)
            ) {
//                qDebug() << "spec_file_parser::parse_struct_members(): " << " setting field to true due to token " << token;
                
                is_field            = true;
                field_start_index   = token_index;
//                qDebug() << "spec_file_parser::parse_struct_members(): " << " field_start_index " << field_start_index;
            };   // detecting special character field declaration
            
            // Struct member constraints can be easily spotted by their distinct declarative keyword "keep"
            if( token == "keep" ) {
                is_constraint           = true;
                constraint_start_index  = token_index;
            };
            
            // Constraint detection
            if( is_constraint == true ) {
                if( token.contains("{") ) {
                    constraint_bracket_balance++;
                };
                if( token.contains("}") ) {
                    constraint_bracket_balance--;
                };
                
                if( token.contains(";") and (constraint_bracket_balance == 0)) {
                    is_constraint = false;
                    
                    for (int index = constraint_start_index; index <= token_index; ++index) {
                        constraint_token_list.append(tokens.at(index));
                    };
                    
                    parse_struct_constraint(struct_name, constraint_token_list);
                    constraint_token_list.clear();
                    constraint_start_index = 0;
                };
            };   // end of detecting constraints
            
            // Events can be easily spotted by their distince declarative keyword "event"
            if( token == "event" ) {
                is_event            = true;
                event_start_index   = token_index;
            };
            
            // Event detection
            if( is_event == true ) {
                if( token.contains("{") ) {
                    event_bracket_balance++;
                };
                if( token.contains("}") ) {
                    event_bracket_balance--;
                };
                
                if( token.contains(";") and event_bracket_balance == 0) {
                    is_event = false;
                    
                    for (int index = event_start_index; index <= token_index; ++index) {
                        event_token_list.append(tokens.at(index));
                    };
                    
                    parse_struct_event(struct_name, event_token_list);
                
                    // Reset fields
                    event_token_list.clear();
                    event_start_index = 0;
                };
            };   // end of detecting events
            
            // Coverage groups are distinctly marked by the keyword "cover"
            if( token == "cover" ) {
                is_coverage = true;
                cover_start_index = token_index;
            };
            
            // Cover detection
            if( is_coverage == true ) {
                if( token.contains("{") ) {
                    cover_bracket_balance++;
                };
                if( token.contains("}") ) {
                    cover_bracket_balance--;
                };
                
                if( token.contains(";") and cover_bracket_balance == 0 ) {
                    is_coverage = false;
                    
                    for (int index = cover_start_index; index <= token_index; ++index) {
                        cover_token_list.append(tokens.at(index));
                    };
                    
                    parse_struct_cover(struct_name, cover_token_list);
                    
                    cover_token_list.clear();
                    cover_start_index = 0;
                };
            };   // end of handling cover
            
            // On-Event Handlers are marked by the single keyword "on"
            if( token == "on" ) {
                is_on           = true;
                on_start_index  = token_index;
            };
            
            // Event Handler "on" detection
            if( is_on == true ) {
                if( token.contains("{") ) {
                    on_bracket_balance++;
                };
                if( token.contains("}") ) {
                    on_bracket_balance--;
                };
                
                // End of on handler detection
                if( token.contains(";") and on_bracket_balance == 0 ) {
                    is_on = false;
                    
                    for (int index = on_start_index; index <= token_index; ++index) {
                        on_token_list.append(tokens.at(index));
                    };
                    
                    // An event handler is essentially a method. To make it easier for the method parsing method,
                    // the on event handler is transformed to look like a method and then passed to the method parser
                    on_token_list.removeFirst();
                    on_token_list.replace(0, QString("on_" + on_token_list.first()));
                    on_token_list.insert(1, QString("is"));
                    on_token_list.insert(1, QString(")"));
                    on_token_list.insert(1, QString("("));
                    
                    // Run method parser
                    parse_struct_method(struct_name, on_token_list);
                    
                    // Reset
                    on_token_list.clear();
                    on_start_index = 0;
                };
            };   // end of handling "on" 
            
            // Subtype "when" detection
            if( token == "when" ) {
                is_when             = true;
                when_start_index    = token_index+1;
            };   // end of detecting when tokens
            
            
            // Field detection
            if(
                (is_method     == false) and
                (is_constraint == false) and (is_event == false) and (is_coverage == false) and (is_on == false)
            ) {
                
                if( token == ":" and is_field == false ) {
//                    qDebug() << "spec_file_parser::parse_struct_members(): " << " token index " << token_index << " previous token " << tokens.at(token_index-1);
                    is_field = true;
                    // Distinguish between list with given length declared in []-brackets and regular field
                    if( tokens.at(token_index -1) == "]" ) {
                        field_start_index = token_index-4;
                    } else {
                        // No square brackets found.
                        field_start_index = token_index-1;
                    };
                };
                
                // Detect the end-of-field declaration
                if( (is_field == true) and (token == ";") ) {
                    
                    for (int index = field_start_index; index <= token_index; ++index) {
                        field_token_list.append(tokens.at(index));
                    };
//                    qDebug() << "spec_file_parser::parse_struct_members(): " << " at token index " << token_index;
//                    qDebug() << "spec_file_parser::parse_struct_members(): " << field_token_list;
                    // The actual field parsing is a separate call
                    parse_struct_field(struct_name, field_token_list);
                    
                    // Reset all the values
                    is_field            = false;
                    field_token_list.clear();
                    field_start_index   = 0;
                };   // end of handling end of field detection
            };   // handling non-method -> field detection
            
            // Method detection
            if(
                is_field       == false and 
                (is_constraint == false) and (is_event == false) and (is_coverage == false) and (is_on == false)
            ) {
                // Check if the current token uses an open-bracket
                // Initialize the "method-detected" stream
                if( token.contains("(") and (is_method == false) ) {
                    is_method = true;
                    method_start_index = token_index - 1;
//                    qDebug() << "spec_file_parser::parse_struct_members(): " << "[" << token_index << "]: MS " << tokens.at(method_start_index);
                };
                
                if( is_method == true ) {
                    if( token.contains("{") ) {
                        struct_method_bracket_balance++;
                    };
                    if( token.contains("}") ) {
                        struct_method_bracket_balance--;
                    };
                    
                    is_method_end = (
                        (token.contains(";") and (struct_method_bracket_balance == 0))
                    );
//                    qDebug() << "spec_file_parser::parse_struct_members(): " << " [" << token_index << "]: {}-balance " << struct_method_bracket_balance << " : " << token;
                    
                    if(  is_method_end == true ) {
                        // This is the end of the method:
                        for (int index = method_start_index; index <= token_index; ++index) {
                            method_token_list.append(tokens.at(index));
                        };
                        
                        // Now parse method for its details
                        parse_struct_method(struct_name, method_token_list);
                        
                        // Reset all method detection
                        is_method           = false;
                        method_start_index  = 0;
                        is_method_end       = false;
                        method_token_list.clear();
                    };
                };
                
            };   // end of handling "no field keyword detected"
        };   // end of checking non-when blocks
        
        // Subtype detection
        if( is_when == true ) {
            if( token == "{" ) {
                // Do not execute this block any more, unless it is the first open bracket of a sub-typed block
                if( when_bracket_balance == 0 ) {
                    // At this point, all the sub-type fields can be parsed
                    for( int index = when_start_index; index < token_index; ++index ) {
                        if( (tokens.at(index) != "\'") and (tokens.at(index-1) != "\'") and (tokens.at(index) != struct_name)) {
                            when_struct_string += (tokens.at(index) + "__");
                        };
                    };   // end of iterating over each sub-type token in when struct-member
                    
                    when_struct_string += struct_name;
                    
                    // Now adjusting the start index token to parse and capture the whle sub-typed block
                    when_start_index = token_index+1;
                    
//                    qDebug() << "spec_file_parser::parse_struct_members(): " << struct_name << " -> subtype tokens " << when_struct_string;
                    
                    emit struct_declaration(
                        when_struct_string,
                        QList <QString*> {
                            &when_struct_string   ,    // contains the struct's name
                            new QString("SUBTYPE"),    // contains the name of the package, but will be empty if no package given
                            new QString("SUBTYPE"),    // contains the like inheritance name
                            new QString("SUBTYPE"),    // contains whether the current struct is a unit or a struct type
                            &when_struct_string
                        }
                    );
                };   // end of only executing the when sub-typing once
                // Increment the code counter
                when_bracket_balance++;
            };   // end of handling opening when-block
            
            if( token == "}" ) {
                when_bracket_balance--;
                // Check if all the sub-type brackets have been closed
                if( when_bracket_balance == 0 ) {
                    // SPLIT away the tokens from the QStringList up to the current token_index
                    // Don't fucking remove tokens from the current token stream ... just build a new stream and pass that one on.
                    QStringList recursive_tokens;
                    for( int i = when_start_index; i < token_index; ++i ) {
                        recursive_tokens.push_back( tokens.at(i) );
                    };
//                    qDebug() << " calling parse_struct_members " << tokens;
                    parse_struct_members(
                        when_struct_string,
                        recursive_tokens
                    );
//                    qDebug() << " Done processing " << when_struct_string;
                    
                    is_when             = false;
                    when_token_list.clear();
                    when_start_index    = 0;
                    when_struct_string  = tr("");
                };   // end of resetting when subtyping detection
            };   // end of handling closing when-block brackets
        };   // end of handling when subtyping
        
        // Increment the token stream index
        token_index++;
    };   // end of iterating over esch token of the struct members token list
}   // end of parse_struct_members

void spec_file_parser::parse_struct_method(
    QString     struct_name,
    QStringList tokens
) {
// ===> START OF INITIAL DEBUG CODE
    QString a_method;
    
    for(QString token : tokens) {
        a_method.append(token);
        a_method.append(" ");
    };
    qDebug() << "\t" << struct_name << " ==> METHOD: \n\t\t" << a_method;
    // <=== END OF DEBUG CODE
    
    
}   // end of parse_method

void spec_file_parser::parse_struct_field(
    QString     struct_name,
    QStringList tokens
) {
    // ===> START OF INITIAL DEBUG CODE
//    QString a_field;
    
//    for(QString token : tokens) {
//        a_field.append(token);
//        a_field.append(" ");
//    };
//    qDebug() << "spec_file_parser::parse_struct_field(): " << "\t" << struct_name << " ==> " << a_field;
    // <=== END OF DEBUG CODE
    
    // Attributes that are being collected on each detected field
    QString         field_name              ;
    QString         field_type              ;
    QString         field_width             ;
    QString         field_port              ;
    QString         field_port_kind         ;
    QString         field_port_data_type    ;
    QString         field_port_direction    ;
    QString         field_no_gen            ;
    QString         field_phyiscal          ;
    QString         field_list_depth        ;
    QString         field_is_instance       = "false";
    
    // Helper variables that are used in parsing
    int             pivot_index             = 0;
    int             field_list_of_counter   = 0;
    
    int             token_index             ;
    
    // Get the index of the first occurring colon character from the left
    pivot_index         = tokens.indexOf(QRegularExpression(":"));
    
    // In case a closing square bracket is found, then the field name
    // position has to be adjusted
    if( tokens.at(pivot_index-1) == "]" ) {
        field_name = tokens.at(pivot_index-4);
    } else {
        field_name = tokens.at(pivot_index-1);
    };
    // Detect if an do-not-generate character was detected before the actual type name
    token_index = tokens.indexOf(QRegularExpression("!"));
    if( (0 <= token_index) and (token_index < (pivot_index-1))  ) {
        field_no_gen = "true";
    } else {
        field_no_gen = "false";
    };
    // Detect if a phyiscal field character was detected before the actual type name
    token_index = tokens.indexOf(QRegularExpression("%"));
    if( (0 <= token_index) and (token_index < (pivot_index-1))  ) {
        field_phyiscal = "true";
    } else {
        field_phyiscal = "false";
    };
    
    // With the pivot element, the field's current position can be tracked and more easily analyzed.
    // The loop determines the additional element characteristics
    for (int index = pivot_index; index < tokens.count(); ++index) {
        if( tokens.at(index-1) == "list" and tokens.at(index) == "of" ) {
               field_list_of_counter++;
               // Move the pivot_index further ahead, because the list identifier has to be before the type or anything
               // else
               pivot_index = index;
        };   // end of detecting "list of"
        
        // Now get the direction, if there is a direction
        if( tokens.at(index) == "in" or tokens.at(index) == "out" or tokens.at(index) == "inout" ) {
            field_port_direction = tokens.at(index);
        };
        
        // Check for any kind of any ports
        if(
            (tokens.at(index-1).contains("_port") and tokens.at(index) == "of")
                                    or
            (tokens.at(index-1).contains("event_port") and tokens.at(index) == "is")
        ) {
            if( field_port_direction.isEmpty() ) {
                field_port_direction = QString("inout");
            };
            field_port = tokens.at(index-1);
            pivot_index = index;
            
//            qDebug() << "spec_file_parser::parse_struct_field(): " << "Detected a " << field_port;
        };
        
        // Check for the instantiation
        if( tokens.at(index-1) == "is" and tokens.at(index) == "instance" ) {
            field_is_instance = "true";
        };
        
        // Perform additional port detection to further classify which port type we are looking at.
        if( not field_port.isEmpty() and index > pivot_index) {
            if( field_port == "interface_port" and tokens.at(index-1) == "of") {
                if( field_port_kind.isEmpty() ) {
                    field_port_kind = tokens.at(index);
                } else {
                    // Add the data type
                    field_port_data_type = tokens.at(index);
                };
                
                pivot_index = index;
            };   // end of handling interface_port
            
            if( field_port == "simple_port" and tokens.at(index-1) == "of" ) {
                field_port_data_type = tokens.at(index);
            };   // end of handling simple_port
            
            if( field_port == "event_port" ) {
                // An event port does not have any kind of type, hence all we could do here is to
                // get the "is instance" token matching... but that is already done earlier :)
                
            };   // end of handling event_port
        } else {   // handle the non-port fields to detect their field_type and field_width
            // The field type is either declared after ':' or 'list of', which are tracked by the pivot_index
            if( index > pivot_index ) {
                // Check if the field_type is empty and only then set the field_type
                if( field_type.isEmpty() and tokens.at(index) != "list" and tokens.at(index) != "of" ) {
                    field_type = tokens.at(index);
                    pivot_index = index;
                };
                
                if( tokens.at(index-1) == ":" ) {
                    field_width = tokens.at( index );
                };
            };
        };   // end of handling non-port fields
    };   // end of parsing tokens after pivot element
      
    
//    qDebug() << "spec_file_parser::parse_struct_field(): " << "':' found at " << pivot_index;
    
//    qDebug() << "spec_file_parser::parse_struct_field(): " << "Found field: " << field_name << "";
    QString prop_str;
    
    if( QString::compare(field_no_gen, "true") == 0 ) {
        prop_str += tr("Do Not Generate  ");
    } else {
        prop_str += tr("Generatable  ");
    };
    
    if( QString::compare(field_phyiscal, "true") == 0 ) {
        prop_str += tr("Physical  ");
    } else {
        prop_str += tr("Virtual  ");
    };
    
    if( field_port.isEmpty() ) {
        prop_str += "Type " + field_type + "  ";
        if( not field_width.isEmpty() ) {
            prop_str += "Width " + field_width + "  ";
        };
    };
    
    field_list_depth = QString::number(field_list_of_counter);
    
    if( field_list_of_counter != 0 ) {
        prop_str += "" + field_list_depth + "-dimensional list of ";
    };
    if( QString::compare(field_is_instance, "true") == 0 ) {
      prop_str += tr("Field is instantiated  ");
    };
    
    qDebug() << "spec_file_parser::parse_struct_field(): " << "FIELD --> " + field_name + " <-- :" << prop_str;
    emit field_declaration(
        struct_name,
        field_name,
        QList <QString*> {
            &field_no_gen,
            &field_phyiscal,
            &field_list_depth,
            &field_type,
            &field_width,
            &field_is_instance,
            &field_port,
            &field_port_kind,
            &field_port_data_type,
            &field_port_direction
        }
    );
    
}   // end of parse_field

void spec_file_parser::parse_constraint_expression(
    QString     struct_name,
    QString     constraint_name,
    QStringList tokens
) {
    // ===> START OF INITIAL DEBUG CODE
    QString a_constraint;
    
    for(QString token : tokens) {
        a_constraint.append(token);
        a_constraint.append(" ");
    };
    spec_msg( "" );
    qDebug() << "=====================================================================================================";
    qDebug() << "\t" << struct_name << " ==> CONSTRAINT: " << a_constraint;
    qDebug() << "=====================================================================================================";
    // <=== END OF DEBUG CODE
    
    bool    is_soft_expr            = false;
    bool    is_compound_expr        = false;
    bool    is_soft_select_expr     = false;
    bool    is_for_each_expr        = false;
    bool    is_implication_expr     = false;
    bool    is_read_only_expr       = false;
    int     op_index                = 0;
    int     token_index             = 0;
    int     new_token_start_index   = 0;
    int     token_count             = 0;
    int     curly_bracket_counter   = 0;
    int     round_bracket_counter   = 0;
    int     select_bracket_counter  = 0;
    bool    is_assemble_expression  = false;
    bool    is_balanced_brackets    = false;
    bool    is_boolean_operator     = false;
    bool    is_end_of_expression    = false;
    bool    is_descend_expression   = true;
    bool    do_remove_last_token    = false;
    QString expression0             = "";
    QString expression1             = "";
    QString op                      = "";
    
    QStringList ex                  ;
    QList<QString*> cons_list       ;
    
    if( tokens.count() <= 1 ) {
//        if( tokens.count() == 0 ) {
//            qDebug() << "SINGLE Expression/Field found " << tokens.at(0);
//        } else {
//            qDebug() << "EMPTY Expression detected";
//        };
        return;
    };
    
    // The assumption is that we are already in a boolean expression.
    for( QString token : tokens ) {
//        qDebug() << "CONS_TOKEN " << token;
        if( is_assemble_expression ) {
            ex.append( token );
        };
        // Grouping
        if( token == "(" ) {
            round_bracket_counter++;
        };
        
        if( token == ")" ) {
            round_bracket_counter--;
        };
        
        // TODO: Add handling of keep soft <NAME> == select { ... };
        // We need to:
        //   1.) ignore the special handling while we are assembling constraints...
        //   2.) and we need to handle this language construct
        if( not is_assemble_expression and (token == "soft") ) {
            // Check if this keyword is accompanied by a "select"
            // We are only able to enter a proper "soft ... select" constraints if we have at least two more tokens.
            if( token_index < tokens.count() - 4 ) {
                if( (tokens.at(token_index+2) == "==") and (tokens.at(token_index+3) == "select") ) {
                    qDebug() << "Detected soft ... select constraint";
                    cons_list.clear();
                    is_soft_select_expr = true;
                    // We are pointing to the first token that describes the field of the soft...select expression
                    op_index = token_index+1;
                };
            };
        };   // end of handling soft select detection while not assembling a compound constraint
        
        // Using a soft-select expression has a different governing syntax and sematic rule-sets as other constraints
        if( is_soft_select_expr ) {
            if( token == "==" ) {
                // Flag the soft select constraint and mark it as "field" or "EXpression"
                cons_list.append( new QString("soft_select") );
                cons_list.append( new QString("EX"));
                
                // Build the expression to also support hierarchical soft selects
                for( int expr_index = op_index; expr_index < token_index ; ++expr_index ) {
//                    ex.append( tokens.at(expr_index) );
                    cons_list.append( new QString( tokens.at(expr_index) ) );
                };
                qDebug() << "        SINGLE soft ... select Expression: " << token << "   " << cons_list;
                
                emit constraint_declaration(
                    new QString( struct_name ),
                    new QString( constraint_name ),
                    cons_list
                );
                
                // Clear the expression string
                cons_list.clear();
                
                // and move to the next item
                token_index++;
                continue;
            };
            
            // Detecting the opening bracket, which is always accompanied by a previous select
            if( token == "{" ) {
                // Adjust the op_index tracking index to point to the first token of the distribution model
                if( select_bracket_counter == 0 ) {
                    op_index = token_index + 1;
                };
                select_bracket_counter++;
            };
            
            // Detecting the end of the distribution expression is done by adjustint the select bracket counter
            // and also checking if the closing bracket is closing the distribution model
            if( token == "}" ) {
                select_bracket_counter--;
                if( select_bracket_counter == 0 ) {
                    for( int expr_index = op_index; expr_index < token_index; ++expr_index ) {
                        cons_list.append( new QString(tokens.at(expr_index)) );
                    };
                    qDebug() << "        SINGLE soft ... select Expression DISTRIBUTION: " << token << "   " << cons_list;
                    
                    emit constraint_declaration(
                        new QString( struct_name ),
                        new QString( constraint_name ),
                        cons_list
                    );
                    
                    // Clear the list
                    cons_list.clear();
                    
                    // Ensure that the next semi-colon will be removed
                    do_remove_last_token = true;
                };
            };
            
            // Clean-up the soft...select detection
            if( do_remove_last_token and token == ";" ) {
                do_remove_last_token = false;
                is_soft_select_expr  = false;
                token_index++;
                continue;
            };
            
            // Skip the execution of the rest of this loop, since we are gathering only soft ... select expression
            token_index++;
            continue;
        };   // end of soft ... select with distribution model parsing
        
        // Detect the for each constraint language construct
        if( token == "each" and tokens.at(token_index-1) == "for" ) {
            is_for_each_expr = true;
        };
        
        // Within boolean expressions there may be curly brackets for grouping a list of expressions.
        // Since these can be nested, we will have to descent into each expression separately
        if( token == "{" ) {
            if( curly_bracket_counter == 0 ) {
                // Ensure that we are operating on an empty list
                ex.clear();
                is_assemble_expression = true;
            };
            curly_bracket_counter++;
        };
        
        if( do_remove_last_token and token == ";" ) {
            ex.takeLast();
            do_remove_last_token = false;
        };
        
        if( token == "}" ) {
            curly_bracket_counter--;
            if( curly_bracket_counter == 0 ) {
                ex.takeLast();
                do_remove_last_token = true;
            };
        };
        
//        qDebug() << "                                                                  Token " << token;
//        qDebug() << "                                                                  Ex    " << ex;
        
        is_boolean_operator     = expr_operators.contains( token ) and not is_for_each_expr;
        is_end_of_expression    = (token == ";" or is_boolean_operator );
        is_balanced_brackets    = (round_bracket_counter == 0 and curly_bracket_counter == 0);
        
        // We can only analyze the expressions, if the brackets are balanced.
        // If we don't ensure bracket balancing, then we will not be able to reliably detect single expressions
        if( is_balanced_brackets ) {
//            qDebug() << "Entering BALANCED_BRACKETS " << token;
            if( is_end_of_expression ) {
//                qDebug() << "    Entering END_OF_EXPRESSION " << token;
                if( is_assemble_expression ) {
//                    qDebug() << "        ASSEMBLED Expression: " << token << "   " << ex;
                    is_descend_expression = true;
                } else {
                    // Now get all the tokens up until the operator has been detected
                    for(int expr_index = op_index; expr_index < token_index; ++expr_index ) {
                        ex.append( tokens.at(expr_index) );
                        // Remove the last semi-colon in an expression
                        if( (expr_index == token_index-1) and (tokens.at(expr_index) == ";") ) {
                            ex.takeLast();
                        };
                    };
//                    qDebug() << "        SINGLE Expression: " << token << "   " << ex;
//                    qDebug() << "        SINGLE Expression: " << ex;
                    
                    if( token == ";" ) {
                        cons_list.append( new QString( "END_EX" ) );
                    } else {
                        cons_list.append( new QString( "" ) );
                    };
                    cons_list.append( new QString( "EX") );
                    for( QString cstr : ex ) {
                        cons_list.append( new QString( cstr ) );
                    };
                    spec_msg( "TODO: The error is being thrown at this emit!!!" );
                    emit constraint_declaration(
                        new QString( struct_name ),
                        new QString( constraint_name ),
                        cons_list
                    );
                    
                    if( ex.count() == 1 ) {
                        is_descend_expression = false;
                    };
                };   // end of differentiating between assembled and single expressions
                
                if( is_descend_expression ) {
                    // Call this method recursively to traversw through the tree
                    parse_constraint_expression( struct_name, constraint_name, ex );
                };
                
                if( is_boolean_operator ) {
//                    qDebug() << "    OPERATOR: " << token;
                    // Remove the previously collected transformation string
                    cons_list.clear();
                    
                    cons_list.append( new QString( "" ) );
                    cons_list.append( new QString( "OP" ) );
                    cons_list.append( new QString( token ) );
                    
                    emit constraint_declaration(
                        new QString( struct_name ),
                        new QString( constraint_name ),
                        cons_list
                    );
                    
                };   // end of handling of detected boolean operators
                
                // Set the next expression start index only if there are more tokens in the current token list.
                if( op_index < tokens.count()-1 ) {
                    op_index = token_index + 1;
                };
                
                // Before using the expression, ensure that the current list is not creeping into the next list
                ex.clear();
                cons_list.clear();
            } else {
//                qDebug() << "Entering EXPRESSION_NOT_DONE " << token << "   " << ex;
                
            };
        };   // end of handling balanced brackets
        
        // Increment the token index counter
        token_index++;
    };   // end of iterating over each given token
}

void spec_file_parser::parse_struct_constraint(
    QString     struct_name,
    QStringList tokens
) {
    // ===> START OF INITIAL DEBUG CODE
    QString a_constraint;
    
//    for(QString token : tokens) {
//        a_constraint.append(token);
//        a_constraint.append(" ");
//    };
//    qDebug() << "\t" << struct_name << " ==> CONSTRAINT: " << a_constraint;
    // <=== END OF DEBUG CODE
    
    // First we'll want to track if the detected constraint is a named constraint.
    bool    is_named_constraint         = false;
    bool    is_named_only_constraint    = false;
    int     take_number_of_elements     = 0;
    
    QString constraint_name             = "";
    
    // First determine if we are looking at a named constraint
    is_named_constraint = (tokens.at(2) == "is");
    // ... and then check if we are possibly looking at an overwritten "is only" constraint
    if( is_named_constraint == true ) {
        is_named_only_constraint = (tokens.at(3) == "only");
        // Since we are here already, we could just extract the name of the named constraint
        constraint_name = tokens.at(1);
        
        // Now we know if we can check for soft constraint
        if( is_named_only_constraint == true ) {
            take_number_of_elements = 4;
        } else {
            take_number_of_elements = 3;
        };
    } else {
        take_number_of_elements = 1;
    };
    
    // Remove constraint keywords and names. these are now signalled via emitted events
    for( int i = 0; i < take_number_of_elements; i++ ) {
//        qDebug() << "Removing token " +
        tokens.takeFirst();
    };
    
    // The actual constraints parsing is done via the parse_constraint_expression traversal method.
    // This method supports recursive descension for nested constraints.
    parse_constraint_expression(
        struct_name,
        constraint_name,
        tokens
    );
};   // end of parse_struct_constraint

void spec_file_parser::parse_struct_event(
    QString     struct_name,
    QStringList tokens
) {
    // ===> START OF INITIAL DEBUG CODE
    QString an_event;
    
    for(QString token : tokens) {
        an_event.append(token);
        an_event.append(" ");
    };
    qDebug() << "\t" << struct_name << " ==> EVENT: " << an_event;
    // <=== END OF DEBUG CODE
    
    
};   // end of parse_struct_constraint

void spec_file_parser::parse_struct_cover(
    QString     struct_name,
    QStringList tokens
) {
    // ===> START OF INITIAL DEBUG CODE
    QString a_cover;
    
    for(QString token : tokens) {
        a_cover.append(token);
        a_cover.append(" ");
    };
    qDebug() << "\t" << struct_name << " ==> COVER: " << a_cover;
    // <=== END OF DEBUG CODE
    
    
};   // end of parse_struct_constraint

// Use to remove comments from QString line
QString spec_file_parser::remove_comment(QString line) {
    int cmt_index   = -1;       // initialize to "no index found"
    
    // First check if the VHDL style comment was found in the line
    cmt_index = line.indexOf("--");
    if( cmt_index == -1 ) {
        // Then check if the Verilog style comment was found in the line
        cmt_index = line.indexOf("//");
    };
    
    // Then remove the comment from the current line
    if( cmt_index != -1 ) {
        line.truncate( cmt_index );
    }
    return line;
};   // end of remove_comment

// Use to remove delimiter that may or may not be "stuck" to a token
QString spec_file_parser::remove_delimiter(QString token) {
    // Check if the end-of-line character was detected
    if( token.indexOf(";") > 0 ) {
        // and remove it from the file name
        token.truncate( token.indexOf(";") );
    };
    return token;
};   // end of remove_delimiter

QString spec_file_parser::remove_character(
        QString character,
        QString token
) {
    if( token.indexOf( character ) >= 0 ) {
        token.remove( character );
    };
    return token;
};   // end of remove_character

void spec_file_parser::add_specman_path(
        QString path
        ) {
    file_search_paths->append( path );
};   // end of add_specman_path

void spec_file_parser::remove_specman_path(
        unsigned int index
        ) {
    file_search_paths->removeAt( index );
};   // end of add_specman_path


void spec_file_parser::read_sv_interface() {
    qDebug() << "This is where we'd be running ANTLR code... if Daniel knew how to integrate it :)";
    
    
};
