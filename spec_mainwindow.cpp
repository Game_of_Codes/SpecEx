// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_mainwindow.h"


spec_main_window::spec_main_window(QWidget *parent) : QMainWindow(parent), ui(new Ui::spec_MainWindow) {
    ui -> setupUi( this );
    
    // All the MAIN GUI elements will be instantiated and configured in this method
    init_gui_elements();
}

Ui::spec_MainWindow* spec_main_window::get_ui_pointer() {
    return ui;
}

spec_main_window::~spec_main_window() {
    delete ui;
}

void spec_main_window::mousePressEvent(QMouseEvent *event) {
    _initial = event -> pos();
    _current = event -> pos();
    
    // Get the initial values of the tree and table view
//    _initTreeViewX       = ui -> hierarchyTreeScrollArea -> x();
//    _initTreeViewY       = ui -> hierarchyTreeScrollArea -> y();
//    _initTreeViewWidth   = ui -> hierarchyTreeScrollArea -> width();
//    _initTreeViewHeight  = ui -> hierarchyTreeScrollArea -> height();
    
//    _initTableViewX      = ui -> hierarchyTableScrollArea -> x();
//    _initTableViewY      = ui -> hierarchyTableScrollArea -> y();
//    _initTableViewWidth  = ui -> hierarchyTableScrollArea -> width();
//    _initTableViewHeight = ui -> hierarchyTableScrollArea -> height();
    
//    int curX = _current.x();
//    int curY = _current.y();
    
//    int x0 = _initTableViewX + 0;
//    int x1 = _initTableViewX + 15;
    
//    int y0 = _initTreeViewY;
//    int y1 = _initTreeViewY + _initTreeViewHeight;
    
////    qDebug() << "Current X == " << curX << "  x0 " << x0 << "  x1 " << x1;
////    qDebug() << "Current Y == " << curY << "  y0 " << y0 << "  y1 " << y1;
    
                
//    bool isAtDragLine  = (
//        (curX >= x0) && (curX <  x1) &&
//        (curY >= y0) && (curY <= y1)
//    );
    
//    if( !isAtDragLine ) {
//        _initial = QPoint( -1, -1 );
//    };
    
//    qDebug() << "START: _INITIAL value(" << _initial.x() << "/" << _initial.y() << ")";
//    qDebug() << "START: _CURRENT value(" << curX << "/" << curY << ")";
    
//    qDebug() << "TreeWidget  x: " << _initTreeViewX  << " y: " << _initTreeViewY << "width: "<< _initTreeViewWidth << " height:  " << _initTreeViewHeight;
//    qDebug() << "TableWidget x: " << _initTableViewX << " y: " << _initTableViewY << "width: "<< _initTableViewWidth << " height:  " << _initTableViewHeight;
};

void spec_main_window::mouseMoveEvent(QMouseEvent *event) {
    // Check if the current mouseMoveEvent is NOT a resize operation. Resize operations start with x == y == 0
    if( _initial.x() != 0 && _initial.y() != 0 ) {
        _current = event->pos();
        
        qDebug() << "TRANSITION: FROM (" << _initial.x() << "/" << _initial.y() << ") --->  _CURRENT value(" << _current.x() << "/" << _current.y() << ")";
        
        updateWidgetGeometry();
    };
};

void spec_main_window::mouseReleaseEvent(QMouseEvent *event) {
    _current = QPoint( -1, -1 );
    _initial = QPoint( -1, -1 );
};

void spec_main_window::updateWidgetGeometry() {
    if( _initial == QPoint( -1, -1 ) ) {
        return;
    };
    
    int treeViewWidth;
    
    int tableViewX;
    int tableViewWidth;
    
    if( _initial.x() > _current.x() ) {
        treeViewWidth  = _initTreeViewWidth - ( _initial.x() - _current.x() );
        
        tableViewWidth = _initTableViewWidth + ( _initial.x() - _current.x() );
        tableViewX     = _initTableViewX - ( _initial.x() - _current.x() );
    } else {
        treeViewWidth  = _initTreeViewWidth + ( _current.x() - _initial.x() );
        
        tableViewWidth = _initTableViewWidth - ( _current.x() - _initial.x() );
        tableViewX     =  _initTableViewX    + ( _current.x() - _initial.x() );
    };
    
//    qDebug() << "TreeView  rectangle x: " << _initTreeViewX << " y: " << _initTreeViewY  << " width: " << treeViewWidth  << " height: " << _initTreeViewHeight;
//    qDebug() << "TableView rectangle x: " << tableViewX     << " y: " << _initTableViewY << " width: " << tableViewWidth << " height: " << _initTableViewHeight;
    
//    ui -> hierarchyTreeScrollArea  -> setGeometry( _initTreeViewX, _initTreeViewY , treeViewWidth , _initTreeViewHeight  );
//    hierarchy_tree_editor            -> setGeometry( 0             , 0              , treeViewWidth , _initTreeViewHeight  );
//    ui -> hierarchyTableScrollArea -> setGeometry( tableViewX    , _initTableViewY, tableViewWidth, _initTableViewHeight );
//    hierarchy_table_editor           -> setGeometry( 0             , 0              , tableViewWidth, _initTableViewHeight );
    
    qDebug() << "UPDATE WIDGET main window size " << size();
    qDebug() << "UPDATE WIDGET tabWidget size " << ui->tabWidget->size();
    
}

void spec_main_window::resizeEvent(QResizeEvent *event) {
    
    int oldWidth  = event -> oldSize().width();
    int oldHeight = event -> oldSize().height();
    
    int newWidth  = event -> size().width();
    int newHeight = event -> size().height();
    
    if( (oldWidth == -1) && (oldHeight == -1) ) {
        return;
    };
    
    int deltaWidth  = newWidth - oldWidth;
    int deltaHeight = newHeight - oldHeight;
    qDebug() << "Adjusting element widths by " << deltaWidth << " and heights by " << deltaHeight;
    
    // Calculate each element with same set of x, y, width and height variables sequentially
    int wX      = ui->tabWidget->x();
    int wY      = ui->tabWidget->y();
    int wWidth  = ui->tabWidget->width();
    int wHeight = ui->tabWidget->height();
    
//    qDebug() << "=======================================================================================================";
//    qDebug() << "tabWidget: x() " << wX << "  y() " << wY << "  width() " << wWidth << "  wHeight " << wHeight;
    wWidth += deltaWidth;
    wHeight += deltaHeight;
    ui->tabWidget->setGeometry( wX, wY, wWidth, wHeight );
//    qDebug() << "NEW tabWidget: x() " << wX << "  y() " << wY << "  width() " << wWidth << "  wHeight " << wHeight;
    
    
//    wX      = hierarchy_tree_editor->x();
//    wY      = hierarchy_tree_editor->y();
//    wWidth  = hierarchy_tree_editor->width();
//    wHeight = hierarchy_tree_editor->height();
////    qDebug() << "=======================================================================================================";
//    qDebug() << "hierarchyTreeEditor: x() " << wX << "  y() " << wY << "  width() " << wWidth << "  wHeight " << wHeight;
    wWidth += (deltaWidth/2);
    wHeight += deltaHeight;
//    hierarchy_tree_editor->setGeometry( wX, wY, wWidth, wHeight );
//    qDebug() << "NEW hierarchyTreeEditor: x() " << wX << "  y() " << wY << "  width() " << wWidth << "  wHeight " << wHeight;
    
    
//    wX      = ui->hierarchyTreeScrollArea->x();
//    wY      = ui->hierarchyTreeScrollArea->y();
//    wWidth  = ui->hierarchyTreeScrollArea->width();
//    wHeight = ui->hierarchyTreeScrollArea->height();
////    qDebug() << "=======================================================================================================";
////    qDebug() << "hierarchyTreeScrollArea: x() " << wX << "  y() " << wY << "  width() " << wWidth << "  wHeight " << wHeight;
//    wWidth += (deltaWidth/2);
//    wHeight += deltaHeight;
//    ui->hierarchyTreeScrollArea->setGeometry( wX, wY, wWidth, wHeight );
////    qDebug() << "NEW hierarchyTreeScrollArea: x() " << wX << "  y() " << wY << "  width() " << wWidth << "  wHeight " << wHeight;
    
    
//    wX      = ui->hierarchyTableScrollArea->x();
//    wY      = ui->hierarchyTableScrollArea->y();
//    wWidth  = ui->hierarchyTableScrollArea->width();
//    wHeight = ui->hierarchyTableScrollArea->height();
////    qDebug() << "=======================================================================================================";
////    qDebug() << "hierarchyTableScrollArea: x() " << wX << "  y() " << wY << "  width() " << wWidth << "  wHeight " << wHeight;
//    wX      += (deltaWidth/2);
//    wWidth  += deltaWidth;
//    wHeight += deltaHeight;
//    ui->hierarchyTableScrollArea->setGeometry( wX, wY, wWidth, wHeight );
//    qDebug() << "NEW hierarchyTableScrollArea: x() " << wX << "  y() " << wY << "  width() " << wWidth << "  wHeight " << wHeight;
//    qDebug() << "=======================================================================================================";
    
//    wX      = hierarchy_table_editor->x();
//    wY      = hierarchy_table_editor->y();
//    wWidth  = hierarchy_table_editor->width();
//    wHeight = hierarchy_table_editor->height();
//    qDebug() << "=======================================================================================================";
//    qDebug() << "hierarchyTableEditor: x() " << wX << "  y() " << wY << "  width() " << wWidth << "  wHeight " << wHeight;
    wWidth += (deltaWidth/2);
    wHeight += deltaHeight;
//    hierarchy_table_editor->setGeometry( wX, wY, wWidth, wHeight );
//    qDebug() << "NEW hierarchyTableEditor: x() " << wX << "  y() " << wY << "  width() " << wWidth << "  wHeight " << wHeight;
    qDebug() << "RESIZE main window size " << size();
    qDebug() << "RESIZE tabWidget size " << ui->tabWidget->size();
    
    // TODO:
    // Adjust all the widgets to reflect the new size at the end of resize
    
    // The previous/old MainWindow size is stored in the event
    // event -> oldSize()
    
    // The current MainWindow (this) size is stored in the event:
    // event -> size()
    
    /*******************************************************************************************************************
     * GUI Element Name                         x       y       width       height
     * =================================================================================================================
     * centralWidget                            -       -         -           -
     * tabWidget                                -       -         y           y
     * waveformEditor                           -       -         y           y
     * hierarchyEditor                          -       -         y           y
     * hierarchyTreeScrollArea                  -       -         y           y
     * hierarchyTreeScrollAreaWidgetContents    -       -         y           y
     * hierarchyTableScrollArea                 y       y         y           y
     * hierarchyTableScrollAreaWidgetContents   y       y         y           y
     * 
     ******************************************************************************************************************/
    
    
    //==================================================================================================================
    //== Adjusting the  --> centralWidget <--
    // The current centralWidget size ...
    // ui -> centralWidget -> size();       // QSize
    
    // ... and geometry
    // ui -> centralWidget -> geometry();   // QRect
    
    
    //==================================================================================================================
    //== Adjusting the  -->  <--
    
    
    
    // If old window size is -1 x -1, then ignore the rest of the method, because it's the initial creation of the object
    // bool skipResizeAdjustments = (event->oldSize().width() == -1 && event->oldSize().height() == -1);
    
    // The maximize button will adjust/resize the window once
}

void spec_main_window::show_message_about() {
    QMessageBox *msg_box = new QMessageBox();
    
    msg_box -> setIconPixmap( QPixmap(":images/spec_ex_icon.ico") );
    msg_box -> setInformativeText(
        ("Created by Daniel Bayer\n"
        "For more information please visit:\n"
        "https://gitlab.com/Game_of_Codes/SpecEx")
    );
    msg_box -> setTextFormat( Qt::RichText );
    msg_box->show();
}

void spec_main_window::init_gui_elements() {
    // Create and construct the interface wizard
    if_wizard = new spec_if_gen();
    if_wizard -> p_ui = ui;
    if_wizard -> initialize();
    
    // Create and construct the module wizard
    mod_wizard = new spec_mod_gen();
    mod_wizard -> p_ui = ui;
    mod_wizard -> initialize();
    
    // Create and construct the module browser
    e_browser = new spec_e_browser();
    e_browser -> p_ui = ui;
    e_browser -> init_gui_elements();
    
    connect(
        ui -> main_action_about , &QAction::triggered,
        this                    , &spec_main_window::show_message_about
    );
}   // end of instantiate_gui_elements
