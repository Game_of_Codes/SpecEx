#-------------------------------------------------
#
# Project created by QtCreator 2017-12-22T16:23:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SpecEx
TEMPLATE = app
CONFIG += debug_and_release static

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
QMAKE_CXXFLAGS += -std=c++11

SOURCES += \
    BaseClasses/spec_logger.cpp \
    BaseClasses/spec_tablewidget.cpp \
    BaseClasses/spec_treewidget.cpp \
    Widgets/spec_cell_base_widget.cpp \
    Widgets/spec_cell_check_box_widget.cpp \
    Widgets/spec_cell_combo_box_widget.cpp \
    Widgets/spec_cell_icon_widget.cpp \
    Widgets/spec_cell_line_combo_box_widget.cpp \
    Widgets/spec_cell_line_edit_widget.cpp \
    Widgets/spec_cell_spin_box_widget.cpp \
    Widgets/spec_if_config_table.cpp \
    Widgets/spec_if_signal_map_table.cpp \
    Widgets/spec_interface_wave_cell_widget.cpp \
    Widgets/spec_interface_wave_table.cpp \
    \
    CodeModel/spec_methodnode.cpp \
    CodeModel/spec_methodargumentnode.cpp \
    CodeModel/spec_coveragegroupnode.cpp \
    CodeModel/spec_coverageitemnode.cpp \
    CodeModel/spec_coverageitemoptionnode.cpp \
    CodeModel/spec_constraintnode.cpp \
    CodeModel/spec_temporalnode.cpp \
    CodeModel/spec_methodactionnode.cpp \
    \
    \
    \
        main.cpp \
        spec_mainwindow.cpp \
    spec_imagecache.cpp \
    spec_interfaceverificationcomponenttemplate.cpp \
    testwindow.cpp \
    TemplateStorage/spec_interfacewizardtemplate.cpp \
    TemplateStorage/spec_datacontainer.cpp \
    Examples/examplesignalinterface.cpp \
    CodeModel/spec_interface_model.cpp \
    FileParser/spec_file_parser.cpp \
    ParseModel/spec_parse_module.cpp \
    CodeModel/spec_struct_data_node.cpp \
    CodeModel/spec_field_node.cpp \
    CodeModel/spec_code_model.cpp \
    CodeModel/spec_type_node.cpp \
    CodeGenerator/spec_e_code_generator.cpp \
    CodeModel/spec_module_model.cpp \
    CodeGenerator/spec_e_interface_code_generator.cpp \
    CodeGenerator/spec_e_module_code_generator.cpp \
    CodeGenerator/spec_e_clock_uvc_generator.cpp \
    CodeGenerator/spec_e_reset_uvc_generator.cpp \
    CodeModel/spec_clock_model.cpp \
    CodeModel/spec_reset_model.cpp \
    \
    \
    \
    CodeModel/spec_base_code_model.cpp \
    ModuleWizard/spec_mod_gen.cpp \
    InterfaceWizard/spec_if_gen.cpp \ 
    ModuleWizard/spec_mod_lib_table_widget.cpp \
    ModuleWizard/spec_mod_lib_tree_widget.cpp \
    ModuleWizard/spec_mod_clock_configuration_dialog.cpp \
    ModuleWizard/spec_mod_reset_configuration_dialog.cpp \
    CodeBrowser/spec_code_browser.cpp \
    EBrowser/spec_e_browser.cpp \
    CommonDialogs/spec_process_viewer.cpp \
    Widgets/spec_combo_box.cpp \
    Widgets/spec_env_widget.cpp \
    Widgets/spec_agent_widget.cpp \
    Widgets/spec_event_port_widget.cpp \
    Widgets/spec_simple_port_widget.cpp \
    Widgets/spec_tlm_port_widget.cpp \
    Widgets/spec_enum_types_widget.cpp \
    ModuleWizard/spec_mod_uvc_configuration_dialog.cpp \
    Widgets/spec_config_params_widget.cpp
    
    
HEADERS += \
    BaseClasses/spec_logger.h \
    BaseClasses/spec_tablewidget.h \
    BaseClasses/spec_treewidget.h \
    Widgets/spec_cell_base_widget.h \
    Widgets/spec_cell_check_box_widget.h \
    Widgets/spec_cell_combo_box_widget.h \
    Widgets/spec_cell_icon_widget.h \
    Widgets/spec_cell_line_combo_box_widget.h \
    Widgets/spec_cell_line_edit_widget.h \
    Widgets/spec_cell_spin_box_widget.h \
    Widgets/spec_if_config_table.h \
    Widgets/spec_if_signal_map_table.h \
    Widgets/spec_interface_wave_cell_widget.h \
    Widgets/spec_interface_wave_table.h \
    \
    CodeModel/spec_methodnode.h \
    CodeModel/spec_methodargumentnode.h \
    CodeModel/spec_temporalnode.h \
    CodeModel/spec_coveragegroupnode.h \
    CodeModel/spec_coverageitemnode.h \
    CodeModel/spec_coverageitemoptionnode.h \
    CodeModel/spec_constraintnode.h \
    CodeModel/spec_methodactionnode.h \
    \
    \
    \
        spec_mainwindow.h \
    spec_imagecache.h \
    spec_interfaceverificationcomponenttemplate.h \
    testwindow.h \
    TemplateStorage/spec_interfacewizardtemplate.h \
    TemplateStorage/spec_datacontainer.h \
    Examples/examplesignalinterface.h \
    CodeModel/spec_interface_model.h \
    FileParser/spec_file_parser.h \
    ParseModel/spec_parse_module.h \
    CodeModel/spec_struct_data_node.h \
    CodeModel/spec_field_node.h \
    CodeModel/spec_code_model.h \
    CodeModel/spec_type_node.h \
    CodeGenerator/spec_e_code_generator.h \
    CodeModel/spec_module_model.h \
    CodeGenerator/spec_e_interface_code_generator.h \
    CodeGenerator/spec_e_module_code_generator.h \
    CodeGenerator/spec_e_clock_uvc_generator.h \
    CodeGenerator/spec_e_reset_uvc_generator.h \
    CodeModel/spec_clock_model.h \
    CodeModel/spec_reset_model.h \
    \
    \
    \
    CodeModel/spec_base_code_model.h \
    ModuleWizard/spec_mod_gen.h \
    InterfaceWizard/spec_if_gen.h \
    ModuleWizard/spec_mod_lib_table_widget.h \
    ModuleWizard/spec_mod_lib_tree_widget.h \
    ModuleWizard/spec_mod_clock_configuration_dialog.h \
    ModuleWizard/spec_mod_reset_configuration_dialog.h \
    ModuleWizard/spec_mod_uvc_configuration_dialog.h \
    CodeBrowser/spec_code_browser.h \
    EBrowser/spec_e_browser.h \
    CommonDialogs/spec_process_viewer.h \
    Widgets/spec_combo_box.h \
    Widgets/spec_env_widget.h \
    Widgets/spec_agent_widget.h \
    Widgets/spec_event_port_widget.h \
    Widgets/spec_simple_port_widget.h \
    Widgets/spec_tlm_port_widget.h \
    Widgets/spec_enum_types_widget.h \
    Widgets/spec_config_params_widget.h
    
    
FORMS += \
        spec_mainwindow.ui \
    spec_type_dialog.ui

RESOURCES += \
    spec_resources.qrc
