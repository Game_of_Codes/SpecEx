// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_IMAGECACHE_H
#define SPEC_IMAGECACHE_H

#include <QImage>
#include <QList>
#include <QObject>
#include <QStringList>

#include <BaseClasses/spec_logger.h>

enum spec_wave_state {
    lvl_0       = 0,
    trans_01    = 1,
    lvl_1       = 2,
    trans_10    = 3,
    data_const  = 4,
    data_change = 5,
    
    MvlX            = 6,
    MvlX0           = 7,
    MvlX1           = 8,
    MvlXData        = 9,
    MvlDataX        = 10,
    MvlZ            = 11,
    MvlZ0           = 12,
    MvlZ1           = 13,
    MvlZData        = 14,
    MvlDataZ        = 15,
    Mvl0X           = 16,
    Mvl0Z           = 17,
    Mvl1X           = 18,
    Mvl1Z           = 19,
    
    PosClock        = 20,
    NegClock        = 21
};

class spec_image_cache : public QObject {
        Q_OBJECT
    
    public:
        explicit spec_image_cache(QObject *parent = nullptr);
        
        QImage      get_wave_image         ( int index );
        QImage      get_scaled_wave_image   ( int index, int pixelSize );
        int         image_count           (  );
        
        QString     state_string( int state );
        
        enum PictureId {
            Level0          = 0,
            Transition01    = 1,
            Level1          = 2,
            Transition10    = 3,
            DataConst       = 4,
            DataChange      = 5,
            
            MvlX            = 6,
            MvlX0           = 7,
            MvlX1           = 8,
            MvlXData        = 9,
            MvlDataX        = 10,
            MvlZ            = 11,
            MvlZ0           = 12,
            MvlZ1           = 13,
            MvlZData        = 14,
            MvlDataZ        = 15,
            Mvl0X           = 16,
            Mvl0Z           = 17,
            Mvl1X           = 18,
            Mvl1Z           = 19,
            
            PosClock        = 20,
            NegClock        = 21
        };
        
        QVector<QImage> get_all_scaled_wave_images( int pixel_size = 50 );
        
    signals:
        
    public slots:
        
        
    private:
        QList <QImage *>    waveImages;
        QStringList         get_all_image_files();
};

#endif // SPEC_IMAGECACHE_H
