// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_EVENT_PORT_WIDGET_H
#define SPEC_EVENT_PORT_WIDGET_H

#include <QCheckBox>
#include <QComboBox>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QObject>
#include <QPushButton>
#include <QSpinBox>
#include <QVector>
#include <QWidget>

#include <BaseClasses/spec_logger.h>
#include <Widgets/spec_combo_box.h>


class spec_event_port_widget : public QWidget {
    Q_OBJECT
    public:
        spec_event_port_widget();
        spec_event_port_widget( spec_event_port_widget *other );
        
        void    initialize();
        
        void    set_port_object_name(
            QString name,
            bool    is_editable = true
        );
        QString get_port_object_name();
        
        void    set_direction       (
            QString direction,
            bool    is_editable = true
        );
        QString get_direction       ();
        QString get_signal_name     ();
        
        void    set_hdl_name        (
            QString name,
            bool    is_editable = true
        );
        QString get_hdl_name        ();
        
        void    set_event_trigger   (
            QString name,
            bool    is_editable = true
        );
        QString get_event_trigger   ();
        
        bool    is_associated_clock ();
        bool    is_associated_reset ();
        
        void            print_widget_info   ();
        QList<QString>  get_incomplete_config_elements();
        
    signals:
        
        
    public slots:
        
    private slots:
        void            clock_domain_check_box_state_changed(int state);
        void            reset_domain_check_box_state_changed(int state);
        
    private:
        QGroupBox       *port_group_box_label;
        QLabel          *port_name_label;
        QLabel          *port_direction_label;
        QLabel          *port_signal_name_label;
        QLabel          *port_sensitivity_label;
        QLabel          *hdl_hierarchy_label;
        QLabel          *clock_domain_label;
        QLabel          *reset_domain_label;
        
        QLineEdit       *port_name_value_line_edit;
        QLineEdit       *port_direction_value_line_edit;
        QLineEdit       *port_signal_name_line_edit;
        QLineEdit       *port_sensitivity_value_line_edit;
        QLineEdit       *hdl_hierarchy_line_edit;
        QCheckBox       *is_clock_domain_clock_signal_check_box;
        QCheckBox       *is_reset_domain_reset_signal_check_box;
        
};

#endif // SPEC_EVENT_PORT_WIDGET_H
