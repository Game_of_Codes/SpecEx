// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_ENV_WIDGET_H
#define SPEC_ENV_WIDGET_H

#include <QComboBox>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QObject>
#include <QPushButton>
#include <QSpinBox>
#include <QVector>
#include <QWidget>

#include <BaseClasses/spec_logger.h>
#include <Widgets/spec_combo_box.h>

class spec_env_widget : public QWidget {
    Q_OBJECT
    public:
        // Standard Constructor
        spec_env_widget ();
        
        // Copy Constructor
        spec_env_widget ( const spec_env_widget *other );
        
        void                    initialize();
        
        void                    set_env_name_list(
            QList<QString>  domains
        );
        QString                 get_selected_env_name               ();
        
        int                     get_active_agents_count         ();
        int                     get_passive_agents_count        ();
        
        QList<QString>          get_incomplete_config_elements  ();
        
        void                    print_widget_infos              ();
        
        void                    set_hdl_tb_top                  ( QString new_hdl_top_name );
        QString                 get_hdl_tb_top                  ();
        
    signals:
        void                    agent_count_change              ( int value );
        void                    active_agent_count_change       ( int value );
        
    public slots:
        void                    handle_agent_count_change       ( int value );
        void                    handle_active_agent_count_change( int value );
        
        
    private:
        QGroupBox               *field_group_box;
        QLabel                  *agents_count_label;
        QLabel                  *active_agents_count_label;
        QLabel                  *env_name_label;
        QLabel                  *hdl_hierarchy_label;
        
        QGroupBox               *value_group_box;
        QSpinBox                *passive_agent_size_spinbox;
        QSpinBox                *active_agent_size_spinbox;
        spec_combo_box          *env_name_lineedit;
        QLineEdit               *hdl_hierarchy_lineedit;
        
        // Associated node
//        spec_struct_data_node   *cur_node;
        
//        // Actual module level configuration model
//        spec_module_model       *module_model;
};

#endif // SPEC_ENV_WIDGET_H
