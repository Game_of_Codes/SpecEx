// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_if_signal_map_table.h"

spec_if_signal_map_table::spec_if_signal_map_table() {
    initialize();
}

void spec_if_signal_map_table::initialize() {
    int column_count    = 5;
    int row_count       = 1;
    int cur_column_width= 200;
    
    setColumnCount  ( column_count );
    setRowCount     ( row_count );
    
    for( int column_index = 0; column_index < column_count;++column_index ) {
        setHorizontalHeaderItem( column_index, new QTableWidgetItem("") );
        setColumnWidth( column_index, cur_column_width );
    };
    
    horizontalHeaderItem( 0 )   -> setText( "Name" );
    horizontalHeaderItem( 0 )   -> setFont( QFont("Courier New") );
    horizontalHeaderItem( 0 )   -> setToolTip( "This column indicates the HDL name of the signals." );
    
    horizontalHeaderItem( 1 )   -> setText( "Type" );
    horizontalHeaderItem( 1 )   -> setFont( QFont("Courier New") );
    horizontalHeaderItem( 1 )   -> setToolTip( "This column indicates what data type is used for each signals." );
    
    horizontalHeaderItem( 2 )   -> setText( "Width" );
    horizontalHeaderItem( 2 )   -> setFont( QFont("Courier New") );
    horizontalHeaderItem( 2 )   -> setToolTip( "This column indicates the bus width of the signals." );
    
    horizontalHeaderItem( 3 )   -> setText( "Access Direction" );
    horizontalHeaderItem( 0 )   -> setFont( QFont("Courier New") );
    horizontalHeaderItem( 0 )   -> setToolTip( "This column indicates how the testbench can access the HDL code" );
    
    horizontalHeaderItem( 4 )   -> setText( "HDL Name" );
    horizontalHeaderItem( 0 )   -> setFont( QFont("Courier New") );
    horizontalHeaderItem( 0 )   -> setToolTip( "This column represents the default name of the field within the HDL domain." );
    
    horizontalHeader()          -> setSectionResizeMode( QHeaderView::Fixed );
    verticalHeader()            -> setSectionResizeMode( QHeaderView::Fixed );
    
    set_row( 0 );
    
    // Call the cell_content_change method that extracts the content of the cell and passes that via an emit to anything
    // that is connected to this table
    connect(
        this, &spec_if_signal_map_table::cellEntered,
        this, &spec_if_signal_map_table::cell_entered
    );
    
    connect(
        verticalHeader(), &QHeaderView::sectionClicked,
        this            , &spec_if_signal_map_table::row_selected
    );
}

bool spec_if_signal_map_table::validate_table      () {
    // Assuming all values are set. A single mistake will ensure that this method will return false
    bool    result      = true;
    bool    is_valid    = true;
    int     row_count   = rowCount();
    
    for( int row_index = 0; row_index < row_count; ++row_index ) {
        is_valid   = not static_cast<spec_cell_line_edit_widget*>(cellWidget(row_index,0))->get_value().isEmpty();
        static_cast<spec_cell_line_edit_widget*>(cellWidget(row_index,0)) -> set_invalid_color( not is_valid );
        result = result and is_valid;
        
        is_valid   = not static_cast<spec_cell_line_combo_box_widget*>(cellWidget(row_index,1))->get_value().isEmpty();
        static_cast<spec_cell_line_combo_box_widget*>(cellWidget(row_index,1)) -> set_invalid_color( not is_valid );
        result = result and is_valid;
        
        is_valid   = not static_cast<spec_cell_line_edit_widget*>(cellWidget(row_index,4))->get_value().isEmpty();
        static_cast<spec_cell_line_edit_widget*>(cellWidget(row_index,4)) -> set_invalid_color( not is_valid );
        result = result and is_valid;
    };  // end of iterating over all rows
    
    return result;
}

QString spec_if_signal_map_table::get_cell_value(int row, int column) {
    switch( column ) {
        case 0: {   // Signal Name
            return static_cast<spec_cell_line_edit_widget*>(cellWidget( row, 0))->get_value();
        };
        case 1: {   // Signal Type
            return static_cast<spec_cell_line_combo_box_widget*>(cellWidget( row, 1))->get_value();
        };
        case 2: {   // Signal Bit-Width
            QString calculated = static_cast<spec_cell_line_combo_box_widget*>(cellWidget( row, 1))->get_value();
            if( calculated == "uint" or calculated == "int" or calculated == "longuint" or calculated == "longint" ) {
                return static_cast<spec_cell_spin_box_widget*>(cellWidget( row, 2))->get_value();
            } else {
                return QString("1");
            };
            
        };
        case 3: {   // Signal Access Direction
            return static_cast<spec_cell_combo_box_widget*>(cellWidget( row, 3))->get_value();
        };
        case 4: {   // Default HDL Name
            return QString( static_cast<spec_cell_line_edit_widget*>(cellWidget( row, 4))->get_value() );
        };
    };
    
    return QString("");
}

void spec_if_signal_map_table::set_row( int row_index ) {
    spec_cell_line_edit_widget      *line_edit_widget;
    spec_cell_line_combo_box_widget *line_combo_box_widget;
    spec_cell_combo_box_widget      *combo_box_widget;
    spec_cell_spin_box_widget       *spin_box_widget;
    
    int                             cur_row_height  = 50;
    int                             cur_column_width = columnWidth( 0 );
    
    setRowHeight( row_index, cur_row_height );
    
    // Create the first column: The name of the signal
    line_edit_widget    = new spec_cell_line_edit_widget();
    line_edit_widget    -> set_indices( row_index, 0 );
    line_edit_widget    -> set_line_edit_geometry( 0, 0, cur_column_width-1, cur_row_height-1);
    setCellWidget( row_index, 0, line_edit_widget );
    
    connect(
        line_edit_widget        , &spec_cell_base_widget::cell_content_change,
        this                    , &spec_if_signal_map_table::cell_content_change
    );
    connect(
        line_edit_widget        , &spec_cell_base_widget::switch_cell,
        this                    , &spec_if_signal_map_table::switch_cell
    );
    connect(
        line_edit_widget        , &spec_cell_base_widget::switch_cell_back,
        this                    , &spec_if_signal_map_table::switch_cell_back
    );
    
    // Create the second column: The signal's data type
    line_combo_box_widget   = new spec_cell_line_combo_box_widget();
    line_combo_box_widget   -> add_items( {"bit", "uint", "real"} );
    line_combo_box_widget   -> set_indices( row_index, 1);
    line_combo_box_widget   -> set_line_combo_box_geometry( 0, 0, cur_column_width-1, cur_row_height-1 );
    setCellWidget( row_index, 1, line_combo_box_widget );
    
    connect(
        line_combo_box_widget   , &spec_cell_base_widget::cell_content_change,
        this                    , &spec_if_signal_map_table::cell_content_change
    );
    connect(
        line_combo_box_widget   , &spec_cell_base_widget::switch_cell,
        this                    , &spec_if_signal_map_table::switch_cell
    );
    connect(
        line_combo_box_widget   , &spec_cell_base_widget::switch_cell_back,
        this                    , &spec_if_signal_map_table::switch_cell_back
    );
    
    // Create the third column: The signal's width
    spin_box_widget         = new spec_cell_spin_box_widget();
    spin_box_widget         -> set_indices( row_index, 2 );
    spin_box_widget         -> set_value( 1 );
    spin_box_widget         -> set_minimum_value( 0 );
    spin_box_widget         -> set_maximum_value( 65536 );
    spin_box_widget         -> set_tool_tip( "This indicates th bit-width of your field. Value 0 indicates that bit-width is not considered" );
    spin_box_widget         -> set_spin_box_geometry( 0, 0, cur_column_width-1, cur_row_height-1 );
    setCellWidget( row_index, 2, spin_box_widget );
    
    connect(
        spin_box_widget         , &spec_cell_base_widget::cell_content_change,
        this                    , &spec_if_signal_map_table::cell_content_change
    );
    connect(
        spin_box_widget         , &spec_cell_base_widget::switch_cell,
        this                    , &spec_if_signal_map_table::switch_cell
    );
    connect(
        spin_box_widget         , &spec_cell_base_widget::switch_cell_back,
        this                    , &spec_if_signal_map_table::switch_cell_back
    );
    
    // Create the fourth column: The signal's access from the testbench
    combo_box_widget    = new spec_cell_combo_box_widget();
    combo_box_widget    -> set_indices( row_index, 3);
    combo_box_widget    -> add_items( {"HDL Read/Write", "HDL Read", "HDL Write"} );
    combo_box_widget    -> set_combo_box_geometry( 0, 0, cur_column_width-1, cur_row_height-1 );
    setCellWidget( row_index, 3, combo_box_widget );
    
    connect(
        combo_box_widget        , &spec_cell_base_widget::cell_content_change,
        this                    , &spec_if_signal_map_table::cell_content_change
    );
    connect(
        combo_box_widget        , &spec_cell_base_widget::switch_cell,
        this                    , &spec_if_signal_map_table::switch_cell
    );
    connect(
        combo_box_widget        , &spec_cell_base_widget::switch_cell_back,
        this                    , &spec_if_signal_map_table::switch_cell_back
    );
    
    // Create the fifth column: The HDL name that the testbench is going to connect to
    line_edit_widget    = new spec_cell_line_edit_widget();
    line_edit_widget    -> set_indices( row_index, 4 );
    line_edit_widget    -> set_line_edit_geometry( 0, 0, cur_column_width-1, cur_row_height-1);
    setCellWidget( row_index, 4, line_edit_widget );
    
    connect(
        line_edit_widget        , &spec_cell_base_widget::cell_content_change,
        this                    , &spec_if_signal_map_table::cell_content_change
    );
    connect(
        line_edit_widget        , &spec_cell_base_widget::switch_cell,
        this                    , &spec_if_signal_map_table::switch_cell
    );
    connect(
        line_edit_widget        , &spec_cell_base_widget::switch_cell_back,
        this                    , &spec_if_signal_map_table::switch_cell_back
    );
}

void spec_if_signal_map_table::add_row() {
    int column_count    = columnCount();
    int row_count       = rowCount();
    int selected_row    = -1;
    
    insertRow( row_count );
    
    set_row( row_count );
}

void spec_if_signal_map_table::delete_selected_row() {
    int row_count   = rowCount();
    
    // Error handling for if there was on selected row (ie: empty table)
    if( selected_row < 0 ) {
        // can't delete anything ... abort executing on an empty table
        return;
    };
    
    // Do not allow the last line to be deleted!
    if( row_count >= 2 ) {
        removeRow( selected_row );
        emit row_removed( selected_row );
        selected_row = -1;
    };
}

void spec_if_signal_map_table::cell_entered(
    int row_index,
    int column_index
) {
    spec_msg( "Cell entered (" << row_index << "|" << column_index << ")");
}

void spec_if_signal_map_table::cell_content_change(
    int     row_index,
    int     column_index,
    QString content
) {
    // If the content is empty, then the actual cell may just be a simple QTableWidgetItem...
    if( content.isEmpty() ) {
        // ... but it may not be initialized... hence we are checking and only once both conditions are met, then
        // we're allowed to extract the values.
        if( item( row_index, column_index ) != nullptr ) {
            content = item(row_index, column_index)->text();
        };
    };
    
//    spec_msg( "Detected field change: (" << row_index << "|" << column_index <<"): " + content);
    
    // Throw the signal with the appropiate signal to the envionment
    emit field_changed( row_index, column_index, content );
}

void spec_if_signal_map_table::row_selected( int row_index ) {
    selected_row = row_index;
}

QList<QString*> spec_if_signal_map_table::get_row_data( int row_index ) {
    QList<QString*> result;
    QString calculated;
    
    // Signal Name
    result.append( new QString(static_cast<spec_cell_line_edit_widget*>(cellWidget( row_index, 0))->get_value()) );
    
    // Signal Type
    calculated = static_cast<spec_cell_line_combo_box_widget*>(cellWidget( row_index, 1))->get_value();
    result.append( new QString(calculated) );
    
    // Signal Bit-Width
    // Only int based data types will get the option to have a defined bit-width. For any other data type, the bit-width
    // will be ignored.
    if( calculated == "uint" or calculated == "int" or calculated == "longuint" or calculated == "longint" ) {
        result.append( new QString(static_cast<spec_cell_spin_box_widget*>(cellWidget( row_index, 2))->get_value()) );
    } else {
        result.append( new QString( "" ) );
    };
    
    // Signal HDL Access Direction
    result.append( new QString(static_cast<spec_cell_combo_box_widget*>(cellWidget( row_index, 3))->get_value()) );
    
    // Signal HDL Name
    result.append( new QString(static_cast<spec_cell_line_edit_widget*>(cellWidget( row_index, 4))->get_value()) );
    
    return result;
}

void spec_if_signal_map_table::switch_cell( int row, int column ) {
    int row_count       = rowCount();
    int column_count    = columnCount();
    
    bool is_last_row    = (row      == row_count-1);
    bool is_last_column = (column   == column_count-1);
    
    // This is a convenience feature that allows you to simlpy add a new line by using the TAB key after the last element.
    if( is_last_row and is_last_column ) {
        add_row();
        emit row_added();
        static_cast<spec_cell_base_widget*>(cellWidget( row+1, 0 ))->set_focus();
    } else {
        if( is_last_column ) {
            // Switch to next row: (row+1 / 0)
            static_cast<spec_cell_base_widget*>(cellWidget( row+1, 0 ))->set_focus();
        } else {
            // Switch to next column: row / column+1
            static_cast<spec_cell_base_widget*>(cellWidget( row, column+1 ))->set_focus();
        };
    };
}

void spec_if_signal_map_table::switch_cell_back( int row, int column ) {
    int row_count           = rowCount();
    int column_count        = columnCount();
    
    bool is_first_row       = (row      == 0);
    bool is_first_column    = (column   == 0);
    
    if( is_first_row and is_first_column) {
        // Implementing wrap-around semantics
        static_cast<spec_cell_base_widget*>(cellWidget( row_count-1, column_count-1 ))->set_focus();
    } else {
        if( is_first_column ) {
            // Switch to next row: (row-1 / 0)
            static_cast<spec_cell_base_widget*>(cellWidget( row-1, column_count-1 ))->set_focus();
        } else {
            // Switch to next column: row / column+1
            static_cast<spec_cell_base_widget*>(cellWidget( row, column-1 ))->set_focus();
        }
    };
}
