// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_interface_wave_cell_widget.h"

spec_interface_wave_cell_widget::spec_interface_wave_cell_widget(  ) {
    image_label         = new QLabel();
    image_label     -> setGeometry(0,0,50,50);
    image_label     -> setParent( this );
}

spec_interface_wave_cell_widget::spec_interface_wave_cell_widget( QImage image ) {
    signal_image    = image;
    image_label     = new QLabel();
    image_label     -> setPixmap( QPixmap::fromImage( image ) );
    image_label     -> setGeometry(0,0,50,50);
    image_label     -> setParent( this );
}

spec_interface_wave_cell_widget::spec_interface_wave_cell_widget ( spec_interface_wave_cell_widget *other ) {
    image_label         = new QLabel();
    image_label         -> setGeometry( 0, 0, 50, 50 );
    image_label         -> setParent( this );
    
    set_name            ( other -> get_name() );
    set_width           ( other -> get_width() );
    set_state           ( other -> get_state() );
    set_signal_clock    ( other -> is_clock() );
    set_signal_reset    ( other -> is_reset() );
    set_image           ( other -> signal_image );
}

void spec_interface_wave_cell_widget::set_name    ( QString name ) {
    signal_name = name;
}
QString spec_interface_wave_cell_widget::get_name    () {
    return signal_name;
}

void    spec_interface_wave_cell_widget::set_width   ( int width ) {
    signal_width = width;
}
int     spec_interface_wave_cell_widget::get_width   () {
    return signal_width;
}

void    spec_interface_wave_cell_widget::set_state   ( int state ) {
    signal_state = state;
}
int spec_interface_wave_cell_widget::get_state   () {
    return signal_state;
}

void spec_interface_wave_cell_widget::set_signal_clock        ( bool is_clock ) {
    is_signal_clock = is_clock;
}
void spec_interface_wave_cell_widget::set_signal_reset        ( bool is_reset ) {
    is_signal_reset = is_reset;
}

bool spec_interface_wave_cell_widget::is_clock                () {
    return is_signal_clock;
}
bool spec_interface_wave_cell_widget::is_reset                () {
    return is_signal_reset;
}

void spec_interface_wave_cell_widget::change_cell_width       (
    bool    is_single_bit,
    int     width,
    QImage  image
) {
    if( is_single_bit ) {
        signal_width    = 1;
        signal_state    = spec_wave_state::lvl_0;
    } else {
        signal_width    = width;
        signal_state    = spec_wave_state::data_const;
    };
    set_image( image );
}

void spec_interface_wave_cell_widget::set_image   ( QImage image ) {
    signal_image    = image;
    image_label     -> setPixmap( QPixmap::fromImage( image ) );
}
