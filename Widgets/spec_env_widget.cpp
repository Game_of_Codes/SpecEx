// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_env_widget.h"

spec_env_widget::spec_env_widget() {
    spec_msg( "Constructor of " << this );
}

// Copy Constructor
spec_env_widget::spec_env_widget( const spec_env_widget *other ) {
    initialize();
    
    // Copying of all fields manually
    passive_agent_size_spinbox  -> setValue( other->passive_agent_size_spinbox->value() );
    active_agent_size_spinbox   -> setValue( other->active_agent_size_spinbox->value() );
    
    if( other->env_name_lineedit->count() > 0 ) {
        for( int index = 0; index < other->env_name_lineedit->count();++index ) {
            env_name_lineedit->addItem( other->env_name_lineedit->itemText( index ) );
        };
        env_name_lineedit->setCurrentIndex( other->env_name_lineedit->currentIndex() );
    };  // end of adding all the name values
    
    hdl_hierarchy_lineedit  -> setText( other->hdl_hierarchy_lineedit->text() );
}

void spec_env_widget::initialize() {
    spec_msg( "Initialization of " << this);
    field_group_box             = new QGroupBox ();
    agents_count_label          = new QLabel    ();
    active_agents_count_label   = new QLabel    ();
    env_name_label              = new QLabel    ();
    hdl_hierarchy_label         = new QLabel    ();
    
    passive_agent_size_spinbox  = new QSpinBox  ();
    active_agent_size_spinbox   = new QSpinBox  ();
    env_name_lineedit           = new spec_combo_box ();
    hdl_hierarchy_lineedit      = new QLineEdit ();
    
    field_group_box             -> setTitle     ( "Environment Fields" );
    field_group_box             -> setGeometry  (  10,  10, 500, 300 );
    
    agents_count_label          -> setText      ( "Number of PASSIVE Agents");
    agents_count_label          -> setGeometry  (  15,  25, 200,  30 );
    
    active_agents_count_label   -> setText      ( "Number of ACTIVE Agents" );
    active_agents_count_label   -> setGeometry  (  15,  75, 200,  30 );
    
    env_name_label              -> setText      ( "Environment Name" );
    env_name_label              -> setGeometry  (  15, 125, 200,  30 );
    
    hdl_hierarchy_label         -> setText      ( "HDL Testbench Top Module" );
    hdl_hierarchy_label         -> setGeometry  (  15, 175, 200,  30 );
    
    passive_agent_size_spinbox  -> setValue     ( 0 );
    passive_agent_size_spinbox  -> setGeometry  ( 250,  25,  50,  30 );
    
    active_agent_size_spinbox   -> setValue     ( 0 );
    active_agent_size_spinbox   -> setGeometry  ( 250,  75,  50,  30 );
    
    env_name_lineedit           -> setGeometry  ( 250, 125, 200,  30 );
    
    hdl_hierarchy_lineedit      -> setGeometry  ( 250, 175, 200,  30 );
    
    field_group_box             -> setParent    ( this );
    agents_count_label          -> setParent    ( field_group_box);
    active_agents_count_label   -> setParent    ( field_group_box );
    env_name_label              -> setParent    ( field_group_box );
    hdl_hierarchy_label         -> setParent    ( field_group_box );
    passive_agent_size_spinbox  -> setParent    ( field_group_box );
    active_agent_size_spinbox   -> setParent    ( field_group_box );
    env_name_lineedit           -> setParent    ( field_group_box );
    hdl_hierarchy_lineedit      -> setParent    ( field_group_box );
    
    connect(
        active_agent_size_spinbox   , SIGNAL( valueChanged(int) ),
        this                        , SLOT  ( handle_active_agent_count_change(int))
    );
    
    connect(
        passive_agent_size_spinbox  , SIGNAL( valueChanged(int) ),
        this                        , SLOT  ( handle_agent_count_change(int))
    );
}

void spec_env_widget::set_env_name_list(
    QList<QString>  domains
) {
    if( env_name_lineedit->count() == 0 ) {
        env_name_lineedit -> addItems( domains );
    };
}

QString spec_env_widget::get_selected_env_name() {
    spec_msg( "Checking on current text " << env_name_lineedit );
    return env_name_lineedit ->currentText();
}

void spec_env_widget::handle_active_agent_count_change( int value ) {
    spec_msg("Handle_active_agent_count_change -> " + QString::number( value ));
    emit active_agent_count_change( value );
}

void spec_env_widget::handle_agent_count_change( int value ) {
    spec_msg("Handle_agent_count_change -> " + QString::number( value ));
    emit agent_count_change( value );
}

int spec_env_widget::get_active_agents_count() {
    return active_agent_size_spinbox    -> value();
}
int spec_env_widget::get_passive_agents_count() {
    return passive_agent_size_spinbox   -> value();
}

void spec_env_widget::print_widget_infos      () {
    QList<QString>  message_output;
    message_output.append( "Env Widget:");
    message_output.append( "  PASSIVE Agent Count: " + QString::number(passive_agent_size_spinbox->value()));
    message_output.append( "  ACTIVE  Agent Count: " + QString::number(active_agent_size_spinbox->value()));
    message_output.append( "  Env Name           : " + env_name_lineedit->currentText());
    message_output.append( "  HDL Hierarchy      : " + hdl_hierarchy_lineedit->text());
    for( QString m : message_output ) {
        spec_msg( m );
    };
}

void spec_env_widget::set_hdl_tb_top                  ( QString new_hdl_top_name ) {
    hdl_hierarchy_lineedit->setText( new_hdl_top_name );
}

QString spec_env_widget::get_hdl_tb_top() {
    return hdl_hierarchy_lineedit->text();
}

QList<QString> spec_env_widget::get_incomplete_config_elements() {
    QList<QString>  result;
    
    if( active_agent_size_spinbox->value() == 0 and passive_agent_size_spinbox->value() == 0) {
        result.append( "No instantiated agent." );
    };
    if( hdl_hierarchy_lineedit ->text().isEmpty() ) {
        result.append( "No HDL testbench name given." );
    };
    return result;
}   // end of get_incomplete_config_elements
