// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_INTERFACE_WAVE_CELL_WIDGET_H
#define SPEC_INTERFACE_WAVE_CELL_WIDGET_H

#include <QImage>
#include <QLabel>
#include <QObject>
#include <QWidget>

#include <BaseClasses/spec_logger.h>
#include <spec_imagecache.h>


class spec_interface_wave_cell_widget : public QWidget {
    Q_OBJECT
    
    public:
        spec_interface_wave_cell_widget ();
        spec_interface_wave_cell_widget ( QImage image );
        spec_interface_wave_cell_widget ( spec_interface_wave_cell_widget *other );
        
        void    set_name                ( QString name );
        QString get_name                ();
        
        void    set_width               ( int width );
        int     get_width               ();
        
        void    set_state               ( int state );
        int     get_state               ();
        
        void    set_signal_clock        ( bool is_clock );
        void    set_signal_reset        ( bool is_reset );
        
        bool    is_clock                ();
        bool    is_reset                ();
        
        void    change_cell_width       (
                bool    is_single_bit,
                int     width,
                QImage  image
        );
        
        void    set_image               ( QImage image );
        
        
    signals:
        
    public slots:
        
    private:
        QString     signal_name;
        int         signal_width;
        int         signal_state;
        bool        is_signal_clock;
        bool        is_signal_reset;
        
        QLabel      *image_label;
        QImage      signal_image;
};

#endif // SPEC_INTERFACE_WAVE_CELL_WIDGET_H
