// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_SIMPLE_PORT_WIDGET_H
#define SPEC_SIMPLE_PORT_WIDGET_H

#include <QCheckBox>
#include <QComboBox>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QObject>
#include <QPushButton>
#include <QSpinBox>
#include <QVector>
#include <QWidget>

#include <BaseClasses/spec_logger.h>

#include <Widgets/spec_combo_box.h>

class spec_simple_port_widget : public QWidget {
    Q_OBJECT
    public:
        spec_simple_port_widget();
        spec_simple_port_widget( spec_simple_port_widget *other );
        
        void    initialize();
        
        void    set_port_name       ( QString name      , bool is_editable = true );
        void    set_access_direction( QString direction , bool is_editable = true );
        void    set_signal_name     ( QString name      , bool is_editable = true );
        void    set_type_value      ( QString value     , bool is_editable = true );
        void    set_hdl_hierarchy   ( QString hierarchy , bool is_editable = true );
        
        bool            is_associated_clock             ();
        bool            is_associated_reset             ();
        void            print_widget_info               ();
        QString         get_access_direction            ();
        QString         get_signal_name                 ();
        QString         get_signal_type                 ();
        int             get_signal_type_width           ();
        QList<QString>  get_incomplete_config_elements  ();
        
    signals:
        
    public slots:
        
    private slots:
        void            clock_domain_check_box_state_changed(int state);
        void            reset_domain_check_box_state_changed(int state);
        
    private:
        QGroupBox       *port_group_box;
        QLabel          *port_name_label;
        QLabel          *port_direction_label;
        QLabel          *port_signal_name_label;
        QLabel          *port_type_label;
        QLabel          *hdl_hierarchy_label;
        QLabel          *clock_domain_label;
        QLabel          *reset_domain_label;
        
        QLineEdit       *port_name_value_line_edit;
        QLineEdit       *port_direction_value_line_edit;
        QComboBox       *port_direction_value_combo_box;
        QLineEdit       *port_signal_name_line_edit;
        QLineEdit       *port_type_value_line_edit;
        QLineEdit       *hdl_hierarchy_line_edit;
        QCheckBox       *is_clock_domain_clock_signal_check_box;
        QCheckBox       *is_reset_domain_reset_signal_check_box;
};

#endif // SPEC_SIMPLE_PORT_WIDGET_H
