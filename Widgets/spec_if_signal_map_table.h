// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_IF_SIGNAL_MAP_TABLE_H
#define SPEC_IF_SIGNAL_MAP_TABLE_H

#include <QComboBox>
#include <QHeaderView>
#include <QSpinBox>
#include <QTableWidget>

#include <BaseClasses/spec_logger.h>
#include <Widgets/spec_cell_line_edit_widget.h>
#include <Widgets/spec_cell_line_combo_box_widget.h>
#include <Widgets/spec_cell_combo_box_widget.h>
#include <Widgets/spec_combo_box.h>
#include <Widgets/spec_cell_spin_box_widget.h>


class spec_if_signal_map_table : public QTableWidget {
    Q_OBJECT
    
    public:
        spec_if_signal_map_table            ();
        
        void            initialize          ();
        
        bool            validate_table      ();
        QString         get_cell_value      ( int row, int column );
        
    public slots:
        void            add_row             ();
        void            set_row             ( int row_index );
        void            delete_selected_row ();
        
        QList<QString*> get_row_data        ( int row_index );
        
        void            cell_content_change (
                int     row_index,
                int     column_index,
                QString content = ""
                );
        void            cell_entered        (
                int row_index,
                int column_index
                );
        
        void            row_selected        ( int row_index );
        
    signals:
        void            field_changed   (
                int     row,
                int     column,
                QString new_value
                );
        
        void            row_added       ();
        void            row_removed     ( int row_index );
        
    private slots:
        void            switch_cell     ( int row, int column );
        void            switch_cell_back( int row, int column );
        
    private:
        int             selected_row    = -1;
};

#endif // SPEC_IF_SIGNAL_MAP_TABLE_H
