// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// -------------------------------------------------------------------------

#ifndef SPEC_CELL_LINE_COMBO_BOX_WIDGET_H
#define SPEC_CELL_LINE_COMBO_BOX_WIDGET_H



#include <Widgets/spec_cell_base_widget.h>
#include <Widgets/spec_combo_box.h>
#include <BaseClasses/spec_logger.h>

class spec_cell_line_combo_box_widget : public spec_cell_base_widget {
    Q_OBJECT
    
    public:
        spec_cell_line_combo_box_widget();
        
        void        set_line_combo_box_geometry(
                int x,
                int y,
                int width,
                int height
                );
        
        void    add_item    ( const QString item );
        void    add_items   ( const QStringList items );
        
        void    set_invalid_color( bool is_invalid );
        
        QString get_value   ();
        
        void            set_focus       ();
        
    signals:
        
    public slots:
        void        combo_box_text_changed( const QString &text );
        
    private:
        spec_combo_box  *combo_box;
};

#endif // SPEC_CELL_LINE_COMBO_BOX_WIDGET_H
