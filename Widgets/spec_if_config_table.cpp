// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_if_config_table.h"

spec_if_config_table::spec_if_config_table() {
    initialize();
}

void spec_if_config_table::initialize() {
    int column_count    = 6;
    int row_count       = 1;
    int cur_column_width= 50;
    
    setColumnCount  ( column_count );
    setRowCount     ( row_count);
    
    for( int column_index = 0; column_index < column_count; ++column_index ) {
        setHorizontalHeaderItem( column_index, new QTableWidgetItem("") );
        switch( column_index ) {
            case 0: { cur_column_width = 250; break; };
            case 1: { cur_column_width = 50;  break; };
            case 2: { cur_column_width = 50;  break; };
            case 3: { cur_column_width = 200; break; };
            case 4: { cur_column_width = 200; break; };
            case 5: { cur_column_width = 200; break; };
        };
        setColumnWidth( column_index, cur_column_width );
    };
    
    horizontalHeader()          -> setSectionResizeMode( QHeaderView::Fixed );
    
    horizontalHeaderItem( 0 )   -> setText( "Name" );
    horizontalHeaderItem( 0 )   -> setFont( QFont("Courier New") );
    
    horizontalHeaderItem( 1 )   -> setIcon( QIcon( ":images/dice-307540_960_720.png" ) );
    horizontalHeaderItem( 1 )   -> setToolTip( "Checked box indicates randomized field" );
    
    horizontalHeaderItem( 2 )   -> setIcon( QIcon( ":images/posedge_clock.png" ) );
    horizontalHeaderItem( 2 )   -> setToolTip( "Checked box indicates field is \"seen on the wire\"" );
    
    horizontalHeaderItem( 3 )   -> setText( "Type" );
    horizontalHeaderItem( 3 )   -> setFont( QFont("Courier New") );
    
    horizontalHeaderItem( 4 )   -> setText( "Bit Width" );
    horizontalHeaderItem( 4 )   -> setFont( QFont("Courier New") );
    
    horizontalHeaderItem( 5 )   -> setText( "n-Dimensions" );
    horizontalHeaderItem( 5 )   -> setToolTip( "This column indicates how many nested lists this field has. n indicates dimensionality of list.If 0, then it is not a list. " );
    horizontalHeaderItem( 5 )   -> setFont( QFont("Courier New") );
    
    // Now configuring the first line row header
    verticalHeader()            -> setSectionResizeMode( QHeaderView::Fixed );
    
    // The constructor always creates a row, hence we are going to set the row using the set_row method
    set_row( 0 );
    
    connect(
        verticalHeader(), &QHeaderView::sectionClicked,
        this            , &spec_if_config_table::row_selected
    );
}

void spec_if_config_table::set_row( int row_index ) {
    spec_cell_check_box_widget      *check_box_widget;
    spec_cell_line_combo_box_widget *line_combo_widget;
    spec_cell_line_edit_widget      *line_edit_widget;
    spec_cell_spin_box_widget       *spin_box_widget;
    
    int cur_row_height  = 50;
    
    setRowHeight( row_index, cur_row_height );
    
    // Create and set the first column element: The name of the field. This is a simple text field
    line_edit_widget    = new spec_cell_line_edit_widget();
    line_edit_widget    -> set_indices( row_index, 0 );
    line_edit_widget    -> set_line_edit_geometry( 0, 0, columnWidth( 0 )-1, cur_row_height-1);
    setCellWidget( row_index, 0, line_edit_widget );
    
    connect(
        line_edit_widget        , &spec_cell_base_widget::switch_cell,
        this                    , &spec_if_config_table::switch_cell
    );
    connect(
        line_edit_widget        , &spec_cell_base_widget::switch_cell_back,
        this                    , &spec_if_config_table::switch_cell_back
    );
    
    // Create and s4et the second column element: Does it generate
    check_box_widget    = new spec_cell_check_box_widget();
    check_box_widget    -> set_indices( row_index, 1 );
    check_box_widget    -> set_check_state( Qt::Checked );
    setCellWidget( row_index, 1, check_box_widget );
    
    connect(
        check_box_widget        , &spec_cell_base_widget::switch_cell,
        this                    , &spec_if_config_table::switch_cell
    );
    connect(
        check_box_widget        , &spec_cell_base_widget::switch_cell_back,
        this                    , &spec_if_config_table::switch_cell_back
    );
    
    // Create and set the third column element: Is it part of the protocol
    check_box_widget    = new spec_cell_check_box_widget();
    check_box_widget    -> set_indices( row_index, 2 );
    check_box_widget    -> set_check_state( Qt::Checked );
    setCellWidget( row_index, 2, check_box_widget );
    
    connect(
        check_box_widget        , &spec_cell_base_widget::switch_cell,
        this                    , &spec_if_config_table::switch_cell
    );
    connect(
        check_box_widget        , &spec_cell_base_widget::switch_cell_back,
        this                    , &spec_if_config_table::switch_cell_back
    );
    
    // Create and set the fourth column element: The Data Type of the field. This is the special combo box field
    line_combo_widget   = new spec_cell_line_combo_box_widget();
    line_combo_widget   -> set_indices( row_index, 3 );
    line_combo_widget   -> add_items( {"bit","uint","time", "real"} );
    line_combo_widget   -> set_line_combo_box_geometry( 0, 0, columnWidth(3)-1, cur_row_height-1);
    setCellWidget( row_index, 3, line_combo_widget );
    
    connect(
        line_combo_widget       , &spec_cell_base_widget::switch_cell,
        this                    , &spec_if_config_table::switch_cell
    );
    connect(
        line_combo_widget       , &spec_cell_base_widget::switch_cell_back,
        this                    , &spec_if_config_table::switch_cell_back
    );
    
    // Create and set the fifth column element: The data width of the field
    spin_box_widget     = new spec_cell_spin_box_widget();
    spin_box_widget     -> set_indices( row_index, 4 );
    spin_box_widget     -> set_value( 1 );
    spin_box_widget     -> set_minimum_value( 0 );
    spin_box_widget     -> set_maximum_value( 65536 );
    spin_box_widget     -> set_tool_tip( "This indicates th bit-width of your field. Value 0 indicates that bit-width is not considered" );
    spin_box_widget     -> set_spin_box_geometry( 0, 0, columnWidth(4)-1, cur_row_height-1);
    setCellWidget( row_index, 4, spin_box_widget );
    
    connect(
        spin_box_widget         , &spec_cell_base_widget::switch_cell,
        this                    , &spec_if_config_table::switch_cell
    );
    connect(
        spin_box_widget         , &spec_cell_base_widget::switch_cell_back,
        this                    , &spec_if_config_table::switch_cell_back
    );
    
    // Create and set the sixth column element: The number of nested lists
    spin_box_widget     = new spec_cell_spin_box_widget();
    spin_box_widget     -> set_indices( row_index, 5 );
    spin_box_widget     -> set_value( 0 );
    spin_box_widget     -> set_minimum_value( 0 );
    spin_box_widget     -> set_maximum_value( 65536 );
    spin_box_widget     -> set_tool_tip( "The value indicates the number of dimensions for the item." );
    spin_box_widget     -> set_spin_box_geometry(0, 0, columnWidth(5)-1, cur_row_height-1);
    setCellWidget( row_index, 5, spin_box_widget );
    
    connect(
        spin_box_widget         , &spec_cell_base_widget::switch_cell,
        this                    , &spec_if_config_table::switch_cell
    );
    connect(
        spin_box_widget         , &spec_cell_base_widget::switch_cell_back,
        this                    , &spec_if_config_table::switch_cell_back
    );
    
}

void spec_if_config_table::add_row() {
    int column_count    = columnCount();
    int row_count       = rowCount();
    int selected_row    = -1;
    
    insertRow( row_count );
    
    set_row( row_count );
}

void spec_if_config_table::delete_selected_row() {
    int row_count   = rowCount();
    
    // Error handling for if there was on selected row (ie: empty table)
    if( selected_row < 0 ) {
        // can't delete anything ... abort executing on an empty table
        return;
    };
    
    // Do not allow the last line to be deleted!
    if( row_count >= 2 ) {
        removeRow( selected_row );
        selected_row = -1;
    };
}

void spec_if_config_table::row_selected( int row_index ) {
    selected_row = row_index;
}

bool spec_if_config_table::validate_table              ( bool check_mandatory_elem ) {
    // Assuming all values are set. A single mistake will ensure that this method will return false
    bool    result      = true;
    bool    is_valid    = true;
    int     row_count   = rowCount();
    bool    is_name_empty;
    bool    is_type_empty;
    
    for( int row_index = 0; row_index < row_count; ++row_index ) {
        is_name_empty   = static_cast<spec_cell_line_edit_widget*>(cellWidget(row_index,0))->get_value().isEmpty();;
        is_type_empty   = static_cast<spec_cell_line_combo_box_widget*>(cellWidget(row_index,3))->get_value().isEmpty();
        
        if( check_mandatory_elem == true ) {
            // Checking the Name field
            is_valid   = not is_name_empty;
            static_cast<spec_cell_line_edit_widget*>(cellWidget(row_index,0)) -> set_invalid_color( not is_valid );
            result = result and is_valid;
            
            // Checking the Type field
            is_valid   = not is_type_empty;
            static_cast<spec_cell_line_combo_box_widget*>(cellWidget(row_index,3)) -> set_invalid_color( not is_valid );
            result = result and is_valid;
        } else {
            if( (is_name_empty and not is_type_empty) or (not is_name_empty and is_type_empty) ) {
                static_cast<spec_cell_line_edit_widget*>(cellWidget(row_index,0)) -> set_invalid_color( is_name_empty );
                result = result and not is_name_empty;
                static_cast<spec_cell_line_combo_box_widget*>(cellWidget(row_index,3)) -> set_invalid_color( is_type_empty );
                result = result and not is_type_empty;
            };
        };
    };  // end of iterating over each single line
    
    return result;
}

QList<QString*> spec_if_config_table::get_row_data( int row_index ) {
    QList<QString*> result;
    QString calculated;
    
    // Is item generatable/randomized
    result.append( new QString(static_cast<spec_cell_check_box_widget*>(cellWidget( row_index, 1))->get_value()) );
    
    // Is item Physical item
    calculated =  static_cast<spec_cell_check_box_widget*>(cellWidget( row_index, 2))->get_value() == "true"? "false" : "true";
    result.append( new QString(calculated) );
    
    // Item Field Name
    result.append( new QString(static_cast<spec_cell_line_edit_widget*>(cellWidget( row_index, 0))->get_value()) );
    
    // Item Data Type
    calculated = static_cast<spec_cell_line_combo_box_widget*>(cellWidget( row_index, 3))->get_value();
    result.append( new QString( calculated ) );
    
    // Item Bit-Width
    // Only int based data types will get the option to have a defined bit-width. For any other data type, the bit-width
    // will be ignored.
    if( calculated == "uint" or calculated == "int" or calculated == "longuint" or calculated == "longint") {
        result.append( new QString(static_cast<spec_cell_spin_box_widget*>(cellWidget( row_index, 4))->get_value()) );
    } else {
        result.append( new QString( "" ) );
    };
    
    // Number of nested lists
    result.append( new QString(static_cast<spec_cell_spin_box_widget*>(cellWidget( row_index, 5))->get_value()) );
    
    // DUMMY VALUE
    result.append( new QString("") );
    
    return result;
}

void spec_if_config_table::switch_cell( int row, int column ) {
    int row_count       = rowCount();
    int column_count    = columnCount();
    
    bool is_last_row    = (row      == row_count-1);
    bool is_last_column = (column   == column_count-1);
    
    // This is a convenience feature that allows you to simlpy add a new line by using the TAB key after the last element.
    if( is_last_row and is_last_column ) {
        add_row();
        static_cast<spec_cell_base_widget*>(cellWidget( row+1, 0 ))->set_focus();
    } else {
        if( is_last_column ) {
            // Switch to next row: (row+1 / 0)
            static_cast<spec_cell_base_widget*>(cellWidget( row+1, 0 ))->set_focus();
        } else {
            // Switch to next column: row / column+1
            static_cast<spec_cell_base_widget*>(cellWidget( row, column+1 ))->set_focus();
        };
    };
}

void spec_if_config_table::switch_cell_back( int row, int column ) {
    int row_count           = rowCount();
    int column_count        = columnCount();
    
    bool is_first_row       = (row      == 0);
    bool is_first_column    = (column   == 0);
    
    if( is_first_row and is_first_column) {
        // Implementing wrap-around semantics
        static_cast<spec_cell_base_widget*>(cellWidget( row_count-1, column_count-1 ))->set_focus();
    } else {
        if( is_first_column ) {
            // Switch to next row: (row-1 / 0)
            static_cast<spec_cell_base_widget*>(cellWidget( row-1, column_count-1 ))->set_focus();
        } else {
            // Switch to next column: row / column+1
            static_cast<spec_cell_base_widget*>(cellWidget( row, column-1 ))->set_focus();
        }
    };
}
