// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_cell_spin_box_widget.h"

spec_cell_spin_box_widget::spec_cell_spin_box_widget() {
    spin_box = new QSpinBox();
    spin_box    -> setFont( QFont("Courier New") );
    spin_box    -> setParent( this );
    
    spin_box    -> installEventFilter( this );
    
    connect(
        spin_box    , SIGNAL( valueChanged(int) ),
        this        , SLOT  ( value_changed(int) )
    );
}

void spec_cell_spin_box_widget::set_spin_box_geometry   (
    int x,
    int y,
    int width,
    int height
) {
    spin_box    -> setGeometry( x, y, width, height );
}

void spec_cell_spin_box_widget::set_value               ( int value ) {
    spin_box    -> setValue( value );
}
void spec_cell_spin_box_widget::set_minimum_value       ( int value ) {
    spin_box    -> setMinimum( value );
}
void spec_cell_spin_box_widget::set_maximum_value       ( int value ) {
    spin_box    -> setMaximum( value );
}
void spec_cell_spin_box_widget::set_tool_tip            ( const QString &text ) {
    spin_box    -> setToolTip( text );
}

void spec_cell_spin_box_widget::value_changed           ( int value ) {
    emit cell_content_change( row_index, column_index, QString::number( value ));
}

QString spec_cell_spin_box_widget::get_value() {
    return QString::number(spin_box -> value());
}

void spec_cell_spin_box_widget::set_focus() {
    spin_box   -> setFocus();
}
