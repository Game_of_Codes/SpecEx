// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_AGENT_WIDGET_H
#define SPEC_AGENT_WIDGET_H

#include <QComboBox>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QObject>
#include <QPushButton>
#include <QSpinBox>
#include <QVector>
#include <QWidget>

#include <BaseClasses/spec_logger.h>
#include <Widgets/spec_combo_box.h>

#include <QDebug>

class spec_agent_widget : public QWidget {
    Q_OBJECT
    public:
        spec_agent_widget();
        spec_agent_widget( spec_agent_widget *other );
        
        void                initialize();
        
        void                set_interface_domain_list(
            QList<QString>  domains
        );
        QList<QString>      get_interface_domain_list();
        QString             get_selected_interface_domain();
        
        void                set_active_passive(
            bool    is_active,
            bool    enable_edit
        );
        QString             get_active_passive();
        
        void                set_clock_domain_list(
            QList<QString>  domains
        );
        QString             get_selected_clock_domain();
        
        void                set_reset_domain_list(
            QList <QString> domains
        );
        QString             get_selected_reset_domain       ();
        QString             get_hdl_path                    ();
        void                print_widget_info               ();
        QList<QString>      get_incomplete_config_elements  ();
        
    signals:
    
    public slots:
        
    private:
        QGroupBox               *agent_group_box;
        QLabel                  *agent_if_name_label;
        QLabel                  *active_passive_label;
        QLabel                  *hdl_hierarchy_label;
        QLabel                  *clock_domain_label;
        QLabel                  *reset_domain_label;
        
        
        spec_combo_box          *agent_if_name_combo_box;
        QComboBox               *active_passive_combo_box;
        QLineEdit               *hdl_hierarchy_line_edit;
        QComboBox               *clock_domain_combo_box;
        QComboBox               *reset_domain_combo_box;
};

#endif // SPEC_AGENT_WIDGET_H
