// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_TLM_PORT_WIDGET_H
#define SPEC_TLM_PORT_WIDGET_H

#include <QCheckBox>
#include <QComboBox>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QObject>
#include <QPushButton>
#include <QSpinBox>
#include <QVector>
#include <QWidget>

#include <BaseClasses/spec_logger.h>
#include <Widgets/spec_combo_box.h>


class spec_tlm_port_widget : public QWidget {
    Q_OBJECT
    public:
        spec_tlm_port_widget();
        spec_tlm_port_widget( spec_tlm_port_widget *other );
        
        
        void    initialize();
        
        void    set_port_data_type      ( QString type,         bool enable_edit );
        void    set_port_direction      ( QString direction,    bool enable_edit );
        void    set_port_object_name    ( QString name,         bool enable_edit );
        void    set_port_kind           ( QString port_kind,    bool enable_edit );
        
        
    signals:
        
    public slots:
        
        
    private slots:
        
    private:
        QGroupBox       *port_group_box;
        QLabel          *port_name_label;
        QLabel          *port_tlm_type_label;
        QLabel          *port_data_type_label;
        QLabel          *port_direction_label;
        
        QLineEdit       *port_name_line_edit;
        QComboBox       *port_tlm_type_combo_box;
        spec_combo_box  *port_data_type_combo_box;
        QComboBox       *port_direction_combo_box;
};

#endif // SPEC_TLM_PORT_WIDGET_H
