// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_IF_CONFIG_TABLE_H
#define SPEC_IF_CONFIG_TABLE_H

#include <QHeaderView>
#include <QLayout>
#include <QObject>
#include <QCheckBox>
#include <QSpinBox>
#include <QTableWidget>

#include <BaseClasses/spec_logger.h>
#include <Widgets/spec_cell_check_box_widget.h>

#include <Widgets/spec_combo_box.h>
#include <Widgets/spec_cell_line_combo_box_widget.h>
#include <Widgets/spec_cell_combo_box_widget.h>
#include <Widgets/spec_cell_line_edit_widget.h>
#include <Widgets/spec_cell_spin_box_widget.h>


class spec_if_config_table : public QTableWidget {
    Q_OBJECT
        
    public:
        spec_if_config_table();
        
        void    initialize                  ();
        void    row_selected                ( int row_index );
        
        bool    validate_table              ( bool check_mandatory_elem );
        
    public slots:
        void            add_row             ();
        void            set_row             ( int row_index );
        void            delete_selected_row ();
        
        QList<QString*> get_row_data        ( int row_index );
        
    private slots:
        void            switch_cell     ( int row, int column );
        void            switch_cell_back( int row, int column );
        
    private:
        int             selected_row    = -1;
};

#endif // SPEC_IF_CONFIG_TABLE_H
