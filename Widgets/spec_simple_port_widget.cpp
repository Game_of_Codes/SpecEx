// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_simple_port_widget.h"

spec_simple_port_widget::spec_simple_port_widget() {
    
}

spec_simple_port_widget::spec_simple_port_widget( spec_simple_port_widget *other ) {
    initialize();
    
    // Now copy all the values manually
    port_name_value_line_edit               -> setText( other->port_name_value_line_edit->text() );
    port_name_value_line_edit               -> setDisabled( not other->port_name_value_line_edit->isEnabled() );
    
    port_direction_value_line_edit          -> setText( other->port_direction_value_line_edit->text() );
    port_direction_value_line_edit          -> setDisabled( not other->port_direction_value_line_edit->isEnabled() );
    
    // port_direction_value_combo_box
    for( int cb_index = 0; cb_index < other->port_direction_value_combo_box->count(); ++cb_index ) {
        port_direction_value_combo_box->addItem( other->port_direction_value_combo_box->itemText( cb_index) );
    };
    
    port_direction_value_combo_box          -> setCurrentIndex( other->port_direction_value_combo_box->currentIndex() );
    port_direction_value_combo_box          ->setEnabled( not other->port_direction_value_combo_box->isEnabled() );
    
    port_signal_name_line_edit              -> setText( other->port_signal_name_line_edit->text() );
    port_signal_name_line_edit              -> setDisabled( not other->port_signal_name_line_edit->isEnabled() );
    
    port_type_value_line_edit               -> setText( other->port_type_value_line_edit->text() );
    port_type_value_line_edit               -> setDisabled( not other->port_type_value_line_edit->isEnabled() );
    
    hdl_hierarchy_line_edit                 -> setText( other->hdl_hierarchy_line_edit->text() );
    hdl_hierarchy_line_edit                 -> setDisabled( not other->hdl_hierarchy_line_edit->isEnabled() );
    
    is_clock_domain_clock_signal_check_box  -> setCheckState( other->is_clock_domain_clock_signal_check_box->checkState() );
    
    is_reset_domain_reset_signal_check_box  -> setCheckState( other->is_reset_domain_reset_signal_check_box->checkState() );
    
}

void spec_simple_port_widget::initialize() {
    port_group_box                          = new QGroupBox();
    port_name_label                         = new QLabel();
    port_direction_label                    = new QLabel();
//    port_signal_name_label                  = new QLabel();
    port_type_label                         = new QLabel();
    hdl_hierarchy_label                     = new QLabel();
    clock_domain_label                      = new QLabel();
    reset_domain_label                      = new QLabel();
    
    port_name_value_line_edit               = new QLineEdit();
    port_direction_value_line_edit          = new QLineEdit();
    port_direction_value_combo_box          = new QComboBox();
    port_signal_name_line_edit              = new QLineEdit();
    port_type_value_line_edit               = new QLineEdit();
    hdl_hierarchy_line_edit                 = new QLineEdit();
    is_clock_domain_clock_signal_check_box  = new QCheckBox();
    is_reset_domain_reset_signal_check_box  = new QCheckBox();
    
    port_group_box                          -> setTitle( "Simple Port Details" );
    port_group_box                          -> setGeometry(  10,  10,500, 400 );
    
    port_name_label                         -> setText( "Port Object Name" );
    port_name_label                         -> setGeometry(  15,  25, 200,  30 );
    
    port_direction_label                    -> setText( "Direction" );
    port_direction_label                    -> setGeometry(  15,  75, 200,  30 );
    
//    port_signal_name_label                  -> setText( "Signal Name" );
//    port_signal_name_label                  -> setGeometry(  15, 125, 200,  30 );
    
    port_type_label                         ->setText( "Signal Type" );
    port_type_label                         -> setGeometry(  15, 175, 200,  30 );
    
    hdl_hierarchy_label                     -> setText( "HDL Hierarchy" );
    hdl_hierarchy_label                     -> setGeometry(  15, 225, 200,  30 );
    
    clock_domain_label                      -> setText( "Signal is used for CLOCK" );
    clock_domain_label                      -> setToolTip   ( "Please note that Event Port can only either be CLOCK or reset, not both at the same time." );
    clock_domain_label                      -> setGeometry(  15, 275, 200,  30 );
    
    reset_domain_label                      -> setText( "Signal is used for RESET" );
    reset_domain_label                      -> setToolTip   ( "Please note that Event Port can only either be RESET or clock, not both at the same time." );
    reset_domain_label                      -> setGeometry(  15, 325, 200,  30 );
    
    
    port_name_value_line_edit               -> setGeometry( 250,  25, 200,  30 );
//    port_direction_value_line_edit          -> setGeometry( 250,  75, 200,  30 );
    port_direction_value_combo_box          -> addItems   ( {"Signal Write Access", "Read Only Signal"} );
    port_direction_value_combo_box          -> setGeometry( 250,  75, 200,  30 );
    port_signal_name_line_edit              -> setGeometry( 250, 125, 200,  30 );
    port_type_value_line_edit               -> setGeometry( 250, 175, 200,  30 );
    hdl_hierarchy_line_edit                 -> setGeometry( 250, 225, 200,  30 );
    is_clock_domain_clock_signal_check_box  -> setGeometry( 250, 275,  30,  30 );
    is_clock_domain_clock_signal_check_box  -> setToolTip   ( "Please note that Event Port can only either be CLOCK or reset, not both at the same time." );
    is_reset_domain_reset_signal_check_box  -> setGeometry( 250, 325,  30,  30 );
    is_reset_domain_reset_signal_check_box  -> setToolTip   ( "Please note that Event Port can only either be RESET or clock, not both at the same time." );
    
    
    port_group_box                          -> setParent( this );
    port_name_label                         -> setParent( port_group_box );
    port_direction_label                    -> setParent( port_group_box );
//    port_signal_name_label                  -> setParent( port_group_box );
    port_type_label                         -> setParent( port_group_box );
    hdl_hierarchy_label                     -> setParent( port_group_box );
    clock_domain_label                      -> setParent( port_group_box );
    reset_domain_label                      -> setParent( port_group_box );
    
    port_name_value_line_edit               -> setParent( port_group_box );
//    port_direction_value_line_edit          -> setParent( port_group_box );
    port_direction_value_combo_box          -> setParent( port_group_box );
    port_signal_name_line_edit              -> setParent( port_group_box );
    port_type_value_line_edit               -> setParent( port_group_box );
    hdl_hierarchy_line_edit                 -> setParent( port_group_box );
    is_clock_domain_clock_signal_check_box  -> setParent( port_group_box );
    is_reset_domain_reset_signal_check_box  -> setParent( port_group_box );
    
    connect(
        is_clock_domain_clock_signal_check_box  , &QCheckBox::stateChanged,
        this                                    , &spec_simple_port_widget::clock_domain_check_box_state_changed
    );
    
    connect(
        is_reset_domain_reset_signal_check_box  , &QCheckBox::stateChanged,
        this                                    , &spec_simple_port_widget::reset_domain_check_box_state_changed
    );
}

void spec_simple_port_widget::set_port_name       (
    QString name,
    bool is_editable
) {
    port_name_value_line_edit -> setText( name );
    port_name_value_line_edit -> setDisabled( not is_editable);
}
void spec_simple_port_widget::set_access_direction(
    QString direction,
    bool    is_editable
) {
    QString direction_string;
    if( direction == "in" ) {
        port_direction_value_combo_box->setCurrentIndex( 1 );//direction_string = "HDL Read Access only";
    } else {    // handle "inout" and "out" signal access
        port_direction_value_combo_box->setCurrentIndex( 0 );
//        direction_string = "HDL Write Access only";
    };
//    port_direction_value_line_edit -> setText( direction_string );
    port_direction_value_line_edit -> setDisabled( not is_editable);
}
void spec_simple_port_widget::set_signal_name     (
    QString name,
    bool    is_editable
) {
    port_signal_name_line_edit -> setText( name );
    port_signal_name_line_edit -> setDisabled( not is_editable );
}
void spec_simple_port_widget::set_type_value      (
    QString value,
    bool    is_editable
) {
    port_type_value_line_edit -> setText( value );
    port_type_value_line_edit -> setDisabled( not is_editable );
}
void spec_simple_port_widget::set_hdl_hierarchy   (
    QString hierarchy,
    bool    is_editable
) {
    hdl_hierarchy_line_edit -> setText( hierarchy );
    hdl_hierarchy_line_edit -> setDisabled( not is_editable );
}

void spec_simple_port_widget::clock_domain_check_box_state_changed(int state) {
    if( state == Qt::Checked ) {
        is_reset_domain_reset_signal_check_box  -> setCheckState( Qt::Unchecked );
        hdl_hierarchy_line_edit                 -> setEnabled( false );
    } else {
        if( is_reset_domain_reset_signal_check_box-> checkState() == Qt::Unchecked ) {
            hdl_hierarchy_line_edit                -> setEnabled( true );
        };
    };
}

void spec_simple_port_widget::reset_domain_check_box_state_changed(int state) {
    if( state == Qt::Checked ) {
        is_clock_domain_clock_signal_check_box  -> setCheckState( Qt::Unchecked );
        hdl_hierarchy_line_edit                 -> setEnabled( false );
    } else {
        if( is_clock_domain_clock_signal_check_box -> checkState() == Qt::Unchecked  ) {
            hdl_hierarchy_line_edit                 -> setEnabled( true );
        };
    };
}

bool spec_simple_port_widget::is_associated_clock             () {
    return ( is_clock_domain_clock_signal_check_box->checkState() == Qt::Checked );
}
bool spec_simple_port_widget::is_associated_reset             () {
    return ( is_reset_domain_reset_signal_check_box->checkState() == Qt::Checked );
}

QString spec_simple_port_widget::get_access_direction            () {
    return port_direction_value_combo_box->currentText();
}
QString spec_simple_port_widget::get_signal_name                 () {
    // TODO: FIX THE NAMING
    return hdl_hierarchy_line_edit->text();
}

QString spec_simple_port_widget::get_signal_type                 () {
    return port_type_value_line_edit->text();
}
int spec_simple_port_widget::get_signal_type_width           () {
    QString type = port_type_value_line_edit->text();
    
    if( type.contains( "bits" ) ) {
        // This is where the custom bit-width get extracted
    } else {
        // Extract the bit-widths for predefined types
        if( type == "bit" ) {
            return 1;
        };
        if( type == "nibble" ) {
            return 4;
        };
        if( type == "byte" ) {
            return 8;
        };
        if( type == "uint" or type == "int") {
            return 32;
        };
        if( type == "longint" or type == "longuint" or type == "time" ) {
            return 64;
        };
    };
    // Return a default value of 1 to avoid
    spec_msg( "An unsupported simple_port bit-width has been detected on type " << type );
    return 1;
}

void spec_simple_port_widget::print_widget_info      () {
    QList<QString>  message_output;
    message_output.append( " SIMPLE PORT Widget:"           );
    message_output.append( "  Port Name          : " + port_name_value_line_edit -> text()          );
    message_output.append( "  Direction          : " + port_direction_value_line_edit->text()       );
    message_output.append( "  HDL Hierarchy Path : " + hdl_hierarchy_line_edit -> text()            );
    for( QString m : message_output ) {
        spec_msg( m );
    };
}

QList<QString> spec_simple_port_widget::get_incomplete_config_elements() {
    QList<QString>  result;
    
    if( hdl_hierarchy_line_edit == nullptr ) {
        result.append( "Simple port configuration has not been configured." );
    } else {
        if( hdl_hierarchy_line_edit->text().isEmpty() ) {
            result.append( "Simple port " + port_name_value_line_edit->text() + " has no given HDL signal name." );
        };
    };
    
    return result;
}
