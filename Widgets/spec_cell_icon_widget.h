// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_CELL_ICON_WIDGET_H
#define SPEC_CELL_ICON_WIDGET_H

#include <QHBoxLayout>
#include <QIcon>

#include <Widgets/spec_cell_base_widget.h>

class spec_cell_icon_widget : public spec_cell_base_widget {
    Q_OBJECT
        
    public:
        spec_cell_icon_widget();
        
        void        set_line_combo_box_geometry (
                int x,
                int y,
                int width,
                int height
                );
        
        void        set_focus                   ();
        
    signals:
        
    public slots:
        
    private:
        
};

#endif // SPEC_CELL_ICON_WIDGET_H
