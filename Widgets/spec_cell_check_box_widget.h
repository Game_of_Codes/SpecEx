// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_CELL_CHECK_BOX_WIDGET_H
#define SPEC_CELL_CHECK_BOX_WIDGET_H

#include <QCheckBox>
#include <QHBoxLayout>
#include <QString>

#include <Widgets/spec_cell_base_widget.h>

class spec_cell_check_box_widget : public spec_cell_base_widget {
    Q_OBJECT
    
    public:
        spec_cell_check_box_widget();
        
        void            set_check_state ( Qt::CheckState state );
        Qt::CheckState  get_check_state ();
        
        void            set_tool_tip    ( const QString &text );
        QString         get_value       ();
        
        void            set_focus       ();
        
    signals:
        
    public slots:
        
    private:
        QHBoxLayout     *hbox_layout;
        QCheckBox       *check_box;
};

#endif // SPEC_CELL_CHECK_BOX_WIDGET_H
