// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_INTERFACE_WAVE_TABLE_H
#define SPEC_INTERFACE_WAVE_TABLE_H

#include <QComboBox>
#include <QHeaderView>
#include <QMessageBox>
#include <QObject>
#include <QTableWidget>

#include <BaseClasses/spec_logger.h>
#include <spec_imagecache.h>
#include <Widgets/spec_interface_wave_cell_widget.h>
#include <Widgets/spec_cell_combo_box_widget.h>


class spec_interface_wave_table : public QTableWidget {
    Q_OBJECT
    
    public:
        spec_interface_wave_table   ();
        spec_interface_wave_table   ( spec_interface_wave_table *other );
        
        void    initialize          ();
        
        void            set_scenario_name   ( QString name );
        QString         get_scenario_name   ();
        
        QString         get_signal_name     ( int row );
        bool            is_signal_write     ( int row );
        int             get_time_step_count ();
        QList<int>      get_signal_flow     ( int row );
        
        void            set_signal_width    (
                int     row,
                bool    is_bit,
                int     width
        );
        
        void            print_waves         ();
        
    public slots:
        void    add_signal          ();
        void    add_step            ();
        
        void    update_signal       (
                    int         row,
                    int         column,
                    QString     name
        );
        
        void    remove_signal       (  int row_index );
        void    remove_step         ();
        
        void    process_clicked_cell( int row, int column       );
        void    update_signal_flow  ( int row, int start_column,int prev_wave_state );
        
        void    update_clock_name   ( QString name );
        void    update_reset_name   ( QString name );
        
    private:
        QString                         scenario_name;
        spec_image_cache                *image_cache;
        spec_interface_wave_cell_widget *current_cell_widget;
        spec_interface_wave_cell_widget *previous_cell_widget;
        spec_interface_wave_cell_widget *next_cell_widget;
        
        int     get_new_state(
                int prev_state,
                int cur_state,
                int next_state
        );
        bool    is_consistent_state(
                int prev_state,
                int cur_state,
                int next_state
        );
        QString state_to_string     ( int state );
};

#endif // SPEC_INTERFACE_WAVE_TABLE_H
