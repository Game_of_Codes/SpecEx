// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_config_params_widget.h"

spec_config_params_widget::spec_config_params_widget() {
    initialize();
}

spec_config_params_widget::spec_config_params_widget( spec_config_params_widget *other ) {
    initialize();
    
    QString name, type, value, class_name;
    
    int     row_count = other -> field_table_widget->rowCount();
    int     col_count = other -> field_table_widget->columnCount();
    
    if( row_count > 0 ) {
        // Create a copy of the passed field_table_widget object
        delete field_table_widget;
        field_table_widget  = new QTableWidget();
        field_table_widget -> setColumnCount( 3 );
        field_table_widget -> setRowCount( row_count );
        field_table_widget -> setHorizontalHeaderItem( 0 , new QTableWidgetItem( "Name" ) );
        field_table_widget -> setHorizontalHeaderItem( 1 , new QTableWidgetItem( "Type" ) );
        field_table_widget -> setHorizontalHeaderItem( 2 , new QTableWidgetItem( "Value" ) );
        
        field_table_widget  -> setGeometry( 15, 20, 450, 275 );
        field_table_widget  -> setParent( config_group_box );
        
        for( int row_index = 0; row_index < row_count; ++row_index ) {
            for( int col_index = 0; col_index < col_count; ++col_index ) {
                class_name = other->field_table_widget->cellWidget(row_index, col_index )->metaObject()->className();
                if( class_name == "QLabel" ) {
                    QLabel *cell_label = new QLabel();
                    cell_label -> setText( static_cast<QLabel*>(other->field_table_widget->cellWidget(row_index, col_index))->text());
                    field_table_widget->setCellWidget(row_index, col_index, cell_label );
                    continue;
                };
                
                if( class_name == "QComboBox" ) {
                    // Get the original combo box to avoid using those cast operations
                    QComboBox *original_combo_box = static_cast<QComboBox*>(other->field_table_widget->cellWidget(row_index, col_index));
                    // Create a new combo box widget
                    QComboBox *cb = new QComboBox();
                    
                    // Copy all of the values from the original to the copy combo box
                    for( int cb_index = 0; cb_index < original_combo_box->count(); ++cb_index ) {
                        cb->addItem( original_combo_box->itemText(cb_index) );
                    };
                    // and preserve the current index as well
                    cb->setCurrentIndex( original_combo_box->currentIndex() );
                    
                    field_table_widget->setCellWidget(row_index, col_index, cb );
                    continue;
                };
                
                if( class_name == "spec_combo_box" ) {
                    spec_combo_box  *original_combo_box = static_cast<spec_combo_box*>(other->field_table_widget->cellWidget(row_index, col_index) );
                    spec_combo_box  *cb = new spec_combo_box();
                    
                    // We are not counting the empty line of the spec_combo_box, hence we start copying values from index 1!
                    for( int cb_index = 1; cb_index < original_combo_box->count(); ++cb_index ) {
                        cb->addItem( original_combo_box->itemText( cb_index ) );
                    };  // end of copying each of the items from the original spec_combo_box
                    
                    cb->setCurrentIndex( original_combo_box->currentIndex() );
                    field_table_widget->setCellWidget(row_index, col_index, cb );
                    continue;
                };
            };
        };
    };  // end of copying the spec_table
    
    field_names     = QList<QString>        ( other->field_names );
    field_values    = QList<QList<QString>> ( other->field_values );
}

void spec_config_params_widget::initialize() {
    config_group_box    = new QGroupBox();
    
    config_group_box    -> setTitle( "Configurable Attributes" );
    config_group_box    -> setGeometry(  10, 10, 500, 300 );
    
    field_table_widget = new QTableWidget();
    field_table_widget -> setColumnCount( 3 );
    
    field_table_widget -> setHorizontalHeaderItem( 0 , new QTableWidgetItem( "Name" ) );
    field_table_widget -> setHorizontalHeaderItem( 1 , new QTableWidgetItem( "Type" ) );
    field_table_widget -> setHorizontalHeaderItem( 2 , new QTableWidgetItem( "Value" ) );
    
    field_table_widget  -> setGeometry( 15, 20, 450, 275 );
    
    config_group_box    -> setParent( this );
    field_table_widget  -> setParent( config_group_box );
}

void spec_config_params_widget::add_field_info(
    int     field_index,
    QString name,
    QString type,
    QString value
) {
    spec_msg( "[" + QString::number(field_index) + "]: " << name << "  " << type << "  " << value );
    int current_row_index = field_table_widget->rowCount();
    
    // Here we'll ensure that we don't duplicate the field upon updating them over and over again
    if( field_index <= current_row_index-1 ) {
        if( static_cast<QLabel*>(field_table_widget->cellWidget( field_index, 0 ))->text() == name ) {
            return;
        };
    };
    QLabel  *name_label = new QLabel( QString(name) );
    QLabel  *type_label = new QLabel( QString(type) );
    
    field_table_widget -> insertRow( current_row_index );
    
    field_table_widget  -> setCellWidget( current_row_index, 0, name_label );
    field_table_widget  -> setCellWidget( current_row_index, 1, type_label );
    
    
    int fld_index = field_names.indexOf( name );
    if( fld_index >= 0 ) {
        spec_combo_box *combo_box = new spec_combo_box();
        combo_box -> addItems( field_values.at( fld_index) );
        field_table_widget -> setCellWidget( current_row_index, 2, combo_box );
    } else {
        if( type == "bool" ) {
            QComboBox *combo_box = new QComboBox();
            combo_box -> addItems( {"TRUE","FALSE"} );
            field_table_widget -> setCellWidget( current_row_index, 2, combo_box );
        };
        field_table_widget -> setItem( current_row_index, 2, new QTableWidgetItem( QString(value) ));
    };
}

void spec_config_params_widget::set_field_value( int index, QString value ) {
    
}

QList<QString> spec_config_params_widget::get_field_value( int index ) {
    QList<QString> result;
    
    
    
}

bool spec_config_params_widget::has_field_name( QString name ) {
    return field_names.contains( name );
}

void spec_config_params_widget::add_field_enum_type(
    QString         name,
    QList<QString>  enum_values
) {
    field_names.append( name );
    field_values.append( enum_values );
}

int spec_config_params_widget::get_line_count  () {
    return field_table_widget->rowCount();
}
QList<QString> spec_config_params_widget::get_line_at     ( int index ) {
    QList<QString>  result;
    QWidget         *cell_widget =  field_table_widget->cellWidget( index, 2 );
    QString         class_name;
    if( cell_widget == nullptr ) {
        class_name  = "";
    } else {
        class_name  = cell_widget->metaObject()->className();
    };
    
    // First column is the name of the field
    result.append( static_cast<QLabel*>(field_table_widget->cellWidget( index, 0 ))->text() );
    
    // Second column represents the type of the field
    result.append( static_cast<QLabel*>(field_table_widget->cellWidget( index, 1 ))->text() );
    
    // Third column is a bit more tricky, since the widget in that cell is not always the same widget type...
    
    if( class_name == "spec_combo_box" ) {
        // Extracting the information of the item, we first need to cast it to the already detected widget class
        // ... and then get the comboBox currentText value.
        result.append(
            static_cast<spec_combo_box*>( cell_widget ) -> currentText()
        );
    };
    
    if( class_name == "QComboBox" ) {
        result.append(
            static_cast<QComboBox*>( cell_widget ) -> currentText()
        );
    };
    
    if( class_name == "QSpinBox" ) {
        result.append(
            QString::number(
                static_cast<QSpinBox*>( cell_widget )->value()
            )
        );
    };
    
    if(  class_name.isEmpty() ) {
        result.append( field_table_widget->item( index, 2 )->text() );
    };
    
    return result;
}
QList<QList<QString>> spec_config_params_widget::get_all_lines   () {
    
}
