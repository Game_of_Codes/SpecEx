// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_cell_base_widget.h"

spec_cell_base_widget::spec_cell_base_widget() {
    
}

void spec_cell_base_widget::set_row_index       ( int index ) {
    row_index = index;
}
void spec_cell_base_widget::set_column_index    ( int index ) {
    column_index = index;
}
void spec_cell_base_widget::set_indices         ( int row, int column ) {
    row_index       = row;
    column_index    = column;
}

bool spec_cell_base_widget::eventFilter( QObject *watched, QEvent *event ) {
    if( event -> type() == QEvent::KeyPress ) {
        if( static_cast<QKeyEvent*>(event)->key() == Qt::Key_Tab ) {
            emit switch_cell( row_index, column_index );
        };
        if( static_cast<QKeyEvent*>(event)->key() == Qt::Key_Backtab ) {
            emit switch_cell_back( row_index, column_index );
        };
        return QWidget::eventFilter( watched, event );
    } else {
        // Passing the event to the parent class... if this is left out, then the widget will not show
        return QWidget::eventFilter( watched, event );
    };
}

int spec_cell_base_widget::get_row_index       () {
    return row_index;
}
int spec_cell_base_widget::get_column_index    () {
    return column_index;
}

