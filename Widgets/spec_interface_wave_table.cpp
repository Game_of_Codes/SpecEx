// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_interface_wave_table.h"

spec_interface_wave_table::spec_interface_wave_table() {
    initialize();
}

spec_interface_wave_table::spec_interface_wave_table( spec_interface_wave_table *other ) {
    spec_msg( "Copy constructor" );
    int     row_count       = other->rowCount();
    int     col_count       = other->columnCount();
    
    int     row_height      = 50;
    int     column_width    = 50;
    
    set_scenario_name( other -> get_scenario_name() );
    
    spec_interface_wave_cell_widget *wave_cell;
    
    // Create a new image cache instance
    image_cache = new spec_image_cache();
    
    setGeometry( other->geometry() );
    setParent( static_cast<QWidget*>(other->parent()) );
    
    setColumnCount( col_count );
    
    horizontalHeader()  -> setSectionResizeMode( QHeaderView::Fixed );
    verticalHeader()    -> setSectionResizeMode( QHeaderView::Fixed );
    setHorizontalHeaderItem(0, new QTableWidgetItem( other->horizontalHeaderItem(0)->text() ));
    horizontalHeaderItem( 0 ) -> setFont( other -> horizontalHeaderItem( 0 )->font() );
    setColumnWidth( 0, other -> columnWidth( 0) );
    setStyleSheet( "QTableWidgetItem::item{ padding: 0px }" );
    setShowGrid( false );
    
    // We do not have to copy any links to the current, previous or next cell widgets, because that is handled only
    // within the respective update methods.
    
    // Copy the horizontal and vertical header
    // Copy each table cell
    for( int row_index = 0; row_index < row_count;++row_index ) {
        // To alleviate any additional instantiation, we are using the add_signal method to create the new line
        add_signal();
        // ...and then copy the column header naming and font
        verticalHeaderItem( row_index ) -> setText( other->verticalHeaderItem( row_index )->text() );
        verticalHeaderItem( row_index ) -> setFont( other->verticalHeaderItem( row_index )->font() );
        // ... and also only need to set the first column's access port direction, which is directly copied from the 
        //     cell combo widget
        setCellWidget(
            row_index,
            0,
            new spec_cell_combo_box_widget(
                static_cast<spec_cell_combo_box_widget*>( other->cellWidget( row_index, 0 ) )
            )
        );
        
        // All subsequent columns contain wave cell images
        for( int column_index = 1; column_index < col_count;++column_index ) {
            if( row_index == 0 ) {
                // Set column formatting and width uniformly, based on the other table format only during the first row
                setColumnWidth( column_index, other->columnWidth( column_index) );
                setHorizontalHeaderItem( column_index, new QTableWidgetItem( other->horizontalHeaderItem( column_index )->text() ) );
                horizontalHeaderItem( column_index ) -> setFont( other -> horizontalHeaderItem( column_index)->font() );
            };
            // Create the new wave_cell
            wave_cell = new spec_interface_wave_cell_widget(
                static_cast<spec_interface_wave_cell_widget*>(other->cellWidget( row_index, column_index ))
            );
            // and assign this as the cell widget
            setCellWidget( row_index, column_index, wave_cell );
        };  // end of iterating over each column
        
        connect(
            this, &spec_interface_wave_table::cellClicked,
            this, &spec_interface_wave_table::process_clicked_cell
        );
    };  // end of iterating over each row
}

void spec_interface_wave_table::initialize() {
    // Set the default number of columns and number of rows. Reset and clock are always present as a placeholder
    int     col_count   = 6;
    int     row_count   = 2;
    int     row_height  = 50;
    
    int     image_id    = 0;
    
    QString column_name;
    QString row_name;
    
    // First create the image cache to access the signals!
    image_cache = new spec_image_cache();
    
    // This intermediate cell_widget is used to populate the table view
    spec_interface_wave_cell_widget *cell_widget;
    spec_cell_combo_box_widget      *cell_combo_box_widget;
    
    // Table column creation
    setRowCount     ( row_count );
    setColumnCount  ( col_count );
    
    // Table related configuration
    horizontalHeader()  -> setSectionResizeMode( QHeaderView::Fixed );
    verticalHeader()    -> setSectionResizeMode( QHeaderView::Fixed );
    setStyleSheet( "QTableWidgetItem::item{ padding: 0px }" );
    setShowGrid( false );
    
    // Create and setup columns
    // Create the table by first iterating over each row, then over each row's column
    for( int r_index = 0; r_index < row_count; ++r_index ) {
        // First initialize the header information
        if( r_index == 0 ) {
            row_name = "<Clock>";
        };
        if( r_index == 1 ) {
            row_name = "<Reset>";
        };
        setVerticalHeaderItem( r_index, new QTableWidgetItem( QString( row_name )) );
        verticalHeaderItem( r_index )->setFont( QFont("Courier New") );
        
        // Set the height for each row
        setRowHeight( r_index, row_height );
        
        // Iterating over the columns for each row
        for( int c_index = 0; c_index < col_count; ++c_index ) {
            // Only initialize the column headers once... that is when the first row is being created
            if( r_index == 0 ) {
                if( c_index == 0 ) {
                    column_name = "Access";
                    
                    setColumnWidth( c_index, 100 );
                } else {
                    column_name = QString::number( c_index-1 );
                    
                    // Only set the picture columns to size 50
                    setColumnWidth( c_index, 50 );
                };
                
                // First create the column's header (aka the time step)
                setHorizontalHeaderItem( c_index, new QTableWidgetItem( QString( column_name ) ) );
                // ...and I like to make it look like code
                horizontalHeaderItem( c_index )->setFont( QFont("Courier New" ) );
                
            };  // end of initializing the column headers in the FIRST ROW only
            
            // First create the object, which can then be adapted to each of the information represented in the row
            setItem( r_index, c_index, new QTableWidgetItem() );
            
            if( c_index == 0 ) {
                cell_combo_box_widget   = new spec_cell_combo_box_widget();
                cell_combo_box_widget   -> set_combo_box_geometry( 0, 0, columnWidth( 0 )-1, row_height-1 );
                cell_combo_box_widget   -> add_items( {"Drive", "Read"} );
                if( r_index <= 1 ) {
                    cell_combo_box_widget   -> set_current_index( 1 );
                    cell_combo_box_widget   -> set_enabled( false );
                };
                setCellWidget( r_index, c_index, cell_combo_box_widget );
            } else {
                if( r_index == 0 ) {
                    // Any column other than the first one, is being configured to use a wave cell widget
                    // Also, the first row is reserved to be for the clock signal only!
                    image_id = spec_image_cache::PictureId::PosClock;
                } else if ( r_index == 1 ) {
                    // The second row is reserved for the reset signal only.
                    // We are using active-low as initial value
                    image_id = spec_image_cache::PictureId::Level0;
                } else {
                    // All of the subsequent images are being set to undefined (MvlX)
                    image_id = spec_image_cache::PictureId::MvlX;
                };
                
                cell_widget = new spec_interface_wave_cell_widget(
                    image_cache->get_scaled_wave_image(
                        image_id,
                        50
                    )
                );
                cell_widget ->set_state( image_id );
                
                // Set the cell_widget for any columns > 1
                cell_widget -> set_name         ( row_name );
                cell_widget -> set_width        ( 1 );
                cell_widget -> set_signal_clock ( r_index == 0 );
                cell_widget -> set_signal_reset ( r_index == 1 );
                
                setCellWidget( r_index, c_index, cell_widget );
            };
        };  // end of iterating over columns
    };  // end of iterating over each row
    
    // The signals table has already an empty row defined, hence we need this empty row in here
    add_signal();
    
    setGeometry( 0, 0, 1231, 301 );
    
    connect(
        this, &spec_interface_wave_table::cellClicked,
        this, &spec_interface_wave_table::process_clicked_cell
    );
}

QString spec_interface_wave_table::get_signal_name(int row) {
    return verticalHeaderItem( row )->text();
}

bool spec_interface_wave_table::is_signal_write(int row) {
    return (static_cast<spec_cell_combo_box_widget*>(cellWidget( row, 0 ))->get_value() == "Drive");
}

int spec_interface_wave_table::get_time_step_count() {
    // We are decrementing, because the first column of the table is reserved for the HDL access attribute
    return columnCount()-1;
}

QList<int> spec_interface_wave_table::get_signal_flow( int row ) {
    QList<int> result;
    int column_count    = columnCount();
    for( int col_index = 1; col_index < column_count;++col_index ) {
        result.append(static_cast<spec_interface_wave_cell_widget*>(cellWidget( row, col_index) )->get_state() );
    };
    return result;
}

void spec_interface_wave_table::add_signal          () {
    // Keep track of the number of rows and columns
    int row_count       = rowCount();
    int column_count    = columnCount();
    int row_height      = 50;
    int state_id        = spec_image_cache::PictureId::Level0;
    
    // This intermediate cell_widget is used to populate the table view
    spec_interface_wave_cell_widget *cell_widget;
    spec_cell_combo_box_widget      *cell_combo_box_widget;
    
    // Add a row at the end of the table
    insertRow( row_count );
    
    // Add a new row header
    setVerticalHeaderItem( row_count, new QTableWidgetItem() );
    
    // Ensure the new row's height
    setRowHeight( row_count, row_height );
    
    // Each new added row gets an editable combo-box to make the signal read or writable
    cell_combo_box_widget   = new spec_cell_combo_box_widget();
    cell_combo_box_widget   -> set_combo_box_geometry( 0, 0, columnWidth(0)-1, row_height-1);
    cell_combo_box_widget   -> set_indices( row_count, 0 );
    cell_combo_box_widget   -> add_items( {"Drive", "Read"} );
    cell_combo_box_widget   -> set_current_index( 0 );
    cell_combo_box_widget   -> set_enabled( true );
    
    // The access combo box is always at 0.
    setCellWidget( row_count, 0, cell_combo_box_widget );
    
    for( int c_index = 1; c_index < column_count; ++c_index ) {
        cell_widget = new spec_interface_wave_cell_widget();
        cell_widget -> set_name         ( "INIT" );
        cell_widget -> set_state        ( state_id );
        cell_widget -> set_width        ( 1 );
        cell_widget -> set_image        ( image_cache->get_scaled_wave_image( state_id, 50 ) );
        cell_widget -> set_signal_clock ( false );
        cell_widget -> set_signal_reset ( false );
        setCellWidget( row_count, c_index, cell_widget );
    };
    // We are not iterating over each column element in this call, because we would set wrong information and wast CPU
    // cycles here
}

void spec_interface_wave_table::add_step            () {
    int row_count       = rowCount();
    int column_count    = columnCount();
    
    int prev_state      = -1;
    int new_state       = -1;
    
    spec_interface_wave_cell_widget *new_cell_widget;
    spec_interface_wave_cell_widget  *prev_cell_widget;
    
    // Only adding single column
    insertColumn( column_count );
    setColumnWidth( column_count, 50 );
    setHorizontalHeaderItem( column_count, new QTableWidgetItem( QString::number( column_count-1 ) ) );
    
    for( int r_index = 0; r_index < row_count; ++r_index ) {
        prev_cell_widget    = static_cast<spec_interface_wave_cell_widget*>(cellWidget( r_index, column_count-1));
        
        // The clock row is a special row and handled at index 0
        if( r_index == 0 ) {
            // TODO: Get the clock signal picture, then set the state accordingly
            new_state = spec_image_cache::PictureId::PosClock;
        } else {
            prev_state          = prev_cell_widget -> get_state();
            
            switch( prev_state ) {
                case    spec_image_cache::PictureId::Level0 : {
                    new_state = spec_image_cache::PictureId::Level0;
                    break;
                };
                case    spec_image_cache::PictureId::Transition10 : {
                    new_state = spec_image_cache::PictureId::Level0;
                    break;
                };
                case    spec_image_cache::PictureId::Level1 : {
                    new_state = spec_image_cache::PictureId::Level1;
                    break;
                };
                case    spec_image_cache::PictureId::Transition01 : {
                    new_state = spec_image_cache::PictureId::Level1;
                    break;
                };
                case    spec_image_cache::PictureId::DataConst : {
                    new_state = spec_image_cache::PictureId::DataConst;
                    break;
                };
                case    spec_image_cache::PictureId::DataChange : {
                    new_state = spec_image_cache::PictureId::DataConst;
                    break;
                };
            };  // end of adding new wave item
            
        };
        
        // Now creating the cell widget and setting all the cell attributes
        new_cell_widget = new spec_interface_wave_cell_widget();
        new_cell_widget -> set_name( prev_cell_widget -> get_name() );
        new_cell_widget -> set_width( prev_cell_widget -> get_width() );
        new_cell_widget -> set_state( new_state );
        new_cell_widget -> set_image( image_cache->get_scaled_wave_image( new_state, 50 ) );
        new_cell_widget -> set_signal_clock( prev_cell_widget -> is_clock() );
        new_cell_widget -> set_signal_reset( prev_cell_widget -> is_reset() );
        
        setCellWidget( r_index, column_count, new_cell_widget );
    };  // end of iterating over each line
}

void spec_interface_wave_table::update_signal       (
    int         row,
    int         column,
    QString     name
) {
    spec_interface_wave_cell_widget *cell_widget;
    bool                            is_single_bit   = false;
    bool                            is_update_width = false;
    int                             signal_width    = 0;
    int                             wave_row        = row+2;  // the first two rows are reserved for clock and reset, hence the offset +2
    
    // We need to avoid the case where the row creation is causing a call to the update method as well... and end the method execution
    if( wave_row > verticalHeader()->count()-1 ) {
        return;
    };
    
    switch( column ) {
        case 0 : {  // Handle name changes
            setVerticalHeaderItem   ( wave_row, new QTableWidgetItem( name ) );
            verticalHeaderItem      ( wave_row )->setFont( QFont("Courier New") );
            break;
        };
        case 1 : {  // Handle data type
            spec_msg( "(" +QString::number(row) + "|" + QString::number(column) +"): " + name );
            if( name == "bit" ) {
                is_single_bit = true;
            } else {
                is_single_bit = false;
                if( name == "nibble" ) {
                    signal_width = 4;
                } else if( name == "byte" ) {
                    signal_width = 8;
                } else if( name == "uint"  or name == "int") {
                    signal_width = 32;
                } else if( name == "longuint" or name == "longint" ) {
                    signal_width = 64;
                };
            };
            is_update_width = true;
            break;
        };
        case 2 : {  // Handle signal width
            spec_msg( "(" +QString::number(row) + "|" + QString::number(column) +"): " + name );
            if( name == "1" ) {
                is_single_bit   = true;
                signal_width    = 1;
            } else {
                is_single_bit   = false;
                signal_width    = name.toInt();
            };
            is_update_width = true;
            break;
        };
    };  // end of attribute detection
    
    if( is_update_width ) {
        for( int c_index = 1; c_index < columnCount(); ++c_index ) {
            cell_widget = new spec_interface_wave_cell_widget();
            cell_widget -> set_name ( verticalHeaderItem( row )->text() );
            
            if( column == 1 or column == 2 ) {
                if( is_single_bit ) {
                    cell_widget -> set_width( 1 );
                    cell_widget -> set_state( spec_image_cache::PictureId::Level0 );
                    cell_widget -> set_image( image_cache->get_scaled_wave_image( spec_wave_state::lvl_0, 50) );
                } else {
                    cell_widget -> set_width( signal_width );
                    cell_widget -> set_state( spec_image_cache::PictureId::DataConst );
                    cell_widget -> set_image( image_cache->get_scaled_wave_image( spec_wave_state::data_const, 50) );
                };
            };  // end of updating the bit-width
            setCellWidget( wave_row, c_index, cell_widget );
        };  // end of iterating over each row item
    };  // end of updating the cells, only if an update is requested
}

void spec_interface_wave_table::remove_signal       ( int row_index ) {
    removeRow( row_index +2 );
}

void spec_interface_wave_table::remove_step         () {
    // First, we'll get the number of rows and columns
    int column_count    = columnCount();
    
    if( column_count > 2 ) {
        removeColumn( column_count-1 );
    } else {
        QMessageBox *msg_box = new QMessageBox();
        msg_box -> setText( "There are currently 2 time-steps, removing more time-steps is not permitted." );
        msg_box -> show();
    };
}

// This method will return the new state for the given widget
int spec_interface_wave_table::get_new_state(
    int prev_state,
    int cur_state,
    int next_state
) {
    // The result is initialized as -1, which helps to trap any kind of error, if an unsupported state was detected
    int result = -1;
    
    switch( prev_state ) {
        case    spec_wave_state::lvl_0 : {
            if( cur_state == spec_image_cache::PictureId::Transition01 ) {
                result  = spec_wave_state::lvl_0;
            } else if( cur_state == spec_image_cache::PictureId::Transition10 and next_state == spec_wave_state::lvl_0 ) {
                result  = spec_image_cache::PictureId::Level0;
            } else {
                result  = spec_image_cache::PictureId::Transition01;
            };
            break;
        };
        case    spec_image_cache::PictureId::Transition01 : {
            if( cur_state == spec_image_cache::PictureId::Level1 ) {
                result      = spec_image_cache::PictureId::Transition10;
            } else if( cur_state == spec_image_cache::PictureId::Level0 ){
                result      = spec_image_cache::PictureId::Transition10;
            } else {
                result      = spec_image_cache::PictureId::Level1;
            };
            break;
        };
        case    spec_image_cache::PictureId::Level1 : {
            if( cur_state == spec_image_cache::PictureId::Transition10 ) {
                result      = spec_image_cache::PictureId::Level1;
            } else {
                result      = spec_image_cache::PictureId::Transition10;
            };
            break;
        };
        case    spec_image_cache::PictureId::Transition10 : {
            if( cur_state == spec_image_cache::PictureId::Level0 ) {
                result    = spec_image_cache::PictureId::Transition01;
            } else if( cur_state == spec_image_cache::PictureId::Level1 ) {
                result    = spec_image_cache::PictureId::Transition01;
            } else {
                result    = spec_image_cache::PictureId::Level0;
            };
            break;
        };
            
        // These transitions only take care of multi-bit busses
        case    spec_image_cache::PictureId::DataConst : {
            result    = spec_image_cache::PictureId::DataChange;
            break;
        };
        case    spec_image_cache::PictureId::DataChange : {
            result    = spec_image_cache::PictureId::DataConst;
            break;
        };
    };  // end of handling previous image ID
    
    
    return result;
}

QString spec_interface_wave_table::state_to_string     ( int state ) {
    QString result;
    
    switch( state ) {
        case    spec_image_cache::PictureId::Level0: {
            result  = "LEVEL_0";
            break;
        };
        case    spec_image_cache::PictureId::Transition01: {
            result  = "TRANSITION_01";
            break;
        };
        case    spec_image_cache::PictureId::Level1: {
            result  = "LEVEL_1";
            break;
        };
        case    spec_image_cache::PictureId::Transition10: {
            result  = "TRANSITION_10";
            break;
        };
        case    spec_image_cache::PictureId::DataConst: {
            result  = "DATA_CONST";
            break;
        };
        case    spec_image_cache::PictureId::DataChange: {
            result  = "DATA_CHANGE";
            break;
        };
        case    spec_image_cache::PictureId::PosClock: {
            result  = "POS_CLOCK";
            break;
        };
        case    spec_image_cache::PictureId::NegClock: {
            result  = "NEG_CLOCK";
            break;
        };
    };
    
    return result;
}

bool spec_interface_wave_table::is_consistent_state(
    int prev_state,
    int cur_state,
    int next_state
) {
    bool result = false;
    
    if( next_state == -1 ) {
        return true;
    } else {
        result = (
            (prev_state == spec_wave_state::trans_01 and cur_state == spec_wave_state::lvl_1    and next_state == spec_wave_state::lvl_1)
                    or
            (prev_state == spec_wave_state::trans_01 and cur_state == spec_wave_state::lvl_1    and next_state == spec_wave_state::trans_10)
                    or
            (prev_state == spec_wave_state::trans_01 and cur_state == spec_wave_state::trans_10 and next_state == spec_wave_state::lvl_0)
                    or
            (prev_state == spec_wave_state::trans_01 and cur_state == spec_wave_state::trans_10 and next_state == spec_wave_state::trans_01)
                    or
            (prev_state == spec_wave_state::trans_10 and cur_state == spec_wave_state::lvl_0    and next_state == spec_wave_state::lvl_0)
                    or
            (prev_state == spec_wave_state::trans_10 and cur_state == spec_wave_state::lvl_0    and next_state == spec_wave_state::trans_01)
                    or
            (prev_state == spec_wave_state::trans_10 and cur_state == spec_wave_state::trans_01 and next_state == spec_wave_state::lvl_1)
                    or
            (prev_state == spec_wave_state::trans_10 and cur_state == spec_wave_state::trans_01 and next_state == spec_wave_state::trans_10)
                    or
            (prev_state == spec_wave_state::lvl_0    and cur_state == spec_wave_state::lvl_0    and next_state == spec_wave_state::lvl_0)
                    or
            (prev_state == spec_wave_state::lvl_0    and cur_state == spec_wave_state::lvl_0    and next_state == spec_wave_state::trans_01)
                    or
            (prev_state == spec_wave_state::lvl_0    and cur_state == spec_wave_state::trans_01 and next_state == spec_wave_state::lvl_1)
                    or
            (prev_state == spec_wave_state::lvl_0    and cur_state == spec_wave_state::trans_01 and next_state == spec_wave_state::trans_10)
                    or
            (prev_state == spec_wave_state::lvl_1    and cur_state == spec_wave_state::lvl_1    and next_state == spec_wave_state::lvl_1)
                    or
            (prev_state == spec_wave_state::lvl_1    and cur_state == spec_wave_state::lvl_1    and next_state == spec_wave_state::trans_10)
                    or
            (prev_state == spec_wave_state::lvl_1    and cur_state == spec_wave_state::trans_10 and next_state == spec_wave_state::lvl_0)
                    or
            (prev_state == spec_wave_state::lvl_1    and cur_state == spec_wave_state::trans_10 and next_state == spec_wave_state::trans_01)
        );
    };
    
    return result;
}

void spec_interface_wave_table::update_signal_flow  ( int row, int start_column, int prev_wave_state ) {
    // To make the processing easier, ensure that we know the column count
    int column_count    = columnCount();
    
    // And also use the IDs to calculate which picture update needs to be done
    int     cur_wave_state      = -1;
    int     next_wave_state     = -1;
    
    int     update_wave_state   = -1;
    
    bool    is_last_cell        = false;
    
    // When updating one widget cell, we need to ensure the trickle effect updates the signals appropriately
    for( int c_index = start_column; c_index < column_count; ++c_index ) {
        // Always get the current cell widget
        current_cell_widget = static_cast<spec_interface_wave_cell_widget*>(cellWidget( row, c_index ));
        
        // Extract the current image ID
        cur_wave_state      = current_cell_widget -> get_state();
        
        // Check the last element
        if( c_index == column_count-1 ) {
            is_last_cell    = true;
            next_wave_state = -1;
        } else {
            next_wave_state = static_cast<spec_interface_wave_cell_widget*>(cellWidget( row, c_index+1 ))->get_state();
        };
        
        update_wave_state           = get_new_state( prev_wave_state, cur_wave_state, next_wave_state );
        
        if( is_consistent_state(prev_wave_state, update_wave_state, next_wave_state) ) {
            // Set the state and image
            current_cell_widget -> set_image( image_cache->get_scaled_wave_image( update_wave_state, 50 ) );
            current_cell_widget -> set_state( update_wave_state );
            break;
        };
        
        // Track the current changed image ID to be the previous image ID
        prev_wave_state             = update_wave_state;
        
        // Set the state and image
        current_cell_widget -> set_image( image_cache->get_scaled_wave_image( update_wave_state, 50 ) );
        current_cell_widget -> set_state( update_wave_state );
    };  // end of cycling through each of the columns
}

void spec_interface_wave_table::process_clicked_cell( int row, int column ) {
    spec_msg( "Scenario " + scenario_name + ": CELL ("+ QString::number(row) +"|"+ QString::number(column)  +")" );
    int column_count    = columnCount();
    int cur_image_id    = -1;
    int new_image_id    = -1;
    int prev_image_id   = -1;
    int next_image_id   = -1;
    
    // Only columns after the access column contain the wave-widgets
    if( column > 0 ) {
        current_cell_widget = static_cast<spec_interface_wave_cell_widget*>(cellWidget( row, column ));
        if( column < column_count-1 ) {
            next_image_id = static_cast<spec_interface_wave_cell_widget*>(cellWidget( row, column+1 ))->get_state();
        };
//        spec_msg( "Name  " + current_cell_widget->get_name() );
//        spec_msg( "Width " + QString::number(current_cell_widget->get_width()) );
//        spec_msg( "Is Clock? " << current_cell_widget->is_clock() << "  || Is Reset? " << current_cell_widget->is_reset() );
//        spec_msg( "State " + QString::number(current_cell_widget->get_state()) );
        
        // Special treatment for row 0, since this is the clock
        if( row == 0 ) {
            // We could allow for setting the clock in this row as well... but does that make sense?
        } else {
            // Extract the current image ID for convenient processing
            cur_image_id = current_cell_widget->get_state();
            
            // The multi-bit busses need to treated differently than the single-bit busses
            if(
               (cur_image_id == spec_wave_state::data_const)
                    or
               (cur_image_id == spec_wave_state::data_change)
               ) {
                if( cur_image_id == spec_wave_state::data_const ) {
                    new_image_id    = spec_wave_state::data_change;
                } else {
                    new_image_id    = spec_wave_state::data_const;
                };
                
                // Set the cell widget's image and state
                current_cell_widget -> set_image( image_cache->get_scaled_wave_image(new_image_id, 50 ) );
                current_cell_widget -> set_state( new_image_id );
            } else {
                // The single-bit drawings are more complicated due to the nature of the rising and falling edges
                // This column is used as the init-value definition
                if( column == 1 ) {
                    switch ( cur_image_id ) {
                        case    spec_image_cache::PictureId::Level0 : {
                            new_image_id    = spec_image_cache::PictureId::Transition01;
                            break;
                        };
                        case    spec_image_cache::PictureId::Transition01 : {
                            new_image_id    = spec_image_cache::PictureId::Level1;
                            break;
                        };
                        case    spec_image_cache::PictureId::Level1 : {
                            new_image_id    = spec_image_cache::PictureId::Transition10;
                            break;
                        };
                        case    spec_image_cache::PictureId::Transition10 : {
                            new_image_id    = spec_image_cache::PictureId::Level0;
                            break;
                        };
                        case    spec_wave_state::data_const : {
                            new_image_id    = spec_wave_state::data_change;
                            break;
                        };
                        case    spec_wave_state::data_change : {
                            new_image_id    = spec_wave_state::data_const;
                            break;
                        };
                    };
                    // Increment to point to the next column that needs to be handled
                    ++column;
                } else {    // now handling subsequent columns
                    previous_cell_widget    = static_cast<spec_interface_wave_cell_widget*>( cellWidget( row, column-1 ) );
                    prev_image_id           = previous_cell_widget -> get_state();
                    
                    new_image_id            = get_new_state( prev_image_id, cur_image_id, next_image_id );
                    ++column;
                };  // end of handling subsequent columns
                
                // Set the cell widget's image and state
                current_cell_widget -> set_image( image_cache->get_scaled_wave_image(new_image_id, 50 ) );
                current_cell_widget -> set_state( new_image_id );
                
                if( not is_consistent_state(prev_image_id, new_image_id, next_image_id) ) {
                    // Move on to processing the full signal-flow
                    update_signal_flow( row, column, new_image_id );
                };
                
                
            };  // end of handling single-bit sequence
            
            
        };  // end of handling all the other cells
    };  // end of handling wave widget display
}

void spec_interface_wave_table::update_clock_name   ( QString name ) {
    if( not name.isEmpty() ) {
        verticalHeaderItem( 0 ) -> setText( name );
    } else {
        verticalHeaderItem( 0 ) -> setText( "<Clock>" );
    };
}

void spec_interface_wave_table::update_reset_name   ( QString name ) {
    if( not name.isEmpty() ) {
        verticalHeaderItem( 1 ) -> setText( name );
    } else {
        verticalHeaderItem( 1 ) -> setText( "<Reset>" );
    };
}

void spec_interface_wave_table::set_scenario_name(QString name) {
    scenario_name = name;
}

QString spec_interface_wave_table::get_scenario_name() {
    return scenario_name;
}

void spec_interface_wave_table::set_signal_width(
        int     row,
        bool    is_bit,
        int     width
) {
    
    for( int c_index = 1; c_index < columnCount(); ++c_index ) {
        if( is_bit ) {
            static_cast<spec_interface_wave_cell_widget*>(cellWidget( row+2, c_index ))->change_cell_width(
                is_bit,
                1,
                image_cache -> get_scaled_wave_image( spec_wave_state::lvl_0, 50 )
            );
        } else {
            static_cast<spec_interface_wave_cell_widget*>(cellWidget( row+2, c_index ))->change_cell_width(
                is_bit,
                width,
                image_cache -> get_scaled_wave_image( spec_wave_state::data_const, 50 )
            );
        }
        
    };
}

void spec_interface_wave_table::print_waves         () {
    int row_count       = rowCount();
    int column_count    = columnCount();
    
    spec_interface_wave_cell_widget *cur_cell;
    
    for( int row_index = 0; row_index < row_count;++row_index ) {
        for( int col_index = 1; col_index < column_count;++col_index ) {
            cur_cell = static_cast<spec_interface_wave_cell_widget*>(cellWidget( row_index, col_index) );
//            qDebug() << "(" + QString::number(row_index) + "|" + QString::number(col_index) + "): " +
//                        cur_cell->get_name() + " state " + state_to_string(cur_cell->get_state());
        };
    };
}
