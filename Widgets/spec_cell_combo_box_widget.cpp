// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_cell_combo_box_widget.h"

spec_cell_combo_box_widget::spec_cell_combo_box_widget() {
    combo_box   = new QComboBox();
    combo_box   -> setFont( QFont("Courier New") );
    combo_box   -> setParent( this );
    
    combo_box   -> installEventFilter( this );
    
    connect(
        combo_box   , &QComboBox::currentTextChanged,
        this        , &spec_cell_combo_box_widget::combo_box_text_changed
    );
}

spec_cell_combo_box_widget::spec_cell_combo_box_widget( spec_cell_combo_box_widget *other ) {
    combo_box   = new QComboBox( other->combo_box );
    set_combo_box_geometry(
                other->combo_box->geometry().x(),
                other->combo_box->geometry().y(),
                other->combo_box->geometry().width(),
                other->combo_box->geometry().height()
    );
    for( int it_index = 0; it_index < other->combo_box->count();++it_index ) {
        combo_box->addItem( other->combo_box->itemText( it_index ) );
    };
    combo_box   -> setFont( other->combo_box->font() );
    combo_box   -> setCurrentIndex( other->combo_box->currentIndex() );
    combo_box   -> setParent( this );
    combo_box   -> setEnabled( other->combo_box->isEnabled() );
    
    combo_box   -> installEventFilter( this );
    
    connect(
        combo_box   , &QComboBox::currentTextChanged,
        this        , &spec_cell_combo_box_widget::combo_box_text_changed
    );
}

void spec_cell_combo_box_widget::set_combo_box_geometry(
    int x,
    int y,
    int width,
    int height
) {
    combo_box   -> setGeometry( x, y, width, height );
}

void spec_cell_combo_box_widget::add_items( const QStringList items ) {
    combo_box   -> addItems( items );
}

void spec_cell_combo_box_widget::add_item( const QString item ) {
    combo_box   -> addItem( item );
}

void  spec_cell_combo_box_widget::set_current_index   ( int index ) {
    combo_box -> setCurrentIndex( index );
}
void  spec_cell_combo_box_widget::set_enabled         ( bool enabled ) {
    combo_box -> setEnabled( enabled );
}

int  spec_cell_combo_box_widget::get_current_index   () {
    return combo_box -> currentIndex();
}

QString spec_cell_combo_box_widget::get_value() {
    return combo_box -> currentText();
}

void spec_cell_combo_box_widget::combo_box_text_changed( const QString &text ) {
    emit cell_content_change( row_index, column_index, text );
}

void spec_cell_combo_box_widget::set_focus() {
    combo_box   -> setFocus();
}
