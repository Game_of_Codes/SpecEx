// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_cell_check_box_widget.h"

spec_cell_check_box_widget::spec_cell_check_box_widget() {
    hbox_layout = new QHBoxLayout();
    check_box   = new QCheckBox();
    
    check_box   -> installEventFilter( this );
    
    hbox_layout -> setAlignment( Qt::AlignCenter );
    hbox_layout -> addWidget( check_box );
    setLayout( hbox_layout );
}

void spec_cell_check_box_widget::set_check_state( Qt::CheckState state ) {
    check_box   -> setCheckState( state );
}
Qt::CheckState spec_cell_check_box_widget::get_check_state() {
    return check_box->checkState();
}

void spec_cell_check_box_widget::set_tool_tip( const QString &text ) {
    setToolTip( text );
}

QString spec_cell_check_box_widget::get_value() {
    return (check_box->checkState() == Qt::Checked)? "true" : "false";
}

void spec_cell_check_box_widget::set_focus() {
    check_box   -> setFocus();
}
