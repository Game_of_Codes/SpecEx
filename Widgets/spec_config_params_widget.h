// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_CONFIG_PARAMS_WIDGET_H
#define SPEC_CONFIG_PARAMS_WIDGET_H

#include <QComboBox>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>
#include <QObject>
#include <QPushButton>
#include <QSpinBox>
#include <QVector>
#include <QWidget>

#include <BaseClasses/spec_logger.h>
#include <ModuleWizard/spec_mod_lib_table_widget.h>

#include <Widgets/spec_combo_box.h>

#include <QDebug>

class spec_config_params_widget : public QWidget {
    Q_OBJECT
    
    public:
        spec_config_params_widget   ();
        spec_config_params_widget   ( spec_config_params_widget *other );
        
        void            initialize                  ();
        
        void            add_field_info              (
            int     field_index,
            QString name,
            QString type,
            QString value
        );
        
        
        void            set_field_value ( int index, QString value );
        QList<QString>  get_field_value ( int index );
        bool            has_field_name( QString name );
        void            add_field_enum_type(
            QString         name,
            QList<QString>  enum_values
        );
        
        int                     get_line_count  ();
        QList<QString>          get_line_at     ( int index );
        QList<QList<QString>>   get_all_lines   ();
        
    signals:
        
    public slots:
        
    private:
        QGroupBox                   *config_group_box;
        
        // Build a table that is based on the cur_node's fields.
        // Each field has its own line, with the first cell being its name
        // The second cell being its type and the third cell being its value.
        // Based on the widget instantiation, the table's cell can be edited or will be
        // Left disabled
//        spec_struct_data_node       *cur_node;
        
        QTableWidget               *field_table_widget;
        QList<QString>              field_names;
        QList<QList<QString>>       field_values;
};

#endif // SPEC_CONFIG_PARAMS_WIDGET_H
