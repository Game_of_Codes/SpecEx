// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_tlm_port_widget.h"

spec_tlm_port_widget::spec_tlm_port_widget() {
    
}

spec_tlm_port_widget::spec_tlm_port_widget( spec_tlm_port_widget *other ) {
    initialize();
    
    port_name_line_edit         -> setText( other->port_name_line_edit->text() );
    port_name_line_edit         -> setDisabled( not other->port_name_line_edit->isEnabled() );
    
    for( int cb_index = 0 ; cb_index < other->port_tlm_type_combo_box->count(); ++cb_index ) {
        port_tlm_type_combo_box     -> addItem( other->port_tlm_type_combo_box->itemText( cb_index) );
    };
    port_tlm_type_combo_box     -> setCurrentIndex( other->port_tlm_type_combo_box->currentIndex() );
    port_tlm_type_combo_box     -> setDisabled( not other->port_tlm_type_combo_box->isEnabled() );
    
    for( int cb_index = 0 ; cb_index < other->port_data_type_combo_box->count(); ++cb_index ) {
        port_data_type_combo_box    -> addItem( other->port_data_type_combo_box->itemText( cb_index) );
    };
    port_data_type_combo_box    -> setCurrentIndex( other->port_data_type_combo_box->currentIndex() );
    port_data_type_combo_box    -> setDisabled( not other->port_data_type_combo_box->isEnabled() );
    
    for( int cb_index = 0 ; cb_index < other->port_direction_combo_box->count(); ++cb_index ) {
        port_direction_combo_box    ->addItem( other->port_direction_combo_box->itemText( cb_index) );
    };
    port_direction_combo_box    -> setCurrentIndex( other->port_direction_combo_box->currentIndex() );
    port_direction_combo_box    -> setDisabled( not other->port_direction_combo_box->isEnabled() );
}

void spec_tlm_port_widget::initialize() {
    port_group_box              = new QGroupBox();
    port_name_label             = new QLabel();
    port_tlm_type_label         = new QLabel();
    port_data_type_label        = new QLabel();
    port_direction_label        = new QLabel();
    
    port_name_line_edit         = new QLineEdit();
    port_tlm_type_combo_box     = new QComboBox();
    port_data_type_combo_box    = new spec_combo_box();
    port_direction_combo_box    = new QComboBox();
    
    
    port_group_box              -> setTitle( "TLM Port Details" );
    port_group_box              -> setGeometry(  10,  10, 500, 300 );
    
    port_name_label             -> setText( "TLM Port Name" );
    port_name_label             -> setGeometry(  15,  25, 200,  30 );
    
    port_tlm_type_label         -> setText( "TLM Port Type" );
    port_tlm_type_label         -> setGeometry(  15,  75, 200,  30 );
    
    port_data_type_label        -> setText( "Port Data Type" );
    port_data_type_label        -> setGeometry(  15, 125, 200,  30 );
    
    port_direction_label        -> setText( "TLM Port Direction" );
    port_direction_label        -> setGeometry(  15, 175, 200,  30 );
    
    port_name_line_edit         -> setGeometry( 250,  25, 200,  30 );
    
//    port_tlm_type_combo_box     -> addItems(
//                QStringList() << "Analysis" << "Export" << "Get" << "Put"
//                );
    port_tlm_type_combo_box     -> setGeometry( 250,  75, 200,  30 );
    
//    port_data_type_combo_box    -> addItems(
//                QStringList() << "uint" << "int" << "string" << "real"
//                );
    port_data_type_combo_box    -> setGeometry( 250, 125, 200,  30 );
    
//    port_direction_combo_box    -> addItems(
//                QStringList() << "Initiator" << "Target"
//                );
    port_direction_combo_box    -> setGeometry( 250, 175, 200,  30 );
    
    
    
    port_group_box              -> setParent( this );
    port_name_label             -> setParent( port_group_box );
    port_tlm_type_label         -> setParent( port_group_box );
    port_data_type_label        -> setParent( port_group_box );
    port_direction_label        -> setParent( port_group_box );
    
    port_name_line_edit         -> setParent( port_group_box );
    port_tlm_type_combo_box     -> setParent( port_group_box );
    port_data_type_combo_box    -> setParent( port_group_box );
    port_direction_combo_box    -> setParent( port_group_box );
}

void spec_tlm_port_widget::set_port_data_type      (
    QString type,
    bool    enable_edit
) {
    if( port_data_type_combo_box->count() > 0 ) {
        return;
    };
    port_data_type_combo_box -> addItem( type );
    port_data_type_combo_box -> setDisabled( not enable_edit );
    
}
void spec_tlm_port_widget::set_port_direction      (
    QString direction,
    bool    enable_edit
) {
    if( port_direction_combo_box->count() > 0 ) {
        return;
    };
    QString property_string;
    if( direction == "in" ) {
        property_string = "Receiver";
    } else {
        property_string = "Transmitter";
    };
    port_direction_combo_box -> addItem( property_string );
    port_direction_combo_box -> setDisabled( not enable_edit );
}
void spec_tlm_port_widget::set_port_object_name    (
    QString name,
    bool    enable_edit
) {
    port_name_line_edit -> setText( name );
    port_name_line_edit -> setDisabled( not enable_edit );
}
void spec_tlm_port_widget::set_port_kind           (
    QString port_kind,
    bool    enable_edit
) {
    port_tlm_type_combo_box -> addItem     ( port_kind );
    port_tlm_type_combo_box -> setDisabled ( not enable_edit );
}

