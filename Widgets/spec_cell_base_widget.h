// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_CELL_BASE_WIDGET_H
#define SPEC_CELL_BASE_WIDGET_H

#include <QEvent>
#include <QKeyEvent>
#include <QWidget>
#include <BaseClasses/spec_logger.h>


class spec_cell_base_widget : public QWidget {
    Q_OBJECT
    
    public:
        spec_cell_base_widget();
        
        void                set_row_index       ( int index );
        void                set_column_index    ( int index );
        void                set_indices         ( int row, int column );
        
        int                 get_row_index       ();
        int                 get_column_index    ();
        
        virtual QString     get_value           ()                      { return ""; }
        virtual void        set_focus           ()                      {  }
        virtual void        set_tool_tip        ( const QString &text ) {  }
        
        bool                eventFilter         ( QObject *watched, QEvent *event );
        
        
    signals:
        void        cell_content_change (
                int     row,
                int     column,
                QString content
                );
        
        void        switch_cell         ( int row, int column );
        void        switch_cell_back    ( int row, int column );
        
    public slots:
        
    protected:
        int     row_index           = -1;
        int     column_index        = -1;
        QString invalid_style_sheet =
            " {\
                background: red;\
                color     : white;\
             }";
        
    private:
        
        
};

#endif // SPEC_CELL_BASE_WIDGET_H
