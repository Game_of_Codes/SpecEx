// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// -------------------------------------------------------------------------

#include "spec_cell_line_combo_box_widget.h"

spec_cell_line_combo_box_widget::spec_cell_line_combo_box_widget() {
    combo_box   = new spec_combo_box();
    combo_box   -> setFont( QFont("Courier New") );
    combo_box   -> setParent( this );
    
    combo_box   -> installEventFilter( this );
    
    connect(
        combo_box   , &spec_combo_box::currentTextChanged,
        this        , &spec_cell_line_combo_box_widget::combo_box_text_changed
    );
}

void spec_cell_line_combo_box_widget::set_line_combo_box_geometry(
    int x,
    int y,
    int width,
    int height
) {
    combo_box   -> setGeometry( x, y, width, height );
}

void spec_cell_line_combo_box_widget::add_items( const QStringList items ) {
    combo_box   -> addItems( items );
}

void spec_cell_line_combo_box_widget::add_item( const QString item ) {
    combo_box   -> addItem( item );
}

void spec_cell_line_combo_box_widget::set_invalid_color( bool is_invalid ) {
    // Once an entry is discovered to be invalid, then we'll highlight that cell
    if( is_invalid ) {
        combo_box -> setStyleSheet("spec_combo_box" + invalid_style_sheet );
    } else {
        // If a cell is not invalid, then we'll simply remove the style sheet properties, thus reverting back to system
        // defaults ;-)
        combo_box -> setStyleSheet( "" );
    };
}

QString spec_cell_line_combo_box_widget::get_value() {
    return combo_box -> currentText();
}

void spec_cell_line_combo_box_widget::combo_box_text_changed( const QString &text ) {
    emit cell_content_change( row_index, column_index, text );
}

void spec_cell_line_combo_box_widget::set_focus() {
    combo_box   -> setFocus();
}
