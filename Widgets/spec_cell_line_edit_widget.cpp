// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_cell_line_edit_widget.h"

spec_cell_line_edit_widget::spec_cell_line_edit_widget() {
    line_edit   = new QLineEdit();
    line_edit   -> setFont( QFont("Courier New") );
    line_edit   -> setParent( this );
    
    line_edit   -> installEventFilter( this );
    
    connect(
        line_edit   , &QLineEdit::textChanged,
        this        , &spec_cell_line_edit_widget::line_edit_text_changed
    );
}

void spec_cell_line_edit_widget::set_line_edit_geometry(
    int x,
    int y,
    int width,
    int height
) {
    line_edit   -> setGeometry( x, y, width, height );
}

void spec_cell_line_edit_widget::set_invalid_color( bool is_invalid ) {
    // Once an entry is discovered to be invalid, then we'll highlight that cell
    if( is_invalid ) {
        line_edit -> setStyleSheet("QLineEdit" + invalid_style_sheet );
    } else {
        // If a cell is not invalid, then we'll simply remove the style sheet properties, thus reverting back to system
        // defaults ;-)
        line_edit -> setStyleSheet( "" );
    };
}

QString spec_cell_line_edit_widget::get_value() {
    return line_edit->text();
}

void spec_cell_line_edit_widget::line_edit_text_changed( const QString &text ) {
    emit cell_content_change( row_index, column_index, text );
}

void spec_cell_line_edit_widget::set_focus() {
    line_edit   -> setFocus();
}
