// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_agent_widget.h"

spec_agent_widget::spec_agent_widget(  ) {
    
}

spec_agent_widget::spec_agent_widget( spec_agent_widget *other ) {
    initialize();
    
    // Now copying all fields and assigning the currently selected drop-box values
    agent_if_name_combo_box-> setDisabled( not other->agent_if_name_combo_box->isEnabled() );
    if( other -> agent_if_name_combo_box->count() > 0 ) {
        // Do not copy the first element of the spec_combo_box, since this is always the empty string
        for( int index = 1; index < other -> agent_if_name_combo_box -> count(); ++index ) {
            if( not other->agent_if_name_combo_box->itemText( index ).isEmpty() ) {
                agent_if_name_combo_box -> addItem( other->agent_if_name_combo_box->itemText( index) );
            };
        };
        agent_if_name_combo_box -> setCurrentText( other->agent_if_name_combo_box->currentText() );
    };
    
    // There are only two values in this active passive combo box and those are initialized within the initialize() method
    active_passive_combo_box -> setDisabled( not other->active_passive_combo_box->isEnabled() );
    active_passive_combo_box -> setCurrentText( other->active_passive_combo_box->currentText() );
    
    hdl_hierarchy_line_edit -> setDisabled( not other->hdl_hierarchy_line_edit->isEnabled() );
    hdl_hierarchy_line_edit -> setText( other->hdl_hierarchy_line_edit->text() );
    
    clock_domain_combo_box -> setDisabled( not other-clock_domain_combo_box->isEnabled() );
    if( other->clock_domain_combo_box->count() > 0 ) {
        for( int index = 0; index < other -> clock_domain_combo_box -> count(); ++index ) {
            if( not other->clock_domain_combo_box->itemText( index ).isEmpty() ) {
                clock_domain_combo_box->addItem( other->clock_domain_combo_box->itemText( index) );
            };
        };
        clock_domain_combo_box->setCurrentText( other->clock_domain_combo_box->currentText() );
    };
    
    reset_domain_combo_box -> setDisabled( not other->reset_domain_combo_box->isEnabled() );
    if( other->reset_domain_combo_box->count() > 0 ) {
        for( int index = 0; index < other -> reset_domain_combo_box -> count(); ++index ) {
            if( not other->reset_domain_combo_box->itemText( index ).isEmpty() ) {
                reset_domain_combo_box->addItem( other->reset_domain_combo_box->itemText( index) );
            };
        };
        reset_domain_combo_box->setCurrentText( other->reset_domain_combo_box->currentText() );
    };
}

void spec_agent_widget::initialize() {
    agent_group_box             = new QGroupBox();
    agent_if_name_label         = new QLabel();
    active_passive_label        = new QLabel();
    hdl_hierarchy_label         = new QLabel();
    clock_domain_label          = new QLabel();
    reset_domain_label          = new QLabel();
    
    agent_if_name_combo_box     = new spec_combo_box();
    active_passive_combo_box    = new QComboBox();
    hdl_hierarchy_line_edit     = new QLineEdit();
    clock_domain_combo_box      = new QComboBox();
    reset_domain_combo_box      = new QComboBox();
    
    agent_group_box             -> setTitle( "Agent Fields" );
    agent_group_box             -> setGeometry( 10, 10, 500, 300 );
    
    agent_if_name_label         -> setText( "Agent Interface Name" );
    agent_if_name_label         -> setGeometry( 15, 25, 200, 30 );
    
    active_passive_label        -> setText( "Active or Passive Agent" );
    active_passive_label        -> setGeometry( 15, 75, 200, 30 );
    
    hdl_hierarchy_label         -> setText( "HDL Hierarchy" );
    hdl_hierarchy_label         -> setGeometry( 15, 125, 200, 30 );
    
    clock_domain_label          -> setText( "Associated Clock Domain" );
    clock_domain_label          -> setGeometry( 15, 175, 200, 30 );
    
    reset_domain_label          -> setText( "Associated Reset Domain" );
    reset_domain_label          -> setGeometry( 15, 225, 200, 30 );
    
    
    agent_if_name_combo_box     -> setGeometry( 250, 25, 200, 30 );
    
    active_passive_combo_box    -> addItem( "ACTIVE" );
    active_passive_combo_box    -> addItem( "PASSIVE" );
    active_passive_combo_box    -> setGeometry( 250, 75, 200, 30 );
    
    hdl_hierarchy_line_edit     -> setGeometry( 250, 125, 200, 30 );
    
    clock_domain_combo_box      -> setGeometry( 250, 175, 200, 30 );
    
    reset_domain_combo_box      -> setGeometry( 250, 225, 200, 30 );
    
    agent_group_box             -> setParent( this );
    agent_if_name_label         -> setParent( agent_group_box );
    active_passive_label        -> setParent( agent_group_box );
    hdl_hierarchy_label         -> setParent( agent_group_box );
    clock_domain_label          -> setParent( agent_group_box );
    reset_domain_label          -> setParent( agent_group_box );
    agent_if_name_combo_box     -> setParent( agent_group_box );
    active_passive_combo_box    -> setParent( agent_group_box );
    hdl_hierarchy_line_edit     -> setParent( agent_group_box );
    clock_domain_combo_box      -> setParent( agent_group_box );
    reset_domain_combo_box      -> setParent( agent_group_box );
}

void spec_agent_widget::set_interface_domain_list(QList<QString> domains) {
    if( agent_if_name_combo_box->count() == 0 ) {
        agent_if_name_combo_box->addItems( domains );
    } else {
        for(QString domain : domains) {
            agent_if_name_combo_box->add_unique_item( domain );
        };
    };
}

QList<QString> spec_agent_widget::get_interface_domain_list() {
    int if_domain_count = agent_if_name_combo_box->count();
    QList<QString>  result;
    for( int index = 0; index < if_domain_count; ++index ) {
        result.append( agent_if_name_combo_box->itemText(index) );
    };
    return result;
}

QString spec_agent_widget::get_selected_interface_domain() {
    return agent_if_name_combo_box->currentText();
}

void spec_agent_widget::set_active_passive(
    bool    is_active,
    bool    enable_edit
) {
    // Set the value of the combo box
    if( is_active ) {
        active_passive_combo_box -> setCurrentIndex( 0 );
    } else {
        active_passive_combo_box -> setCurrentIndex( 1 );
    };
    
    // Also enable or disable the GUI element
    active_passive_combo_box -> setDisabled( not enable_edit );
}

QString spec_agent_widget::get_active_passive() {
    return active_passive_combo_box->currentText();
}

void spec_agent_widget::set_clock_domain_list( QList <QString> domains ) {
    // Add only new domains, and avoid any duplicates
    bool has_item = false;
    for( QString str : domains ) {
        has_item = false;
        for( int item_index = 0; item_index < clock_domain_combo_box->count();++item_index) {
            if( clock_domain_combo_box->itemText( item_index ) == str ) {
                has_item = true;
                break;
            };
        };
        if( not has_item ) {
            clock_domain_combo_box -> addItem( str );
        };
    };
}

QString spec_agent_widget::get_selected_clock_domain() {
    return clock_domain_combo_box -> currentText();
}

void spec_agent_widget::set_reset_domain_list( QList<QString> domains ) {
    // Add only new domains, and avoid any duplicates
    bool has_item = false;
    for( QString str : domains ) {
        has_item = false;
        for( int item_index = 0; item_index < reset_domain_combo_box->count();++item_index) {
            if( reset_domain_combo_box->itemText( item_index ) == str ) {
                has_item = true;
                break;
            };
        };
        if( not has_item ) {
            reset_domain_combo_box -> addItem( str );
        };
    };
}

QString spec_agent_widget::get_selected_reset_domain() {
    return reset_domain_combo_box -> currentText();
}

QString spec_agent_widget::get_hdl_path() {
    return hdl_hierarchy_line_edit->text();
}

void spec_agent_widget::print_widget_info      () {
    QList<QString>  message_output;
    message_output.append( active_passive_combo_box->currentText() + " AGENT Widget:"           );
    message_output.append( "  Interface Name: " + agent_if_name_combo_box   -> currentText()    );
    message_output.append( "  HDL Hierarchy : " + hdl_hierarchy_line_edit   -> text()           );
    message_output.append( "  Clock Domain  : " + clock_domain_combo_box    -> currentText()    );
    message_output.append( "  Reset Domain  : " + reset_domain_combo_box    -> currentText()    );
    for( QString m : message_output ) {
        spec_msg( m );
    };
}

QList<QString> spec_agent_widget::get_incomplete_config_elements() {
    QList<QString>  result;
    
    if( agent_if_name_combo_box->currentText().isEmpty() ) {
        result.append( "An agent without an interface name was detected." );
    };
    
    return result;
}   // end of get_incomplete_config_elements
