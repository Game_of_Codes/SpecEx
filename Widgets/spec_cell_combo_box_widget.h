// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_CELL_COMBO_BOX_WIDGET_H
#define SPEC_CELL_COMBO_BOX_WIDGET_H

#include <QComboBox>
#include <Widgets/spec_cell_base_widget.h>

#include <BaseClasses/spec_logger.h>

class spec_cell_combo_box_widget : public spec_cell_base_widget {
    Q_OBJECT
    
    public:
        spec_cell_combo_box_widget();
        spec_cell_combo_box_widget( spec_cell_combo_box_widget *other );
        
        void        set_combo_box_geometry(
                int x,
                int y,
                int width,
                int height
                );
        
        void    add_item            ( const QString item );
        void    add_items           ( const QStringList items );
        
        void    set_current_index   ( int index );
        void    set_enabled         ( bool enabled );
        
        int     get_current_index   ();
        QString get_value           ();
        
        void            set_focus       ();
        
    signals:
        
    public slots:
        void        combo_box_text_changed( const QString &text );
        
    private:
        QComboBox   *combo_box;
};

#endif // SPEC_CELL_COMBO_BOX_WIDGET_H
