// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_COMBO_BOX_H
#define SPEC_COMBO_BOX_H

#include <QComboBox>
#include <QLineEdit>
#include <QObject>

class spec_combo_box : public QComboBox {
    Q_OBJECT
    
    public:
        spec_combo_box();
        
        QLineEdit   *custom_line_edit;
        void        add_unique_item(QString item);
        
    public slots:
        void        change_editable( int index );
        
};

#endif // SPEC_COMBO_BOX_H
