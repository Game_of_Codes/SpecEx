#include "spec_e_browser.h"

spec_e_browser::spec_e_browser() {
    e_fp                = new spec_file_parser();
    declared_structs    = new spec_code_model();
    
    tree_widget         = new spec_tree_widget(
        new QList<QString> {
            "Object Name", "Struct Name"
        }
    );
    browser_table_widget    = new spec_table_widget(
        new QList<QString> {
            "Name", "Type", "Width", "!", "%", "List Of"
        }
    );
}

void spec_e_browser::init_gui_elements() {
    // e Module Browser TreeWidget Initialization
    tree_widget->setParent( p_ui->browser_scroll_area );
    tree_widget->resize( 581, 661 );
    
    // ... add the module level browser items that are always available
    tree_widget->addTopLevelItem(
        new QTreeWidgetItem(
            QStringList { "Declarations", "" }
        )
    );
    tree_widget->itemAt( 0, 0 )->addChild(
        new QTreeWidgetItem(
            QStringList { "Types", "" }
        )
    );
    tree_widget->itemAt( 0, 0 )->addChild(
        new QTreeWidgetItem(
            QStringList { "Structs", "" }
        )
    );
    tree_widget->addTopLevelItem(
        new QTreeWidgetItem(
            QStringList { "Instantiations", "sys" }
        )
    );
    tree_widget->itemAt(0,0)->setExpanded(true);
    tree_widget->itemAt(1,0)->setExpanded(true);

    // Add the module table view that will be updated whenever a new item from the module tree widget is
    // selected
    browser_table_widget->init_widget_data_model(
        new QList <QString> {
            "string", "string", "string", "bool", "bool", "bool"
        } 
    );
    browser_table_widget->setParent( p_ui->browser_details_scroll_area );
    browser_table_widget->resize(631, 531);
        
    
    setup_gui_connections();
}

void spec_e_browser::setup_gui_connections() {
    // e Code Browser that is capable of reading and displaying the code declarations
    connect(
        p_ui->load_uvc_button   , &QPushButton::pressed,
        e_fp                    , &spec_file_parser::load_uvc
    );
    
    // e Code Browser can also be cleared using the connection below
    connect(
        p_ui->clear_uvc_button  , &QPushButton::pressed,
        this                    , &spec_e_browser::clear_module
    );
    
    connect(
        tree_widget             , &spec_tree_widget::itemPressed,
        this                    , &spec_e_browser::display_selection
    );
    
    // e Code Browser is building hierarchy only after all files have been loaded
    connect(
        e_fp                    , &spec_file_parser::loading_uvc_done,
        this                    , &spec_e_browser::update_display_module_uvc_browser
    );
    
    // Connecting discovered enumerated type declarations and displaying them in the e Code Browser
    connect(
        e_fp                    , &spec_file_parser::type_declaration,
        this                    , &spec_e_browser::append_module_types
    );
    
    // Connecting discovered structs and displaying them in the e Code Browser
    connect(
        e_fp                    , &spec_file_parser::struct_declaration,
        this                    , &spec_e_browser::append_module_structs
    );
    
    // Connecting discovered field declarations and displaying them in the e Code Browser
    connect(
        e_fp                    , &spec_file_parser::field_declaration,
        this                    , &spec_e_browser::append_module_fields
    );
    
    // Connecting discovered method declarations and displaying them in the e Code Browser
    connect(
        e_fp                    , &spec_file_parser::method_declaration,
        declared_structs        , &spec_code_model::add_struct_method
    );
    
    // TODO:
    // connect( fp, SIGNAL( event_declaration ), this, SLOT() );
    // connect( fp, SIGNAL( constraint_declaration ), this, SLOT() );
    // connect( fp, SIGNAL( coverage_declaration ), this, SLOT() );
    
}

void spec_e_browser::update_display_module_uvc_browser() {
    qDebug() << "update_display_module_uvc_browser(): " << " called";
    declared_structs->build_new_hierarchy("sys");
    
    spec_struct_data_node* hierarchy_tree = declared_structs->get_hierarchy_tree();
    
    // Now check if the previous step already created an instantiation tree and skip updating
    // the GUI, if no sys was found.
    if( hierarchy_tree->text(1) == "sys" ) {
        qDebug() << "update_display_module_uvc_browser(): " << " Found sys extension!";
        // Adding the hierarchy tree directly to the instantiation section
        tree_widget->add_child_node(
            tree_widget->topLevelItem(1),
            hierarchy_tree
        );
    } else {
        qDebug() << "update_display_module_uvc_browser(): " << " NO sys instance found";
    };
    
    
    
//    tree_widget->add_child_node(
//        tree_widget->topLevelItem(1),
//        new QTreeWidgetItem(
//            QStringList { parent, *properties.at(0) }
//        )
//    );
    
}

void spec_e_browser::display_selection(
    QTreeWidgetItem* item,
    int             column
) {
//    qDebug() << "Detected Selection change " << item << "  at column " << column;
//    qDebug() << "declared_structs   " << item->text(column);
    int row_count = browser_table_widget->rowCount();
    int row_index = 0;
    
    qDebug() << "display_selection() " << item->text(column) << " at column " << column;
    
    // Clear all previous rows from the table
    while( browser_table_widget->rowCount() > 0 ) {
        browser_table_widget->removeRow(
            browser_table_widget->rowCount()-1
        );
    };
    // Only column 1 items provide the name for declared structs. Otherwise, we'll skip the execution of the remaining
    // method
    if( column != 1 ) {
        
        return;
    };
    spec_struct_data_node   struct_node   = declared_structs->get_struct( item->text(column) );
    QList<spec_field_node*> struct_fields = declared_structs->get_struct_fields(item->text(column));
    
    if( struct_fields.count() == 0 ) {
        
        // Now check for enumerated types
        return;
    };
    
    // At this point, it is ensured that a matching struct was found and the information can be updated
    // First set the struct name
    p_ui->code_browser_name_line_edit->setText(item->text(column));
    // Then set the like inheritance name
    p_ui->code_browser_like_line_edit->setText( struct_node.get_like_name() );
    // Then set the package name
    p_ui->code_browser_package_line_edit->setText( struct_node.get_package_name() );
    // Then also indicate, if the current struct is a unit
    if( struct_node.is_unit() ) {
        p_ui->code_browser_unit_check_box->setCheckState(Qt::Checked);
    } else {
        p_ui->code_browser_unit_check_box->setCheckState(Qt::Unchecked);
    };
    
    if( not struct_fields.isEmpty() ) {
        for(spec_field_node *node: struct_fields) {
            // First create a new row... 
            browser_table_widget->add_row();
            
            // ...then set all the parameters accordingly
            browser_table_widget->item(row_index,0)->setText(node->get_field_name());
            browser_table_widget->item(row_index,1)->setText(node->get_data_type());
            if( node->get_data_width() >= 1 ) {
                browser_table_widget->item(row_index,2)->setText(QString::number(node->get_data_width()));
            };
            if( node->get_is_no_gen() ) {
                browser_table_widget->item(row_index, 3)->setCheckState(Qt::Checked);
            };
            if( node->get_is_physical() ) {
                browser_table_widget->item(row_index, 4)->setCheckState(Qt::Checked);
            };
            if( node->get_list_depth() >= 1 ) {
                browser_table_widget->item(row_index,5)->setCheckState(Qt::Checked);
            };
            
//            qDebug() << "Showing field " << node->get_field_name() << " with properties: ";
//            qDebug() << "  !            : " << node->get_is_no_gen();
//            qDebug() << "  %            : " << node->get_is_physical();
//            qDebug() << "  list of      : " << node->get_list_depth();
//            qDebug() << "  is instance  : " << node->get_is_instance();
//            if( node->is_port() ) {
//                qDebug() << "  port type    : " << node->get_port_type();
//                qDebug() << "  port kind    : " << node->get_port_kind();
//                qDebug() << "  port datatype: " << node->get_port_data_type();
//                qDebug() << "  port dir     : " << node->get_port_direction();
//            } else {
//                qDebug() << "  data type    : " << node->get_data_type();
//                qDebug() << "  data width   : " << node->get_data_width();
//            };
            row_index++;
//            qDebug() << "Showing field " << node->get_field_properties();
        };   // end of field node iterator loop
    };
    
}

void spec_e_browser::clear_module() {
    e_fp->clear_parsed_data();
    
    tree_widget->clear();
    tree_widget->addTopLevelItem(
        new QTreeWidgetItem(
            QStringList { "Declarations", "" }
        )
    );
    tree_widget->itemAt(0,0)->addChild(
        new QTreeWidgetItem(
            QStringList { "Types", "" }
        )
    );
    tree_widget->itemAt(0,0)->addChild(
        new QTreeWidgetItem(
            QStringList { "Structs", "" }
        )
    );
    tree_widget->addTopLevelItem(
        new QTreeWidgetItem(
            QStringList { "Instantiations", "sys" }
        )
    );
    // Expand the currently added fields
    tree_widget->itemAt(0,0)->setExpanded(true);
    tree_widget->itemAt(1,0)->setExpanded(true);
    
    delete declared_structs;
    declared_structs = new spec_code_model();
}

void spec_e_browser::append_module_types            (
    QString             parent,
    QList <QString*>    properties
) {
    // In case of an enumerated type declaration , add this to the visible list
    tree_widget->add_child_node(
        tree_widget->topLevelItem(0)->child(0),
        new QTreeWidgetItem( 
            QStringList { tr(""), parent  } 
        )
    );
}   // end of append_module_types

void spec_e_browser::append_module_structs(
    QString             parent,
    QList <QString*>    properties
) {
    
    // In case the parent and the name (position 0) match, then add a new struct to the Declaration section.
    // In case the parent and name are different, then add struct as object under the sys section
    if( *(properties.at(3)) == "extend" and declared_structs->has_struct_declaration( parent ) ) {
        qDebug() << "append_module_structs(): " << "Not creating a new struct!";
        // TODO: This condition may have to be changed further to also allow sub-typing
        return;
    } else {
        
    };
    // Update the CODE model
    declared_structs->add_struct(parent, properties);
    
    // Update the GUI model
    if( parent == *properties.at(0) ) {
        qDebug() << "append_module_structs(): " << "parent and property[0] are the same.";
        tree_widget->add_child_node(
            tree_widget->topLevelItem(0)->child(1),
            new QTreeWidgetItem(
                QStringList { tr(""), parent  }
            )
        );
    } else {
        qDebug() << "append_module_structs(): " << "parent and property[0] are not equal";
        tree_widget->add_child_node(
            tree_widget->topLevelItem(1),
            new QTreeWidgetItem(
                QStringList { parent, *properties.at(0) }
            )
        );
    };
}   // end of append_module_structs

void spec_e_browser::append_module_fields(
    QString             struct_name,
    QString             field_name,
    QList <QString*>    field_properties
) {
    declared_structs->add_struct_field(
        struct_name,
        field_name,
        field_properties
    );
}

