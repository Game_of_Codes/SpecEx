// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_E_BROWSER_H
#define SPEC_E_BROWSER_H

#include <QObject>

#include <ui_spec_mainwindow.h>

#include <BaseClasses/spec_logger.h>
#include <BaseClasses/spec_tablewidget.h>
#include <BaseClasses/spec_treewidget.h>
#include <FileParser/spec_file_parser.h>
#include <CodeModel/spec_code_model.h>

class spec_e_browser : public QObject{
    Q_OBJECT
    
    public:
        spec_e_browser();
        
        // Main Window UI pointer
        Ui::spec_MainWindow *p_ui;
        
        void                init_gui_elements();
        
    public slots:
        void        update_display_module_uvc_browser();
        
        void append_module_types            (
            QString             parent,
            QList<QString*>     properties
        );
        void append_module_structs(
            QString             parent,
            QList<QString*>     properties
        );
        void append_module_fields(
            QString             struct_name,
            QString             field_name,
            QList <QString*>    field_properties
        );
        void display_selection(
            QTreeWidgetItem* item,
            int             column
        );
        
        
    private slots:
        void        clear_module();
        
    signals:
        void        loading_uvc_done();
        
        void        top_file(
            QStringList         *folder_names,
            QString             file_name
        );
        void        type_declaration(
            QString             type_name,
            QList <QString*>    type_properties
        );
        void        struct_declaration(
            QString             struct_name,
            QList <QString*>    struct_properties
        );
        
        void        field_declaration(
            QString             struct_name,
            QString             field_name,
            QList <QString*>    field_properties
        );
        
        void        method_declaration(
            QString             struct_name,
            QString             method_name,
            QList <QString*>    method_properties
        );
        
        void        constraint_declaration(
            QString             *struct_name,
            QString             *method_name,
            QList <QString*>    method_properties
        );
        
    private:
        // Declaring the e file parser
        spec_file_parser    *e_fp;
        spec_code_model     *declared_structs;
        
        // This instance is used in the module browser tab to validate and identify that the parser
        // correctly determined the loaded e details (types, structs, struct members, TLM ports)
        spec_tree_widget    *tree_widget;
        
        // This instance table widget is connected with the tree_widget. Any selected tree_widget item will
        // cause the table to be updated
        spec_table_widget   *browser_table_widget;
        
        
        
        void                setup_gui_connections();
        
        
};

#endif // SPEC_E_BROWSER_H
