// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "testwindow.h"

TestWindow::TestWindow(QWidget *parent) : QMainWindow(parent) {
    QWidget *testWidget = new QWidget();
    
    // Interface Table Instantiation 
    QList <QString> *initList = new QList <QString> {
        tr("Name"),
        tr("Type"),
        tr("Width"),
        tr("Direction"),
        tr("HDL Name")
    };
   
    
    testWidget->setParent( this );
    
    spec_table_widget *tb = new spec_table_widget(initList);
    
    
    
    tb->setParent( testWidget );
    
    resize(1024, 768);
    tb->resize(1024, 768);
    show();
};

