#ifndef PROSPECTABLEWIDGET_H
#define PROSPECTABLEWIDGET_H

#include <QTableWidget>
#include <QObject>
#include <QWidget>

class ProspecTableWidget : public QTableWidget {
    Q_OBJECT
        
    public:
        explicit ProspecTableWidget(QWidget *parent = Q_NULLPTR);
        ProspecTableWidget(int rows, int columns, QWidget *parent = Q_NULLPTR);
        ~ProspecTableWidget();
        
    
};

#endif // PROSPECTABLEWIDGET_H
