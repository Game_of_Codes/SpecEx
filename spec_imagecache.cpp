// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_imagecache.h"

spec_image_cache::spec_image_cache(QObject *parent) : QObject(parent) {
    // Get a temporary list of QStringList file names
    QStringList l = get_all_image_files();
    QImage      *imgInst;
    
    
    for( QStringList::iterator elem = l.begin(); elem != l.end(); ++elem ) {
        imgInst = new QImage( QString(*elem) );
        waveImages.append(imgInst);
    };
};

QStringList spec_image_cache::get_all_image_files() {
    QStringList result = (
        QStringList() <<
             // List of all simple levels and data levels
             ":images/level_0.png"          <<
             ":images/transition_0_1.png"   <<
             ":images/level_1.png"          <<
             ":images/transition_1_0.png"   <<
             ":images/data_const.png"       <<
             ":images/data_change.png"      <<
              
             // List of Multi-Value-Logic, inculding UNDEFINED and HIGH_IMPEDANCE
             ":images/mvl_x.png"            <<
             ":images/mvl_x_0.png"          <<
             ":images/mvl_x_1.png"          <<
             ":images/mvl_x_data.png"       <<
             ":images/mvl_data_x.png"       <<
             ":images/mvl_z.png"            <<
             ":images/mvl_z_0.png"          <<
             ":images/mvl_z_1.png"          <<
             ":images/mvl_z_data.png"       <<
             ":images/mvl_data_z.png"       <<
             ":images/mvl_0_x.png"          <<
             ":images/mvl_0_z.png"          <<
             ":images/mvl_1_x.png"          <<
             ":images/mvl_1_z.png"          <<
             
             // List of Clock Signals
             ":images/posedge_clock.png"    <<
             ":images/negedge_clock.png"
                             
    );
    return result;
}

QImage spec_image_cache::get_scaled_wave_image(int index, int pixelSize) {
    return QImage( waveImages.at( index )->scaled( pixelSize, pixelSize, Qt::KeepAspectRatio) );
}

QImage spec_image_cache::get_wave_image(int index) {
    return *waveImages.at( index );
}

QVector<QImage> spec_image_cache::get_all_scaled_wave_images( int pixel_size ) {
    QVector<QImage> result;
    
    for( QImage *img : waveImages ) {
        result.append( img->scaled( pixel_size, pixel_size, Qt::KeepAspectRatio) );
    };
    
    return result;
}

int    spec_image_cache::image_count() {
    return waveImages.count();
}
