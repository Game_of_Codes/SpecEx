// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_IF_CONFIG_TABLE_H
#define SPEC_IF_CONFIG_TABLE_H

#include <QHeaderView>
#include <QObject>
#include <QSpinBox>
#include <QTableWidget>

#include <BaseClasses/spec_logger.h>
#include <Widgets/spec_combo_box.h>

class spec_if_config_table : public QTableWidget {
    Q_OBJECT
        
    public:
        spec_if_config_table();
        
        void    initialize();
        
    public slots:
        void    add_item();
};

#endif // SPEC_IF_CONFIG_TABLE_H
