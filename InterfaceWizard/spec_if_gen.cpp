// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_if_gen.h"

spec_if_gen::spec_if_gen() {
    
}

void spec_if_gen::initialize() {
    QString     os_type = QSysInfo::productType();
    
    if( QSysInfo::productType() == "windows" ) {
        // Leave this line commented out until Linux development is complete
        p_ui->if_simulate_button->setEnabled( false );
        p_ui->if_simulate_button->setToolTip( "Simulating the testbench is not supported on Windows." );
    } else {
        p_ui->if_simulate_button->setToolTip( "Simulation requires a generated interface UVC." );
    };
    
    // Furthermore, disable the simulation button upon start-up, because a simulation can only be run if a
    // code base exists
    p_ui->if_simulate_button->setEnabled( false );
    
    // Setting the fonts for each of the fields to make:
    p_ui    -> if_group_name_line_edit      -> setFont( QFont("Courier New") );
    p_ui    -> if_protoco_name_line_edit    -> setFont( QFont("Courier New") );
    p_ui    -> if_clock_tb_line_edit        -> setFont( QFont("Courier New") );
    p_ui    -> clock_hdl_line_edit          -> setFont( QFont("Courier New") );
    p_ui    -> if_reset_tb_line_edit        -> setFont( QFont("Courier New") );
    p_ui    -> reset_hdl_line_edit          -> setFont( QFont("Courier New") );
    p_ui    -> clock_combo_box              -> setFont( QFont("Courier New") );
    p_ui    -> reset_combo_box              -> setFont( QFont("Courier New") );
    p_ui    -> if_wave_scenario_list_widget -> setFont( QFont("Courier New") );
    p_ui    -> if_wave_scenario_line_edit   -> setFont( QFont("Courier New") );
    
    // Creation of some of these objects may be moved to the header
    image_cache                 = new spec_image_cache( this );
    
    if_wave_table_widget        = new spec_interface_wave_table();
    if_wave_table_widget        -> setGeometry( 0,0, 1225, 474 );
    if_wave_table_widget        -> setParent( p_ui->protocol_model_tab_scroll_area );
    
    if_type_edit_widget         = new spec_table_widget(
        new QList <QString> {
            "Type", "Values"
        }
    );
    
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // Setting up of customized TableWidget models
    if_signals_table_widget         = new spec_if_signal_map_table();
    if_signals_table_widget->setParent( p_ui -> interfaceSignalsScrollAreaWidget );
    if_signals_table_widget->resize( 1128, 489 );
    
    
    // Transaction Item Table Widget model table
    if_transaction_table_widget     = new spec_if_config_table();
    if_transaction_table_widget     -> setParent( p_ui->dataItemScrollAreaWidgetContents );
    if_transaction_table_widget     -> resize( 1128, 578 );
    
    // Agent Configuration Table Widget
    if_agent_config_table_widget    = new spec_if_config_table();
    if_agent_config_table_widget    -> setParent(  p_ui->perInterfaceConfigurationScrollAreaWidgetContents );
    if_agent_config_table_widget    -> resize( 1128, 578 );
    
    if_env_config_table_widget      = new spec_if_config_table();
    if_env_config_table_widget      -> setParent( p_ui->globalInterfaceConfigurationScrollAreaWidgetContents );
    if_env_config_table_widget      -> resize( 1128, 578 );
    
    message_box     = new QMessageBox();
    
    // Create and attach the process logging mechanism
    process_dialog  = new spec_process_viewer();
    process         = new QProcess( this );
    process_dialog -> set_process( process );
    process_dialog -> initialize();
    process_dialog -> setModal( false );
    
    setup_signal_slot_connections();
}   // end of initialize


void spec_if_gen::setup_signal_slot_connections() {
    // Connect the signal map item buttons
    connect(
        p_ui -> if_signal_add_button            , &QPushButton::pressed,
        if_signals_table_widget                 , &spec_if_signal_map_table::add_row
    );
    connect(
        p_ui -> if_signal_remove_button         , &QPushButton::pressed,
        if_signals_table_widget                 , &spec_if_signal_map_table::delete_selected_row
    );
    connect(
        p_ui -> if_signal_type_edit_button      , &QPushButton::pressed,
        this                                    , &spec_if_gen::open_type_edit_dialog
    );
    
    // Connect the transaction item model buttons
    connect(
        p_ui -> if_data_add_button              , &QPushButton::pressed,
        if_transaction_table_widget             , &spec_if_config_table::add_row
    );
    connect(
        p_ui -> if_data_remove_button           , &QPushButton::pressed,
        if_transaction_table_widget             , &spec_if_config_table::delete_selected_row
    );
    connect(
        p_ui -> if_data_type_edit_button        , &QPushButton::pressed,
        this                                    , &spec_if_gen::open_type_edit_dialog
    );
    
    // Connect the environment configuration item buttons
    connect(
        p_ui -> if_env_config_add_button        , &QPushButton::pressed,
        if_env_config_table_widget              , &spec_if_config_table::add_row
    );
    connect(
        p_ui -> if_env_config_remove_button     , &QPushButton::pressed,
        if_env_config_table_widget              , &spec_if_config_table::delete_selected_row
    );
    connect(
        p_ui -> if_env_config_type_edit_button  , &QPushButton::pressed,
        this                                    , &spec_if_gen::open_type_edit_dialog
    );
    
    // Connect agent configuration object buttons
    connect(
        p_ui -> if_agent_conf_add_button        , &QPushButton::pressed,
        if_agent_config_table_widget            , &spec_if_config_table::add_row
    );
    connect(
        p_ui -> if_agent_conf_remove_button     , &QPushButton::pressed,
        if_agent_config_table_widget            , &spec_if_config_table::delete_selected_row
    );
    connect(
        p_ui -> if_agent_config_type_edit_button, &QPushButton::pressed,
        this                                    , &spec_if_gen::open_type_edit_dialog
    );
    
    // Connecting the signal names model with the waveform model
    connect_wave_table_signal_slots( if_wave_table_widget );
    
    // Connect the generate and simulate buttons
    connect(
        p_ui -> if_generate_button              , &QPushButton::pressed,
        this                                    , &spec_if_gen::generate_interface_uvc
    );
    connect(
        p_ui -> if_simulate_button              , &QPushButton::pressed,
        this                                    , &spec_if_gen::simulate_interface_uvc
    );
    
    // Connect the wave form scenario buttons to store and delete waveform scenarios.
    connect(
        p_ui -> if_wave_add_scenario_button     , &QPushButton::pressed,
        this                                    , &spec_if_gen::add_scenario
    );
    
    connect(
        p_ui -> if_wave_delete_scenario_button  , &QPushButton::pressed,
        this                                    , &spec_if_gen::delete_scenario
    );
    
    connect(
        p_ui->if_wave_scenario_list_widget      , &QListWidget::currentRowChanged,
        this                                    , &spec_if_gen::wave_scenario_selected
    );
    
    // Connect the waveform table widget buttons
    connect(
        p_ui -> if_wave_time_add_button         , &QPushButton::pressed,
        this                                    , &spec_if_gen::add_table_time_step
    );
    
    connect(
        p_ui -> if_wave_time_remove_button      , &QPushButton::pressed,
        this                                    , &spec_if_gen::remove_table_time_step
    );
}   // end of setup_signal_slot_connections


void spec_if_gen::open_type_edit_dialog() {
    // Initialize the first time the type edit dialog widget gets created
    if( if_type_edit_widget->rowCount() == 0 ) {
        if_type_edit_widget->init_widget_data_model(
            new QList <QString> {
                "string", "string"
            }
        );
        if_type_edit_widget->resize( 719, 419 );
    };   // end of handling initialization
    
    // First instantiate the new dialog
    type_edit_dialog    = new QDialog;
    // Make it modal
    type_edit_dialog    -> setModal( true );
    // Add the UI elements to the dialog
    type_edit_ui_dialog.setupUi( type_edit_dialog );
    // Then show the enumerated type editor window
    type_edit_dialog    -> setWindowTitle( "Adding Enumerated Types" );
    // Add the interface type table
    if_type_edit_widget->setParent( type_edit_ui_dialog.two_column_scroll_area_widget );
    
    // make it visible
    type_edit_dialog    -> show();
}   // end of open_type_edit_dialog

// Method contains logic to get the interface model from the GUI elements and pass this information to the
// interface UVC code generator
void spec_if_gen::generate_interface_uvc() {
    // Validation flags
    bool is_group_name_validated    = false;
    bool is_project_name_validated  = false;
    
    bool is_clock_tb_validated      = false;
    bool is_clock_hdl_validated     = false;
    
    bool is_reset_tb_validated      = false;
    bool is_reset_hdl_validated     = false;
    
    bool is_types_validated         = true;     // skipping this check
    bool is_signals_validated       = false;
    bool is_transaction_validated   = false;
    bool is_agent_validated         = false;
    bool is_env_validated           = false;
    
    QString invalid_style_sheet     = "QLineEdit { background: red; color: white; }";
    
    // Create a new interface model after checking that all necessary information has been properly entered
    if_model = new spec_interface_model();
    
    // Get the company and project textbox information
    QString group_name = p_ui->if_group_name_line_edit->text();
    if( group_name == "" ) {
        p_ui->if_group_name_line_edit->setStyleSheet( invalid_style_sheet );
        is_group_name_validated = false;
    } else {
        p_ui->if_group_name_line_edit->setStyleSheet( "" );
        if_model->set_group_name( group_name );
        is_group_name_validated = true;
    };
    
    QString project_name = p_ui->if_protoco_name_line_edit->text();
    if( project_name == "" ) {
        p_ui->if_protoco_name_line_edit->setStyleSheet( invalid_style_sheet );
        is_project_name_validated = false;
    } else {
        p_ui->if_protoco_name_line_edit->setStyleSheet( "" );
        if_model->set_project_name( project_name );
        is_project_name_validated = true;
    };
    
    // Get clock information
    QList<QString*>   *clk_properties = new QList<QString*>();
    clk_properties -> append( new QString( p_ui->if_clock_tb_line_edit->text() ) );
    clk_properties -> append( new QString( p_ui->clock_hdl_line_edit->text() ) );
    clk_properties -> append( new QString( p_ui->clock_combo_box->currentText() ) );
    
    if( *(clk_properties->at(0)) == "" ) {
        p_ui->if_clock_tb_line_edit->setStyleSheet( invalid_style_sheet );
        is_clock_tb_validated = false;
    } else {
        p_ui->if_clock_tb_line_edit->setStyleSheet( "" );
        is_clock_tb_validated = true;
    };
    
    if( *(clk_properties->at(1)) == "" ) {
        p_ui->clock_hdl_line_edit->setStyleSheet( invalid_style_sheet );
        is_clock_hdl_validated = false;
    } else {
        p_ui->clock_hdl_line_edit->setStyleSheet( "" );
        is_clock_hdl_validated = true;
    };
    // Only assign the properties, if the TB and HDL names have been validated
    if( is_clock_tb_validated and is_clock_hdl_validated ) {
        if_model->set_clock_properties( *clk_properties );
    };
    
    // Get reset information
    QList <QString*> *reset_properties = new QList <QString*>();
    reset_properties->append( new QString( p_ui->if_reset_tb_line_edit->text()) );
    reset_properties->append( new QString( p_ui->reset_hdl_line_edit->text()) );
    reset_properties->append( new QString( p_ui->reset_combo_box->currentText() ) );
    
    if( *(reset_properties->at(0)) == "" ) {
        p_ui->if_reset_tb_line_edit->setStyleSheet( invalid_style_sheet );
        is_reset_tb_validated = false;
    } else {
        p_ui->if_reset_tb_line_edit->setStyleSheet( "" );
        is_reset_tb_validated = true;
    };
    
    if( *(reset_properties->at(1)) == "" ) {
        p_ui->reset_hdl_line_edit->setStyleSheet( invalid_style_sheet );
        is_reset_hdl_validated = false;
    } else {
        p_ui->reset_hdl_line_edit->setStyleSheet( "" );
        is_reset_hdl_validated = true;
    };
    // Only assign the properties, if the TB and HDL names have been validated
    if( is_reset_tb_validated and is_reset_hdl_validated ) {
        if_model->set_reset_properties( *reset_properties );
    };
    
    
    // Check if there are defined enumerated types and only then add this information
    int new_type_count = ((if_type_edit_widget == nullptr)? 0 : if_type_edit_widget->rowCount());
    if( new_type_count > 0 ) {
        QList <QString*> *type_properties;
        
        for(int line_index = 0; line_index < new_type_count; ++line_index) {
            type_properties = new QList <QString*>();
            
            // Type Name
            type_properties->append( new QString(if_type_edit_widget->item(line_index, 0)->text()) );
            
            // Type Values
            type_properties->append( new QString(if_type_edit_widget->item(line_index, 1)->text()) );
            
            // Add the list to the code model
            if_model->add_type_properties( type_properties );
        };   // end of extracting enumerated types
    };   // end of adding new enumerated types
    
    // Each of the tables are validated separately and the configuration table needs to be checked differently if optional
    // input masks are to be used.
    is_signals_validated        = if_signals_table_widget       -> validate_table(       );
    is_transaction_validated    = if_transaction_table_widget   -> validate_table( true  );
    is_agent_validated          = if_agent_config_table_widget  -> validate_table( false );
    is_env_validated            = if_env_config_table_widget    -> validate_table( false );
    
    // Ensure that all required fields are properly entered. If some data entry is incorrect, then do not generate code.
    if( not 
            (is_group_name_validated and is_project_name_validated
            and
            is_clock_tb_validated and is_clock_hdl_validated
            and
            is_reset_tb_validated and is_reset_hdl_validated
            and
            is_types_validated and is_signals_validated and is_transaction_validated and is_agent_validated and is_env_validated)
            ) {
        message_box->setText( "Missing entries are marked in red and have to be added." );
        message_box->show();
        return;
    };
    
    // At this point, we know that all the entries have been sufficiently validated and we can add this information to 
    // the interface model for generation
    // properties = new QList<QString*>( table )
    for( int row_index = 0; row_index < if_signals_table_widget->rowCount();++row_index ) {
        if_model    -> add_signal_properties    ( new QList<QString*>(if_signals_table_widget->get_row_data( row_index ))       );
    };
    for( int row_index = 0; row_index < if_transaction_table_widget->rowCount();++row_index ) {
        if_model    -> add_item_properties      ( new QList<QString*>(if_transaction_table_widget->get_row_data( row_index ))   );
    };
    for( int row_index = 0; row_index < if_agent_config_table_widget->rowCount();++row_index ) {
        if_model    -> add_agent_properties     ( new QList<QString*>(if_agent_config_table_widget->get_row_data( row_index ))  );
    };
    for( int row_index = 0; row_index < if_env_config_table_widget->rowCount();++row_index ) {
        if_model    -> add_global_properties    ( new QList<QString*>(if_env_config_table_widget->get_row_data( row_index ))    );
    };
    
    // Adding the stimulus generation file
    for( int scenario_index = 0; scenario_index < if_wave_table_scenario_list.count();++scenario_index ) {
        if_model    -> add_scenario_table( if_wave_table_scenario_list.at(scenario_index) );
    };
    
    // Select directory where to store the code base
    QString uvc_directory = QFileDialog::getExistingDirectory(
        Q_NULLPTR,
        "Select Output Folder",
        "",
        QFileDialog::ShowDirsOnly
    );
    // Ensure not to generate the interface UVC if aborted by user
    if( uvc_directory.isEmpty() ) {
        message_box->setText(
            "Interface UVC generation aborted"
        );
        message_box->show();
        return;
    };
    if_model->set_directory( uvc_directory );
    
    if_code_generator = new spec_e_interface_code_generator();
    if_code_generator -> create_interface_uvc( if_model );
    
    // Add a confirmation message to indicate where to find the code base
    // First, we'd like to offer maximum convenience and enable the user to click to open the link :-)
    message_box->setTextFormat( Qt::RichText );
    message_box->setText(
                "The interface UVC code can be found here:<br><br><a href=" + uvc_directory + "/" + if_model->get_prefix() + ">"+
                uvc_directory + "/" + if_model->get_prefix() + "</a>"
                );
    message_box->show();
    
    if( QSysInfo::productType() != "windows" ) {
        // Enable the simulation button
        p_ui -> if_simulate_button->setEnabled( true );
    };
}   // end of generate_interface_uvc

void spec_if_gen::simulate_interface_uvc() {
    
    process_dialog->show();
    
    QByteArray  output;
    QString     folder      = if_model->get_directory();//"/home/osboxes/dev/_verif_gen_";//if_model->get_directory();
    QString     prefix      = if_model->get_prefix();//"cdn_test";//if_model->get_prefix();
    QString     command     = 
        "xrun -gui -snload " + folder + "/" + prefix + "/e/" + prefix + "_top.e " +
        "-snload " + folder + "/" + prefix + "/examples/single_active_interface.e " +
        "-snpath " + folder + " " +
        folder + "/" + prefix + "/hdl/tb_top.sv";
    
    if( if_wave_table_scenario_list.count() > 0 ) {
        command += " -snload " + folder + "/" + prefix + "/examples/scenario_traffic.e ";
    };
    // Reset the queue, just in case some previous commands are still in there
    process_dialog->reset_command_queue();
    // Then create a new folder
    process_dialog->queue_command( "mkdir _sim_", folder );
    // Run the simulator command
    process_dialog->queue_command( command, folder + "/_sim_" );
    // Remove the simulation folder after simulation is done
    process_dialog->queue_command( "rm -rf _sim_", folder);
    
    // actually launch the series of commands
    process_dialog->start_command_queue();
}   // end of simulate_interface_uvc


QVector<QImage> spec_if_gen::get_all_scaled_wave_images( ) {
    QVector<QImage> result = image_cache->get_all_scaled_wave_images();
    return result;
}

void spec_if_gen::add_table_time_step() {
    int scenario_count   = p_ui -> if_wave_scenario_list_widget->count();
    
    if( scenario_count == 0 ) {
        if_wave_table_widget->add_step();
    } else {
        for( int scenario_index = 0; scenario_index < scenario_count;++scenario_index ) {
            // Use the GUI scenario name line and compare that ageinst the current index item of the table scenario list
            if( p_ui->if_wave_scenario_line_edit->text() == p_ui->if_wave_scenario_list_widget->item( scenario_index )->text() ) {
                // Add the time step and exit the method.
                if_wave_table_scenario_list.at( scenario_index )->add_step();
                return;
            };
        };  // end of searching for selected scenario
        
        if_wave_table_widget->add_step();
    };
}

void spec_if_gen::remove_table_time_step() {
    int scenario_count   = p_ui -> if_wave_scenario_list_widget->count();
    
    if( scenario_count == 0 ) {
        if_wave_table_widget->remove_step();
    } else {
        for( int scenario_index = 0; scenario_index < scenario_count;++scenario_index ) {
            // Use the GUI scenario name line and compare that ageinst the current index item of the table scenario list
            if( p_ui->if_wave_scenario_line_edit->text() == p_ui->if_wave_scenario_list_widget->item( scenario_index )->text() ) {
                // Add the time step and exit the method.
                if_wave_table_scenario_list.at( scenario_index )->remove_step();
                return;
            };
        };  // end of searching for selected scenario
        
        if_wave_table_widget->remove_step();
    };
}

void spec_if_gen::connect_wave_table_signal_slots ( spec_interface_wave_table *table ) {
    connect(
        p_ui -> if_signal_add_button    , &QPushButton::pressed,
        table                           , &spec_interface_wave_table::add_signal
    );
    connect(
        if_signals_table_widget         , &spec_if_signal_map_table::field_changed,
        table                           , &spec_interface_wave_table::update_signal
    );
    connect(
        p_ui -> if_clock_tb_line_edit   , &QLineEdit::textChanged,
        table                           , &spec_interface_wave_table::update_clock_name
    );
    connect(
        p_ui -> if_reset_tb_line_edit   , &QLineEdit::textChanged,
        table                           , &spec_interface_wave_table::update_reset_name
    );
    connect(
        if_signals_table_widget         , &spec_if_signal_map_table::row_added,
        table                           , &spec_interface_wave_table::add_signal
    );
    connect(
        if_signals_table_widget         , &spec_if_signal_map_table::row_removed,
        table                           , &spec_interface_wave_table::remove_signal
    );
}

// This method creates the if_wave_table_widget
void spec_if_gen::create_interface_wave_table     (  ) {
    QString cell_value;
    QString cell_type;
    bool    is_single_bit = false;
    
    // Create a new table and initialize all its field with the currently defined signals and their bit-widths
    if_wave_table_widget        = new spec_interface_wave_table();
    if_wave_table_widget        -> setGeometry( 0,0, 1225, 474 );
    if_wave_table_widget        -> setParent( p_ui->protocol_model_tab_scroll_area );
    
    if_wave_table_widget        -> update_clock_name( p_ui -> if_clock_tb_line_edit->text() );
    if_wave_table_widget        -> update_reset_name( p_ui -> if_reset_tb_line_edit->text() );
    
    // Iterate over each signal and set its name as well as its bit-width
    for( int signal_index = 0; signal_index < if_signals_table_widget->rowCount(); ++signal_index ) {
        if( signal_index > 0 ) {
            if_wave_table_widget->add_signal();
        };
        // Set the signal name
        cell_value = if_signals_table_widget->get_cell_value( signal_index, 0);
        if_wave_table_widget->update_signal( signal_index, 0, cell_value );
        
        // Set the signal's data type
        cell_type = (if_signals_table_widget->get_cell_value( signal_index, 1));
        if( cell_type == "bit" ) {
            is_single_bit = true;
        } else {
            if( cell_type == "uint" or cell_type == "int" or cell_type == "longuint" or cell_type == "longint" or cell_type == "byte" or cell_type == "nibble" ) {
                if( if_signals_table_widget->get_cell_value( signal_index, 2) == "1" ) {
                    is_single_bit = true;
                } else {
                    is_single_bit = false;
                };
            };
        };
        // Now it is ensured, probably overengineered, that the single bit and multi-bit detection is correct
        if( is_single_bit ) {
            if_wave_table_widget -> set_signal_width( signal_index,is_single_bit, 1 );
        } else {
            if_wave_table_widget -> set_signal_width(
                signal_index,
                is_single_bit,
                if_signals_table_widget->get_cell_value( signal_index, 2).toInt()
            );
        };
    };  // end of iterating and setting initial table model cell values per signal
}

void spec_if_gen::add_scenario() {
    QString scenario_name = p_ui -> if_wave_scenario_line_edit->text();
    
    if( scenario_name.isEmpty() ) {
        message_box = new QMessageBox();
        message_box -> setText( "An empty scenario name is not allowed, please name your scenario first, then add it again." );
        message_box -> show();
        return;
    } else {
        int     table_item_count = p_ui->if_wave_scenario_list_widget->count();
        int     table_item_detected_index = -1;
        
        // Search the list of saved scenarios for the currently entered name in the line edit widget
        for( int item_index = 0; item_index < table_item_count;++item_index ) {
            if( scenario_name == p_ui->if_wave_scenario_list_widget->item( item_index )->text() ) {
                table_item_detected_index = item_index;
                break;
            };
        };  // end of iterating through each table entry
        
        // If no entry was found, then the search returns the index -1
        if( table_item_detected_index == -1 ) {
            p_ui                        -> if_wave_scenario_list_widget->addItem( scenario_name );
            if_wave_table_widget        -> set_scenario_name( scenario_name );
            
            if( table_item_count == 0 ) {
                if_wave_table_scenario_list.append( new spec_interface_wave_table( if_wave_table_widget ) );
                if_wave_table_widget -> hide();
                // Now show the new signal wave editor
                for( spec_interface_wave_table *t : if_wave_table_scenario_list ) {
                    t->hide();
                };
            } else {
                if_wave_table_scenario_list.append( new spec_interface_wave_table( if_wave_table_widget ) );
                if_wave_table_widget -> hide();
                
                // Now show the new signal wave editor
                for( spec_interface_wave_table *t : if_wave_table_scenario_list ) {
                    t->hide();
                };
            };
            if_wave_table_scenario_list . at(table_item_count) ->set_scenario_name( scenario_name );
            
            // Now connect the signal map signals and buttons to the freshly added interface table
            connect_wave_table_signal_slots( if_wave_table_scenario_list.at(table_item_count) );
            
            // end of adding new wave table to the list
        } else {    // Handle the case of an already selected library
            if_wave_table_widget -> hide();
            
            // Now show the new signal wave editor
            for( spec_interface_wave_table *t : if_wave_table_scenario_list ) {
                t->hide();
            };
        };  // end of handling selected widget processing
        
        // Nothing needs to be done to the wave tables, if the save action is invoked, because we are already operating
        // on the selected and shown model.
        // This method creates the wave_table_widget with all its required settings
        create_interface_wave_table();
        
        // Connect the new table
        connect_wave_table_signal_slots( if_wave_table_widget );
        
        // Always show the current table widget only!
        if_wave_table_widget                                -> show();
        
        // Also reset the text entry field.
        p_ui -> if_wave_scenario_line_edit->setText("");
    };
}

void spec_if_gen::delete_scenario() {
    QList<QListWidgetItem*> selected_items = p_ui->if_wave_scenario_list_widget->selectedItems();
    if( selected_items.count() > 0 ) {
        QString sel_scenario_name       = selected_items.at(0)->text();
        int     sel_scenario_row_index  = -1;
        
        for( int table_item_index = 0; table_item_index < p_ui->if_wave_scenario_list_widget->count(); ++table_item_index ) {
            if( sel_scenario_name == p_ui->if_wave_scenario_list_widget->item( table_item_index )->text() ) {
                sel_scenario_row_index  = table_item_index;
                break;
            };
        };  // end of searching through the list of items
        
        if( sel_scenario_row_index == -1 ) {
            message_box = new QMessageBox();
            message_box -> setText( "Scenario Name '" + sel_scenario_name + "' not found. Please provide a different name." );
            return;
        };
        
        // Remove the selected item from the GUI
        p_ui->if_wave_scenario_list_widget->takeItem( sel_scenario_row_index );
        
        // Remove the selected item from the scenario list model
        if_wave_table_scenario_list.takeAt( sel_scenario_row_index );
        // And also clear the name of the line edit
        p_ui    -> if_wave_scenario_line_edit->setText("");
    };
    
    if( p_ui->if_wave_scenario_list_widget->count() == 0 ) {
        // It is okay removing all the elements, but we need to ensure that at least the interface table is shown!
        create_interface_wave_table();
        connect_wave_table_signal_slots( if_wave_table_widget );
        if_wave_table_widget->show();
    };
}

void spec_if_gen::wave_scenario_selected( int row ) {
    // Get the number of scenarios that are defined in the widget
    int     scenario_count      = p_ui->if_wave_scenario_list_widget->count();
    
    // Ensure the current view is hidden
    if_wave_table_widget -> hide();
    
    for( int index = 0; index < scenario_count;++index ) {
        // Hide every single scenario so we are not faced with seeing a likely non-selected scenario
        if_wave_table_scenario_list.at( index ) -> hide();
        
        // Once the selected row matches the index of the list element of the widget, then we'll update the text showing
        // the current scenario, assigning the selected scenario to eh main table widget list.
        if( index == row ) {
            p_ui -> if_wave_scenario_line_edit -> setText( if_wave_table_scenario_list.at( index ) ->get_scenario_name() );
            if_wave_table_scenario_list.at( row ) -> show();
            if_wave_table_widget    = if_wave_table_scenario_list.at( row );
            connect_wave_table_signal_slots( if_wave_table_widget );
        };
    };
}
