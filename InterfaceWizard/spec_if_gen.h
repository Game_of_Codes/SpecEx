// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_IF_GEN_H
#define SPEC_IF_GEN_H

#include <QFileDialog>
#include <QMessageBox>
#include <QObject>
#include <QThread>

#include <ui_spec_mainwindow.h>
#include <ui_spec_type_dialog.h>

#include <BaseClasses/spec_logger.h>
#include <spec_imagecache.h>
#include <Widgets/spec_if_config_table.h>
#include <Widgets/spec_if_signal_map_table.h>
#include <Widgets/spec_interface_wave_table.h>
#include <FileParser/spec_file_parser.h>
#include <BaseClasses/spec_tablewidget.h>
#include <BaseClasses/spec_treewidget.h>
#include <CommonDialogs/spec_process_viewer.h>

#include <CodeGenerator/spec_e_interface_code_generator.h>
#include <CodeModel/spec_interface_model.h>


class spec_if_gen : public QObject {
    Q_OBJECT
    
    public:
        spec_if_gen();
        
        // Main Window UI object
        Ui::spec_MainWindow                 *p_ui;
        
        void                                initialize();
        QVector<QImage>                     get_all_scaled_wave_images  ();
        
    signals:
        
    public slots:
        void                                open_type_edit_dialog       ();
        void                                add_scenario                ();
        void                                delete_scenario             ();
        void                                wave_scenario_selected      ( int row );
        void                                add_table_time_step         ();
        void                                remove_table_time_step      ();
        
        
    private slots:
        void                                generate_interface_uvc      ();
        void                                simulate_interface_uvc      ();
        
    private:
        // Dialog window object
        Ui_type_edit_dialog                 type_edit_ui_dialog;
        QDialog                             *type_edit_dialog;
        
        spec_image_cache                    *image_cache;
        
        spec_interface_model                *if_model;  // instantiation is done only when code is about to be generated
        spec_e_interface_code_generator     *if_code_generator;
        
        spec_if_signal_map_table            *if_signals_table_widget;
        spec_if_config_table                *if_transaction_table_widget;
        spec_if_config_table                *if_env_config_table_widget;
        spec_if_config_table                *if_agent_config_table_widget;
        spec_table_widget                   *if_type_edit_widget;
        
        spec_interface_wave_table           *if_wave_table_widget;
        QList<spec_interface_wave_table*>   if_wave_table_scenario_list;
        
        QProcess                            *process;
        
        spec_process_viewer                 *process_dialog;
        
        QMessageBox                         *message_box;
        
        void                                setup_signal_slot_connections   (  );
        void                                create_interface_wave_table     (  );
        void                                connect_wave_table_signal_slots ( spec_interface_wave_table *table );
        
};

#endif // SPEC_IF_GEN_H
