// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "BaseClasses/spec_tablewidget.h"

spec_table_widget::spec_table_widget(QList <QString> *headerStringList) {
    spec_msg("");
    initialize( headerStringList );
}   // end of DanielTableWidget

//spec_table_widget::spec_table_widget( spec_table_widget *other ) {
    
//    bool is_matching_class = false;
    
//    int other_column_count = other->columnCount();
//    QList<QString> *header_string_list = new QList<QString>();
//    QString         class_name;
//    for( int index = 0; index < other_column_count; ++index ) {
//        header_string_list->append( other->horizontalHeaderItem( index )->text() );
//    };
//    spec_msg("other_column_count " << other_column_count);
//    initialize( header_string_list );
//    init_widget_data_model( other->data_model );
////    delete_selected_row();
//    setColumnCount( other_column_count );
//    // TODO: Copy the fields in this copy constructor
//    for( int row_index = 0; row_index < other->rowCount(); ++row_index ) {
////        add_row();
//        insertRow( row_index );
//        for( int column_index = 0; column_index < other_column_count; ++column_index ) {
//            setItem( row_index, column_index, new QTableWidgetItem("test") );
////            spec_msg("Widget Type: " << other->cellWidget( row_index, column_index ) );
//            spec_msg("Accessing row: " << row_index << ", column " << column_index );
            
////            if( other->cellWidget(row_index, column_index) != nullptr ) {
////                class_name = other->cellWidget(row_index, column_index)->metaObject()->className();
//////                spec_msg("CellWidget   : " << class_name);
//////                setCellWidget( row_index, column_index, other->cellWidget( row_index, column_index ) );
////                if( class_name == "QLabel" ) {
////                    // TODO: Learn why the item is not being set
////                    spec_msg( "QLabel Text " << static_cast<QLabel*>( other->cellWidget( row_index, column_index) )->text() );
//////                    item(row_index, column_index)->setText( static_cast<QLabel*>(other->cellWidget(row_index,column_index) )->text());
////                    QLabel* cell_widget =  new QLabel( static_cast<QLabel*>(other->cellWidget(row_index,column_index) ) );
////                    setCellWidget( row_index, column_index, cell_widget );
////                    item(row_index, column_index)->setText( cell_widget->text() );
////                    is_matching_class = true;
////                };
////                if( class_name == "QComboBox" ) {
////                    spec_msg( "QComboBox Text " << static_cast<QComboBox*>( other->cellWidget( row_index, column_index) )->currentText() );
////                    QComboBox *cell_widget = new QComboBox( static_cast<QComboBox*>( other->cellWidget(row_index,column_index) ) );
////                    setCellWidget( row_index, column_index, cell_widget );
////                    is_matching_class = true;
////                };
////                if( class_name == "spec_combo_box" ) {
////                    spec_msg( "spec_combo_box Text " << static_cast<spec_combo_box*>( other->cellWidget( row_index, column_index) )->currentText() );
////                    spec_combo_box* cell_widget = new spec_combo_box();
////                    for( int cb_index = 0; cb_index < static_cast<spec_combo_box*>( other->cellWidget( row_index, column_index) )->count(); ++cb_index ) {
////                        cell_widget->addItem(static_cast<spec_combo_box*>( other->cellWidget( row_index, column_index) )->itemText( cb_index ) );
////                    };
                    
////                    setCellWidget( row_index, column_index, cell_widget );
////                    is_matching_class = true;
////                };
                
////                if( class_name == "QSpinBox" ) {
////                    spec_msg( "QSpinBox value " << static_cast<QSpinBox*>( other->cellWidget( row_index, column_index) )->value() );
////                    QSpinBox* cell_widget = new QSpinBox( static_cast<QSpinBox*>( other->cellWidget( row_index, column_index) ) );
////                    setCellWidget( row_index, column_index, cell_widget );
////                    is_matching_class = true;
////                };
                
////                if( not is_matching_class ) {
////                    spec_msg("CellWidget   : " << class_name);
////                };
////                is_matching_class = false;
////            };
////            item(row_index, column_index)->setText( other->item( row_index, column_index)->text() );
            
//        };
//    };  // end of iterating over each row
//    spec_msg( "Done constructing... rows " << rowCount() << "  columns " << colorCount() );
//}

void spec_table_widget::initialize(QList<QString> *header_string_list) {
    int colC = header_string_list->count();
    
    setColumnCount( colC );
    
    // Populate the header columns
    for (int cIndex = 0; cIndex < colC; ++cIndex) {     
        setHorizontalHeaderItem( cIndex, new QTableWidgetItem( header_string_list->at(cIndex) ));
        
        horizontalHeaderItem(cIndex)->setFont( QFont("Courier New") );
        
        // Get the length of the string and determine the character length in pixels
        setColumnWidth(cIndex, header_string_list->at(cIndex).count() * 30);
    };
    
    // Only allow for a single line to be selected
    setSelectionMode( QAbstractItemView::SingleSelection );
    
    // Enable context menu
    setContextMenuPolicy( Qt::CustomContextMenu );
    
    // Set the default context menu
    create_context_menu();
    
    // Connect any cell change and item selection event with the custom methods
    connect(
        this, &spec_table_widget::cellChanged,          //SIGNAL( cellChanged       (int, int) ),
        this, &spec_table_widget::change_cell_content   //SLOT  ( change_cell_content (int, int) )
    );
    
    connect(
                this, SIGNAL( itemSelectionChanged  () ),
                this, SLOT  ( process_selection_change() )
    );
    
    // Connect detecting and enabling mouse context menu
    connect(
                this, SIGNAL( customContextMenuRequested(QPoint)    ),
                this, SLOT  ( show_context_menu(QPoint)             )
    );
}

void spec_table_widget::init_widget_data_model( QList <QString> *rowTypeList ) {
    // Set the data model information first
    data_model = rowTypeList;
    // then add a new row, which will then take care of setting the proper data model per line
    add_row();
}

bool spec_table_widget::is_checked_state(int column_index) {
    bool result;
    QString columnHeaderString = horizontalHeaderItem(column_index)->text();
    
    result = (QString::compare(columnHeaderString,"!")       == 0);
    result = (QString::compare(columnHeaderString,"%")       == 0) || result;
    result = (QString::compare(columnHeaderString,"List Of") == 0) || result;
    result = (QString::compare(data_model->at(column_index), "bool") == 0) || result;
    
    return result;
}

void spec_table_widget::add_row( QString str ) {
    // Adding a new row is always done at the end of the list, hence we are getting the number of elements
    int table_item_count = rowCount();
    int column_count     = columnCount();
    
    // Now adding row at the end of the list
    insertRow( table_item_count );
    
    // Iterating over each element and add content
    set_default_row( table_item_count, column_count );
    
    // Default formatting for new row
    for( int column_index = 0; column_index < column_count; ++column_index ) {
        if( is_checked_state(column_index) ) {
            setItem( table_item_count, column_index, new QTableWidgetItem( Qt::CheckStateRole ) );
            item   ( table_item_count, column_index ) -> setCheckState( Qt::Unchecked );
        } else {
            setItem( table_item_count, column_index, new QTableWidgetItem( str ) );
        };
        // Courier New is a nice font for coding work!
        item( table_item_count, column_index ) -> setFont( QFont("Courier New") );
    };   // end of setting each row element
}   // end of addNewRow method

// This method is a wrapper to be used by signal slot mechanism that doesn't allow for connecting button
// pressed events with add_row
void spec_table_widget::add_empty_row() {
    add_row();
}

void spec_table_widget::delete_selected_row() {
    // Get the number of rows
    int rowC    = rowCount();
    int selRow;
    
    // Get the currently selected row index
    QList <QTableWidgetItem*> selectedWidgetItems = selectedItems();
    
    if( not selectedWidgetItems.isEmpty() ) {
        selRow = selectedWidgetItems.at(0) -> row();
    } else {
        selRow = rowC-1;
    };
    
    if( selRow < 0 ) {
        // can't delete anything ... abort executing on an empty table
        return;
    };
    
    // Remove elements only if there are 2 or more lines
    if( rowC >= 2 ) {
        // Remove the selected row from the view
        removeRow( selRow );
        // Remove the selected row from the dataModel
        // TODO: Determine if this can be wrapped in a more abstract way
        // currentNode = (spec_StructDataNode *) treeWidget -> selectedItems().at( 0 );
        // currentNode -> removeRowAt( selRow );
    } else {
        // Overwrite the selected row with default values
        set_default_row( selRow, columnCount() );
    };   // end of handling else branch
    
    // Send out trigger that allows for processing that a row has been deleted
    emit delete_row_index( selRow );
}   // end of deleteSelectedRow




void spec_table_widget::update_table_widget() {
    
}

void spec_table_widget::change_cell_content(int row, int column) {
    // Throw a signal to the environment1
    emit name_changed( row, column, item(row, column)->text() );
}  // end of change_cell_content

void spec_table_widget::process_selection_change() {
    
}

void spec_table_widget::set_default_row(int row_index, int column_count) {
    for( int column_index = 0; column_index < column_count; ++column_index ) {
        // Ensure that any flagged bool model shall be a Qt::CheckStateRole field
        if( data_model->at(column_index) == "bool" ) {
            setItem( row_index, column_index, new QTableWidgetItem( Qt::CheckStateRole ) );
            item   ( row_index, column_index ) -> setCheckState( Qt::Unchecked );
        } else if ( data_model->at(column_index) == "combo_ports" ) {
            QComboBox* combo = new QComboBox();
            setCellWidget(row_index, column_index, combo);
            combo->addItem( tr("HDL Read/Write") );
            combo->addItem( tr("HDL Read") );
            combo->addItem( tr("HDL Write") );
        } else if( data_model->at(column_index) == "combo_clock" ) {
            QComboBox *combo = new QComboBox();
            setCellWidget(row_index, column_index, combo);
            combo->addItem( tr("Rising Edge") );
            combo->addItem( tr("Falling Edge") );
            combo->addItem( tr("Rising and Falling Edge") );
        } else if( data_model->at(column_index) == "combo_type" ) {
            QComboBox* combo = new QComboBox();
            setCellWidget(row_index, column_index, combo);
            combo->addItem( tr("bit") );
            combo->addItem( tr("byte") );
            combo->addItem( tr("uint") );
            combo->addItem( tr("int") );
            combo->addItem( tr("time") );
            combo->addItem( tr("real") );
        } else if( data_model->at(column_index) == "combo_reset_active" ) {
            QComboBox *combo = new QComboBox();
            setCellWidget(row_index, column_index, combo);
            combo->addItem( "Active-Low" );
            combo->addItem( "Active-High" );
        } else if( data_model->at(column_index) == "combo_reset_sync" ) {
            QComboBox *combo = new QComboBox();
            setCellWidget(row_index, column_index, combo);
            combo->addItem( "Asynchronous" );
            combo->addItem( "Clock Synced" );
        } else {
            setItem( row_index, column_index, new QTableWidgetItem( tr("") ) );
        };
    };
}   // end of setDefaultRow

void spec_table_widget::create_context_menu() {
    // Create default table_context_menu
    table_context_menu = new QMenu( this );
    
    QAction *add_signal_action      = new QAction( tr("Add Element"), this );
//    add_signal_action               -> setStatusTip(  );
    table_context_menu              -> addAction( add_signal_action );
    connect(
        add_signal_action   , SIGNAL( triggered() ),
        this                , SLOT  ( add_row() )
    );
    
    QAction *remove_signal_action   = new QAction( tr("Remove Selected Element"), this );
//    remove_signal_action            -> setStatusTip( tr("Removing signal will also remove it from the waveform") );
    table_context_menu              -> addAction( remove_signal_action );
    connect(
        remove_signal_action, SIGNAL( triggered() ),
        this                , SLOT  ( delete_selected_row() )
    );
}   // end of create_context_menu

void spec_table_widget::show_context_menu(const QPoint &pos) {
    // Remap the positional data for the context menu to appear at the position where clicked
    QPoint global_point = this -> mapToGlobal( pos );
    
    // Call context menu at global_point
    if( table_context_menu == nullptr ) {
        qDebug() << "Context menu can't be called because it was not configured";
    } else {
        qDebug() << "Context menu will be called...";
        table_context_menu->exec( global_point );
    };  
}   // end of show_context_menu
