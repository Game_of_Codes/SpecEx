// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_TREE_WIDGET_H
#define SPEC_TREE_WIDGET_H

#include <QDebug>
#include <QObject>
#include <QTreeWidget>
#include <QTreeWidgetItem>

class spec_tree_widget : public QTreeWidget {
    Q_OBJECT
    
    public:
        spec_tree_widget(
            QList <QString>     *header_string_list
        );
        
        void            set_node_name(
            QString             *name
        );
        
        void            set_data_model(
            QList <QString*>    data_model
        );
        
        QString         get_node_name();
        
    public slots:
        virtual void    init_widget_data_model      (
            QList <QString*>     type_list
        );
        void            add_sibling_node(
            QString             *parent_name
        );
        void            add_child_node(
            QTreeWidgetItem     *parent,
            QTreeWidgetItem     *new_item,
            int                 index = 0
        );
        
        void            remove_node();
        void            remove_subtree();
        
    private:
        QString         *node_name;
        QList <QString*> data_model;
        unsigned int    get_current_node_index();
};

#endif // SPEC_TREE_WIDGET_H
