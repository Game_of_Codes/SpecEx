// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_treewidget.h"

spec_tree_widget::spec_tree_widget(
    QList <QString> *header_string_list
) {
    
    // First set the tree widget's columns
    setHeaderLabels( *header_string_list );
    for(int index = 0; index < header_string_list->count();++index) {
        // I love Courier New as font... makes it feel more technical
        headerItem()->setFont( index, QFont("Courier New"));
    };   // end of iterating over each header item 
};   // end of spec_tree_widget

void spec_tree_widget::init_widget_data_model( QList <QString*> type_list ) {
    // Set the data model information first
    data_model = type_list;
    // then add a new row, which will then take care of setting the proper data model per line
//    add_row();
};

//void spec_tree_widget::set_node_name(
//    QString             *name
//) {
//    node_name = name;
//};   // end of set_node_name

void spec_tree_widget::set_data_model(
    QList <QString*>    data_model
) {
    
};

//QString spec_tree_widget::get_node_name() {
//    return *node_name;
//};


void spec_tree_widget::add_sibling_node(
    QString             *parent_name
) {
    
};
void spec_tree_widget::add_child_node(
    QTreeWidgetItem     *parent,
    QTreeWidgetItem     *new_item,
    int                 index
) {
    parent->insertChild( index, new_item );
};   // end of add_child_node

void spec_tree_widget::remove_node() {
    
};

void spec_tree_widget::remove_subtree() {
    
};
