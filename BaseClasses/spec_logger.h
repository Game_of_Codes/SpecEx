// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_LOGGER_H
#define SPEC_LOGGER_H

#include <QDebug>
#include <QObject>

#define spec_msg(string_msg) qDebug() <<"(INFO) ["<<__FILE__<<" | "<<__LINE__<<" | "<<__PRETTY_FUNCTION__<<"]: "<<string_msg;

class spec_logger {
    public:
        spec_logger();
        
        void        message(
            QString     msg
        );
        
        void        message(
            QList<QString>  msg
        );
};

#endif // SPEC_LOGGER_H
