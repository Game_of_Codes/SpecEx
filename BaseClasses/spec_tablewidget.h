// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_TABLE_WIDGET_H
#define SPEC_TABLE_WIDGET_H

#include <QComboBox>
#include <QLabel>
#include <QList>
#include <QMenu>
#include <QMouseEvent>
#include <QObject>
#include <QSpinBox>
#include <QString>
#include <QTableWidget>

#include <BaseClasses/spec_logger.h>
#include <Widgets/spec_combo_box.h>

class spec_table_widget : public QTableWidget {
    Q_OBJECT
    
    public:
        spec_table_widget( QList<QString> *headerStringList );
//        spec_table_widget( spec_table_widget *other );
        
    public slots:
        virtual void    add_row                     (
                QString         str = ""
                );
        virtual void    add_empty_row               ();
        virtual void    delete_selected_row         ();
        virtual void    init_widget_data_model      (
                QList <QString> *rowTypeList
                );
        virtual void    show_context_menu           (
                const QPoint &pos
                );
        
    signals:
        void            name_changed(
                int         row,
                int         column,
                QString     name
                );
        void            delete_row_index            (
                int         row
                );
        
    private slots:
        virtual void    update_table_widget         ();
        virtual void    change_cell_content         (
                int row,
                int column
                );
        virtual void    process_selection_change    ();
        virtual void    set_default_row             (
                int rowIndex,
                int colCount
                );
        
    private:
        QList <QString> *data_model                 ;
        QMenu           *table_context_menu         ;
        
        void            initialize                  ( QList<QString> *headerStringList );
        
        virtual bool    is_checked_state            (
                int cIndex
                );
        virtual void    create_context_menu         ();
};

#endif // SPEC_TABLE_WIDGET_H
