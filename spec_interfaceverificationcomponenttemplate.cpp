// ----------------------------------------------------------------------------
//   Copyright 2019 Daniel Bayer ==> daniel_bayer{AT}outlook{DOT}com <==
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_interfaceverificationcomponenttemplate.h"

spec_InterfaceVerificationComponentTemplate::spec_InterfaceVerificationComponentTemplate(QObject *parent) : QObject(parent) {
    
};

// The following shows the e code example that will be created as the default skeleton:
/*
 * 
 /////// INTEFACE eVC \\\\\\\\\
 
 File: my_ex_top.e
 <'
 import uvm_e/e/uvm_e_top;
 
 import my_ex/e/my_ex_types;
 import my_ex/e/my_ex_item;
 import my_ex/e/my_ex_signal_map;
 import my_ex/e/my_ex_monitor;
 import my_ex/e/my_ex_monitor_protocol;
 import my_ex/e/my_ex_driver;
 import my_ex/e/my_ex_bfm;
 import my_ex/e/my_ex_bfm_protocol;
 import my_ex/e/my_ex_agent;
 import my_ex/e/my_ex_environment;
 
 import my_ex/e/my_ex_checks;
 import my_ex/e/my_ex_sequences;
 import my_ex/e/my_ex_coverage;
 '>
 ===================================================================================================================
 File: my_ex_types.e
 <'
 type my_ex_name_t: [DEFAULT];
 type my_ex_if_t  : [NONE, IF0, IF1, IF2, IF3, IF4, IF5, IF6, IF7, IF8, IF9, IF10];
 '>
 ===================================================================================================================
 File: my_ex_item.e
 <'
 struct my_ex_item_s like any_sequence_item {
   if_name      : my_ex_if_t;
   
   %data        : uint(bits: 32);
   
   request_delay: uint(bits: 32);
   
   grant_delay  : uint(bits: 32);
   
   !start_time  : time;
   !end_time    : time;
 };
 '>
 ===================================================================================================================
 
 ===================================================================================================================
 File: my_ex_monitor
 <'
 unit my_ex_monitor_u like uvm_monitor {
   if_name: my_ex_if_t;
   
   !p_smp: my_ex_signal_map_u;
   
   event clock is @p_smp.clock;
   event reset is fall( @p_smp.reset$ )@sim;
   
   monitor()@clock is empty;
   
   on reset {
     rerun();
   };
   
   run() is also {
     start monitor();
   };
 };
 '>
 ===================================================================================================================
 File: my_ex_monitor_protocol
 <'
 extend my_ex_monitor_u
   // TODO: Implement the low-level monitor protocol
   monitor()@clock is {
     // ...
   };
 };
 '>
 ===================================================================================================================
 File: my_ex_driver
 <'
 sequence my_ex_seq_s using
   item           = my_ex_item_s,
   created_kind   = my_ex_sequence_kind_t,
   created_driver = my_ex_driver_u;
 
 extend my_ex_driver_u {
   if_name: my_ex_if_t;
   
   !p_smp: my_ex_signal_map_u;
   
   event clock is only @p_smp.clock;
 };
 '>
 ===================================================================================================================
 File: my_ex_bfm
 <'
 unit my_ex_bfm_u like uvm_bfm {
   if_name: my_ex_if_t;
   
   !p_smp : my_ex_signal_map_u;
   !p_drv : my_ex_driver_u;
   
   event clock is @p_smp.clock;
   
   protocol( tr : my_ex_item )@clock is empty;
   
   get_and_drive_item()@clock is {
     var tr_item: my_ex_item;
     
     while( TRUE ) {
       tr_item = p_drv.get_next_item();
       protocol( tr_item );
       emit p_drv.item_done;
     };
   };
   
   run() is also {
     start get_and_drive();
   };
 };
 '>
 ===================================================================================================================
 File: my_ex_bfm_protocol
 <'
 extend my_ex_bfm_u {
   // TODO: Implement the specific protocol
   protocol( tr: my_ex_item )@clock is {
     // ...
   };
 };
 '>
 ===================================================================================================================
 File: my_ex_agent
 <'
 unit my_ex_agent_u like uvm_agent {
   if_name: my_ex_if_t;
 
   smp    : my_ex_signal_map_u is instance;
   mon    : my_ex_monitor_u    is instance;
   
   keep MY_EX_AGENT_IF_NAME_C is all of {
     smp.if_name == read_only( if_name );
     mon.if_name == read_only( if_name );
   };
   
   connect_pointers() is also {
     mon.p_smp = smp;
   };
   
   when ACTIVE {
     bfm: my_ex_bfm_u is instance;
     drv: my_ex_drv_u is instance;
     
     keep MY_EX_ACTIVE_AGENT_IF_NAME_C is all of {
       bfm.smp.if_name == read_only( if_name );
       drv.smp.if_name == read_only( if_name );
     };
     
     connect_pointers() is also {
       bfm.p_smp = smp;
       drv.p_smp = smp;
       bfm.p_drv = drv;
     };
   };
 };
 '>
 ===================================================================================================================
 File: my_ex_environment
 <'
 unit my_ex_env_u like uvm_env {
   env_name: my_ex_name_t;
   
   agents  : list of my_ex_agent_u is instance;
   
   keep MY_EX_ENV_DEFAULT_C is all of {
     soft agents.size() == 1;
   };
 };
 '>
 ===================================================================================================================
 File: my_ex_checks
 <'
 // TODO: Implement protocol specific checks
 
 '>
 ===================================================================================================================
 File: my_ex_sequences
 <'
 extend my_ex_sequence_kind_t: [NEW_SIMPLE];
 extend NEW_SIMPLE my_ex_sequence_s {
   
   simple_tr: my_ex_item;
   
   body()@driver.clock is only {
     do simple_tr keeping {
       .request_delay == 0;
     };
   };
 };
 '>
 ===================================================================================================================
 File: my_ex_coverage
 <'
 // TODO: Implement protocol specific coverage
 
 '>
 ===================================================================================================================
 
 ===================================================================================================================
 File: my_ex_tb_inst.e
 
 <'
 extend sys {
   my_ex_env_inst : my_ex_env_u is instance;
   
   keep SYS_INST_C is all of {
     my_ex_env_inst.agents.size() == 2;
     my_ex_env_inst.agents[0].active_passive == PASSIVE;
     my_ex_env_inst.agents[1].active_passive == ACTIVE;
   };
 };
 '>
 
 **/
void spec_InterfaceVerificationComponentTemplate::createSmpUnit() {
    /*
     * File: my_ex_signal_map.e
 <'
 unit my_ex_signal_map_u like uvm_signal_map {
   if_name: my_ex_if_t;
   
   clk_ip  : in simple_port of bit is instance;
   
   event clock is rise( clk_ip$ )@sim;
   
   reset_ip: in simple_port of bit is instance;
   
   my_ex_request_iop : inout simple_port of bit is instance;
   my_ex_grant_iop   : inout simple_port of bit is instance;
   my_ex_data_iop    : inout simple_port of uint(bits: 32) is instance;
   
   keep MY_EX_CLK_IP_C is all of {
     soft clk_ip.hdl_path() == "top_clk";
     bind( clk_ip, external );
   };
   
   keep MY_EX_RESET_IP_C is all of {
     soft reset_ip.hdl_path() == "top_reset";
     bind( reset_ip, external );
   };
   
   keep MY_EX_REQUEST_IOP_C is all of {
     soft my_ex_request_iop.hdl_path() == "top_request";
     bind( my_ex_request_iop, external );
   };
   
   keep MY_GRANT_IOP_C is all of {
     soft my_grant_iop.hdl_path() == "top_grant";
     bind( my_grant_iop, external );
   };
   
   keep MY_EX_DATA_IOP_C is all of {
     soft my_ex_data_iop.hdl_path() == "top_data";
     bind( my_ex_data, external );
   };
 };
 '>
     * */
    qDebug() << "Creating Signal Map";
    
    // TODO: Extract the groupName and interfaceName from the GUI
    QString prefix = groupName + "_" + interfaceName + "_" ;
    
//    smpUnit -> setName      (  prefix + "smp_u" );
//    smpUnit -> setStructUnit( true              );
//    smpUnit -> setList      ( false             );
    
    // Now add the fields
    // if_name: my_ex_if_t;
//    smpUnit -> addField(
//                "if_name"   , // field name
//                "my_ex_if_t", // field type
//                false       , // use do not generate
//                false       , // is unit
//                false       , // physical field
//                0           , // list of level depth
//                ""            // use default value
//    );
    
//    // clk_ip  : in simple_port of bit is instance;
//    smpUnit -> addField(
//                "clk_ip"                            , // name
//                "in simple_port of uint(bits: 1)"   , // type
//                false                               , // use do not generate
//                true                                , // is unit
//                false                               , // is physical
//                0                                   , // list of level depth
//                ""                                    // default value
//    );
    
//    // reset_ip: in simple_port of bit is instance;
//    smpUnit -> addField(
//                "reset_ip",
//                "in simple_port of uint(bits: 1)",
//                false,
//                true,
//                false,
//                0,
//                ""
//    );
    
//    // my_ex_request_iop : inout simple_port of bit is instance;
//    smpUnit -> addField(
//                "my_ex_request_iop",
//                "inout simple_port of uint(bits: 1)",
//                false,
//                true,
//                false,
//                0,
//                ""
//    );
    
//    // my_ex_grant_iop   : inout simple_port of bit is instance;
//    smpUnit -> addField(
//                "my_ex_grant_iop",
//                "inout simple_port of uint(bits: 1)",
//                false,
//                true,
//                false,
//                0,
//                ""
//    );
    
//    // my_ex_data_iop    : inout simple_port of uint(bits: 32) is instance;
//    smpUnit -> addField(
//                "my_ex_data_iop",
//                "inout simple_port of uint(bits: 32)",
//                false,
//                true,
//                false,
//                0
//                ,
//                ""
//    );
    
//    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    // Now adding TEMPORALS 
    
//    // event clock is rise( clk_ip$ )@sim;
//    smpUnit -> addTemporal(
//                "clock"                     , // argTemporalName
//                false                       , // argIsExpect
//                "rise( clk_ip$ )@sim"         // argTemporalExpression
//    );
    
//    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//    // Now adding CONSTRAINTS. 
//    //Please note that currentl only single line constraints, not grouped constraints are possible
    
//    // keep soft clk_ip.hdl_path() == "top_clk";
//    smpUnit -> addConstraint(
//                "clk_ip.hdl_path() == \"top_clk\"",
//                true,
//                ""
//    );
    
//    // keep bind( clk_ip, external );
//    smpUnit -> addConstraint(
//                "bind( clk_ip, external )",
//                false,
//                ""
//    );
    
//    // keep soft reset_ip.hdl_path() == "top_reset";
//    smpUnit -> addConstraint(
//                "reset_ip.hdl_path == \"top_reset\"",
//                true,
//                ""
//    );
//    // keep bind( reset_ip, external );
//    smpUnit -> addConstraint(
//                "bind( reset_ip, external )",
//                false,
//                ""
//    );
    
//    // keep soft my_ex_request_iop.hdl_path() == "top_request";
//    smpUnit -> addConstraint(
//                "my_ex_request_iop.hdl_path == \"top_request\"",
//                true,
//                ""
//    );
//    // keep bind( my_ex_request_iop, external );
//    smpUnit -> addConstraint(
//                "bind( my_ex_request_iop, external )",
//                false,
//                ""
//    );
//    // keep soft my_grant_iop.hdl_path() == "top_grant";
//    smpUnit -> addConstraint(
//                "my_grant_iop.hdl_path == \"top_grant\"",
//                true,
//                ""
//    );
//    // keep bind( my_grant_iop, external );
//    smpUnit -> addConstraint(
//                "bind( my_grant_iop, external )",
//                false,
//                ""
//    );
//    // keep soft my_ex_data_iop.hdl_path() == "top_data";
//    // keep bind( my_ex_data, external );
//    smpUnit -> addConstraint(
//                "bind( my_ex_data_iop, external )",
//                false,
//                ""
//    );
};
