// ----------------------------------------------------------------------------
//   Copyright 2018 Daniel Bayer
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_hierarchytreewidgeteditor.h"

spec_HierarchyTreeWidgetEditor::spec_HierarchyTreeWidgetEditor(){
//    // Set the column names
//    QStringList headerLabels;
//    headerLabels << "ID"        // index 0
//                 <<  "Name"     // index 1
//                 << "Type"      // index 2
//                 << "Unit"      // index 3
//                 << "List"      // index 4
//                 << "Notes";    // index 5
    
//    // Set the number of columns
//    setColumnCount( headerLabels.count() );
    
//    setHeaderLabels( headerLabels );
    
//    headerItem()->setFont(0, QFont("Courier New"));
//    headerItem()->setFont(1, QFont("Courier New"));
//    headerItem()->setFont(2, QFont("Courier New"));
//    headerItem()->setFont(3, QFont("Courier New"));
//    headerItem()->setFont(4, QFont("Courier New"));
//    headerItem()->setFont(5, QFont("Courier New"));
    
//    setColumnWidth(3, 30);
//    setColumnWidth(4, 30);
    
    
//    // Add first line that shows
//    //"ID"    -> @0
//    //"Name"  -> sys
//    //"Unit"  -> CheckMark
//    //"List"  -> CheckMark
//    //"Type"  -> sys
//    //"Notes" -> "This is the root-level entry of the testbench"
//    spec_struct_data_node *globalNode    = new spec_struct_data_node();
//    spec_struct_data_node *sysNode       = new spec_struct_data_node();
    
//    globalNode -> setId( 0 );
//    globalNode -> setName( tr("Global") );
//    globalNode -> setStructType( "" );
//    globalNode -> setStructUnit( false );
//    globalNode -> setList( false );
//    globalNode -> setNotes( tr("Global scope contains class declarations, no instantiations") );
    
//    addTopLevelItem( globalNode );
    
//    // This shows as an example how to add nodes to the QTreeListWidget
//    sysNode -> setId        ( curRowIndex++                                         );
//    sysNode -> setName      ( tr("sys")                                             );
//    sysNode -> setStructType( tr("sys")                                             );
//    sysNode -> setStructUnit( true                                                  );
//    sysNode -> setList      ( false                                                 );
//    sysNode -> setNotes     ( tr("sys is the testbench root and can't be removed")  );
    
////    qDebug()  << "Creating the TreeWidget " << sysNode;
    
//    addTopLevelItem( sysNode );
};

spec_struct_data_node *spec_HierarchyTreeWidgetEditor::getTopLevelItem(int index) {
    return (spec_struct_data_node *) topLevelItem( index );
};

void spec_HierarchyTreeWidgetEditor::addSiblingNode() {
//    // This list contains either no elements, or a single element, because the QTreeWidget will not allow multi
//    // selection items
//    QList <QTreeWidgetItem *> curItems = selectedItems();
    
//    if( curItems.count() == 0 ) {
//        qDebug() << "No item selected, not adding a sibling.";
//    } else {
//        QModelIndexList selectedIndexList = selectedIndexes();
////        qDebug() << "Selected index " << selectedIndexList.at(0).row();
        
//        // Get the parentNode of the selected current item
//        spec_struct_data_node *parentNode = (spec_struct_data_node *) curItems.at(0) -> parent();
        
//        // In case the parentNode does not exist, then we'll indicate a warning and continue to proceed adding a child
//        // node instead.
//        if( parentNode == nullptr ) {
//            qDebug() << "Root element selected, can't add sibling, adding child instead!!";
//            addChildNode();
//        } else {
//            // If a parent node was found, then we'll add the child node to the end of the sibling list
//            spec_struct_data_node *newNode = new spec_struct_data_node();
//            newNode    -> setId      ( curRowIndex++ );
//            newNode    -> setFlags   ( newNode->flags() | (Qt::ItemIsEditable) );
            
//            parentNode -> addChild   ( newNode );
//            parentNode -> setExpanded( true );
//        };
//    };
};

void spec_HierarchyTreeWidgetEditor::addChildNode() {
//    QList <QTreeWidgetItem *> curItems = selectedItems();
    
//    if( curItems.count() == 0 ) {
//        qDebug() << "No item selected, not adding a child.";
//    } else {
//        // Get the current item, which can only be the one at the top
//        spec_struct_data_node *currentNode = (spec_struct_data_node *) curItems.at( 0 );
//        spec_struct_data_node *newNode     = new spec_struct_data_node();
        
//        newNode     -> setId      ( curRowIndex++ );
//        newNode     -> setFlags   ( newNode->flags() | (Qt::ItemIsEditable) );
        
//        currentNode -> addChild   ( newNode );
//        currentNode -> setExpanded( true );
//    };
};

void spec_HierarchyTreeWidgetEditor::createStructDeclarations() {
//    // The idCounter in global has not correlation to the idCounter in the sys
//    int idCounter = 0;
    
//    // Getting the pointer to global and creating a new node that is used for constructing classes
//    spec_struct_data_node *globalNode = getTopLevelItem(0);
//    spec_struct_data_node *buildNode  = new spec_struct_data_node();
    
//    ////////////////////////////////////////////////////////////////////////////////////////////////////
//    // Declaring: my_ex_item_s like any_sequence_item
//    buildNode -> setId          ( ++idCounter );
//    buildNode -> setName        ( "my_ex_item_s" );
//    buildNode -> setStructType  ( "any_sequence_item" );
//    buildNode -> setStructUnit  ( false );
//    buildNode -> setList        ( false );
    
//    buildNode -> addNodeContent(
//        "data"          , // name
//        "uint(bits: 32)", // type
//        false           , // Do-Not-Generate
//        true            , // Physical
//        false           , // list
//        false           , // unit
//        ""                // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "request_delay" , // name
//        "uint(bits: 32)", // type
//        false           , // Do-Not-Generate
//        false           , // Physical
//        false           , // list
//        false           , // unit
//        ""                // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "grant_delay"   , // name
//        "uint(bits: 32)", // type
//        false           , // Do-Not-Generate
//        false           , // Physical
//        false           , // list
//        false           , // unit
//        ""                // initial value
//    );
    
//    globalNode -> addChild( buildNode );
    
//    ////////////////////////////////////////////////////////////////////////////////////////////////////
//    // Declaring: my_ex_signal_map
//    buildNode  = new spec_struct_data_node();
//    buildNode -> setId          ( ++idCounter );
//    buildNode -> setName        ( "my_ex_signal_map_u" );
//    buildNode -> setStructType  ( "uvm_signal_map" );
//    buildNode -> setStructUnit  ( true );
//    buildNode -> setList        ( false );
    
//    buildNode -> addNodeContent(
//        "if_name"       , // name
//        "my_ex_if_t"    , // type
//        false           , // Do-Not-Generate
//        false           , // Physical
//        false           , // list
//        false           , // unit
//        ""                // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "clk_ip"                , // name
//        "in simple_port of bit" , // type
//        false                   , // Do-Not-Generate
//        false                   , // Physical
//        false                   , // list
//        true                    , // unit
//        ""                        // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "reset_ip"              , // name
//        "in simple_port of bit" , // type
//        false                   , // Do-Not-Generate
//        false                   , // Physical
//        false                   , // list
//        true                    , // unit
//        ""                        // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "request_iop"           , // name
//        "in simple_port of bit" , // type
//        false                   , // Do-Not-Generate
//        false                   , // Physical
//        false                   , // list
//        true                    , // unit
//        ""                        // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "grant_iop"             , // name
//        "in simple_port of bit" , // type
//        false                   , // Do-Not-Generate
//        false                   , // Physical
//        false                   , // list
//        true                    , // unit
//        ""                        // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "data_iop"                          , // name
//        "in simple_port of uint(bits: 32)"  , // type
//        false                               , // Do-Not-Generate
//        false                               , // Physical
//        false                               , // list
//        true                                , // unit
//        ""                                    // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "clock"                 , // name
//        "event"                 , // type
//        false                   , // Do-Not-Generate
//        false                   , // Physical
//        false                   , // list
//        true                    , // unit
//        "rise( clk_ip$ )@sim"     // initial value
//    );
    
//    globalNode -> addChild( buildNode );
    
//    ////////////////////////////////////////////////////////////////////////////////////////////////////
//    // Declaring: 
//    buildNode  = new spec_struct_data_node();
//    buildNode -> setId          ( ++idCounter );
//    buildNode -> setName        ( "my_ex_monitor_u" );
//    buildNode -> setStructType  ( "uvm_monitor" );
//    buildNode -> setStructUnit  ( true );
//    buildNode -> setList        ( false );
    
//    buildNode -> addNodeContent(
//        "if_name"       , // name
//        "my_ex_if_t"    , // type
//        false           , // Do-Not-Generate
//        false           , // Physical
//        false           , // list
//        false           , // unit
//        ""                // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "p_smp"                 , // name
//        "my_ex_signal_map_u"    , // type
//        true                    , // Do-Not-Generate
//        false                   , // Physical
//        false                   , // list
//        true                    , // unit
//        ""                        // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "clock_e"       , // name
//        "event"         , // type
//        false           , // Do-Not-Generate
//        false           , // Physical
//        false           , // list
//        false           , // unit
//        "@p_smp.clock"    // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "reset_e"                   , // name
//        "event"                     , // type
//        false                       , // Do-Not-Generate
//        false                       , // Physical
//        false                       , // list
//        false                       , // unit
//        "fall( p_smp.reset$ )@sim"    // initial value
//    );
    
//    globalNode -> addChild( buildNode );
    
//    ////////////////////////////////////////////////////////////////////////////////////////////////////
//    // Declaring: my_ex_driver
//    buildNode  = new spec_struct_data_node();
//    buildNode -> setId          ( ++idCounter );
//    buildNode -> setName        ( "my_ex_seq_s" );
//    buildNode -> setStructType  ( "sequence" );   // <-- use sequence type as keyword to create macro
//    buildNode -> setStructUnit  ( false );
//    buildNode -> setList        ( false );
    
//    buildNode -> addNodeContent(
//        "my_ex_item_s"  , // name
//        "item"          , // type
//        false           , // Do-Not-Generate
//        false           , // Physical
//        false           , // list
//        false           , // unit
//        ""                // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "my_ex_sequence_kind_t" , // name
//        "created_kind"          , // type
//        false                   , // Do-Not-Generate
//        false                   , // Physical
//        false                   , // list
//        false                   , // unit
//        ""                        // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "my_ex_driver_u"    , // name
//        "created_driver"    , // type
//        false               , // Do-Not-Generate
//        false               , // Physical
//        false               , // list
//        true                , // unit
//        ""                    // initial value
//    );
    
//    globalNode -> addChild( buildNode );
    
//    ////////////////////////////////////////////////////////////////////////////////////////////////////
//    // Declaring: my_ex_driver_u
//    buildNode  = new spec_struct_data_node();
//    buildNode -> setId          ( ++idCounter );
//    buildNode -> setName        ( "my_ex_driver_u" );
//    buildNode -> setStructType  ( "created_driver" );
//    buildNode -> setStructUnit  ( true );
//    buildNode -> setList        ( false );
    
//    buildNode -> addNodeContent(
//        "if_name"       , // name
//        "my_ex_if_t"    , // type
//        false           , // Do-Not-Generate
//        false           , // Physical
//        false           , // list
//        false           , // unit
//        ""                // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "p_smp"                 , // name
//        "my_ex_signal_map_u"    , // type
//        true                    , // Do-Not-Generate
//        false                   , // Physical
//        false                   , // list
//        true                    , // unit
//        ""                        // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "clock"             , // name
//        "event"             , // type
//        false               , // Do-Not-Generate
//        false               , // Physical
//        false               , // list
//        false               , // unit
//        "only @p_smp.clock"   // initial value
//    );
//    // Add the driver to the macro... should be handled in one file
//    globalNode -> child( globalNode -> childCount()-1 ) ->addChild(buildNode);
    
//    ////////////////////////////////////////////////////////////////////////////////////////////////////
//    // Declaring: 
//    buildNode  = new spec_struct_data_node();
//    buildNode -> setId          ( ++idCounter );
//    buildNode -> setName        ( "my_ex_bfm_u" );
//    buildNode -> setStructType  ( "uvm_bfm" );
//    buildNode -> setStructUnit  ( true );
//    buildNode -> setList        ( false );
    
//    buildNode -> addNodeContent(
//        "if_name"       , // name
//        "my_ex_if_t"    , // type
//        false           , // Do-Not-Generate
//        false           , // Physical
//        false           , // list
//        false           , // unit
//        ""                // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "p_smp"             , // name
//        "my_ex_signal_map_u", // type
//        true                , // Do-Not-Generate
//        false               , // Physical
//        false               , // list
//        true                , // unit
//        ""                    // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "p_drv"         , // name
//        "my_ex_driver_u", // type
//        true            , // Do-Not-Generate
//        false           , // Physical
//        false           , // list
//        true            , // unit
//        ""                // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "clock"         , // name
//        "event"         , // type
//        false           , // Do-Not-Generate
//        false           , // Physical
//        false           , // list
//        false           , // unit
//        "@p_smp.clock"    // initial value
//    );
    
//    globalNode -> addChild( buildNode );
    
//    ////////////////////////////////////////////////////////////////////////////////////////////////////
//    // Declaring: agent
//    buildNode  = new spec_struct_data_node();
//    buildNode -> setId          ( ++idCounter );
//    buildNode -> setName        ( "my_ex_agent" );
//    buildNode -> setStructType  ( "uvm_agent" );
//    buildNode -> setStructUnit  ( true );
//    buildNode -> setList        ( false );
    
//    buildNode -> addNodeContent(
//        "if_name"   , // name
//        "my_ex_if_t", // type
//        false       , // Do-Not-Generate
//        false       , // Physical
//        false       , // list
//        false       , // unit
//        ""            // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "smp"               , // name
//        "my_ex_signal_map_u", // type
//        false               , // Do-Not-Generate
//        false               , // Physical
//        false               , // list
//        true                , // unit
//        ""                    // initial value
//    );
//    buildNode -> addNodeContent(
//        "mon"               , // name
//        "my_ex_monitor_u"   , // type
//        false               , // Do-Not-Generate
//        false               , // Physical
//        false               , // list
//        true                , // unit
//        ""                    // initial value
//    );
    
//    globalNode -> addChild( buildNode );
    
    
//    ////////////////////////////////////////////////////////////////////////////////////////////////////
//    // Declaring: ACTIVE agent
//    buildNode  = new spec_struct_data_node();
//    buildNode -> setId          ( ++idCounter );
//    buildNode -> setName        ( "ACTIVE my_ex_agent_u" );
//    buildNode -> setStructType  ( "" );
//    buildNode -> setStructUnit  ( true );
//    buildNode -> setList        ( false );
    
//    buildNode -> addNodeContent(
//        "bfm"           , // name
//        "my_ex_bfm_u"   , // type
//        false           , // Do-Not-Generate
//        false           , // Physical
//        false           , // list
//        true            , // unit
//        ""                // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "drv"           , // name
//        "my_ex_drv_u"   , // type
//        false           , // Do-Not-Generate
//        false           , // Physical
//        false           , // list
//        true            , // unit
//        ""                // initial value
//    );
    
//    globalNode -> child( globalNode -> childCount()-1 ) ->addChild(buildNode);
    
    
//    ////////////////////////////////////////////////////////////////////////////////////////////////////
//    // Declaring: my_ex_environmant
//    buildNode  = new spec_struct_data_node();
//    buildNode -> setId          ( ++idCounter );
//    buildNode -> setName        ( "my_ex_env_u" );
//    buildNode -> setStructType  ( "uvm_env" );
//    buildNode -> setStructUnit  ( true );
//    buildNode -> setList        ( false );
    
//    buildNode -> addNodeContent(
//        "env_name"      , // name
//        "my_ex_name_t"  , // type
//        false           , // Do-Not-Generate
//        false           , // Physical
//        false           , // list
//        false           , // unit
//        ""                // initial value
//    );
    
//    buildNode -> addNodeContent(
//        "agents"        , // name
//        "my_ex_agent_u" , // type
//        false           , // Do-Not-Generate
//        false           , // Physical
//        true            , // list
//        true            , // unit
//        ""                // initial value
//    );
    
//    globalNode -> addChild( buildNode );
    
    
////    ////////////////////////////////////////////////////////////////////////////////////////////////////
////    // Declaring: 
////    buildNode  = new spec_HierarchyDataNode();
////    buildNode -> setId          ( ++idCounter );
////    buildNode -> setName        ( "" );
////    buildNode -> setStructType  ( "" );
////    buildNode -> setStructUnit  ( false );
////    buildNode -> setList        ( false );
    
////    buildNode -> addNodeContent(
////        ""   , // name
////        "", // type
////        false           , // Do-Not-Generate
////        false           , // Physical
////        false           , // list
////        false           , // unit
////        ""                // initial value
////    );
    
////    globalNode -> addChild( buildNode );
    
//    // Expand the full global node after declaring all classes
//    globalNode -> setExpanded( true );
//};

//void spec_HierarchyTreeWidgetEditor::loadTemplate() {
//    qDebug() << "Loading template TODO: Make configurable via dialog";
    
//    // The following shows the e code example that will be created as the default skeleton:
//    /*
//     * 
//     /////// INTEFACE eVC \\\\\\\\\
     
//     File: my_ex_top.e
//     <'
//     import uvm_e/e/uvm_e_top;
     
//     import my_ex/e/my_ex_types;
//     import my_ex/e/my_ex_item;
//     import my_ex/e/my_ex_signal_map;
//     import my_ex/e/my_ex_monitor;
//     import my_ex/e/my_ex_monitor_protocol;
//     import my_ex/e/my_ex_driver;
//     import my_ex/e/my_ex_bfm;
//     import my_ex/e/my_ex_bfm_protocol;
//     import my_ex/e/my_ex_agent;
//     import my_ex/e/my_ex_environment;
     
//     import my_ex/e/my_ex_checks;
//     import my_ex/e/my_ex_sequences;
//     import my_ex/e/my_ex_coverage;
//     '>
//     ===================================================================================================================
//     File: my_ex_types.e
//     <'
//     type my_ex_name_t: [DEFAULT];
//     type my_ex_if_t  : [IF0, IF1, IF2, IF3, IF4, IF5, IF6, IF7, IF8, IF9, IF10];
//     '>
//     ===================================================================================================================
//     File: my_ex_item.e
//     <'
//     struct my_ex_item_s like any_sequence_item {
//       %data        : uint(bits: 32);
       
//       request_delay: uint(bits: 32);
       
//       grant_delay  : uint(bits: 32);
//     };
//     '>
//     ===================================================================================================================
//     File: my_ex_signal_map.e
//     <'
//     unit my_ex_signal_map_u like uvm_signal_map {
//       if_name: my_ex_if_t;
       
//       clk_ip  : in simple_port of bit is instance;
       
//       event clock is rise( clk_ip$ )@sim;
       
//       reset_ip: in simple_port of bit is instance;
       
//       my_ex_request_iop : inout simple_port of bit is instance;
//       my_ex_grant_iop   : inout simple_port of bit is instance;
//       my_ex_data_iop    : inout simple_port of uint(bits: 32) is instance;
       
//       keep MY_EX_CLK_IP_C is all of {
//         soft clk_ip.hdl_path() == "top_clk";
//         bind( clk_ip, external );
//       };
       
//       keep MY_EX_RESET_IP_C is all of {
//         soft reset_ip.hdl_path() == "top_reset";
//         bind( reset_ip, external );
//       };
       
//       keep MY_EX_REQUEST_IOP_C is all of {
//         soft my_ex_request_iop.hdl_path() == "top_request";
//         bind( my_ex_request_iop, external );
//       };
       
//       keep MY_GRANT_IOP_C is all of {
//         soft my_grant_iop.hdl_path() == "top_grant";
//         bind( my_grant_iop, external );
//       };
       
//       keep MY_EX_DATA_IOP_C is all of {
//         soft my_ex_data_iop.hdl_path() == "top_data";
//         bind( my_ex_data, external );
//       };
//     };
//     '>
//     ===================================================================================================================
//     File: my_ex_monitor
//     <'
//     unit my_ex_monitor_u like uvm_monitor {
//       if_name: my_ex_if_t;
       
//       !p_smp: my_ex_signal_map_u;
       
//       event clock is @p_smp.clock;
//       event reset is fall( @p_smp.reset$ )@sim;
       
//       monitor()@clock is empty;
       
//       on reset {
//         rerun();
//       };
       
//       run() is also {
//         start monitor();
//       };
//     };
//     '>
//     ===================================================================================================================
//     File: my_ex_monitor_protocol
//     <'
//     extend my_ex_monitor_u
//       // TODO: Implement the low-level monitor protocol
//       monitor()@clock is {
//         // ...
//       };
//     };
//     '>
//     ===================================================================================================================
//     File: my_ex_driver
//     <'
//     sequence my_ex_seq_s using
//       item           = my_ex_item_s
//       created_kind   = my_ex_sequence_kind_t,
//       created_driver = my_ex_driver_u;
     
//     extend my_ex_driver_u {
//       if_name: my_ex_if_t;
       
//       !p_smp: my_ex_signal_map_u;
       
//       event clock is only @p_smp.clock;
//     };
//     '>
//     ===================================================================================================================
//     File: my_ex_bfm
//     <'
//     unit my_ex_bfm_u like uvm_bfm {
//       if_name: my_ex_if_t;
       
//       !p_smp : my_ex_signal_map_u;
//       !p_drv : my_ex_driver_u;
       
//       event clock is @p_smp.clock;
       
//       protocol( tr : my_ex_item )@clock is empty;
       
//       get_and_drive_item()@clock is {
//         var tr_item: my_ex_item;
         
//         while( TRUE ) {
//           tr_item = p_drv.get_next_item();
//           protocol( tr_item );
//           emit p_drv.item_done;
//         };
//       };
       
//       run() is also {
//         start get_and_drive();
//       };
//     };
//     '>
//     ===================================================================================================================
//     File: my_ex_bfm_protocol
//     <'
//     extend my_ex_bfm_u {
//       // TODO: Implement the specific protocol
//       protocol( tr: my_ex_item )@clock is {
//         // ...
//       };
//     };
//     '>
//     ===================================================================================================================
//     File: my_ex_agent
//     <'
//     unit my_ex_agent_u like uvm_agent {
//       if_name: my_ex_if_t;
     
//       smp    : my_ex_signal_map_u is instance;
//       mon    : my_ex_monitor_u    is instance;
       
//       keep MY_EX_AGENT_IF_NAME_C is all of {
//         smp.if_name == read_only( if_name );
//         mon.if_name == read_only( if_name );
//       };
       
//       connect_pointers() is also {
//         mon.p_smp = smp;
//       };
       
//       when ACTIVE {
//         bfm: my_ex_bfm_u is instance;
//         drv: my_ex_drv_u is instance;
         
//         keep MY_EX_ACTIVE_AGENT_IF_NAME_C is all of {
//           bfm.smp.if_name == read_only( if_name );
//           drv.smp.if_name == read_only( if_name );
//         };
         
//         connect_pointers() is also {
//           bfm.p_smp = smp;
//           drv.p_smp = smp;
//           bfm.p_drv = drv;
//         };
//       };
//     };
//     '>
//     ===================================================================================================================
//     File: my_ex_environment
//     <'
//     unit my_ex_env_u like uvm_env {
//       env_name: my_ex_name_t;
       
//       agents  : list of my_ex_agent_u is instance;
       
//       keep MY_EX_ENV_DEFAULT_C is all of {
//         soft agents.size() == 1;
//       };
//     };
//     '>
//     ===================================================================================================================
//     File: my_ex_checks
//     <'
//     // TODO: Implement protocol specific checks
     
//     '>
//     ===================================================================================================================
//     File: my_ex_sequences
//     <'
//     extend my_ex_sequence_kind_t: [NEW_SIMPLE];
//     extend NEW_SIMPLE my_ex_sequence_s {
       
//       simple_tr: my_ex_item;
       
//       body()@driver.clock is only {
//         do simple_tr keeping {
//           .request_delay == 0;
//         };
//       };
//     };
//     '>
//     ===================================================================================================================
//     File: my_ex_coverage
//     <'
//     // TODO: Implement protocol specific coverage
     
//     '>
//     ===================================================================================================================
     
//     ===================================================================================================================
//     File: my_ex_tb_inst.e
     
//     <'
//     extend sys {
//       my_ex_env_inst : my_ex_env_u is instance;
       
//       keep SYS_INST_C is all of {
//         my_ex_env_inst.agents.size() == 2;
//         my_ex_env_inst.agents[0].active_passive == PASSIVE;
//         my_ex_env_inst.agents[1].active_passive == ACTIVE;
//       };
//     };
//     '>
     
//     **/
    
//    // Adding variables to track the tree location
//    int id_counter          = 0;
//    int agent_list_index    = 0;
    
//    // Getting sys
//    spec_struct_data_node *currentNode = getTopLevelItem(1);
//    spec_struct_data_node *buildNode   = new spec_struct_data_node();
    
//    // Set new node properties
//    buildNode -> setId          ( ++id_counter );
//    buildNode -> setName        ( tr("my_ex_env_inst") );
//    buildNode -> setStructType  ( tr("my_ex_env_u"));
//    buildNode -> setStructUnit  ( true );
//    buildNode -> setList        ( false );
    
//    buildNode -> addNodeContent(
//        tr("env_name"),
//        tr("my_ex_name_t"),
//        false,
//        false,
//        false,
//        false,
//        tr("DEFAULT")
//    );
    
//    // Add new node as a child
//    currentNode -> addChild( buildNode );
//    currentNode -> setExpanded( true );
    
//    // Get new node as current node
//    // currentNode is now instance of --> my_ex_env_u <--
//    currentNode = (spec_struct_data_node *) currentNode -> child( 0 );
    
//    buildNode = new spec_struct_data_node();
//    buildNode -> setId          ( ++id_counter );
//    buildNode -> setName        ( tr("agents") );
//    buildNode -> setStructType  ( tr("my_ex_agent_u"));
//    buildNode -> setStructUnit  ( true );
//    buildNode -> setList        ( true );
    
//    buildNode -> addNodeContent(
//        tr("if_name"),
//        tr("my_ex_if_t"),
//        false,
//        false,
//        false,
//        false
//    );
    
//    // Add new node as a child
//    currentNode -> addChild( buildNode );
//    currentNode -> setExpanded( true );
    
//    // Get new node as current node
//    // currentNode is now --> agents <--
//    currentNode = (spec_struct_data_node *) currentNode -> child( 0 );
    
//    id_counter = addTemplateAgent( 0, id_counter, false, currentNode );
//    ((spec_struct_data_node *) currentNode ->child(0))->addNodeContent(
//        "active_passive",
//        "erm_active_passive",
//        false,
//        false,
//        false,
//        false,
//        "PASSIVE"
//    );
    
//    id_counter = addTemplateAgent( 1, id_counter, true, currentNode );
//    ((spec_struct_data_node *) currentNode ->child(1))->addNodeContent(
//        "active_passive",
//        "erm_active_passive",
//        false,
//        false,
//        false,
//        false,
//        "ACTIVE"
//    );
    
};

int spec_HierarchyTreeWidgetEditor::addTemplateAgent( int elem_index, int id_counter, bool isActive, spec_struct_data_node * currentNode ) {
//    spec_struct_data_node *buildNode   = new spec_struct_data_node();
    
//    QString stringBuilder = "agents[" + QString::number(elem_index) + "]";
    
//    // The agents list is the currentNode pointer object
//    // Creating and adding "agents[elem_index]"
//    buildNode = new spec_struct_data_node();
//    buildNode -> setId          ( ++id_counter );
//    buildNode -> setName        ( stringBuilder );
//    buildNode -> setStructType  ( tr("my_ex_agent_u"));
//    buildNode -> setStructUnit  ( true );
//    buildNode -> setList        ( false );
    
//    stringBuilder = "IF" + QString::number(elem_index);
//    buildNode -> addNodeContent(
//        tr("if_name"),
//        tr("my_ex_if_t"),
//        false,
//        false,
//        false,
//        false,
//        stringBuilder
//    );
    
//    currentNode -> addChild( buildNode );   // adding "agents[elem_index]"
//    currentNode -> setExpanded( true );
    
//    // Creating "smp" agent[elem_index]
//    buildNode = new spec_struct_data_node();
//    buildNode -> setId          ( ++id_counter );
//    buildNode -> setName        ( tr("smp") );
//    buildNode -> setStructType  ( tr("my_ex_signal_map_u"));
//    buildNode -> setStructUnit  ( true );
//    buildNode -> setList        ( false );
    
//    buildNode -> addNodeContent(
//        tr("if_name"),
//        tr("my_ex_if_t"),
//        false,
//        false,
//        false,
//        false,
//        stringBuilder
//    );
    
//    buildNode -> addNodeContent(
//        tr("clk_ip"),
//        tr("in simple_port of bit"),
//        false,
//        false,
//        false,
//        true,
//        tr("")
//    );
    
//    buildNode -> addNodeContent(
//        tr("clock"),
//        tr("event"),
//        false,
//        false,
//        false,
//        false,
//        tr(" rise( clk_ip$ )@sim")
//    );
    
//    buildNode -> addNodeContent(
//        tr("rest_ip"),
//        tr("in simple_port of bit"),
//        false,
//        false,
//        false,
//        true,
//        tr("")
//    );
    
//    buildNode -> addNodeContent(
//        tr("my_ex_request_iop"),
//        tr("inout simple_port of bit"),
//        false,
//        false,
//        false,
//        true,
//        tr("")
//    );
    
//    buildNode -> addNodeContent(
//        tr("my_ex_grant_iop"),
//        tr("inout of simple_port of bit"),
//        false,
//        false,
//        false,
//        true,
//        tr("")
//    );
    
//    buildNode -> addNodeContent(
//        tr("my_ex_data_iop"),
//        tr("inout simple_port of uint(bits: 32)"),
//        false,
//        false,
//        false,
//        true,
//        tr("")
//    );
    
//    currentNode -> child( elem_index ) -> addChild( buildNode );   // adding "smp" agent[elem_index]
//    currentNode -> child( elem_index ) -> setExpanded( true );
    
//    // Creating "mon" agent[elem_index]
//    buildNode = new spec_struct_data_node();
//    buildNode -> setId          ( ++id_counter );
//    buildNode -> setName        ( tr("mon") );
//    buildNode -> setStructType  ( tr("my_ex_monitor_u"));
//    buildNode -> setStructUnit  ( true );
//    buildNode -> setList        ( false );
    
//    buildNode -> addNodeContent(
//        tr("if_name"),
//        tr("my_ex_if_t"),
//        false,
//        false,
//        false,
//        false,
//        stringBuilder
//    );
    
//    buildNode -> addNodeContent(
//        tr("p_smp"),
//        tr("my_ex_signal_map_u"),
//        true,
//        false,
//        false,
//        false
//    );
    
//    buildNode -> addNodeContent(
//        tr("clock"),
//        tr("event"),
//        false,
//        false,
//        false,
//        false,
//        " @p_smp.clock"
//    );
    
//    buildNode -> addNodeContent(
//        tr("reset"),
//        tr("event"),
//        false,
//        false,
//        false,
//        false,
//        " fall( @p_smp.reset$ )@sim"
//    );
    
//    currentNode -> child( elem_index ) -> addChild( buildNode );   // adding "smp" agent[elem_index]
//    currentNode -> child( elem_index ) -> setExpanded( true );
    
//    if( isActive ) {
//        // Creating "drv" on agents[elem_index]
//        buildNode = new spec_struct_data_node();
//        buildNode -> setId          ( ++id_counter );
//        buildNode -> setName        ( tr("drv") );
//        buildNode -> setStructType  ( tr("my_ex_driver_u"));
//        buildNode -> setStructUnit  ( true );
//        buildNode -> setList        ( false );
        
//        buildNode -> addNodeContent(
//            tr("if_name"),
//            tr("my_ex_if_t"),
//            false,
//            false,
//            false,
//            false,
//            stringBuilder
//        );
        
//        buildNode -> addNodeContent(
//            tr("p_smp"),
//            tr("my_ex_signal_map_u"),
//            true,
//            false,
//            false
//        );
        
//        buildNode -> addNodeContent(
//            tr("clock"),
//            tr("event"),
//            false,
//            false,
//            false,
//            false,
//            " only @p_smp.clock"
//        );
        
//        currentNode -> child( elem_index ) -> addChild( buildNode );   // adding "smp" agent[elem_index]
//        currentNode -> child( elem_index ) -> setExpanded( true );        
        
//        // Creating "bfm" on agents[elem_index]
//        buildNode = new spec_struct_data_node();
//        buildNode -> setId          ( ++id_counter );
//        buildNode -> setName        ( tr("bfm") );
//        buildNode -> setStructType  ( tr("my_ex_bfm_u"));
//        buildNode -> setStructUnit  ( true );
//        buildNode -> setList        ( false );
        
//        buildNode -> addNodeContent(
//            tr("if_name"),
//            tr("my_ex_if_t"),
//            false,
//            false,
//            false,
//            false,
//            stringBuilder
//        );
        
//        buildNode -> addNodeContent(
//            tr("p_smp"),
//            tr("my_ex_signal_map_u"),
//            true,
//            false,
//            false
//        );
        
//        buildNode -> addNodeContent(
//            tr("p_drv"),
//            tr("my_ex_driver_u"),
//            false,
//            false,
//            false
//        );
        
//        buildNode -> addNodeContent(
//            tr("clock"),
//            tr("event"),
//            false,
//            false,
//            false,
//            false,
//            " @p_smp.clock"
//        );
        
//        currentNode -> child( elem_index ) -> addChild( buildNode );   // adding "smp" agent[elem_index]
//        currentNode -> child( elem_index ) -> setExpanded( true );        
//    };
    
//    return id_counter;
};

void spec_HierarchyTreeWidgetEditor::removeNode() {
    
//    qDebug() << "Called removeNode()";
    
//    QList <QTreeWidgetItem*> curItems = selectedItems();
    
//    if( curItems.count() == 0 ) {
        
//    } else {
//        spec_HierarchyDataNode *currentNode = (spec_HierarchyDataNode *) curItems.at( 0 );
//        spec_HierarchyDataNode *parentNode  = (spec_HierarchyDataNode *) curItems.at( 0 )->parent();
        
//        qDebug() << "Child Count: " << currentNode->childCount();
        
//        spec_HierarchyDataNode *sysNode = new spec_HierarchyDataNode();
        
//        for (int c_idx = 0; c_idx < currentNode->childCount(); ++c_idx) {
            // TODO: Implement the addChild method... for some reason this doesn't seem to work properly
//            parentNode->addChild( currentNode->child(0) );
//        };
//        qDebug() << "Child Count: " << currentNode->childCount();
        
////        delete currentNode;
//    };
};

void spec_HierarchyTreeWidgetEditor::removeSubtree() {
////    qDebug() << "Called removeSubtree()";
//    // Get the pointer to the selected item
//    QList <QTreeWidgetItem*> curItems = selectedItems();
    
//    // Don't delete anything if there is not selection made
//    if( curItems.count() == 0 ) {
//        qDebug() << "No node selected, not deleting anything";
//    } else {
//        // Get the currently selected element
//        spec_struct_data_node *currentNode = (spec_struct_data_node *) curItems.at( 0 );
//        // And make sure you only delete the node if the ID is != 0
//        if( currentNode->getId() != 0 ) {
//            delete currentNode;
//        };
//    };
};
