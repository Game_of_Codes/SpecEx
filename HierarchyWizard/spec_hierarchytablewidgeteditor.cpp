// ----------------------------------------------------------------------------
//   Copyright 2018 Daniel Bayer
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#include "spec_hierarchytablewidgeteditor.h"

// This constructor has already been implemented in the DanielBaseClass
spec_HierarchyTableWidgetEditor::spec_HierarchyTableWidgetEditor() {
    int colC = 7;
    
    setColumnCount( colC );
    setHorizontalHeaderItem( 0, new QTableWidgetItem("Name"));
    
    setHorizontalHeaderItem( 1, new QTableWidgetItem("Type"));
    setHorizontalHeaderItem( 2, new QTableWidgetItem("!"));
    setHorizontalHeaderItem( 3, new QTableWidgetItem("%"));
    setHorizontalHeaderItem( 4, new QTableWidgetItem("List"));
    setHorizontalHeaderItem( 5, new QTableWidgetItem("Unit"));
    setHorizontalHeaderItem( 6, new QTableWidgetItem("Value"));
    
    horizontalHeaderItem(0)->setFont( QFont("Courier New") );   // Name
    horizontalHeaderItem(1)->setFont( QFont("Courier New") );   // Type
    horizontalHeaderItem(2)->setFont( QFont("Courier New") );   // !
    horizontalHeaderItem(3)->setFont( QFont("Courier New") );   // %
    horizontalHeaderItem(4)->setFont( QFont("Courier New") );   // List
    horizontalHeaderItem(5)->setFont( QFont("Courier New") );   // Unit
    horizontalHeaderItem(6)->setFont( QFont("Courier New") );   // Value
    
    setColumnWidth(2, 30);
    setColumnWidth(3, 30);
    setColumnWidth(4, 50);
    setColumnWidth(5, 50);
    
    setSelectionMode(QAbstractItemView::SingleSelection);
    
    connect(
        this, SIGNAL( cellChanged      (int, int)   ),
        this, SLOT  ( changeCellContent(int, int)   )
    );
    connect(
        this, SIGNAL( itemSelectionChanged()        ),
        this, SLOT  ( processSelectionChange()      )
    );
};

// This method has already been implemented in the DanielBaseClass
void spec_HierarchyTableWidgetEditor::addNewRow() {
//    // Adding a new row is always done at the end of the list    
//    int tableItemCount = rowCount();
    
//    // Get the currently selected node from the tree hierarchy
//    currentNode = (spec_struct_data_node *) treeWidget -> selectedItems().at( 0 );
    
//    // Add new elements to the end of the node's defined data structures
//    currentNode -> addNodeContent(
//        tr(""),
//        tr(""),
//        false,
//        false,
//        false,
//        false,
//        tr("")
//    );
    
//    // Insert a new table row
//    insertRow( tableItemCount );
    
//    // And set the elements accordingly
//    setItem( tableItemCount, 0, new QTableWidgetItem( currentNode -> getMemNameAt(tableItemCount) ) );
//    setItem( tableItemCount, 1, new QTableWidgetItem( currentNode -> getMemTypeAt(tableItemCount) ) );
//    setItem( tableItemCount, 2, new QTableWidgetItem( Qt::CheckStateRole  ) );
//    setItem( tableItemCount, 3, new QTableWidgetItem( Qt::CheckStateRole ) );
//    setItem( tableItemCount, 4, new QTableWidgetItem( Qt::CheckStateRole ) );
//    setItem( tableItemCount, 5, new QTableWidgetItem( Qt::CheckStateRole ) );
//    setItem( tableItemCount, 6, new QTableWidgetItem( currentNode -> getMemValueAt(tableItemCount) ) );
    
//    item( tableItemCount, 2 ) -> setCheckState(Qt::Unchecked );
//    item( tableItemCount, 3 ) -> setCheckState(Qt::Unchecked );
//    item( tableItemCount, 4 ) -> setCheckState(Qt::Unchecked );
//    item( tableItemCount, 5 ) -> setCheckState(Qt::Unchecked );
    
    
    
//    itemAt( 2 , tableItemCount) -> setCheckState(Qt::Checked);
    
////    verticalHeaderItem( tableItemCount ) -> setFont( QFont("Courier New"));
//    for (int c_idx = 0; c_idx < columnCount(); ++c_idx) {
//        item( tableItemCount, c_idx ) -> setFont( QFont("Courier New"));
//    };
};

// This method has already been moved into the DanielBaseClass
void spec_HierarchyTableWidgetEditor::deleteSelectedRow() {
//    int rowC    = rowCount();
//    int selRow  = selectedItems().at(0)->row();
//    currentNode = (spec_struct_data_node *) treeWidget -> selectedItems().at( 0 );
    
//    // Remove elements, only if we have 2 or more lines
//    if( rowC >= 2) {
//        // Remove the selected row from the view
//        removeRow( selRow );
//        // Remove the selected row from the data model
//        currentNode -> removeRowAt( selRow );
//    } else {
//        // In this case, only a single element exists and we'll simply set everything back to default values
//        // Set the view to default values
//        setItem( selRow, 0, new QTableWidgetItem( "" ) );
//        setItem( selRow, 1, new QTableWidgetItem( "" ) );
//        setItem( selRow, 2, new QTableWidgetItem(  Qt::CheckStateRole  ) );
//        setItem( selRow, 3, new QTableWidgetItem(  Qt::CheckStateRole  ) );
//        setItem( selRow, 4, new QTableWidgetItem(  Qt::CheckStateRole  ) );
//        setItem( selRow, 5, new QTableWidgetItem(  Qt::CheckStateRole  ) );
//        setItem( selRow, 6, new QTableWidgetItem( "" ) );
        
//        // Set the data model to default values
//        currentNode -> updateRowAt( selRow );
//    };
};

void spec_HierarchyTableWidgetEditor::processSelectionChange() {
    // TODO: Implement convenience method to add a new row after the current last row if Tab key is 
    // pressed...
    int rowC = rowCount();
    int colC = columnCount();
    
    QList <QTableWidgetItem *> curItems = selectedItems();
    
    if( curItems.count() > 0 ) {
        if( curItems.at(0)->row() == rowC-1 && curItems.at(0)->column() == colC-1 ) {
//            qDebug() << "Detected last element selected";
        };
    };
};

void spec_HierarchyTableWidgetEditor::setTreeWidgetEditor( spec_HierarchyTreeWidgetEditor *tw ) {
    treeWidget = tw;
};

void spec_HierarchyTableWidgetEditor::updateTableWidgetEditor() {
//    isUpdateBlock = true;
    
//    // Get a pointer to the treeWidget view and get all of its selected items.
//    QList <QTreeWidgetItem *> curItems = treeWidget->selectedItems();
    
//    if( curItems.count() == 0 ) {
//        // This case should not be possible, yet handling this case to avoid any crashes
//         qDebug() << "Table View: No Items selected";
//    } else {
//        // Get the currently selected node. Since we only support a single selection model,
//        // we can safely get the first node from the selected model.
//        currentNode = (spec_struct_data_node *) curItems.at( 0 );
        
//        // Get the number of elements within the selected node
//        int nodeItemCount = currentNode->memberCount();
        
//        // Get the number of elements being currently displayed in the table view
//        int tableItemCount= rowCount();
        
////        qDebug() << "curItems " << currentNode;
        
//        // If the table is empty, which is the initial state, then we only need to create new
//        // elements
//        if( tableItemCount != 0 ) {
//            // However, if there were elements left over from the previous view,
//            // then we need to remove those first.
//            for (int r_idx = tableItemCount-1; r_idx >= 0; --r_idx) {
//                removeRow( r_idx );
//            };
//        };
        
//        // In case the selected node had no previously defined elements, we create and provide
//        // a new empty string element
//        if( nodeItemCount == 0 ) {
//            // Just create a new element for each of the columns
//            // This is accessing the data nodes from the tree widget
//            currentNode -> addNodeContent(
//                        tr(""),
//                        tr("")
//            );
            
//            nodeItemCount++;
//        };
        
//        // Populate the table view:
//        for (int r_idx = 0; r_idx < nodeItemCount; ++r_idx) {
//            // 1.) adding a new row at the end of the view
//            insertRow( r_idx );
//            // 2.) setting the values as defined in the data model in the view
//            setItem( r_idx, 0, new QTableWidgetItem( currentNode -> getMemNameAt( r_idx ) ) );
            
//            setItem( r_idx, 1, new QTableWidgetItem( currentNode -> getMemTypeAt(r_idx) ) );
            
//            setItem( r_idx, 2, new QTableWidgetItem( Qt::CheckStateRole ) );
            
//            if( currentNode -> getMemIsDNGAt( r_idx) == true ) {
//                item( r_idx, 2 ) -> setCheckState(Qt::Checked );
//            } else {
//                item( r_idx, 2 ) -> setCheckState(Qt::Unchecked );
//            };
            
//            setItem( r_idx, 3, new QTableWidgetItem( Qt::CheckStateRole ) );
//            if( currentNode -> getMemIsPhysicalAt( r_idx) == true ) {
//                item( r_idx, 3 ) -> setCheckState(Qt::Checked );
//            } else {
//                item( r_idx, 3 ) -> setCheckState(Qt::Unchecked );
//            };
            
//            setItem( r_idx, 4, new QTableWidgetItem( Qt::CheckStateRole ) );
//            if( currentNode -> getMemIsListAt( r_idx ) == true ) {
//                item( r_idx, 4 ) -> setCheckState(Qt::Checked );
//            } else {
//                item( r_idx, 4 ) -> setCheckState(Qt::Unchecked );
//            };
            
//            setItem( r_idx, 5, new QTableWidgetItem( Qt::CheckStateRole ) );
//            if( currentNode -> getMemIsUnitAt( r_idx ) == true ) {
//                item( r_idx, 5 ) -> setCheckState(Qt::Checked );
//            } else {
//                item( r_idx, 5 ) -> setCheckState(Qt::Unchecked );
//            };
            
//            setItem( r_idx, 6, new QTableWidgetItem( currentNode -> getMemValueAt(r_idx) ) );
            
////            verticalHeaderItem( r_idx ) -> setFont( QFont("Courier New") );
            
//            for (int c_idx = 0; c_idx < columnCount(); ++c_idx) {
//                item( r_idx, c_idx ) -> setFont( QFont("Courier New"));
//            };
////            qDebug() << "Struct Member Name: " << currentNode -> structMemberNames[r_idx];
//        };
//    };
    
//    isUpdateBlock = false;
};

void spec_HierarchyTableWidgetEditor::changeCellContent(int row, int column) {
//    // Do not update the values of table content when selecting a node in the tree view
//    if( isUpdateBlock == false ) {
//        // Get the pointer to the currently selected node from the tree hierarchy
//        QList <QTreeWidgetItem *> curItems = treeWidget->selectedItems();
//        currentNode = (spec_struct_data_node *) curItems.at( 0 );
        
//        qDebug() << "Detected cell change row " << row << " column " << column;
        
//        switch( column ) {
//            case 0:
//                currentNode -> updateMemNameAt      ( row, item( row, column )->text() );
//                break;
                
//            case 1:
//                currentNode -> updateMemTypeAt      ( row, item( row, column )->text() );
//                break;
                
//            case 2:
//                currentNode -> updateMemIsDNGAt(
//                    row,
//                    item( row, column ) -> checkState() == Qt::Checked
//                );
//                break;
                
//            case 3:
//                currentNode -> updateMemIsPhysicalAt(
//                    row,
//                    item( row, column ) -> checkState() == Qt::Checked
//                );
//                break;
                
//            case 4:
//                currentNode -> updateMemIsListAt    (
//                    row, 
//                    item( row, column ) -> checkState() == Qt::Checked
//                );
//                break;
                
//            case 5:
//                currentNode -> updateMemIsUnitAt    (
//                    row, 
//                    item( row, column ) -> checkState() == Qt::Checked
//                );
//                break;
//            case 6:
//                currentNode -> updateMemValueAt     ( row, item( row, column )->text() );
//                break;
                
//            default:
//                break;
//            };
//    };
};
