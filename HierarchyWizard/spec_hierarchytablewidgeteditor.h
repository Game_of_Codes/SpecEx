// ----------------------------------------------------------------------------
//   Copyright 2018 Daniel Bayer
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_HIERARCHYTABLEWIDGETEDITOR_H
#define SPEC_HIERARCHYTABLEWIDGETEDITOR_H

#include <QHBoxLayout>
#include <QList>
#include <QObject>
#include <QString>
#include <QStringList>
#include <QTableWidget>

#include <HierarchyWizard/spec_hierarchytreewidgeteditor.h>


class spec_HierarchyTableWidgetEditor : public QTableWidget {
    Q_OBJECT
        
    public:
        spec_HierarchyTableWidgetEditor       ();
        
        void    setTreeWidgetEditor             ( spec_HierarchyTreeWidgetEditor *tw );
        
    public slots:
        void    addNewRow                       ();
        void    deleteSelectedRow               ();
        
    private:
        spec_struct_data_node                   *currentNode;
        spec_HierarchyTreeWidgetEditor        *treeWidget;
        bool                                    isUpdateBlock;
        
    private slots:
        void    updateTableWidgetEditor         ();
        void    changeCellContent               ( int row, int column );
        void    processSelectionChange          ();
};

#endif // SPEC_HIERARCHYTABLEWIDGETEDITOR_H
