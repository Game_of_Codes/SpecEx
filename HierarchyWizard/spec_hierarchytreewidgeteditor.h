// ----------------------------------------------------------------------------
//   Copyright 2018 Daniel Bayer
//
//   Licensed under the Apache License, Version 2.0 (the
//   "License"); you may not use this file except in
//   compliance with the License.  You may obtain a copy of
//   the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in
//   writing, software distributed under the License is
//   distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
//   CONDITIONS OF ANY KIND, either express or implied.  See
//   the License for the specific language governing
//   permissions and limitations under the License.
// ----------------------------------------------------------------------------

#ifndef SPEC_TREEWIDGETEDITOR_H
#define SPEC_TREEWIDGETEDITOR_H

#include <QDebug>

#include <QObject>

#include <QTreeWidget>
#include <QWidget>

#include <CodeModel/spec_struct_data_node.h>

class spec_HierarchyTreeWidgetEditor : public QTreeWidget {
    Q_OBJECT

    public:
        spec_HierarchyTreeWidgetEditor( );
        
        spec_struct_data_node *getTopLevelItem(int index);
        
    public slots:
        void    addSiblingNode();
        
        void    addChildNode();
        
        void    loadTemplate();
        
        void    createStructDeclarations();
        
        int     addTemplateAgent( int elem_index, int id_counter, bool isActive, spec_struct_data_node* currentNode );
        
        void    removeNode();
        
        void    removeSubtree();
        
    private:
        unsigned int curRowIndex = 0;
};

#endif // SPEC_TREEWIDGETEDITOR_H
